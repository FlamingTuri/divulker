package it.unibo.models

class StateDeserializer extends TraitEnumDeserializer[State] {

  override protected val caseObjects: List[State] = {
    List(Fixed, Vulnerable, NotAffected)
  }

}
