package it.unibo.models

import com.fasterxml.jackson.annotation.JsonValue
import com.fasterxml.jackson.databind.annotation.JsonDeserialize

@JsonDeserialize(using = classOf[StateDeserializer])
sealed trait State extends TraitEnum

case object Fixed extends State {
  @JsonValue override val jsonValue: String = "Fixed"
}

case object Vulnerable extends State {
  @JsonValue override val jsonValue: String = "Vulnerable"
}

case object NotAffected extends State {
  @JsonValue override val jsonValue: String = "Not Affected"
}
