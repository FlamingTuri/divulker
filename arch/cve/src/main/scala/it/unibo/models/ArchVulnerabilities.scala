package it.unibo.models

case class ArchVulnerabilities(avgId: String,
                               cveId: List[String],
                               affectedPackages: List[String],
                               state: State,
                               advisory: Option[String]) extends Key {

  override def getKey: String = avgId
}
