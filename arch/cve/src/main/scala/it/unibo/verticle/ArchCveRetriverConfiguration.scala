package it.unibo.verticle

import io.vertx.core.buffer.Buffer
import io.vertx.scala.core.Vertx
import io.vertx.scala.ext.web.client.WebClient
import it.unibo.constants.ArchConstants
import it.unibo.data.subscriber.manager.DistroSpecificDataSubscriberManager
import it.unibo.models.ArchVulnerabilities
import it.unibo.models.data.VulnerabilityData
import it.unibo.models.ws.messages.Entries
import it.unibo.parsers.{ArchIssuesPageParser, Parser}
import it.unibo.updates.operation.result.UpdateResult
import it.unibo.verticle.retriever.configuration.LocalDataLoadWithPollingRetrieverConfiguration
import it.unibo.vertx.FileSystemUtils

case class ArchCveRetriverConfiguration(override protected val vertx: Vertx,
                                        override protected val webClient: WebClient,
                                        private val data: VulnerabilityData[ArchVulnerabilities],
                                        private val dataSubscribersManager: DistroSpecificDataSubscriberManager)
  extends LocalDataLoadWithPollingRetrieverConfiguration[ArchVulnerabilities] {

  override protected val saveFolderName: String = "arch/cve"
  override protected val lastSaveTimestampFileName: String = "save-time.txt"
  override protected val savedDataFileName: String = "all-issues.txt"
  override protected val fsUtils: FileSystemUtils = FileSystemUtils(vertx.fileSystem())

  override def loadLocalData(buffer: Buffer): UpdateResult[ArchVulnerabilities] = loadDownloadedData(buffer)

  override protected val dataHost: String = ArchConstants.Host
  override protected val dataUri: String = "/issues/all"
  protected val parser: Parser[Array[ArchVulnerabilities]] = ArchIssuesPageParser()

  override def loadDownloadedData(buffer: Buffer): UpdateResult[ArchVulnerabilities] = {
    val archCve = parser.fromPlainText(buffer.toString())
    // TODO: some entries could be removed but now the notifySubscribers only supports
    //  updated and new entries notification
    //data.insertOrUpdate(archCve, debianCveUpdatePolicy)
    ???
  }

  override def notifySubscribers(operationResult: UpdateResult[ArchVulnerabilities]): Unit = {
    dataSubscribersManager.notifySubscribers(Entries(operationResult))
  }

}

