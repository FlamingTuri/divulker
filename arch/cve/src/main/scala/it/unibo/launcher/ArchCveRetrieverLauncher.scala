package it.unibo.launcher

import it.unibo.verticle.{ArchCveRetrieverVerticle, VerticleLauncher}

object ArchCveRetrieverLauncher extends App with VerticleLauncher {
  deployVerticle[ArchCveRetrieverVerticle]()
}
