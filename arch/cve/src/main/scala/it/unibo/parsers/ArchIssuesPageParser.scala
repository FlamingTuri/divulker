package it.unibo.parsers

import it.unibo.models.{ArchVulnerabilities, Fixed, NotAffected, Vulnerable}
import net.ruippeixotog.scalascraper.browser.JsoupBrowser
import net.ruippeixotog.scalascraper.dsl.DSL._
import net.ruippeixotog.scalascraper.model.Element
import net.ruippeixotog.scalascraper.scraper.ContentExtractors.{element, elementList}

case class ArchIssuesPageParser() extends Parser[Array[ArchVulnerabilities]] {

  private val browser = JsoupBrowser()

  override def fromPlainText(text: String): Array[ArchVulnerabilities] = {
    ???
  }

  def fromHtml(html: String): List[ArchVulnerabilities] = {
    val doc = browser.parseString(html)
    // get the table with all the vulnerabilities and then get each table row
    doc >> element("body table tbody") >> elementList("tr") map parseTableRow
  }

  def parseTableRow(row: Element): ArchVulnerabilities = {
    val children = row.children.toList
    val avg = children.head.text
    val cveIds = children(1).text.split(' ').map(_.trim).toList
    val affectedPackages = children(2).text.split(',').map(_.trim).toList
    val state = children(6).text match {
      case Fixed.jsonValue => Fixed
      case Vulnerable.jsonValue => Vulnerable
      case NotAffected.jsonValue => NotAffected
      case value => throw new IllegalArgumentException(s"Undefined deserializer for value: $value")
    }
    val asa = children(8).text match {
      case "" => None
      case value => Option(value)
    }
    ArchVulnerabilities(avg, cveIds, affectedPackages, state, asa)
  }

}
