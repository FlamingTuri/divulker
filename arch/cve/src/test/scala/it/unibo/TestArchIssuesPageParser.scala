package it.unibo

import it.unibo.parsers.ArchIssuesPageParser
import org.scalatest.funsuite.AnyFunSuite

class TestArchIssuesPageParser extends AnyFunSuite {

  private val parser = ArchIssuesPageParser()

  test("Test issues/all page parsing") {
    val htmlPage =
      """<!DOCTYPE html>
        |<!-- saved from url=(0041)https://security.archlinux.org/issues/all -->
        |<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        |		<title>Issues - Arch Linux</title>
        |
        |		<link rel="icon" href="https://security.archlinux.org/static/favicon.ico">
        |		<link rel="stylesheet" href="./Issues - Arch Linux_files/normalize.css" type="text/css" media="all">
        |		<link rel="stylesheet" href="./Issues - Arch Linux_files/style.css" type="text/css" media="all">
        |		<link href="https://security.archlinux.org/advisory/feed.atom" rel="alternate" title="Recent advisories" type="application/atom+xml">
        |	</head>
        |	<body>
        |		<div id="archnavbar">
        |			<div id="logo"><a href="https://archlinux.org/" title="Return to the main page">Arch Linux</a></div>
        |			<div id="archnavbarmenu">
        |				<ul id="archnavbarlist">
        |					<li id="anb-home"><a href="https://archlinux.org/" title="Arch news, packages, projects and more">Home</a></li>
        |					<li id="anb-packages"><a href="https://archlinux.org/packages/" title="Arch Package Database">Packages</a></li>
        |					<li id="anb-forums"><a href="https://bbs.archlinux.org/" title="Community forums">Forums</a></li>
        |					<li id="anb-wiki"><a href="https://wiki.archlinux.org/" title="Community documentation">Wiki</a></li>
        |					<li id="anb-bugs"><a href="https://bugs.archlinux.org/" title="Report and track bugs">Bugs</a></li>
        |					<li id="anb-security"><a href="https://security.archlinux.org/" title="Arch Linux Security Tracker">Security</a></li>
        |					<li id="anb-aur"><a href="https://aur.archlinux.org/" title="Arch Linux User Repository">AUR</a></li>
        |					<li id="anb-download"><a href="https://archlinux.org/download/" title="Get Arch Linux">Download</a></li>
        |				</ul>
        |			</div>
        |		</div>
        |		<div class="content">
        |			<div class="navbar">
        |				<ul>
        |					<li><a href="https://security.archlinux.org/">issues</a></li>
        |					<li><a href="https://security.archlinux.org/advisory">advisories</a></li>
        |					<li><a href="https://security.archlinux.org/todo">todo</a></li>
        |					<li><a href="https://security.archlinux.org/stats">stats</a></li>
        |					<li><a href="https://security.archlinux.org/log">log</a></li>
        |					<li><a href="https://security.archlinux.org/login" accesskey="l">login</a></li>
        |				</ul>
        |			</div>
        |			<hr>
        |			<h1>Issues</h1>
        |			<div class="navbar">
        |				<ul>
        |					<li><a href="https://security.archlinux.org/issues">vulnerable</a></li>
        |					<li><b>all</b></li>
        |				</ul>
        |			</div>
        |			<div class="scroll-x">
        |				<table class="styled-table full size">
        |					<thead>
        |						<tr>
        |							<th>Group</th>
        |							<th>Issue</th>
        |							<th>Package</th>
        |							<th>Affected</th>
        |							<th>Fixed</th>
        |							<th>Severity</th>
        |							<th>Status</th>
        |							<th>Ticket</th>
        |							<th class="center">Advisory</th>
        |						</tr>
        |					</thead>
        |					<tbody>
        |						<tr>
        |							<td><a href="https://security.archlinux.org/AVG-1157">AVG-1157</a></td>
        |							<td>
        |								<a href="https://security.archlinux.org/CVE-2020-12823">CVE-2020-12823</a>
        |							</td>
        |							<td class="wrap">
        |								<span class="no-wrap"><a href="https://security.archlinux.org/package/openconnect">openconnect</a></span>
        |							</td>
        |							<td>1:8.05-1</td>
        |							<td></td>
        |							<td><span class="purple">Unknown</span></td>
        |							<td><span class="red">Vulnerable</span></td>
        |							<td></td>
        |							<td class="center"></td>
        |						</tr>
        |						<tr>
        |							<td><a href="https://security.archlinux.org/AVG-1146">AVG-1146</a></td>
        |							<td>
        |								<a href="https://security.archlinux.org/CVE-2020-11945">CVE-2020-11945</a><br>
        |								<a href="https://security.archlinux.org/CVE-2019-12521">CVE-2019-12521</a><br>
        |								<a href="https://security.archlinux.org/CVE-2019-12519">CVE-2019-12519</a>
        |							</td>
        |							<td class="wrap">
        |								<span class="no-wrap"><a href="https://security.archlinux.org/package/squid">squid</a></span>
        |							</td>
        |							<td>4.10-2</td>
        |							<td></td>
        |							<td><span class="red">Critical</span></td>
        |							<td><span class="red">Vulnerable</span></td>
        |							<td></td>
        |							<td class="center"></td>
        |						</tr>
        |					</tbody>
        |				</table>
        |			</div>
        |		</div>
        |
        |</body></html>""".stripMargin

    parser.fromHtml(htmlPage)
  }
}
