application {
    mainClassName = "it.unibo.launcher.ArchCveRetrieverLauncher"
    extra.set("mainClassName", mainClassName)
}

dependencies {
    val scalaVersion: String? by project.extra
    implementation("net.ruippeixotog", "scala-scraper_$scalaVersion", "2.2.0")
}
