package it.unibo.constants

trait ArchConstants {
  val Host: String = "security.archlinux.org"
}

object ArchConstants extends ArchConstants
