package it.unibo

import it.unibo.jackson.MarshallableImplicit._
import it.unibo.jackson.XmlUtil.{fromXml, xmlElemToString}
import it.unibo.models.redhat.{RedHatSecurityAdvisory, RpmRelatedCve}
import org.scalatest.funsuite.AnyFunSuite

class TestRpmToCveModels extends AnyFunSuite {

  test("Parsing of a single RedHat rpm-to-cve entry with single cve field") {
    val xmlEntry =
      <rpm rpm="initscripts-0:7.31.30.EL-1">
        <erratum released="2006-03-15">RHSA-2006:0015</erratum>
        <cve>CVE-2005-3629</cve>
      </rpm>

    val rpmRelatedCve = fromXml[RpmRelatedCve](xmlEntry)
    assertResult("initscripts-0:7.31.30.EL-1")(rpmRelatedCve.rpm)
    assertResult("initscripts")(rpmRelatedCve.rpmName)

    val rhsaInfo = rpmRelatedCve.rhsaInfo
    assertResult("RHSA-2006:0015")(rhsaInfo.id)
    assertResult("2006-03-15")(rhsaInfo.publishedDate)

    assertResult(1)(rpmRelatedCve.cveIds.length)
    assertResult("CVE-2005-3629")(rpmRelatedCve.cveIds(0))
  }

  test("Parsing of a single RedHat rpm-to-cve entry with multiple cve field") {
    val xmlEntry =
      <rpm rpm="kernel-doc-0:2.6.9-55.EL">
        <erratum released="2007-04-28">RHBA-2007:0304</erratum>
        <cve>CVE-2005-2873</cve>
        <cve>CVE-2005-3257</cve>
        <cve>CVE-2006-0557</cve>
        <cve>CVE-2006-1863</cve>
        <cve>CVE-2007-1592</cve>
        <cve>CVE-2007-3379</cve>
      </rpm>

    val rpmRelatedCve = fromXml[RpmRelatedCve](xmlEntry)
    assertResult("kernel-doc-0:2.6.9-55.EL")(rpmRelatedCve.rpm)
    assertResult("kernel-doc")(rpmRelatedCve.rpmName)

    val rhsaInfo = rpmRelatedCve.rhsaInfo
    assertResult("RHBA-2007:0304")(rhsaInfo.id)
    assertResult("2007-04-28")(rhsaInfo.publishedDate)

    assertResult(6)(rpmRelatedCve.cveIds.length)
    assertResult("CVE-2005-2873")(rpmRelatedCve.cveIds(0))
  }

  test("Parsing of a single RedHat rpm-to-cve entry with no cve field") {
    val xmlEntry =
      <rpm rpm="redhat-release-0:3Desktop-13.9.7">
        <erratum released="2009-11-09">RHSA-2009:1526</erratum>
      </rpm>

    val rpmRelatedCve = fromXml[RpmRelatedCve](xmlEntry)
    assertResult("redhat-release-0:3Desktop-13.9.7")(rpmRelatedCve.rpm)
    assertResult("redhat-release")(rpmRelatedCve.rpmName)

    val rhsaInfo = rpmRelatedCve.rhsaInfo
    assertResult("RHSA-2009:1526")(rhsaInfo.id)
    assertResult("2009-11-09")(rhsaInfo.publishedDate)

    assertResult(0)(rpmRelatedCve.cveIds.length)
  }

  test("Parsing of RedHat rpm-to-cve.xml subset") {
    val xmlRpms =
      <rpms>
        <rpm rpm="htdig-2:3.1.6-7.el3">
          <erratum released="2007-06-07">RHBA-2007:0026</erratum>
          <cve>CVE-2000-1191</cve>
        </rpm>
        <rpm rpm="kernel-doc-0:2.6.9-55.EL">
          <erratum released="2007-04-28">RHBA-2007:0304</erratum>
          <cve>CVE-2005-2873</cve>
          <cve>CVE-2005-3257</cve>
          <cve>CVE-2006-0557</cve>
          <cve>CVE-2006-1863</cve>
          <cve>CVE-2007-1592</cve>
          <cve>CVE-2007-3379</cve>
        </rpm>
        <rpm rpm="subversion-0:1.6.11-12.el6_6">
          <erratum released="2015-02-10">RHSA-2015:0165</erratum>
          <cve>CVE-2014-3528</cve>
          <cve>CVE-2014-3580</cve>
        </rpm>
        <rpm rpm="subversion-tools-0:1.7.14-7.el7_0">
          <erratum released="2015-02-10">RHSA-2015:0166</erratum>
          <cve>CVE-2014-3528</cve>
          <cve>CVE-2014-3580</cve>
          <cve>CVE-2014-8108</cve>
        </rpm>
      </rpms>

    val rpmRelatedCveArray = fromXml[Array[RpmRelatedCve]](xmlRpms)
    assertResult(4)(rpmRelatedCveArray.length)
    assertResult("htdig")(rpmRelatedCveArray(0).rpmName)
    assertResult("kernel-doc")(rpmRelatedCveArray(1).rpmName)
    assertResult("subversion")(rpmRelatedCveArray(2).rpmName)
    assertResult("subversion-tools")(rpmRelatedCveArray(3).rpmName)
  }

  test("RedHat rpm-to-cve entry json conversion") {
    val rpmRelatedCve = RpmRelatedCve("kernel-doc",
      RedHatSecurityAdvisory("RHBA-2007:0304", "2007-04-28"))
      .setCveIds("CVE-2005-2873", "CVE-2005-3257")
    val rpmRelatedCveJson =
      s"""{"rpmName":"kernel-doc",
         |"rhsaInfo":{"id":"RHBA-2007:0304","publishedDate":"2007-04-28","relatedCveId":[],"affectedPackages":[]},
         |"cveIds":["CVE-2005-2873","CVE-2005-3257"]}""".stripMargin.replace("\n", "")
    assertResult(rpmRelatedCveJson)(rpmRelatedCve.toJson)
    assertResult(rpmRelatedCve)(rpmRelatedCve.toJson.fromJson[RpmRelatedCve]())
  }

}
