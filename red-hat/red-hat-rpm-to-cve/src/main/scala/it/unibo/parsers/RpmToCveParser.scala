package it.unibo.parsers

import it.unibo.jackson.MarshallableImplicit.Unmarshallable
import it.unibo.models.redhat.RpmRelatedCve

case class RpmToCveParser() extends Parser[Array[RpmRelatedCve]] {

  override def fromPlainText(text: String): Array[RpmRelatedCve] = {
    /* Red Hat Errata:
     * - RHSA: Red Hat Security Advisory
     * - RHBA: Red Hat Bug Advisory
     * - RHEA: Red Hat Enhancement Advisory
     */
    text.fromXml[Array[RpmRelatedCve]]()
      // removes RHBA and RHEA
      .filterNot { e =>
        val rhsaId = e.rhsaInfo.id
        rhsaId.contains("RHBA") || rhsaId.contains("RHEA")
      }
  }
}
