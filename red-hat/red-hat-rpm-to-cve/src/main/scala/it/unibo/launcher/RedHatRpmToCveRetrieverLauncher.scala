package it.unibo.launcher

import it.unibo.verticle.{RedHatRpmToCveRetrieverVerticle, VerticleLauncher}

object RedHatRpmToCveRetrieverLauncher extends App with VerticleLauncher {
  deployVerticle[RedHatRpmToCveRetrieverVerticle]()
}
