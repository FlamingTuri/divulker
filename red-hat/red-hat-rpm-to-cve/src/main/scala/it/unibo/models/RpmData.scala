package it.unibo.models

import it.unibo.models.data.SourceData
import it.unibo.models.redhat.RpmRelatedCve
import it.unibo.updates.collections.BufferUpdateUtils._
import it.unibo.updates.operation.result.OperationResultUtil.OperationResult
import it.unibo.updates.operation.result.{OperationResultUtil, Result, UpdateResult}

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

case class RpmData() extends SourceData[RpmRelatedCve] {

  val data: mutable.SortedMap[Char, mutable.SortedMap[String, ListBuffer[RpmRelatedCve]]] = mutable.SortedMap()

  def insertOrUpdate(updates: Iterable[RpmRelatedCve],
                     updateEntryStrategy: (RpmRelatedCve, RpmRelatedCve) => Boolean): UpdateResult[RpmRelatedCve] = {
    val updateResult = new UpdateResult[RpmRelatedCve]()
    updates.map(insertOrUpdate(_, updateEntryStrategy)).foreach(updateResult.addEntry)
    updateResult
  }

  def insertOrUpdate(update: RpmRelatedCve,
                     updateEntryStrategy: (RpmRelatedCve, RpmRelatedCve) => Boolean): OperationResult[RpmRelatedCve] = {
    val letter = update.rpmName.head
    val name = update.rpmName
    data.get(letter) match {
      case Some(numericPortionMap) =>
        numericPortionMap.get(name) match {
          case Some(cveList) =>
            cveList.insertOrUpdate(update,
              (current: RpmRelatedCve, update: RpmRelatedCve) => current.rpm == update.rpm,
              updateEntryStrategy)
          case None =>
            numericPortionMap += (name -> ListBuffer(update))
            OperationResultUtil(Result.New, update)
        }
      case None =>
        data += (letter -> mutable.SortedMap(name -> ListBuffer(update)))
        OperationResultUtil(Result.New, update)
    }
  }

  def countElements: Int = data.values.flatMap(_.values).map(_.size).sum

  override def getAsIterable: Iterable[RpmRelatedCve] = data.values.flatMap(_.values).flatten
}
