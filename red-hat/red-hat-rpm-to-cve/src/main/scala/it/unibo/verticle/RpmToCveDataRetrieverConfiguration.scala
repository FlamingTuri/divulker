package it.unibo.verticle

import io.vertx.core.buffer.Buffer
import io.vertx.scala.core.Vertx
import io.vertx.scala.ext.web.client.WebClient
import it.unibo.constants.RedHatSecurityDataConstants
import it.unibo.constants.RedHatSecurityDataConstants.SecurityDataMetricsUri
import it.unibo.data.subscriber.manager.DistroSpecificDataSubscriberManager
import it.unibo.models.RpmData
import it.unibo.models.redhat.RpmRelatedCve
import it.unibo.models.ws.messages.Entries
import it.unibo.parsers.{Parser, RpmToCveParser}
import it.unibo.updates.operation.result.UpdateResult
import it.unibo.verticle.retriever.configuration.LocalDataLoadWithPollingRetrieverConfiguration
import it.unibo.vertx.FileSystemUtils

case class RpmToCveDataRetrieverConfiguration(override protected val vertx: Vertx,
                                              override protected val webClient: WebClient,
                                              private val data: RpmData,
                                              private val dataSubscribersManager: DistroSpecificDataSubscriberManager)
  extends LocalDataLoadWithPollingRetrieverConfiguration[RpmRelatedCve] {

  override protected val saveFolderName: String = "red-hat/rpm-to-cve"
  override protected val lastSaveTimestampFileName: String = "save-time.txt"
  override protected val savedDataFileName: String = "rpm-to-cve.xml"
  override protected val fsUtils: FileSystemUtils = FileSystemUtils(vertx.fileSystem())

  override def loadLocalData(buffer: Buffer): UpdateResult[RpmRelatedCve] = loadDownloadedData(buffer)

  override protected val dataHost: String = RedHatSecurityDataConstants.Host
  override protected val dataUri: String = s"$SecurityDataMetricsUri/rpm-to-cve.xml"
  protected val parser: Parser[Array[RpmRelatedCve]] = RpmToCveParser()

  override def loadDownloadedData(buffer: Buffer): UpdateResult[RpmRelatedCve] = {
    val rpmRelatedCves = parser.fromPlainText(buffer.toString())
    data.insertOrUpdate(rpmRelatedCves, (current, update) => {
      val cveIdsUpdated = !(current.cveIds sameElements update.cveIds)
      if (cveIdsUpdated) {
        current.setCveIds(update.cveIds: _*)
      }
      val publishedDateUpdated = current.rhsaInfo.publishedDate != update.rhsaInfo.publishedDate
      if (publishedDateUpdated) {
        current.rhsaInfo.publishedDate = update.rhsaInfo.publishedDate
      }
      cveIdsUpdated || publishedDateUpdated
    })
  }

  override def notifySubscribers(operationResult: UpdateResult[RpmRelatedCve]): Unit = {
    operationResult.printEntriesNumber()
    dataSubscribersManager.notifySubscribers(Entries(operationResult))
  }

}



