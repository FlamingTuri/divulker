package it.unibo.verticle

import it.unibo.constants.RpmToCveSourceInfo
import it.unibo.models.RpmData
import it.unibo.verticle.retriever.SpecificDataRetrieverVerticle
import it.unibo.verticle.retriever.configuration.DataRetrieverConfiguration

class RedHatRpmToCveRetrieverVerticle extends SpecificDataRetrieverVerticle[RpmData](RpmToCveSourceInfo()) {

  override val data: RpmData = RpmData()

  override protected def dataRetrieverConfigurations: List[DataRetrieverConfiguration] = {
    List(RpmToCveDataRetrieverConfiguration(vertx, webClient, data, dataSubscriberManager))
  }

  override protected def dataAsIterable: Iterable[_] = data.getAsIterable

}
