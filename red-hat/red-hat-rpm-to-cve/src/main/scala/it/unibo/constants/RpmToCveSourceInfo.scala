package it.unibo.constants

import it.unibo.models.service.{ServiceKind, ServiceName}

case class RpmToCveSourceInfo() extends SourceVerticleInfo with RedHatSecurityDataConstants {
  /** The service name */
  override val serviceName: ServiceName.Value = ServiceName.RedHatRpmToCveService
  /** The service kind */
  override val serviceKind: ServiceKind.Value = ServiceKind.DistroSpecificRetriever
  /** The path accepted for websocket messages */
  override val WebSocketPath: String = "/rpm-to-cve"
}
