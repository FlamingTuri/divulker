package it.unibo.verticle

import it.unibo.constants.{RedHatDataAggregatorSourceInfo, RegistryPaths, SourceVerticleInfo}
import it.unibo.handlers.HandlerMatchers
import it.unibo.handlers.assigner.SpecificDataSourceHandlerAssigner
import it.unibo.models.aggregator.GatheredData
import it.unibo.models.service.ServiceName._
import it.unibo.models.service.{ServiceInfo, ServiceName}
import it.unibo.verticle.aggregator.SpecificDataAggregatorVerticle

class RedHatDataAggregatorVerticle extends SpecificDataAggregatorVerticle[GatheredData] {

  override protected val registerRequestUri: String = RegistryPaths.SourcesPath + "?sourceKind=DistroSpecificRetriever"
  override protected val verticleInfo: SourceVerticleInfo = RedHatDataAggregatorSourceInfo()
  override protected val handlerAssigner: SpecificDataSourceHandlerAssigner[GatheredData] = HandlerMatchers()
  protected val distroSpecificDataSources: Seq[ServiceName.Value] = {
    Seq(RedHatRpmToCveService, RedHatCveDatesService, RhsaReleaseDates)
  }
  override protected val serviceCondition: ServiceInfo => Boolean = serviceInfo => {
    distroSpecificDataSources.contains[ServiceName.Value](serviceInfo.name)
  }
}
