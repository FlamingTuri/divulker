package it.unibo.constants

import it.unibo.models.service.{ServiceKind, ServiceName}

case class RedHatDataAggregatorSourceInfo() extends SourceVerticleInfo {
  /** The service name */
  override val serviceName: ServiceName.Value = ServiceName.RedHatDataAggregator
  /** The service type */
  override val serviceKind: ServiceKind.Value = ServiceKind.DistroAggregator
  /** The path accepted for websocket messages */
  override val WebSocketPath: String = "/red-hat-aggregator"
}
