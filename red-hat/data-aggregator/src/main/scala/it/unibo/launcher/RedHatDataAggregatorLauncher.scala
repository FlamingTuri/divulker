package it.unibo.launcher

import it.unibo.verticle.{RedHatDataAggregatorVerticle, VerticleLauncher}

object RedHatDataAggregatorLauncher extends App with VerticleLauncher {
  deployVerticle[RedHatDataAggregatorVerticle]()
}
