package it.unibo.handlers

import it.unibo.models.aggregator.GatheredData

trait RedHatSpecificDataSourceWsHandler extends SpecificDataSourceWsHandler[GatheredData]
  with RedHatInfo {

}
