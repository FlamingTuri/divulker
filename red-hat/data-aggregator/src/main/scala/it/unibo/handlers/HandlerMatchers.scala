package it.unibo.handlers

import it.unibo.handlers.assigner.SpecificDataSourceHandlerAssigner
import it.unibo.models.aggregator.GatheredData
import it.unibo.models.service.ServiceName

case class HandlerMatchers() extends SpecificDataSourceHandlerAssigner[GatheredData] {

  override protected def serviceMatcher(serviceName: ServiceName.Value): SpecificRetrieverWsHandler[GatheredData] = {
    serviceName match {
      case ServiceName.RedHatRpmToCveService => RpmToCveHandler()
      case ServiceName.RedHatCveDatesService => CveDatesHandler()
      case ServiceName.RhsaReleaseDates => RhsaReleaseDatesHandler()
      case _ => ???
    }
  }
}
