package it.unibo.handlers

import it.unibo.models.service.ServiceName

trait RedHatInfo extends DistroNameInfo {

  override protected def distroName: ServiceName.Value = ServiceName.RedHatDataAggregator

}
