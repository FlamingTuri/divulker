package it.unibo.handlers

import io.vertx.scala.core.http.WebSocket
import it.unibo.jackson.MarshallableImplicit._
import it.unibo.models.aggregator.GatheredData
import it.unibo.models.redhat.RedHatCve
import it.unibo.models.service.ServiceName
import it.unibo.models.ws.messages.Entries
import it.unibo.models.ws.messages.distro.DistroSourceResponseMessage
import it.unibo.utils.Logger

case class CveDatesHandler() extends RedHatSpecificDataSourceWsHandler {

  override protected val sourceName: ServiceName.Value = ServiceName.RedHatCveDatesService

  override protected def textMessageHandler(webSocket: WebSocket, text: String): Unit = {
    val res = text.fromJson[DistroSourceResponseMessage[RedHatCve]]()
    val newEntries = res.data.newEntries
    val updatedEntries = res.data.updatedEntries
    val operationResult = verticle.data.insertOrUpdate(newEntries ++ updatedEntries, updateStrategy, newEntryStrategy)
    Logger.printOut("gathered data " + verticle.data.countElements)
    operationResult.printEntriesNumber()
    notifySubscribers(Entries(operationResult))
  }

  private def updateStrategy(current: GatheredData, update: RedHatCve): Boolean = {
    false
  }

  private def newEntryStrategy(update: RedHatCve): GatheredData = {
    GatheredData(update.id)
  }

}
