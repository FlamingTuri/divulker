package it.unibo.handlers

import io.vertx.scala.core.http.WebSocket
import it.unibo.jackson.MarshallableImplicit._
import it.unibo.models.aggregator.{GatheredData, Source}
import it.unibo.models.data.CveIdVulnerabilityData
import it.unibo.models.redhat.RedHatSecurityAdvisory
import it.unibo.models.service.ServiceName
import it.unibo.models.ws.messages.Entries
import it.unibo.models.ws.messages.distro.DistroSourceResponseMessage
import it.unibo.parsers.Parser
import it.unibo.updates.operation.result.UpdateResult

import scala.collection.mutable.ListBuffer

case class RhsaReleaseDatesHandler() extends RedHatSpecificDataSourceWsHandler {

  override protected val sourceName: ServiceName.Value = ServiceName.RhsaReleaseDates

  private val dateRegex = "(\\d{4})(\\d{2})(\\d{2})".r

  private def parseRhsaDate(rhsa: RedHatSecurityAdvisory): String = {
    rhsa.publishedDate match {
      case dateRegex(year, month, day) => s"$year-$month-$day"
      case value => Parser.throwParseError(value)
    }
  }

  override protected def textMessageHandler(webSocket: WebSocket, text: String): Unit = {
    val res = text.fromJson[DistroSourceResponseMessage[RedHatSecurityAdvisory]]()
    val updateResult = new UpdateResult[GatheredData]()
    val gatheredData = verticle.data
    updateVerticleData(res.data.newEntries, gatheredData, updateResult)
    updateVerticleData(res.data.updatedEntries, gatheredData, updateResult)
    updateResult.printEntriesNumber()
    notifySubscribers(Entries(updateResult))
  }

  private def updateVerticleData(receivedData: Iterable[RedHatSecurityAdvisory],
                                 gatheredData: CveIdVulnerabilityData[GatheredData],
                                 updateResult: UpdateResult[GatheredData]): Unit = {
    receivedData.filterNot(_.publishedDate == "undefined").flatMap { entry =>
      entry.relatedCveId.map { updateCveId =>
        gatheredData.insertOrUpdate(entry,
          (_: RedHatSecurityAdvisory) => updateCveId,
          (current, update) => updateStrategy(current, update),
          (rhsa: RedHatSecurityAdvisory) => newEntryStrategy(rhsa, updateCveId))
      }
    }.foreach(updateResult.addEntry)
  }

  private def updateStrategy(value: GatheredData, entry: RedHatSecurityAdvisory): Boolean = {
    val fixes = value.fixes
    val publishedDate = parseRhsaDate(entry)
    entry.affectedPackages.map { affectedPackage =>
      fixes.get(affectedPackage) match {
        case Some(value) =>
          value.find(s => s.name == sourceName.toString) match {
            case Some(value) =>
              val publishDateUpdated = value.date != publishedDate
              if (publishDateUpdated) {
                value.date = publishedDate
              }
              publishDateUpdated
            case None =>
              value += Source(sourceName, publishedDate)
              true
          }
        case None =>
          fixes += (affectedPackage -> ListBuffer(Source(sourceName, publishedDate)))
          true
      }
    }.exists(e => e)
  }

  private def newEntryStrategy(rhsa: RedHatSecurityAdvisory, cveId: String): GatheredData = {
    val newEntry = GatheredData(cveId)
    rhsa.affectedPackages.foreach { affectedPackage =>
      newEntry.fixes += (affectedPackage -> ListBuffer(Source(sourceName, parseRhsaDate(rhsa))))
    }
    newEntry
  }

}
