package it.unibo.handlers

import io.vertx.scala.core.http.WebSocket
import it.unibo.jackson.MarshallableImplicit._
import it.unibo.models.aggregator.{GatheredData, Source}
import it.unibo.models.redhat.RedHatSecurityAdvisory.parseDate
import it.unibo.models.redhat.RpmRelatedCve
import it.unibo.models.service.ServiceName
import it.unibo.models.ws.messages.distro.DistroSourceResponseMessage

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

case class RpmToCveHandler() extends RedHatSpecificDataSourceWsHandler {

  override protected val sourceName: ServiceName.Value = ServiceName.RedHatRpmToCveService

  override protected def textMessageHandler(webSocket: WebSocket, text: String): Unit = {
    val res = text.fromJson[DistroSourceResponseMessage[RpmRelatedCve]]()
    // TODO: implementation removed since it was not adding useful data.
    //  Check if it should be reimplemented
    /*val updatedEntries = ListBuffer[GatheredData]()
    val newEntries = ListBuffer[GatheredData]()
    val gatheredData = verticle.data
    res.data.foreach { entry =>
      entry.cveIds.foreach { cveId =>
        gatheredData.find(current => comparePolicy(current, cveId)) match {
          case Some(value) =>
            if (updateStrategy(value, entry) && !newEntries.contains(value) && !updatedEntries.contains(value)) {
              updatedEntries += value
            }
          case None =>
            val newEntry = newEntryStrategy(cveId, entry)
            gatheredData += newEntry
            newEntries += newEntry
        }
      }
    }
    Logger.printOut("gathered data " + gatheredData.size)
    Logger.printOut(s"updated: ${updatedEntries.size} new: ${newEntries.size}")
    notifyDataAggregatorSubscribers(updatedEntries, newEntries)*/
  }

  private def comparePolicy(current: GatheredData, updateCveId: String): Boolean = {
    current.cveId == updateCveId
  }

  private def updateStrategy(current: GatheredData, update: RpmRelatedCve): Boolean = {
    // TODO: decide how to handle situations similar to this one:
    //  Same CVE id, different package version, different release date
    //  <rpm rpm="initscripts-0:7.31.30.EL-1">
    //    <erratum released="2006-03-15">RHSA-2006:0015</erratum>
    //    <cve>CVE-2005-3629</cve>
    //  </rpm>
    //  <rpm rpm="initscripts-0:7.93.24.EL-1.1">
    //    <erratum released="2006-03-07">RHSA-2006:0016</erratum>
    //    <cve>CVE-2005-3629</cve>
    //  </rpm>
    var updated = false
    val updateFixDate = parseDate(update.rhsaInfo)
    val rpmName = update.rpmName
    current.fixes.get(rpmName) match {
      case Some(value) =>
        value.find(s => s.name == sourceName.toString) match {
          case Some(source) =>
            updated = source.date != updateFixDate
            if (updated) {
              source.date = updateFixDate
            }
          case None =>
            value += Source(sourceName, updateFixDate)
            updated = true
        }
      case None =>
        current.fixes += (rpmName -> ListBuffer(Source(sourceName, updateFixDate)))
        updated = true
    }
    updated
  }

  private def newEntryStrategy(cveId: String, update: RpmRelatedCve): GatheredData = {
    val fixes = mutable.ListMap[String, ListBuffer[Source]]()
    val fixSource = Source(sourceName, parseDate(update.rhsaInfo))
    fixes += (update.rpmName -> ListBuffer(fixSource))
    GatheredData(cveId, ListBuffer(), fixes)
  }

}
