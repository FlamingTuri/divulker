package it.unibo.constants

trait RedHatSecurityDataConstants {
  val Host: String = "redhat.com"
  /** Security data metrics uri */
  val SecurityDataMetricsUri: String = "/security/data/metrics"
}

object RedHatSecurityDataConstants extends RedHatSecurityDataConstants
