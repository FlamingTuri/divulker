package it.unibo.models.redhat

import com.fasterxml.jackson.annotation.{JsonGetter, JsonProperty}
import com.fasterxml.jackson.dataformat.xml.annotation.{JacksonXmlElementWrapper, JacksonXmlProperty}
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException

import scala.collection.mutable.ListBuffer
import scala.util.matching.Regex

case class RpmRelatedCve(@JsonProperty("rpmName")
                         private var _rpmName: String,
                         @JsonProperty("rhsaInfo")
                         private var _rhsaInfo: RedHatSecurityAdvisory) {

  private var _cveIds = ListBuffer[String]()

  // An RPM filename should have the [name]-[version]-[release].[arch].rpm format
  // but in the file RedHat it has [name]-[version]-[release] format
  private val fileNameRe: Regex = "(.+)-([^-]+)-([^-]+)$".r

  private var _rpm: String = _

  // do not remove, it is used when parsing the xml data
  @JacksonXmlProperty(localName = "rpm", isAttribute = true)
  private def setRpmName_=(rpm: String): Unit = {
    _rpm = rpm
    _rpmName = rpm match {
      case fileNameRe(name, version, release) => name
      case _ => throw new ValueException("could not parse rpm filename")
    }
  }

  /** Get the rpm filename with the following format: [name]-[version]-[release]
   *
   * @return
   */
  def rpm: String = _rpm

  /** Gets the name of the program or package, from the RPM filename.
   *
   * @return [name]
   */
  @JsonGetter
  def rpmName: String = _rpmName

  // do not remove, it is used when parsing the xml data
  @JacksonXmlProperty(localName = "erratum")
  private def setRhsaInfo_=(rhsaInfo: RedHatSecurityAdvisory): Unit = _rhsaInfo = rhsaInfo

  @JsonGetter
  def rhsaInfo: RedHatSecurityAdvisory = _rhsaInfo

  @JacksonXmlProperty(localName = "cve")
  @JacksonXmlElementWrapper(useWrapping = false)
  def setCveIds(cveIds: String*): RpmRelatedCve = {
    _cveIds = cveIds.to[ListBuffer]
    this
  }

  @JsonGetter
  def cveIds: Array[String] = _cveIds.to[Array]

}
