package it.unibo.models.redhat

import com.fasterxml.jackson.annotation.JsonProperty
import it.unibo.models.Key

case class RedHatCve(@JsonProperty("id") id: String) extends Key {

  override def getKey: String = id
}
