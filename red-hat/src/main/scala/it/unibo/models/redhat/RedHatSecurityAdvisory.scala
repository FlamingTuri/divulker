package it.unibo.models.redhat

import com.fasterxml.jackson.annotation.{JsonGetter, JsonProperty, JsonSetter}
import com.fasterxml.jackson.dataformat.xml.annotation.{JacksonXmlProperty, JacksonXmlText}
import it.unibo.models.Key
import it.unibo.parsers.DateParser

import scala.collection.mutable

object RedHatSecurityAdvisory extends DateParser {

  def parseDate(rhsa: RedHatSecurityAdvisory): String = {
    parseDate(rhsa.publishedDate)
  }

  override def parseDate(date: String): String = date

}

case class RedHatSecurityAdvisory(@JsonProperty("id")
                                  private var _id: String,
                                  @JsonProperty("publishedDate")
                                  private var _publishedDate: String,
                                  relatedCveId: mutable.Set[String] = mutable.Set(),
                                  affectedPackages: mutable.Set[String] = mutable.Set()) extends Key {

  @JsonGetter
  def id: String = _id

  @JacksonXmlText
  // id_= does not work
  def setId_=(id: String): Unit = _id = id

  @JsonGetter
  def publishedDate: String = _publishedDate

  @JacksonXmlProperty(localName = "released", isAttribute = true)
  @JsonSetter
  def publishedDate_=(publishedDate: String): Unit = _publishedDate = publishedDate

  override def getKey: String = id
}
