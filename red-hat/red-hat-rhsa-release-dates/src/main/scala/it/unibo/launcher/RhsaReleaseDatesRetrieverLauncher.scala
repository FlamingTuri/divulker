package it.unibo.launcher

import it.unibo.verticle.{RhsaReleaseDatesRetrieverVerticle, VerticleLauncher}

object RhsaReleaseDatesRetrieverLauncher extends App with VerticleLauncher {
  deployVerticle[RhsaReleaseDatesRetrieverVerticle]()
}
