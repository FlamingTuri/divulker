package it.unibo.verticle

import it.unibo.constants.RhsaReleaseDatesSourceInfo
import it.unibo.models.RedHatAdvisoryData
import it.unibo.models.data.VulnerabilityData
import it.unibo.models.redhat.RedHatSecurityAdvisory
import it.unibo.verticle.retriever.SpecificDataRetrieverVerticle
import it.unibo.verticle.retriever.configuration.DataRetrieverConfiguration

class RhsaReleaseDatesRetrieverVerticle
  extends SpecificDataRetrieverVerticle[VulnerabilityData[RedHatSecurityAdvisory]](RhsaReleaseDatesSourceInfo()) {

  override val data: VulnerabilityData[RedHatSecurityAdvisory] = RedHatAdvisoryData()

  override protected def dataRetrieverConfigurations: List[DataRetrieverConfiguration] = {
    List(RhsaMapCpeConfiguration(vertx, webClient, data, dataSubscriberManager),
      RhsaReleaseConfiguration(vertx, webClient, data, dataSubscriberManager))
  }

  override protected def dataAsIterable: Iterable[_] = data.getAsIterable

}
