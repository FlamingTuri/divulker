package it.unibo.verticle

import io.vertx.core.buffer.Buffer
import io.vertx.scala.core.Vertx
import io.vertx.scala.ext.web.client.WebClient
import it.unibo.constants.RedHatSecurityDataConstants
import it.unibo.constants.RedHatSecurityDataConstants.SecurityDataMetricsUri
import it.unibo.data.subscriber.manager.DistroSpecificDataSubscriberManager
import it.unibo.models.data.VulnerabilityData
import it.unibo.models.redhat.RedHatSecurityAdvisory
import it.unibo.models.ws.messages.Entries
import it.unibo.parsers.{Parser, RhsaReleaseDatesParser}
import it.unibo.updates.operation.result.UpdateResult
import it.unibo.verticle.retriever.configuration.LocalDataLoadWithPollingRetrieverConfiguration
import it.unibo.vertx.FileSystemUtils

case class RhsaReleaseConfiguration(override protected val vertx: Vertx,
                                    override protected val webClient: WebClient,
                                    private val data: VulnerabilityData[RedHatSecurityAdvisory],
                                    private val dataSubscribersManager: DistroSpecificDataSubscriberManager)
  extends LocalDataLoadWithPollingRetrieverConfiguration[RedHatSecurityAdvisory] {

  override protected val saveFolderName: String = "red-hat/rhsa-release-dates"
  override protected val lastSaveTimestampFileName: String = "rhsa-release-save-time.txt"
  override protected val savedDataFileName: String = "rhsa-release-dates.txt"
  override protected val fsUtils: FileSystemUtils = FileSystemUtils(vertx.fileSystem())

  override def loadLocalData(buffer: Buffer): UpdateResult[RedHatSecurityAdvisory] = {
    loadDownloadedData(buffer)
  }

  override protected val dataHost: String = RedHatSecurityDataConstants.Host
  override protected val dataUri: String = s"$SecurityDataMetricsUri/release_dates.txt"
  protected val parser: Parser[Array[RedHatSecurityAdvisory]] = RhsaReleaseDatesParser()

  override def loadDownloadedData(buffer: Buffer): UpdateResult[RedHatSecurityAdvisory] = {
    val rhsaArray = parser.fromPlainText(buffer.toString())
    data.insertOrUpdate(rhsaArray, (current, update) => {
      val dateUpdated = current.publishedDate != update.publishedDate
      if (dateUpdated) {
        current.publishedDate = update.publishedDate
      }
      val previousSize = current.relatedCveId.size
      current.relatedCveId ++= update.relatedCveId
      val relatedCveIdUpdated = previousSize != current.relatedCveId.size
      dateUpdated || relatedCveIdUpdated
    })
  }

  override def notifySubscribers(operationResult: UpdateResult[RedHatSecurityAdvisory]): Unit = {
    operationResult.printEntriesNumber()
    dataSubscribersManager.notifySubscribers(Entries(operationResult))
  }

}
