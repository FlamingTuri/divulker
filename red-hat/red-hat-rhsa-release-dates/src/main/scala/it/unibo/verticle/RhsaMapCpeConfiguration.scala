package it.unibo.verticle

import io.vertx.core.buffer.Buffer
import io.vertx.scala.core.Vertx
import io.vertx.scala.ext.web.client.WebClient
import it.unibo.constants.RedHatSecurityDataConstants
import it.unibo.constants.RedHatSecurityDataConstants.SecurityDataMetricsUri
import it.unibo.data.subscriber.manager.DistroSpecificDataSubscriberManager
import it.unibo.models.data.VulnerabilityData
import it.unibo.models.redhat.RedHatSecurityAdvisory
import it.unibo.models.ws.messages.Entries
import it.unibo.parsers.{Parser, RhsaMapCpeParser}
import it.unibo.updates.operation.result.UpdateResult
import it.unibo.verticle.retriever.configuration.LocalDataLoadWithPollingRetrieverConfiguration
import it.unibo.vertx.FileSystemUtils

case class RhsaMapCpeConfiguration(override protected val vertx: Vertx,
                                   override protected val webClient: WebClient,
                                   private val data: VulnerabilityData[RedHatSecurityAdvisory],
                                   private val dataSubscribersManager: DistroSpecificDataSubscriberManager)
  extends LocalDataLoadWithPollingRetrieverConfiguration[RedHatSecurityAdvisory] {

  override protected val saveFolderName: String = "red-hat/rhsa-release-dates"
  override protected val lastSaveTimestampFileName: String = "rhsa-map-cpe-save-time.txt"
  override protected val savedDataFileName: String = "rhsa-map-cpe.txt"
  override protected val fsUtils: FileSystemUtils = FileSystemUtils(vertx.fileSystem())

  override def loadLocalData(buffer: Buffer): UpdateResult[RedHatSecurityAdvisory] = {
    loadDownloadedData(buffer)
  }

  override protected val dataHost: String = RedHatSecurityDataConstants.Host
  override protected val dataUri: String = s"$SecurityDataMetricsUri/rhsamapcpe.txt"
  protected val parser: Parser[Array[RedHatSecurityAdvisory]] = RhsaMapCpeParser()

  override def loadDownloadedData(buffer: Buffer): UpdateResult[RedHatSecurityAdvisory] = {
    val rhsaArray = parser.fromPlainText(buffer.toString())
    data.insertOrUpdate(rhsaArray, (current, update) => {
      // the security advisory publish date info is not present in rhsa to cpe map file
      val previousSize = current.relatedCveId.size
      current.relatedCveId ++= update.relatedCveId
      previousSize != current.relatedCveId.size
    })
  }

  override def notifySubscribers(operationResult: UpdateResult[RedHatSecurityAdvisory]): Unit = {
    operationResult.printEntriesNumber()
    dataSubscribersManager.notifySubscribers(Entries(operationResult))
  }

}
