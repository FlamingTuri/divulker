package it.unibo.parsers

import it.unibo.models.redhat.RedHatSecurityAdvisory
import it.unibo.utils.Logger

import scala.collection.mutable

case class RhsaMapCpeParser() extends Parser[Array[RedHatSecurityAdvisory]] {

  private val advisoryIdRegex = "(RH[SBE]A-\\d{4}:\\d{3,})"
  private val relatedCveRegex = "(CVE-\\d{4}-\\d{4,}(?:,CVE-\\d{4}-\\d{4,})*)"
  // it happens that not always a cpe is defined
  private val cpeRegex = "(cpe:/.*)?".r
  private val lineRegex = s"$advisoryIdRegex $relatedCveRegex $cpeRegex".r
  //cpe:/part:vendor:product:version:update:sw_edition
  private val affectedPackageFromCpeV1 = "cpe:/[aho]:.*:.*:.*:.*:.*/(.*)".r
  private val affectedPackageFromCpeV2 = "cpe:/[aho]:.*:.*:.*:.*:(.*)".r
  private val affectedPackageFromCpeV3 = "cpe:/[aho]:.*:.*:.*:.*/(.*)".r
  private val affectedPackageFromCpeV4 = "cpe:/[aho]:redhat:(.*):.*".r

  override def fromPlainText(text: String): Array[RedHatSecurityAdvisory] = {
    val lines = text.split('\n')
    lines.map {
      case lineRegex(advisoryId, relatedCve, cpes) =>
        val affectedPackages = if (cpes == null) {
          mutable.Set[String]("unknown")
        } else {
          cpes.split(',').flatMap {
            case affectedPackageFromCpeV1(affectedPackage) => Some(affectedPackage)
            case affectedPackageFromCpeV2(affectedPackage) => Some(affectedPackage)
            case affectedPackageFromCpeV3(affectedPackage) => Some(affectedPackage)
            // this format misses affected software information and is identified as generic linux
            case affectedPackageFromCpeV4(affectedPackage) => Some(affectedPackage)
            case v =>
              Logger.printErr(s"could not parse $advisoryId $v")
              None
          }.to[mutable.Set]
        }
        RedHatSecurityAdvisory(advisoryId, "undefined", relatedCve.split(',').to[mutable.Set], affectedPackages)
      case value => throwParseError(lines, value)
    }.filter(_.affectedPackages.nonEmpty)
  }

}
