package it.unibo.parsers

import it.unibo.models.redhat.RedHatSecurityAdvisory

/** Red Hat Errata:
 * - RHSA: Red Hat Security Advisory
 * - RHBA: Red Hat Bug Advisory
 * - RHEA: Red Hat Enhancement Advisory
 */
case class RhsaReleaseDatesParser() extends Parser[Array[RedHatSecurityAdvisory]] {

  private val securityAdvisoryId = "(RH[SBE]A-\\d{4}:\\d{3,})"
  private val otherAdvisoryId = "((RHBA|RHEA)-\\d{4}:\\d{3,})"
  private val publishedDate = "(\\d{8}(:\\d{4})?)"
  private val source = "(\\(.+\\))"
  private val securityAdvisoryRegex = s"$securityAdvisoryId $publishedDate $source".r
  private val otherRegex = s"$otherAdvisoryId $publishedDate $source".r

  override def fromPlainText(text: String): Array[RedHatSecurityAdvisory] = {
    val lines = text.split('\n')
    lines
      // removes RHBA and RHEA
      /*.filter {
        case otherRegex(_, _, _, _, _) => false
        case _ => true
      }*/ .map {
      case securityAdvisoryRegex(id, fullPublishedDate, hoursMinutes, _) =>
        // removes value info from publishedDate
        val publishedDate =
          if (hoursMinutes == null) {
            fullPublishedDate
          } else {
            fullPublishedDate.substring(0, fullPublishedDate.length - hoursMinutes.length)
          }
        RedHatSecurityAdvisory(id, publishedDate)
      case value => throwParseError(lines, value)
    }
  }
}
