package it.unibo.models

import it.unibo.models.data.VulnerabilityData
import it.unibo.models.redhat.RedHatSecurityAdvisory
import it.unibo.utils.CveUtils
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException

case class RedHatAdvisoryData() extends VulnerabilityData[RedHatSecurityAdvisory] {

  private val redHatAdvisoryIdRegex = "RH[SBE]A-(\\d{4}):(\\d{3,})".r

  override protected def getIdYear(id: String): Int = id match {
    case redHatAdvisoryIdRegex(year, number) => year.toInt
    case value => throw new ValueException(value)
  }

  override protected def getMask(id: String): String = id match {
    case redHatAdvisoryIdRegex(year, number) => CveUtils.maskNumericPortion(number)
    case value => throw new ValueException(value)
  }

}
