package it.unibo.constants

import it.unibo.models.service.{ServiceKind, ServiceName}

case class RhsaReleaseDatesSourceInfo() extends SourceVerticleInfo with RedHatSecurityDataConstants {
  /** The service name */
  override val serviceName: ServiceName.Value = ServiceName.RhsaReleaseDates
  /** The service kind */
  override val serviceKind: ServiceKind.Value = ServiceKind.DistroSpecificRetriever
  /** The path accepted for websocket messages */
  override val WebSocketPath: String = "/rhsa-dates"
}
