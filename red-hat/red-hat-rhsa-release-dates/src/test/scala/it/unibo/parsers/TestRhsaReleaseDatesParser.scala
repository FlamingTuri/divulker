package it.unibo.parsers

import it.unibo.models.redhat.RedHatSecurityAdvisory
import org.scalatest.funsuite.AnyFunSuite

class TestRhsaReleaseDatesParser extends AnyFunSuite {

  private val parser = RhsaReleaseDatesParser()

  def assertResult(id: String, publishedDate: String)(actual: RedHatSecurityAdvisory): Unit = {
    assertResult(RedHatSecurityAdvisory(id, publishedDate))(actual)
  }

  test("RHSA release dates parse") {
    val cveDatesText =
      """RHSA-2000:027 20000521 (Mitre)
        |RHSA-2007:0072 20070124 (reminder mail, RHEA-2007:0024 first fix)
        |RHSA-2010:0940 20101201:2357 (rhn)""".stripMargin

    val rhsaArray = parser.fromPlainText(cveDatesText)
    assertResult(3)(rhsaArray.length)
    assertResult("RHSA-2000:027", "20000521")(rhsaArray(0))
    assertResult("RHSA-2007:0072", "20070124")(rhsaArray(1))
    assertResult("RHSA-2010:0940", "20101201")(rhsaArray(2))
  }

  /*test("Ignore RHBA and RHEA from parsing") {
    val cveDatesText =
      """RHBA-2002:209 20020923:0000 (rhn)
        |RHBA-2004:164 20040511 (hand)
        |RHBA-2007:0565 20071107 (rhn)
        |RHEA-2002:317 20030905:0000 (rhn)
        |RHEA-2006:0355 20060809 (rhn)
        |RHEA-2009:1633 20091203:0415 (rhn)
        |RHSA-2000:027 20000521 (Mitre)
        |RHSA-2007:0072 20070124 (reminder mail, RHEA-2007:0024 first fix)
        |RHSA-2010:0940 20101201:2357 (rhn)""".stripMargin

    val rhsaArray = parser.fromPlainText(cveDatesText)
    assertResult(3)(rhsaArray.length)
  }*/

}
