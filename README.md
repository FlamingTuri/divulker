# DiVulker

DiVulker allows to compute the window of vulnerability of packages of GNU/Linux distributions.
Its microservice based architecture allows adding more distributions to the implemented ones.
Distributions tracked:

*  Debian
*  Red Hat

For Arch Linux there is only a first draft, that requires to be implemented.

DiVulker name is a shorthand for Distro Vulnerability Tracker and, a wordplay with the term
"divulge", since its aim is to track and reveal vulnerability related data.

## Project structure and module dependencies
The project modules must be run in the following order waiting for their correct setup:

*  registry
*  specific retrievers: in any order
    *  debian/debian-cve
    *  debian/debian-security-advisory
    *  debian/snapshot-info
    *  red-hat/red-hat-cve-dates
    *  red-hat/red-hat-rhsa-release-dates
*  general retrievers: in any order
    *  general-retriever/mitre-cvrf-cve-list
    *  general-retriever/nvd-json-feeds
*  specific aggregators: in any order
    *  debian/data-aggregator
    *  red-hat/data-aggregator
*  general-retrievers/data-aggregator
*  distro-data-comparator

Each specific/general retriever microservice can be run with the argument `-PdownloadOutdated=false`
to always use the local version of data. An exception is debian/snapshot-info which uses
`-PdownloadMissing=false`. Since debian/snapshot-info data download can take ages,
I suggest preemptively download and copy the data available in the repository
[download](https://bitbucket.org/FlamingTuri/divulker/downloads/) section into
your `$HOME/.divulker` folder.

```bash
# i.e.
./gradlew :general-retrievers:nvd-json-feeds:run -PdownloadOutdated=false
./gradlew :debian:snapshot-info:run -PdownloadMissing=false
```

To run the provided web-gui just:
```bash
npm install
npx ng serve -o
```
but do not expect too much from it, since it is provided as a mere example, that supports
just few of the available calls to the distro-data-comparator API.
I recognize that it is pretty ugly, but it was not the main objective of this research...
And I do not like doing frontend stuff :)

## Project quick setup
To quickly setup and run the project just run the scripts inside the `scripts` directory.

## Warnings and future fixes
The current implementation of the services keeps all the data in memory, consuming tons of RAM.
This is due to the fact that for time reason I could not back up the microservices with a database.
A good refactoring starting point for at least specific/general retrievers microservices would be:

* check if the local data is outdated and if so download and save a fresh new version
(like it they are already doing) but without keeping it in memory at the end of the process
* when a new client subscribes, always temporary load and send the local data

Backing everything up with a database should be a wiser choice instead of using the local filesystem:
to this end I suggest using MongoDB. Moreover, a db allows data versioning instead of replacing
files content with an updated version. I suggest looking at these StackOverflow questions:

* [ref1](https://stackoverflow.com/questions/25980806/logging-insert-update-and-delete-operations-in-mongodb)
* [ref2](https://stackoverflow.com/questions/4185105/ways-to-implement-data-versioning-in-mongodb)
* [ref3](https://stackoverflow.com/questions/44938525/tracking-history-in-mongodb)

to decide which strategy is more suitable for the task.
