package it.unibo.launcher

import it.unibo.verticle.{RegistryVerticle, VerticleLauncher}

object RegistryLauncher extends App with VerticleLauncher {
  deployVerticle[RegistryVerticle]()
}
