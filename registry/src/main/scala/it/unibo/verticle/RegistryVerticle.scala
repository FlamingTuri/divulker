package it.unibo.verticle

import io.vertx.lang.scala.ScalaVerticle
import io.vertx.scala.core.http.{HttpServer, ServerWebSocket}
import io.vertx.scala.core.net.SocketAddress
import io.vertx.scala.ext.web.client.{WebClient, WebClientOptions}
import io.vertx.scala.ext.web.handler.BodyHandler
import io.vertx.scala.ext.web.{Router, RoutingContext}
import it.unibo.constants.{MimeType, RegistryPaths}
import it.unibo.jackson.MarshallableImplicit._
import it.unibo.models.service.ServiceInfo
import it.unibo.network.{AvailablePortScanner, NetworkUtils}
import it.unibo.utils.Logger
import it.unibo.verticle.Extensions.ServicesInfoExtension
import it.unibo.vertx.WebSocketHelpers
import it.unibo.vertx.WebSocketHelpers._

import scala.collection.mutable.ListBuffer
import scala.util.{Failure, Success}

class RegistryVerticle extends ScalaVerticle {

  private val subscribers = ListBuffer[ServerWebSocket]()
  private val availablePortScanner = new AvailablePortScanner()
  private var httpServer: HttpServer = _
  private var router: Router = _
  private val sourceServiceInfoList = ListBuffer[ServiceInfo]()
  private var webClient: WebClient = _

  override def start(): Unit = {
    httpServer = vertx.createHttpServer()
    val webClientOptions = WebClientOptions()
      .setDefaultPort(80)
      .setMaxWebsocketMessageSize(WebSocketHelpers.MaxMessageSize)
    webClient = WebClient.create(vertx, webClientOptions)

    router = Router.router(vertx)
    router.route().handler(BodyHandler.create())

    // TODO: this infos should be also available via websocket, so that
    //  subscribers are notified if new services are added or a service crashes
    router.get(RegistryPaths.SourcesPath)
      .produces(MimeType.ApplicationJson)
      .handler(getSourceServiceInfoListHandler)
    router.get(s"${RegistryPaths.SourcesPath}/:name")
      .produces(MimeType.ApplicationJson)
      .handler(getSourceServiceInfoHandler)
    router.post(RegistryPaths.SourcesPath)
      .consumes(MimeType.ApplicationJson)
      .produces(MimeType.ApplicationJson)
      .handler(postSourceServiceInfoHandler)

    httpServer.websocketHandler { serverWebSocket =>
      if (serverWebSocket.path == RegistryPaths.BasePath) {
        //serverWebSocket.textMessageHandlerAsync(wsTextMessageHandler)
        serverWebSocket.closeHandler(_ => wsCloseHandler(serverWebSocket))
        serverWebSocket.defaultExceptionHandler()
        subscribers += serverWebSocket
        serverWebSocket.writeTextMessageFramed(sourceServiceInfoList.toJson)
      } else {
        serverWebSocket.close(0.toShort, Option("wrong path requested"))
      }
    }

    listen()
  }

  protected def listen(startingPort: Int = 8080) {
    val port = availablePortScanner.findFreePort(startingPort)
    httpServer.requestHandler(router).listenFuture(port).onComplete {
      case Success(result) =>
        val hostAddress = NetworkUtils.hostAddress
        val port = result.actualPort();
        Logger.printOut(s"Server is now listening on http://$hostAddress:$port")
        startServiceHeartBeat()
      case Failure(cause) =>
        Logger.printErr(s"$cause")
        listen(startingPort + 1)
    }
  }

  private def startServiceHeartBeat(): Unit = {
    vertx.setTimer(10000, _ => checkIfServiceIsUp(0))
  }

  private def checkIfServiceIsUp(idx: Int): Unit = {
    if (idx < sourceServiceInfoList.size) {
      val serviceInfo = sourceServiceInfoList(idx)
      val servicePort = serviceInfo.port
      val serviceHost = serviceInfo.host
      webClient.get(servicePort, serviceHost, "/source/status")
        .sendFuture()
        .onComplete {
          case Success(_) => checkIfServiceIsUp(idx + 1)
          case Failure(_) =>
            Logger.printOut(s"No response from $serviceInfo, updating available services")
            // if the service does not respond,
            // removes it from the available services list
            sourceServiceInfoList -= serviceInfo
            Logger.printOut(s"services count: ${sourceServiceInfoList.size}")
            // TODO: notify service removal to websocket subscribers
            // since a service info is removed the idx is not incremented
            checkIfServiceIsUp(idx)
        }
    } else {
      // after checking that all the registered services
      // are or are not up, starts again the procedure
      startServiceHeartBeat()
    }
  }

  private def getSourceServiceInfoListHandler(routingContext: RoutingContext): Unit = {
    val response = routingContext.response()
    val request = routingContext.request()
    // TODO: check why implicit does not work
    var requestedServices = new ServicesInfoExtension(sourceServiceInfoList)
      .filterByParam(request, "sourceName", (paramValue, serviceInfo) => {
        paramValue.split(',').contains(serviceInfo.name.toString)
      })
    requestedServices = new ServicesInfoExtension(requestedServices)
      .filterByParam(request, "sourceKind", (paramValue, serviceInfo) => {
        paramValue.split(',').contains(serviceInfo.serviceType.toString)
      })
    response.end(requestedServices.toJson)
  }

  private def getSourceServiceInfoHandler(routingContext: RoutingContext): Unit = {
    val response = routingContext.response()
    routingContext.request().getParam("name") match {
      case Some(name) =>
        sourceServiceInfoList.find(e => e.name.toString == name) match {
          case Some(value) => response.end(value.toJson)
          case None => response.setStatusCode(404).end("service not found")
        }
      case None => response.setStatusCode(404).end("missing :name path param")
    }
  }

  private def postSourceServiceInfoHandler(routingContext: RoutingContext): Unit = {
    var newSourceServiceInfo: ServiceInfo = null
    routingContext.getBodyAsString() match {
      case Some(value) =>
        Logger.printOut(value)
        // TODO: duplicate entry (with same name) management
        newSourceServiceInfo = value.fromJson[ServiceInfo]()
        sourceServiceInfoList += newSourceServiceInfo
        val response = newSourceServiceInfo.toJsonArray
        subscribers.foreach { serverWebSocket =>
          serverWebSocket.writeTextMessageFramed(response)
        }
      case None =>
    }
    Logger.printOut(newSourceServiceInfo)
    Logger.printOut(s"services count: ${sourceServiceInfoList.size}")
    routingContext.response().end(newSourceServiceInfo.toJson)
  }

  protected def wsCloseHandler(serverWebSocket: ServerWebSocket): Unit = {
    subscribers -= serverWebSocket
  }

  private def localAndRemoteAddresses(routingContext: RoutingContext): Unit = {
    val request = routingContext.request()

    def printInfo(sock: SocketAddress): Unit = {
      println(s"${sock.host()} ${sock.port()}")
    }

    printInfo(request.localAddress())
    printInfo(request.remoteAddress())
  }

  private def stringToJsonString(s: String): String = {
    s.substring(1, s.length - 1)
      .replaceAll("\\\\", "")
  }

}
