package it.unibo.verticle

import io.vertx.scala.core.http.HttpServerRequest
import it.unibo.jackson.MarshallableImplicit.Unmarshallable
import it.unibo.models.service.ServiceInfo
import it.unibo.utils.Logger

import scala.collection.mutable.ListBuffer

object Extensions {

  implicit class ServicesInfoExtension(list: ListBuffer[ServiceInfo]) {

    private def applyIfParamExists(request: HttpServerRequest, queryParam: String,
                                   function: String => ListBuffer[ServiceInfo]): ListBuffer[ServiceInfo] = {
      request.getParam(queryParam) match {
        case Some(paramValue) =>
          try {
            function(paramValue)
          } catch {
            case e: Throwable =>
              Logger.printErr(e)
              list
          }
        case None => list
      }
    }

    def filterByParam[T](request: HttpServerRequest, queryParam: String,
                         predicate: (String, ServiceInfo) => Boolean)
                        (implicit m: Manifest[T]): ListBuffer[ServiceInfo] = {
      applyIfParamExists(request, queryParam, paramValue => {
        list.filter(serviceInfo => predicate(paramValue, serviceInfo))
      })
    }

    def filterByValues[T](request: HttpServerRequest, queryParam: String,
                          serviceInfoProperty: ServiceInfo => T)
                         (implicit m: Manifest[T]): ListBuffer[ServiceInfo] = {
      filterByParam(request, queryParam, (paramValue, serviceInfo) => {
        val valuesRequested = paramValue.fromJson[Array[T]]()
        valuesRequested.contains(serviceInfoProperty(serviceInfo))
      })
    }

    def filterByValue[T](request: HttpServerRequest, queryParam: String,
                         serviceInfoProperty: ServiceInfo => T)
                        (implicit m: Manifest[T]): ListBuffer[ServiceInfo] = {
      filterByParam(request, queryParam, (paramValue, serviceInfo) => {
        val valuesRequested = paramValue.fromJson[Array[T]]()
        serviceInfoProperty(serviceInfo) == valuesRequested
      })
    }

  }

}
