package it.unibo.models

import it.unibo.jackson.MarshallableImplicit._
import it.unibo.models.aggregator.{GatheredData, Source}
import it.unibo.models.service.ServiceName
import org.scalatest.funsuite.AnyFunSuite

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

class TestSourceAggregatorsModels extends AnyFunSuite {

  private val disclosureSource = Source(ServiceName.NvdService.toString, "2019-10-17")
  // TODO: test dsa date parsing
  private val fixSource = Source(ServiceName.DsaService.toString, "2019-10-14")

  test("Source json conversion") {
    val expectedDisclosureSourceJson = """{"name":"NvdService","date":"2019-10-17"}"""
    assertResult(expectedDisclosureSourceJson)(disclosureSource.toJson)
    assertResult(disclosureSource)(disclosureSource.toJson.fromJson[Source]())

    val expectedFixSourceJson = """{"name":"DsaService","date":"2019-10-14"}"""
    assertResult(expectedFixSourceJson)(fixSource.toJson)
    assertResult(fixSource)(fixSource.toJson.fromJson[Source]())
  }

  test("GatheredData json conversion") {
    val cveId = "CVE-2019-14287"
    val fixes = mutable.ListMap[String, ListBuffer[Source]]("sudo" -> ListBuffer(fixSource))
    val gatheredData = GatheredData(cveId, ListBuffer(disclosureSource), fixes)

    val expectedJson =
      """{"cveId":"CVE-2019-14287",
        |"disclosures":[{"name":"NvdService","date":"2019-10-17"}],
        |"fixes":{"sudo":[{"name":"DsaService","date":"2019-10-14"}]}
        |}""".stripMargin.replace("\n", "")

    assertResult(expectedJson)(gatheredData.toJson)
    assertResult(gatheredData)(gatheredData.toJson.fromJson[GatheredData]())
  }
}
