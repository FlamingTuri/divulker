package it.unibo.models

import it.unibo.jackson.MarshallableImplicit._
import it.unibo.models.ws.messages.operations._
import org.scalatest.funsuite.AnyFunSuite

class TestOperationJsonConversion extends AnyFunSuite {

  test("Test serialization") {
    testOperationSerialization(Subscribe)
    testOperationSerialization(UnSubscribe)
    testOperationSerialization(AddCve)
    testOperationSerialization(RemoveCve)
  }

  private def testOperationSerialization(operation: Operation): Unit = {
    assertResult('"' + operation.jsonValue + '"')(operation.toJson)
  }

  test("Test deserialization") {
    testOperationDeserialization(Subscribe)
    testOperationDeserialization(UnSubscribe)
    testOperationDeserialization(AddCve)
    testOperationDeserialization(RemoveCve)
  }

  private def testOperationDeserialization[O <: Operation : Manifest](operation: O): Unit = {
    assertResult(operation)(operation.toJson.fromJson[O]())
  }

}
