package it.unibo.models

import it.unibo.utils.CveUtils.{getCveIdNumericPortion, getCveIdYear, maskNumericPortion}
import org.scalatest.funsuite.AnyFunSuite

class TestCveUtils extends AnyFunSuite {

  test("Test getCveIdYear") {
    val cveId = "CVE-1999-0710"
    assertResult(1999)(getCveIdYear(cveId))
  }

  test("Test cve id numeric portion masking") {
    def assertNumericPortion(cveId: String, expectedCveNumericPortion: String, expectedCveMask: String): Unit = {
      val cveNumericPortion = getCveIdNumericPortion(cveId)
      assertResult(expectedCveNumericPortion)(cveNumericPortion)
      assertResult(expectedCveMask)(maskNumericPortion(cveNumericPortion))
    }

    assertNumericPortion("CVE-1999-0710", "0710", "0xxx")
    assertNumericPortion("CVE-1999-3710", "3710", "3xxx")
    assertNumericPortion("CVE-1999-13710", "13710", "13xxx")
  }

}
