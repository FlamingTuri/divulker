package it.unibo.utils

import java.io.IOException
import java.net.ServerSocket

import it.unibo.network.AvailablePortScanner
import org.scalatest.funsuite.AnyFunSuite

class TestAvailablePortScanner extends AnyFunSuite {

  private final val MinPortNumber = 2
  private final val MaxPortNumber = 65535
  private val availablePortScanner = new AvailablePortScanner

  private class RichInt(i: Int) {
    def inRange(start: Int, stop: Int): Boolean = {
      i >= start && i <= stop
    }
  }

  private implicit def richInt(i: Int): RichInt = new RichInt(i)

  private def checkPortAvailability(port: Int) {
    var serverSocket: ServerSocket = null
    var success = false
    try {
      serverSocket = new ServerSocket(port)
      serverSocket.setReuseAddress(true)
      assert(!availablePortScanner.isPortAvailable(port))
      serverSocket.close()
      success = true
    } catch {
      case e: IOException => /* failed opening the socket */
    }
    assert(success)
  }

  test("Find random free port should return a value between 2 and 65535") {
    val freePort = availablePortScanner.findFreePort
    assert(freePort.inRange(MinPortNumber, MaxPortNumber))
    checkPortAvailability(freePort)
  }

  test("Find free port should return a value starting from the specified port") {
    val startingPort = 4200
    val freePort = availablePortScanner.findFreePort(startingPort)
    assert(freePort.inRange(startingPort, MaxPortNumber))
    checkPortAvailability(freePort)
  }

}
