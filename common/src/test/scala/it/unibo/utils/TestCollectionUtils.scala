package it.unibo.utils

import io.vertx.scala.core.Future
import it.unibo.utils.CollectionUtils._
import it.unibo.vertx.VertxHelpers._
import org.scalatest.funsuite.AnyFunSuite

import scala.collection.mutable.ListBuffer

class TestCollectionUtils extends AnyFunSuite {

  private class MyIllegalArgumentException extends IllegalArgumentException

  private val range: Seq[Int] = 1 to 10

  test("Test foreach synchronous") {
    val values = ListBuffer[Int]()
    range.foreachSynchronous({
      case 3 | 10 => Future.failedFuture(new IllegalArgumentException())
      case n =>
        values += n
        Future.succeededFuture[Unit]()
    }, Seq(), Option.empty, (_: Int) => "", false).compose { _ =>
      assertResult(8)(values.size)
      assertResult(ListBuffer(1, 2) ++ (4 to 9))(values)
    }
  }

  test("Test foreach synchronous exception recovery") {
    val e = new IllegalArgumentException()
    failEachFirstTime(n => n % 2 == 0, e).compose(_ => failEachFirstTime(n => n % 2 != 0, e))
  }

  test("Test foreach synchronous exception subclass recovery") {
    val e = new MyIllegalArgumentException()
    failEachFirstTime(n => n % 2 == 0, e).compose(_ => failEachFirstTime(n => n % 2 != 0, e))
      .compose { _ =>
        // the given recover exception should not match
        val values = ListBuffer[Int]()
        range.foreachSynchronous({
          case 3 | 10 => Future.failedFuture(new IllegalArgumentException())
          case n =>
            values += n
            Future.succeededFuture[Unit]()
        }, Seq(new NullPointerException()), Option.empty, (_: Int) => "", false).compose { _ =>
          assertResult(8)(values.size)
          assertResult(ListBuffer(1, 2) ++ (4 to 9))(values)
        }
      }
  }

  private def failEachFirstTime(failCondition: Int => Boolean, e: Throwable): Future[Unit] = {
    val values = ListBuffer[Int]()
    var guard = true
    var counter = 0
    range.foreachSynchronous({
      case n if failCondition(n) && guard =>
        guard = false // the next time the even value will not match this block
        Future.failedFuture(e)
      case n =>
        guard = true // resets the guard
        counter += 1
        assertResult(counter)(n)
        values += n
        Future.succeededFuture[Unit]()
    }, Seq(new RuntimeException()), Option.empty, (_: Int) => "", false).compose { _ =>
      assertResult(10)(values.size)
      assertResult((1 to 10).to[ListBuffer])(values)
      Future.succeededFuture[Unit]()
    }
  }
}
