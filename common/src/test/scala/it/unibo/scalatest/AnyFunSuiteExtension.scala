package it.unibo.scalatest

import org.scalatest.funsuite.AnyFunSuite

trait AnyFunSuiteExtension extends AnyFunSuite {

  /** Assert that the property value passed of expected equals the property value of actual.
   *
   * @param expected          the expected object
   * @param actual            the actual object
   * @param propertyToCompare function extracting a property from an object of type [[T]]
   * @tparam T type of the comparing objects
   * @tparam R type of the property to extract
   */
  def compareProperty[T, R](expected: T, actual: T, propertyToCompare: T => R): Unit = {
    assertResult(propertyToCompare(expected))(propertyToCompare(actual))
  }

}
