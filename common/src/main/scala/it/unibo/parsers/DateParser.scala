package it.unibo.parsers

trait DateParser {

  /**
   * Convert the date to integer with yyyy-MM-dd format
   *
   * @param date the date to convert
   * @return the date in yyyy-MM-dd format
   */
  def parseDate(date: String): String

}
