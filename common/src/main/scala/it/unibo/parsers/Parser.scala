package it.unibo.parsers

import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException

trait Parser[T] {

  /** Parse the given text
   *
   * @param text the text to parse
   * @return the parse result
   */
  def fromPlainText(text: String): T

  /** Throws a ValueException indicating which line could not be parsed
   *
   * @param lines the lines that were being parsed
   * @param value the value that could not be parsed
   * @return [[Nothing]]
   */
  protected def throwParseError(lines: Array[String], value: String): Nothing = {
    throwParseError(s"line ${lines.indexOf(value)} with value $value")
  }

  /** Throws a ValueException indicating which value could not be parsed
   *
   * @param value the value that could not be parsed
   * @return [[Nothing]]
   */
  protected def throwParseError(value: String): Nothing = {
    throw new ValueException(s"could not parse $value")
  }

}

object Parser {

  /** Throws a ValueException indicating which value could not be parsed
   *
   * @param value the value that could not be parsed
   * @return [[Nothing]]
   */
  def throwParseError(value: String): Nothing = throw new ValueException(s"could not parse $value")

}
