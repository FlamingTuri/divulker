package it.unibo.verticle

import java.net.ConnectException

import io.vertx.core.buffer.Buffer
import io.vertx.lang.scala.ScalaVerticle
import io.vertx.scala.core.Future
import io.vertx.scala.core.file.FileSystem
import io.vertx.scala.core.http.{HttpServer, HttpServerOptions, ServerWebSocket}
import io.vertx.scala.ext.web.Router
import io.vertx.scala.ext.web.client.{WebClient, WebClientOptions}
import io.vertx.scala.ext.web.handler.BodyHandler
import it.unibo.constants.SourceVerticleInfo
import it.unibo.network.{AvailablePortScanner, NetworkUtils, RegistryUtils}
import it.unibo.utils.{AttemptExecution, Logger}
import it.unibo.vertx.VertxExtensions._
import it.unibo.vertx.VertxHelpers.routerFailureHandler
import it.unibo.vertx.WebSocketHelpers._
import it.unibo.vertx.{FileSystemUtils, WebSocketHelpers}

import scala.util.{Failure, Success}

/** A verticle wrapping the logic to setup a data source */
abstract class SourceVerticle[D] extends ScalaVerticle {

  protected val verticleInfo: SourceVerticleInfo
  private val availablePortScanner = new AvailablePortScanner()
  protected var fs: FileSystem = _
  protected var fsUtils: FileSystemUtils = _
  var webClient: WebClient = _
  protected var httpServer: HttpServer = _
  protected var router: Router = _
  protected var attemptExecution: AttemptExecution = _
  // TODO: in the future this should be converted as a method that returns an empty instance of the
  //  the data structure used to store the vulnerability data, so that everything is just temporary
  //  loaded in memory and then saved to an external database. Obviously upon receiving new data,
  //  the data stored in the database should be loaded and the check whether data updates should be
  //  performed.
  val data: D

  /** Get a buffer content as string
   *
   * @param buffer the buffer to convert
   * @return the string content
   */
  implicit def bufferToString(buffer: Buffer): String = buffer.toString()

  override def start(): Unit = {
    fs = vertx.fileSystem
    fsUtils = FileSystemUtils(fs)
    val webClientOptions = WebClientOptions()
      .setDefaultPort(80)
      .setMaxWebSocketMessageSize(WebSocketHelpers.MaxMessageSize)
    webClient = WebClient.create(vertx, webClientOptions)

    val httpServerOptions = HttpServerOptions()
      .setMaxWebSocketMessageSize(WebSocketHelpers.MaxMessageSize)
    httpServer = vertx.createHttpServer(httpServerOptions)
    router = Router.router(vertx)

    router.route()
      .handler(BodyHandler.create())
      .failureHandler(routerFailureHandler)

    setupRoutes()

    httpServer.requestHandler(router)
    httpServer.websocketHandler { serverWebSocket =>
      if (serverWebSocket.path == verticleInfo.WebSocketPath) {
        serverWebSocket.textMessageHandlerAsync(vertx, wsTextMessageHandler)
        serverWebSocket.closeHandler(_ => wsCloseHandler(serverWebSocket))
        serverWebSocket.defaultExceptionHandler()
      } else {
        serverWebSocket.close(0.toShort, Option("wrong path requested"))
      }
    }

    attemptExecution = AttemptExecution(() => vertx.setTimer(10000))
    setupOperations().onComplete(_ => listen())
  }

  /** Setups REST API routes */
  protected def setupRoutes(): Unit = {
    router.get("/source/status").handler(routingContext => routingContext.response().end())
  }

  /** Makes all the operations necessary before starting the http server listening.
   *
   * @return a future that will be completed when the setup operations are done
   */
  protected def setupOperations(): Future[Unit]

  protected def listen(startingPort: Int = 8081): Unit = {
    val port = availablePortScanner.findFreePort(startingPort)
    httpServer.listenFuture(port).onComplete {
      case Success(result) =>
        val hostAddress = NetworkUtils.hostAddress
        Logger.printOut(s"Server is now listening on http://$hostAddress:$port")
        subscribeToRegistry(result)
      case Failure(cause) =>
        Logger.printErr(s"$cause")
        listen(startingPort + 1)
    }
  }

  /** Sends the verticle host and port to the registry. If the registry is not up,
   * attempts to send the information again after a brief delay.
   *
   * @param result the http server listen result
   */
  private def subscribeToRegistry(result: HttpServer): Unit = {
    attemptExecution.process(result, r => {
      RegistryUtils.subscribe(fs, webClient, r, verticleInfo.serviceName,
        verticleInfo.serviceKind, verticleInfo.WebSocketPath)
    }, scala.Seq(new ConnectException().getClass))
      .compose(_ => Future.succeededFuture(checkIfRegistryIsUp(result)))
  }

  /** Periodically checks if the registry is up. If the registry is not up,
   * starts the [[subscribeToRegistry]] procedure.
   *
   * @param result the http server listen result
   */
  private def checkIfRegistryIsUp(result: HttpServer): Unit = {
    vertx.setTimer(10000, _ => {
      RegistryUtils.checkIfRegistryIsUp(webClient).onComplete { r =>
        if (r.result()) {
          checkIfRegistryIsUp(result)
        } else {
          subscribeToRegistry(result)
        }
      }
    })
  }

  /** Handles the text messages incoming from websocket.
   *
   * @param serverWebSocket the websocket endpoint which sent the text message
   * @param textMessage     the text message received
   */
  protected def wsTextMessageHandler(serverWebSocket: ServerWebSocket, textMessage: String): Unit

  /** Handles the websocket closure events.
   *
   * @param serverWebSocket the websocket endpoint which closed the connection
   */
  protected def wsCloseHandler(serverWebSocket: ServerWebSocket): Unit

  /** Saves the specified buffer to the specified path.
   *
   * @param file   the path where the buffer data will be saved
   * @param buffer the buffer storing the data to save
   * @return a future that will be completed when the save process is ended
   */
  def saveData(file: String, buffer: Buffer): Future[Unit] = fsUtils.saveData(file, buffer)

}
