package it.unibo.verticle.retriever.configuration

import java.text.SimpleDateFormat
import java.util.{Calendar, Date, Locale}

import io.vertx.core.buffer.Buffer
import io.vertx.scala.core.Future
import it.unibo.updates.operation.result.UpdateResult
import it.unibo.utils.{DateUtils, Logger}
import it.unibo.vertx.FileSystemUtils
import it.unibo.vertx.VertxHelpers._

trait LocalDataLoadConfiguration[D] extends DataRetrieverConfiguration {

  protected val sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.US)
  protected val lastUpdate: Calendar = DateUtils.getCalendarInstance
  /** Name of the directory where the files will be saved */
  protected val saveFolderName: String
  /** Name of the file storing the information about the last time data where updated */
  protected val lastSaveTimestampFileName: String
  /** Name of the file storing a local backup of the downloaded data */
  protected val savedDataFileName: String
  protected val fsUtils: FileSystemUtils

  def loadData(): Future[Unit] = {
    val localDataExist = fsUtils.checkFilesExistence(lastSaveTimestampFile, savedDataFile)
    if (localDataExist) {
      var localLastTimeStamp: Date = null
      fsUtils.loadFile(lastSaveTimestampFile).compose { buffer =>
        buffer.toString()
        localLastTimeStamp = parseLocalLastTimestamp(buffer)
        getActualTimestamp
      }.compose { actualTimestamp =>
        // check if local data are outdated
        if (areDataOutdated(localLastTimeStamp, actualTimestamp)) {
          Logger.printOut("data outdated")
          // local files are too old, download fresh data
          freshDownload(actualTimestamp)
        } else {
          Logger.printOut("data up to date")
          lastUpdate.setTime(localLastTimeStamp)
          // load data from file
          fsUtils.loadFile(savedDataFile).compose(e => notifySubscribers(loadLocalData(e)))
        }
      }
    } else {
      // no existing data found, saves timestamp and downloads them
      Logger.printOut("data missing")
      freshDownload(getActualTimestamp)
    }
  }

  /** Gets the absolute path to the file storing the information of the last saved timestamp.
   *
   * @return the absolute path to the file
   */
  def lastSaveTimestampFile: String = {
    pathInsideSaveDirectory(saveFolderName).resolve(lastSaveTimestampFileName)
  }

  /** Gets the absolute path to the file storing the data.
   *
   * @return the absolute path to the file
   */
  def savedDataFile: String = {
    pathInsideSaveDirectory(saveFolderName).resolve(savedDataFileName)
  }

  /** Get the java date stored in the [[lastSaveTimestampFile]] file.
   *
   * @param buffer the buffer to parse
   * @return the date
   */
  def parseLocalLastTimestamp(buffer: Buffer): Date = sdf.parse(buffer.toString())

  /** Gets the actual timestamp.
   *
   * @return the actual timestamp
   */
  def getActualTimestamp: Future[Date]

  /** Checks if data are outdated comparing lastUpdateTimestamp and actualTimestamp.
   *
   * @param lastUpdateTimestamp last update timestamp value
   * @param actualTimestamp     actual update timestamp value
   * @return true if the data are outdated and needs to be redownloaded
   */
  def areDataOutdated(lastUpdateTimestamp: Date, actualTimestamp: Date): Boolean

  /** Downloads, loads and save fresh new data
   *
   * @param actualTimestamp the current timestamp
   * @return the operation result
   */
  protected def freshDownload(actualTimestamp: Future[Date]): Future[Unit] = {
    setAndSaveLastSaveTimestamp(actualTimestamp).compose(_ => downloadData()).compose { buffer =>
      notifySubscribers(loadDownloadedData(buffer))
      saveDownloadedData(buffer)
    }
  }

  /** Sets the last save timestamp and saves it in [[lastSaveTimestampFile]] using
   * the [[getActualTimestamp]] result as timestamp.
   *
   * @return the future representing the save operation result
   */
  def setAndSaveLastSaveTimestamp(): Future[Unit] = setAndSaveLastSaveTimestamp(getActualTimestamp)

  /** Sets the last save timestamp and saves it in [[lastSaveTimestampFile]].
   *
   * @param timestamp the last save timestamp
   * @return the future representing the save operation result
   */
  def setAndSaveLastSaveTimestamp(timestamp: Future[Date]): Future[Unit] = {
    timestamp.compose { date =>
      lastUpdate.setTime(date)
      fsUtils.saveData(lastSaveTimestampFile, Buffer.buffer(lastUpdate.getTime.toString))
    }
  }

  /** Downloads the data.
   *
   * @return the download result
   */
  def downloadData(): Future[Buffer]

  /** Loads the downloaded data.
   *
   * @param buffer the buffer containing the downloaded data
   * @return the load result
   */
  def loadDownloadedData(buffer: Buffer): UpdateResult[D]

  /** Loads content of [[savedDataFile]].
   *
   * @param buffer the buffer [[savedDataFile]] content
   */
  def loadLocalData(buffer: Buffer): UpdateResult[D]

  /** Notifies the source subscribers with the updated and new data.
   *
   * @param operationResult the update operation result
   */
  def notifySubscribers(operationResult: UpdateResult[D]): Unit

  /** Saves the downloaded data into [[savedDataFile]] file.
   *
   * @param buffer the buffer containing the downloaded data
   * @return the save operation result
   */
  def saveDownloadedData(buffer: Buffer): Future[Unit] = fsUtils.saveData(savedDataFile, buffer)

}
