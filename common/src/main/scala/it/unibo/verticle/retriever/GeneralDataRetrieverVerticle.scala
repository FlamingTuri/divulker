package it.unibo.verticle.retriever

import io.vertx.scala.core.http.ServerWebSocket
import it.unibo.data.subscriber.manager.{DistroRelatedCve, GeneralDataSubscriberManager}
import it.unibo.jackson.MarshallableImplicit._
import it.unibo.models.CustomTypes.YearMaskVulnerabilities
import it.unibo.models.GeneralCve
import it.unibo.models.ws.messages.Entries
import it.unibo.models.ws.messages.general.{GeneralSourceRequestMessage, GeneralSourceResponseMessage}
import it.unibo.models.ws.messages.operations._
import it.unibo.utils.Logger
import it.unibo.vertx.WebSocketHelpers.WebSocketExtensions

import scala.collection.mutable

abstract class GeneralDataRetrieverVerticle[T <: GeneralCve]
  extends DataRetrieverVerticle[YearMaskVulnerabilities[T], DistroRelatedCve] {

  override val data: YearMaskVulnerabilities[T] = mutable.SortedMap()
  override val dataSubscriberManager: GeneralDataSubscriberManager[T] = new GeneralDataSubscriberManager[T](data)

  override protected def wsTextMessageHandler(serverWebSocket: ServerWebSocket, textMessage: String): Unit = {
    val wsRequestMessage = textMessage.fromJson[GeneralSourceRequestMessage[GeneralSourceOperation]]()
    val distroName = wsRequestMessage.distroName
    val cveIds = wsRequestMessage.cveIds
    //Logger.printOut(wsRequestMessage)
    Logger.printOut(s"requested operation: ${wsRequestMessage.operation}")

    def handleMessage[O >: GeneralSourceOperation](msg: GeneralSourceRequestMessage[O]): Unit = {
      msg.operation match {
        case Subscribe =>
          val (distroRelatedCve, newCveIds) = dataSubscriberManager.subscribe(distroName, serverWebSocket, cveIds)
          // gets all the data related to all distro cve ids
          val cveData = dataSubscriberManager.getDistroCveData(distroRelatedCve)
          if (cveData.nonEmpty) {
            // sends them to the new subscriber
            notifySubscribers(mutable.Set(serverWebSocket), Entries.newEntries(cveData), distroName)
          }
          // if there are newly added cve ids
          if (newCveIds.nonEmpty) {
            // retrieves the other subscribers
            val alreadyPresentSubscribers = dataSubscriberManager.getDistroSubscribers(distroName)
              .filterNot(subscriber => subscriber == serverWebSocket)
            notifyAllSubscribers(alreadyPresentSubscribers, newCveIds, distroName)
          }
        case UnSubscribe =>
          dataSubscriberManager.unsubscribe(distroName, serverWebSocket)
        case AddCve =>
          Logger.printOut("requested " + wsRequestMessage.cveIds.size)
          dataSubscriberManager.addToTrackList(distroName, wsRequestMessage.cveIds) match {
            case Some(value) =>
              val (distroRelatedCve, cveIds) = value
              Logger.printOut("retrieved " + cveIds.size)
              if (cveIds.nonEmpty) {
                val subscribers = distroRelatedCve.getSubscribers
                notifyAllSubscribers(subscribers, cveIds.to[mutable.Iterable], distroName)
              }
            case None =>
          }
        case RemoveCve =>
          dataSubscriberManager.removeFromTrackList(distroName, wsRequestMessage.cveIds)
      }
    }
    // match the requested operation
    handleMessage(wsRequestMessage)
  }

  override protected def wsCloseHandler(serverWebSocket: ServerWebSocket): Unit = {
    dataSubscriberManager.unsubscribe(serverWebSocket)
  }

  /** Notifies all the subscribers with the data identified by the CVE IDs.
   *
   * @param subscribers the subscribers to notify
   * @param cveIds the CVE IDs used to identify which data should be sent
   * @param distroName the distro to which the data is related to
   */
  protected def notifyAllSubscribers(subscribers: mutable.Set[ServerWebSocket],
                                     cveIds: Iterable[String],
                                     distroName: String): Unit = {
    if (subscribers.nonEmpty) {
      val cveIdsRelatedData = dataSubscriberManager.getCveIdsRelatedData(cveIds)
      Logger.printOut(s"total size ${cveIdsRelatedData.size}")
      notifySubscribers(subscribers, Entries.newEntries(cveIdsRelatedData), distroName)
    }
  }

  /** Notifies all the subscribers with the specified data.
   *
   * @param subscribers the subscribers to notify
   * @param cveData the data that should be sent
   * @param distroName the distro to which the data is related to
   */
  protected def notifySubscribers(subscribers: mutable.Set[ServerWebSocket],
                                  cveData: Entries[T],
                                  distroName: String): Unit = {
    if (subscribers.nonEmpty) {
      val response = GeneralSourceResponseMessage(verticleInfo.serviceName, verticleInfo.serviceKind,
        distroName, cveData)
      val jsonResponse = response.toJson
      subscribers.foreach(_.writeTextMessageFramed(jsonResponse))
    }
  }

}
