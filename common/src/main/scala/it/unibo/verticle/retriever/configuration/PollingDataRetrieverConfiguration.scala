package it.unibo.verticle.retriever.configuration

import java.util.Date
import java.util.concurrent.TimeUnit

import io.vertx.core.buffer.Buffer
import io.vertx.scala.core.{Future, Vertx}
import io.vertx.scala.ext.web.client.WebClient
import it.unibo.updates.operation.result.UpdateResult
import it.unibo.utils.{DateUtils, Logger}
import it.unibo.vertx.HttpResponseFix.fixHttpResponseFuture
import it.unibo.vertx.VertxExtensions._

trait PollingDataRetrieverConfiguration[D] extends DataRetrieverConfiguration {

  protected val downloadDelayInterval: Int = 1
  protected val vertx: Vertx
  protected val webClient: WebClient

  /** The site hosting the data to download. */
  protected val dataHost: String
  /** The uri of the data to download. */
  protected val dataUri: String

  override def initDataUpdate(): Unit = {
    val delayInMillis = TimeUnit.MILLISECONDS.convert(downloadDelayInterval, TimeUnit.HOURS)
    vertx.repeatAfterDelay(delayInMillis, _ => downloadData().compose { buffer =>
      notifySubscribers(loadDownloadedData(buffer))
      saveDownloadedData(buffer)
    })
  }

  /** Gets the actual timestamp.
   *
   * @return the actual timestamp
   */
  def getActualTimestamp: Future[Date] = {
    Future.succeededFuture(DateUtils.getCalendarInstance.getTime)
  }

  /** Checks if data are outdated comparing lastUpdateTimestamp and actualTimestamp.
   *
   * @param lastUpdateTimestamp last update timestamp value
   * @param actualTimestamp     actual update timestamp value
   * @return true if the data are outdated and needs to be redownloaded
   */
  def areDataOutdated(lastUpdateTimestamp: Date, actualTimestamp: Date): Boolean = {
    DateUtils.isTimeElapsed(lastUpdateTimestamp, actualTimestamp, downloadDelayInterval) && downloadOutdated
  }

  /** Method to call to download data hosted at [[dataHost]] and with the uri [[dataUri]].
   * The result can be handled overriding [[loadDownloadedData]].
   *
   * @return the future representing the download result
   */
  def downloadData(): Future[Buffer] = {
    webClient.get(dataHost, dataUri).sendFuture.compose { value =>
      val buffer = value.httpResponse.body.get
      Future.succeededFuture(buffer)
    }
  }

  /** Loads the downloaded data.
   *
   * @param buffer the buffer containing the downloaded data
   * @return the load result
   */
  def loadDownloadedData(buffer: Buffer): UpdateResult[D]

  /** Notifies the source subscribers with the updated and new data.
   *
   * @param operationResult the update operation result
   */
  def notifySubscribers(operationResult: UpdateResult[D]): Unit

  /** Saves the downloaded data into [[savedDataFile]] file.
   *
   * @param buffer the buffer containing the downloaded data
   * @return the save operation result
   */
  def saveDownloadedData(buffer: Buffer): Future[Unit]
}
