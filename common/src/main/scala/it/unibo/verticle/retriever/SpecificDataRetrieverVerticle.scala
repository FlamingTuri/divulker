package it.unibo.verticle.retriever

import io.vertx.scala.core.http.ServerWebSocket
import it.unibo.constants.SourceVerticleInfo
import it.unibo.data.subscriber.manager.DistroSpecificDataSubscriberManager
import it.unibo.jackson.MarshallableImplicit._
import it.unibo.models.ws.messages.distro.DistroSourceRequestMessage
import it.unibo.models.ws.messages.operations.{Subscribe, UnSubscribe}
import it.unibo.utils.Logger

abstract class SpecificDataRetrieverVerticle[T](override val verticleInfo: SourceVerticleInfo)
  extends DataRetrieverVerticle[T, ServerWebSocket] {

  override val dataSubscriberManager: DistroSpecificDataSubscriberManager = DistroSpecificDataSubscriberManager(verticleInfo)

  override protected def wsTextMessageHandler(serverWebSocket: ServerWebSocket, textMessage: String): Unit = {
    val wsRequestMessage = textMessage.fromJson[DistroSourceRequestMessage]()
    Logger.printOut(s"requested operation: ${wsRequestMessage.operation}")
    wsRequestMessage.operation match {
      case Subscribe => dataSubscriberManager.subscribe(serverWebSocket, dataAsIterable)
      case UnSubscribe => dataSubscriberManager.unsubscribe(serverWebSocket)
      case _ =>
    }
  }

  override protected def wsCloseHandler(serverWebSocket: ServerWebSocket): Unit = {
    dataSubscriberManager.unsubscribe(serverWebSocket)
  }

  protected def dataAsIterable: Iterable[_]
}
