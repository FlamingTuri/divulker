package it.unibo.verticle.retriever.configuration

abstract class LocalDataLoadWithPollingRetrieverConfiguration[D: Manifest]
  extends LocalDataLoadConfiguration[D] with PollingDataRetrieverConfiguration[D]
