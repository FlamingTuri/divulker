package it.unibo.verticle.retriever

import io.vertx.scala.core.Future
import it.unibo.data.subscriber.manager.DataSubscribersManager
import it.unibo.utils.CollectionUtils._
import it.unibo.verticle.SourceVerticle
import it.unibo.verticle.retriever.configuration.DataRetrieverConfiguration

abstract class DataRetrieverVerticle[D, S] extends SourceVerticle[D] {

  val dataSubscriberManager: DataSubscribersManager[S]

  override protected def setupOperations(): Future[Unit] = {
    dataRetrieverConfigurations.foreachSynchronous(_.setup())
  }

  /** Gets the configurations used to retrieve the verticle data
   *
   * @return the configurations
   */
  protected def dataRetrieverConfigurations: List[DataRetrieverConfiguration]

}
