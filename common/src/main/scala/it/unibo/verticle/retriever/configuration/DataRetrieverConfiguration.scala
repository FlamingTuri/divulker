package it.unibo.verticle.retriever.configuration

import io.vertx.scala.core.Future
import it.unibo.constants.Constants

trait DataRetrieverConfiguration extends Constants {

  protected val downloadOutdated: Boolean = sys.env.getOrElse("downloadOutdated", "true").toBoolean

  /** Loads the verticle data and the setups the data update procedure.
   *
   * @return a succeeded future when the setup is completed
   */
  def setup(): Future[Unit] = {
    loadData().compose { _ =>
      if (downloadOutdated) {
        initDataUpdate()
      }
      Future.succeededFuture[Unit]()
    }.otherwise(e => {
      e.printStackTrace()
      Future.succeededFuture[Unit]()
    })
  }

  /** Procedure to load the source data.
   *
   * @return the future representing the procedure result
   */
  def loadData(): Future[Unit]

  /** Setups the procedure used to receive data update. */
  def initDataUpdate(): Unit

}
