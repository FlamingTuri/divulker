package it.unibo.verticle.aggregator

import io.vertx.scala.core.http.ServerWebSocket
import it.unibo.handlers.SpecificRetrieverWsHandler
import it.unibo.jackson.MarshallableImplicit._
import it.unibo.models.CustomTypes.GatheredDataType
import it.unibo.models.data.CveIdVulnerabilityData
import it.unibo.models.ws.messages.Entries
import it.unibo.models.ws.messages.aggregator.{AggregatorRequestMessage, AggregatorResponses}
import it.unibo.models.ws.messages.operations.{Subscribe, UnSubscribe}
import it.unibo.utils.Logger

abstract class SpecificDataAggregatorVerticle[D <: GatheredDataType]
  extends DataAggregatorVerticle[D, CveIdVulnerabilityData[D], SpecificDataAggregatorVerticle[D], SpecificRetrieverWsHandler[D]] {

  override val data: CveIdVulnerabilityData[D] = CveIdVulnerabilityData[D]()

  override protected def getWsConnectionUtility: WsConnectionUtility[SpecificDataAggregatorVerticle[D]] = {
    WsConnectionUtility(this, httpClient, vertx)
  }

  override protected def getResponse(dataToSend: Entries[D]): String = {
    AggregatorResponses(verticleInfo.serviceName, dataToSend).toJson
  }

  override protected def wsTextMessageHandler(serverWebSocket: ServerWebSocket, textMessage: String): Unit = {
    val requestMessage = textMessage.fromJson[AggregatorRequestMessage]()
    Logger.printOut(requestMessage)
    requestMessage.operation match {
      case Subscribe =>
        subscribers += serverWebSocket
        notifyAggregatorSubscribers(Entries.newEntries(data.getAsIterable), List(serverWebSocket))
      case UnSubscribe =>
        subscribers -= serverWebSocket
      case _ =>
    }
  }

  override protected def wsCloseHandler(serverWebSocket: ServerWebSocket): Unit = {
    subscribers -= serverWebSocket
  }

}
