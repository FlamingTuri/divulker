package it.unibo.verticle.aggregator

import io.vertx.scala.core.http.HttpClient
import io.vertx.scala.core.{Future, Promise, Vertx}
import it.unibo.handlers.WsHandler
import it.unibo.models.CustomTypes.ServicesData
import it.unibo.models.service.ServiceInfo
import it.unibo.updates.PendingUpdate
import it.unibo.utils.Logger
import it.unibo.verticle.SourceVerticle
import it.unibo.vertx.CompositeFutureHelpers.CompositeFutureFix
import it.unibo.vertx.WebSocketHelpers

import scala.collection.mutable.ListBuffer

/** Utility to setup the handler for sending and receiving messages via websocket
 *
 * @param verticle
 * @param httpClient
 * @param vertx
 * @tparam V
 */
case class WsConnectionUtility[V <: SourceVerticle[_]](verticle: V, httpClient: HttpClient, vertx: Vertx) {

  private val pendingWebSocketUpdates = new PendingUpdate()

  /** Connects to all the specified WebSockets
   *
   * @param wsServices the iterable containing the information necessary to
   *                   establish every the websocket connection
   * @return a future which result reflects whether all the connection
   *         have been successfully established
   */
  def connectAll(wsServices: Iterable[ServicesData[_ <: WsHandler[V]]]): Future[Unit] = {
    val setupCompleted = Promise.promise[Unit]()
    // it is necessary to create a promise for each service since calling multiple times
    // set handler on a single promise cause an unpredictable behaviour
    val allConnected: ListBuffer[Promise[Unit]] = ListBuffer[Promise[Unit]]()
    // Connects to all the websocket endpoints and, after every connection has been successfully
    // established, completes the setupCompleted promise and then completes the promises
    // that will trigger the handlers setup
    wsServices.map { serviceData =>
      val promise = Promise.promise[Unit]()
      allConnected += promise
      wsConnect(serviceData._1, serviceData._2, promise.future())
    }.onAll().onComplete { result =>
      if (result.succeeded()) {
        setupCompleted.complete()
        // completes all the promises so that handlers setup can start
        allConnected.foreach(_.complete())
      } else {
        val cause = result.cause()
        Logger.printErr(cause)
        setupCompleted.fail(cause)
      }
    }
    setupCompleted.future()
  }

  /** Connects to the specified websocket and waits to run the handler code
   * until the startSetupPromise is completed
   *
   * @param serviceInfo the service's info used to connect to its websocket
   * @param handler     the handler for the incoming websocket text messages
   * @param startSetup  the future that should be fulfilled before starting the handler setup
   * @return a future which result reflects whether the connection has been successfully established
   */
  protected def wsConnect[H <: WsHandler[V]](serviceInfo: ServiceInfo, handler: H,
                                             startSetup: Future[Unit]): Future[Unit] = {
    WebSocketHelpers.wsConnect(httpClient, serviceInfo, res => {
      // for each ws connection it is necessary to wait on a different promise
      // since having multiple call of setHandler on the same future causes unpredictable behaviour:
      startSetup.onComplete(_ => handler.setup(res.result(), verticle, vertx, pendingWebSocketUpdates))
    })
  }

}
