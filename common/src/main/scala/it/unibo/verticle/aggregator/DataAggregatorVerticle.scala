package it.unibo.verticle.aggregator

import io.vertx.core.buffer.Buffer
import io.vertx.scala.core.Future
import io.vertx.scala.core.http.ServerWebSocket
import io.vertx.scala.ext.web.client.HttpResponse
import it.unibo.handlers.WsHandler
import it.unibo.handlers.assigner.HandlerAssigner
import it.unibo.jackson.MarshallableImplicit._
import it.unibo.models.CustomTypes.ServicesData
import it.unibo.models.service.ServiceInfo
import it.unibo.models.ws.messages.Entries
import it.unibo.verticle.{RegisterSourcesRetrieverVerticle, SourceVerticle}
import it.unibo.vertx.WebSocketHelpers.WebSocketExtensions

import scala.collection.mutable.ListBuffer

abstract class DataAggregatorVerticle[T, D, V <: SourceVerticle[D], W <: WsHandler[V]]
  extends RegisterSourcesRetrieverVerticle[D] {

  protected val subscribers: ListBuffer[ServerWebSocket] = ListBuffer()
  var wsConnectionUtility: WsConnectionUtility[V] = _
  val services: ListBuffer[ServicesData[W]] = ListBuffer()
  protected val serviceCondition: ServiceInfo => Boolean
  protected val handlerAssigner: HandlerAssigner[W]

  override protected def handleRegisterResponse(httpResponse: HttpResponse[Buffer]): Future[Unit] = {
    /* TODO: connection strategy should be refactored to allow different ways to
         retrieve data from other services (not only via websocket) */
    wsConnectionUtility = getWsConnectionUtility
    val serviceInfoList = httpResponse.bodyAsString().get.fromJson[List[ServiceInfo]]()
    serviceInfoList.filter(serviceCondition).foreach { serviceInfo =>
      additionalOperation(serviceInfo)
      handlerAssigner.assignHandler(services, serviceInfo)
    }
    wsConnectionUtility.connectAll(services)
  }

  /** Get the instance of the ws connection utility.
   *
   * @return the instance of the ws connection utility
   */
  protected def getWsConnectionUtility: WsConnectionUtility[V]

  /** Additional operations to perform before assigning the correct handler to a service.
   *
   * @param serviceInfo the service information
   */
  protected def additionalOperation(serviceInfo: ServiceInfo): Unit = {}

  /** Notifies the subscribers of data aggregator verticle with the specified data.
   *
   * @param dataToSend  the data to send
   * @param subscribers the subscribers to notify, by default all the subscribers will be notified
   */
  def notifyAggregatorSubscribers(dataToSend: Entries[T],
                                  subscribers: Iterable[ServerWebSocket] = subscribers): Unit = {
    dataToSend.printInfo()
    if (subscribers.nonEmpty && dataToSend.nonEmpty) {
      try {
        val jsonData = getResponse(dataToSend)
        subscribers.foreach(serverWebSocket => {
          dataToSend.printInfo("sending")
          serverWebSocket.writeTextMessageFramed(jsonData)
        })
      } catch {
        case e: OutOfMemoryError =>
          // jackson could not convert the data cause its too big, retry with half dataset
          e.printStackTrace()
          dataToSend.split().foreach(notifyAggregatorSubscribers(_))
        case e: Throwable =>
          dataToSend.printInfo("error in notify all")
          e.printStackTrace()
          throw e
      }
    }
  }

  /** Get the data to send in the data aggregator response format.
   *
   * @param dataToSend the data to send
   * @return the data to send in the data aggregator response format
   */
  protected def getResponse(dataToSend: Entries[T]): String

}
