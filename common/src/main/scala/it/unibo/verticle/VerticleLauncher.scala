package it.unibo.verticle

import io.vertx.lang.scala.ScalaVerticle
import io.vertx.scala.core.{DeploymentOptions, Vertx}

import scala.reflect.runtime.universe.TypeTag

trait VerticleLauncher {

  private val vertx = Vertx.vertx //VertxSingleton.getInstance

  /** Deploys a scala verticle.
   *
   * @param tag implicit type tag
   * @tparam T the verticle type
   */
  def deployVerticle[T <: ScalaVerticle]()(implicit tag: TypeTag[T]): Unit = {
    vertx.deployVerticle(ScalaVerticle.nameForVerticle[T])
  }

  /** Deploys a scala verticle with the specified deployment options.
   *
   * @param options the deployment options
   * @param tag     implicit type tag
   * @tparam T the verticle type
   */
  def deployVerticle[T <: ScalaVerticle](options: DeploymentOptions)(implicit tag: TypeTag[T]): Unit = {
    vertx.deployVerticle(ScalaVerticle.nameForVerticle[T], options)
  }

}
