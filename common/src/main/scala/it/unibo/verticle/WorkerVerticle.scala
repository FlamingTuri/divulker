package it.unibo.verticle

import io.vertx.lang.scala.ScalaVerticle
import io.vertx.scala.core.DeploymentOptions
import io.vertx.scala.core.eventbus.Message
import it.unibo.vertx.VertxExtensions.VertxExtension

class WorkerVerticle extends ScalaVerticle {

  override def start(): Unit = {
    vertx.getContextAndPrintInfo

    vertx.eventBus.consumer("sample.data", (message: Message[String]) => {
      System.out.println("[Worker] Consuming data in " + Thread.currentThread.getName)
      val body = message.body
      message.reply(body.toUpperCase)
    })
  }

}

object Main extends App with VerticleLauncher {
  var options = DeploymentOptions().setWorker(true)
  deployVerticle[WorkerVerticle](options)
}
