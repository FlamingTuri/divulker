package it.unibo.verticle

import io.vertx.core.buffer.Buffer
import io.vertx.scala.core.http.{HttpClient, HttpClientOptions}
import io.vertx.scala.core.{Future, Promise}
import io.vertx.scala.ext.web.client.HttpResponse
import it.unibo.network.RegistryUtils
import it.unibo.updates.PendingUpdate
import it.unibo.utils.Logger
import it.unibo.vertx.WebSocketHelpers

import scala.util.{Failure, Success}

abstract class RegisterSourcesRetrieverVerticle[D] extends SourceVerticle[D] {

  protected var httpClient: HttpClient = _
  protected val registerRequestUri: String
  val pendingUpdate = new PendingUpdate()

  override protected def setupOperations(): Future[Unit] = {
    val setupCompleted = Promise.promise[Unit]()
    val httpClientOptions = HttpClientOptions()
      .setMaxWebsocketMessageSize(WebSocketHelpers.MaxMessageSize)
    httpClient = vertx.createHttpClient(httpClientOptions)
    val registryConfig = RegistryUtils.registryConfigData
    val registryPort = registryConfig.port
    val registryHost = registryConfig.host
    webClient.get(registryPort, registryHost, registerRequestUri).sendFuture.onComplete {
      case Success(value) => handleRegisterResponse(value).onComplete(_ => setupCompleted.complete())
      case Failure(exception) =>
        Logger.printErr(exception)
        setupCompleted.fail(exception)
    }
    setupCompleted.future()
  }

  /** Handles the response received from the registry.
   *
   * @param httpResponse the registry response
   * @return a succeeded future representing when the response handling is completed
   */
  protected def handleRegisterResponse(httpResponse: HttpResponse[Buffer]): Future[Unit]

}
