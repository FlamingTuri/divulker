package it.unibo.data.subscriber.manager

import io.vertx.scala.core.http.ServerWebSocket

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

/** Interface representing the cve ids of a specific distro */
trait DistroRelatedCve {

  /** Returns the name of the distro
   *
   * @return the name of the distro
   */
  def distroName: String

  /** Adds the cve ids to the distro tracking list,
   * the duplicates entries will be ignored
   *
   * @param cveIdsToAdd the cve ids to add
   * @return the cve ids actually added
   */
  def addCveIds(cveIdsToAdd: Iterable[String]): Iterable[String]

  /** Removes the cve ids from the distro tracking list
   *
   * @param cveIdsToRemove the cve ids to remove
   * @return this
   */
  def removeCveIds(cveIdsToRemove: Iterable[String]): DistroRelatedCve

  /** Gets the cve ids of the distro
   *
   * @return the cve
   */
  def cveIds: Iterable[String]

  /** Gets the subscribers interested in receiving the updates of the cve ids
   *
   * @return the subscribers
   */
  def getSubscribers: mutable.Set[ServerWebSocket]

  /** Adds a new subscriber
   *
   * @param webSocket the server web socket of the subscriber to add
   * @return this
   */
  def addSubscriber(webSocket: ServerWebSocket): DistroRelatedCve

  /** Removes a subscriber
   *
   * @param webSocket the server web socket of the subscriber to remove
   * @return this
   */
  def removeSubscriber(webSocket: ServerWebSocket): DistroRelatedCve

}

object DistroRelatedCve {

  def apply(distroName: String,
            cveIdList: ListBuffer[String],
            webSocket: ServerWebSocket): DistroRelatedCve = {
    DistroRelatedCveImpl(distroName, cveIdList).addSubscriber(webSocket)
  }

  private case class DistroRelatedCveImpl(private val _distroName: String,
                                          private val _cveIds: ListBuffer[String]) extends DistroRelatedCve {

    private val subscribers = mutable.HashSet[ServerWebSocket]()

    override def distroName: String = _distroName

    override def addCveIds(cveIdsToAdd: Iterable[String]): Iterable[String] = {
      val notAlreadyPresent = cveIdsToAdd filterNot (cveIdToAdd => _cveIds contains cveIdToAdd)
      _cveIds ++= notAlreadyPresent
      notAlreadyPresent
    }

    override def removeCveIds(cveIdsToRemove: Iterable[String]): DistroRelatedCve = {
      _cveIds --= cveIdsToRemove
      this
    }

    override def cveIds: Iterable[String] = _cveIds

    override def addSubscriber(webSocket: ServerWebSocket): DistroRelatedCve = {
      subscribers += webSocket
      this
    }

    override def removeSubscriber(webSocket: ServerWebSocket): DistroRelatedCve = {
      subscribers -= webSocket
      this
    }

    override def getSubscribers: mutable.Set[ServerWebSocket] = subscribers
  }

}
