package it.unibo.data.subscriber.manager

import io.vertx.scala.core.http.ServerWebSocket
import it.unibo.constants.SourceVerticleInfo
import it.unibo.jackson.MarshallableImplicit._
import it.unibo.models.ws.messages.Entries
import it.unibo.models.ws.messages.distro.DistroSourceResponseMessage
import it.unibo.utils.Logger
import it.unibo.vertx.WebSocketHelpers.WebSocketExtensions

case class DistroSpecificDataSubscriberManager(private val Config: SourceVerticleInfo)
  extends DataSubscribersManager[ServerWebSocket] {

  /** Adds a new subscriber to the subscribers list and sends him the current source data
   *
   * @param subscriber the new subscriber
   * @param data       the data to send
   * @tparam T the data type
   */
  def subscribe[T](subscriber: ServerWebSocket, data: Iterable[T]): Unit = {
    subscribers += subscriber
    notifySubscribers(Entries.newEntries(data), subscriber)
  }

  override def unsubscribe(subscriber: ServerWebSocket): Unit = subscribers -= subscriber

  /** Notifies all the source subscribers with the specified data
   *
   * @param entries the data to send
   * @tparam T the data type
   */
  def notifySubscribers[T](entries: Entries[T]): Unit = {
    notifySubscribers(entries, subscribers: _*)
  }

  /** Notifies the subscribers with the specified data
   *
   * @param entries     the data to send
   * @param subscribers the subscribers to notify
   * @tparam T the data type
   */
  def notifySubscribers[T](entries: Entries[T],
                           subscribers: ServerWebSocket*): Unit = {
    if (subscribers.nonEmpty && entries.nonEmpty) {
      val serviceName = Config.serviceName
      val serviceKind = Config.serviceKind
      val response = DistroSourceResponseMessage(serviceName, serviceKind, entries)
      response.tryToJson { response =>
        response.data.split().map(DistroSourceResponseMessage(serviceName, serviceKind, _))
        //val groupSize = math.ceil(data.size.toDouble / 5).toInt
        //response.data.grouped(groupSize)
        //  .map(DistroSourceResponseMessage(serviceName, serviceKind, _)).to[Array]
      }.foreach { jsonResponse =>
        subscribers.foreach(serverWebSocket => {
          entries.printInfo("sending")
          serverWebSocket.writeTextMessageFramed(jsonResponse)
        })
      }
    }
  }

}
