package it.unibo.data.subscriber.manager

import io.vertx.scala.core.http.ServerWebSocket

import scala.collection.mutable.ListBuffer


abstract class DataSubscribersManager[S] {

  val subscribers: ListBuffer[S] = ListBuffer[S]()

  /** Removes the specified subscriber
   *
   * @param subscriber the subscriber to remove
   */
  def unsubscribe(subscriber: ServerWebSocket): Unit
}
