package it.unibo.data.subscriber.manager

import io.vertx.scala.core.http.ServerWebSocket
import it.unibo.models.CustomTypes.YearMaskVulnerabilities
import it.unibo.models.GeneralCve
import it.unibo.utils.CollectionUtils._
import it.unibo.utils.{CveUtils, Logger}

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

class GeneralDataSubscriberManager[T <: GeneralCve](data: YearMaskVulnerabilities[T])
  extends DataSubscribersManager[DistroRelatedCve] {

  /** Registers a new subscriber to track the specified CVE IDs of a distro.
   * If the distro was not already present a new entry will be created with all the specified CVE IDs,
   * If the distro already exists the CVE IDs will be added to the existing ones.
   *
   * @param distroName the distro name to which the CVE IDs are related to
   * @param subscriber the subscriber endpoint
   * @param cveIds     the CVE IDs to add to the tracking
   * @return a pair with the first parameter containing all the distro related CVE and
   *         in the other the new CVE IDs added
   */
  def subscribe(distroName: String, subscriber: ServerWebSocket,
                cveIds: Iterable[String]): (DistroRelatedCve, Iterable[String]) = {
    subscribers.find(_.distroName == distroName) match {
      case Some(value) =>
        if (!value.getSubscribers.contains(subscriber)) {
          value.addSubscriber(subscriber)
        }
        val newCveIds = value.addCveIds(cveIds)
        (value, newCveIds)
      case None =>
        val newDistroRelatedCve = DistroRelatedCve(distroName, ListBuffer(), subscriber)
        subscribers += newDistroRelatedCve
        val newCveIds = newDistroRelatedCve.addCveIds(cveIds)
        (newDistroRelatedCve, newCveIds)
    }
  }

  override def unsubscribe(serverWebSocket: ServerWebSocket): Unit = {
    subscribers.filter(_.getSubscribers.contains(serverWebSocket))
      .foreach(_.removeSubscriber(serverWebSocket))
  }

  /** Removes the subscriber from tracking the CVE IDs of the specified distro.
   *
   * @param distroName      the distro name
   * @param serverWebSocket the subscriber endpoint
   */
  def unsubscribe(distroName: String, serverWebSocket: ServerWebSocket): Unit = {
    subscribers.find(_.distroName == distroName) match {
      case Some(value) => value match {
        case value if value.getSubscribers.size == 1 => subscribers -= value
        case _ => value.removeSubscriber(serverWebSocket)
      }
      case None => Logger.printErr(s"distro name: $distroName not found")
    }
  }

  /** Add to the specified distro new CVE IDs to track.
   *
   * @param distroName    the distro name
   * @param cveIdsToTrack the new CVE IDs to track
   * @return a pair with the first parameter containing all the distro related CVE and
   *         in the other the new CVE IDs added
   */
  def addToTrackList(distroName: String,
                     cveIdsToTrack: Iterable[String]): Option[(DistroRelatedCve, Iterable[String])] = {
    subscribers.find(_.distroName == distroName) match {
      case Some(value) => Some((value, value.addCveIds(cveIdsToTrack)))
      case None =>
        Logger.printErr(
          s"""distro name: $distroName not found
             |available names $subscribers""".stripMargin)
        None
    }
  }

  /** Removes from the specified distro the CVE IDs.
   *
   * @param distroName      the distro name
   * @param cveIdsToUntrack the CVE IDs to untrack
   */
  def removeFromTrackList(distroName: String, cveIdsToUntrack: Iterable[String]): Unit = {
    subscribers.find(_.distroName == distroName) match {
      case Some(value) => value.removeCveIds(cveIdsToUntrack)
      case None => Logger.printErr(s"distro name: $distroName not found")
    }
  }

  /** Get the data related to the specified distro.
   *
   * @param distroName the distro name
   * @return the data stored into an iterable
   */
  def getDistroCveData(distroName: String): Iterable[T] = {
    subscribers.find(_.distroName == distroName) match {
      case Some(value) => getDistroCveData(value)
      case None => Iterable()
    }
  }

  /** Get the data related to the specified distro.
   *
   * @param distroRelatedCve the distro related cve
   * @return the data stored into an iterable
   */
  def getDistroCveData(distroRelatedCve: DistroRelatedCve): Iterable[T] = {
    getCveIdsRelatedData(distroRelatedCve.cveIds)
  }

  /** Gets the data corresponding to the requested CVE IDs.
   *
   * @param cveIds the requested CVE IDs
   * @return the data stored into an iterable
   */
  def getCveIdsRelatedData(cveIds: Iterable[String]): Iterable[T] = {
    for {
      cveId <- cveIds
      numericPortionMap <- data.get(CveUtils.getCveIdYear(cveId))
      cveIdNumericPortion = CveUtils.getCveIdNumericPortion(cveId)
      cveList <- numericPortionMap.get(CveUtils.maskNumericPortion(cveIdNumericPortion))
      cve <- cveList.find(e => e.cveId == cveId)
    } yield cve
  }

  /** Get the subscribers related to the specified distro name.
   *
   * @param distroName the distro name
   * @return its subscribers
   */
  def getDistroSubscribers(distroName: String): mutable.Set[ServerWebSocket] = {
    subscribers.find(p => p.distroName == distroName) match {
      case Some(value) => value.getSubscribers
      case None => mutable.Set[ServerWebSocket]()
    }
  }

  def groupUpdatesBySubscribers(updatedData: Iterable[T]): Iterable[(mutable.Set[ServerWebSocket], Iterable[T], String)] = {
    // TODO: improvement: if the same websocket is used to track multiple distro cve,
    //  it will receive common cve data multiple times
    subscribers.map { s =>
      (s.getSubscribers, updatedData.filter(e => s.cveIds.contains(e.cveId)), s.distroName)
    }
  }

}
