package it.unibo.models.cpe

import it.unibo.parsers.Parser

object Cpe22 {

  private val cpe22Regex = "cpe:/([aho]):(.+?):(.+?):.+".r

  def affectedProduct(cpe22: String): String = {
    cpe22 match {
      case cpe22Regex(_, _, product) => product
      case value => Parser.throwParseError(value)
    }
  }

}
