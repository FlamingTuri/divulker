package it.unibo.models.cpe

import it.unibo.parsers.Parser

object Cpe23 {

  // cpe:2.3:Part:Vendor:Product:Version:Sw_ed:Tgt_sw:Tgt_hw:Other
  // Part can be "a" -> application, "o" -> operating system, "h" -> hardware
  //private val cpe23Regex = "cpe:2\\.3:(\\D):(.+):(.+):(.+):(.+):(.+):(.+):(.+):(.+):(.+):(.+)".r
  private val cpe23Regex = "cpe:2\\.3:([aho]):(.+?):(.+?):.+".r

  def affectedProduct(cpe23: String): String = {
    cpe23 match {
      case cpe23Regex(_, _, product) => product
      case value => Parser.throwParseError(value)
    }
  }

}
