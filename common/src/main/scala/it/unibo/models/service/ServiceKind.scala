package it.unibo.models.service

import com.fasterxml.jackson.core.`type`.TypeReference


object ServiceKind extends Enumeration {
  type ServiceKind = Value
  val GeneralRetriever, DistroSpecificRetriever, DistroAggregator, GeneralAggregator, DistroComparator = Value
}

class ServiceKindType extends TypeReference[ServiceKind.type]
