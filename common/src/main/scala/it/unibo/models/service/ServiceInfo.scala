package it.unibo.models.service

import com.fasterxml.jackson.module.scala.JsonScalaEnumeration

case class ServiceInfo(@JsonScalaEnumeration(classOf[ServiceNameType])
                       name: ServiceName.Value,
                       @JsonScalaEnumeration(classOf[ServiceKindType])
                       serviceType: ServiceKind.Value,
                       host: String,
                       port: Int,
                       wsPath: String)
