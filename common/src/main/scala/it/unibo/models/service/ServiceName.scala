package it.unibo.models.service

import com.fasterxml.jackson.core.`type`.TypeReference


object ServiceName extends Enumeration {
  type ServiceName = Value
  // TODO: with generics it could be avoided to have all services names
  //  in the same enumeration, the problem is how to configure the jackson parser
  //  since generics are not allowed in @JsonScalaEnumeration(classOf[T])
  val DistroDataComparatorService,
  // General retrievers
  MitreService, NvdService,
  // Debian retrievers
  DsaService, DlaService, DebianCveService, DebianSnapshotInfoService,
  // RedHat retrievers
  RedHatRpmToCveService, RedHatCveDatesService, RhsaReleaseDates,
  // Aggregators
  GeneralDataAggregator, DebianDataAggregator, RedHatDataAggregator = Value

  def withNameOpt(string: String): Option[Value] = values.find(_.toString == string)
}

class ServiceNameType extends TypeReference[ServiceName.type]
