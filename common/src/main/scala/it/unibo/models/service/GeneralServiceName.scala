package it.unibo.models.service

import com.fasterxml.jackson.core.`type`.TypeReference

object GeneralServiceName extends Enumeration {
  type GeneralServiceName = Value
  val NvdService, MitreService = Value
}

class GeneralServiceNameType extends TypeReference[GeneralServiceName.type]
