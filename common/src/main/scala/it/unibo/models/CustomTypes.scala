package it.unibo.models

import it.unibo.handlers.WsHandler
import it.unibo.models.aggregator.{GatheredDataTemplate, SourceTemplate}
import it.unibo.models.service.ServiceInfo
import it.unibo.verticle.SourceVerticle

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

object CustomTypes {

  type GatheredDataType = GatheredDataTemplate[_ <: SourceTemplate]

  type ServicesData[W <: WsHandler[_ <: SourceVerticle[_]]] = (ServiceInfo, W)

  type MaskVulnerabilities[T] = mutable.ListMap[String, ListBuffer[T]]

  object MaskVulnerabilities {

    def apply[T](): MaskVulnerabilities[T] = mutable.ListMap()

  }

  type YearMaskVulnerabilities[T] = mutable.SortedMap[Int, MaskVulnerabilities[T]]

  object YearMaskVulnerabilities {

    def apply[T](): YearMaskVulnerabilities[T] = mutable.SortedMap()

  }

}
