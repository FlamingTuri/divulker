package it.unibo.models

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.{DeserializationContext, JsonDeserializer}

trait TraitEnum {
  /** The value assumed when converting the case object to json,
   * remember to add @JsonValue annotation in the extending case objects
   */
  val jsonValue: String
}

/** Class to speedup the setup of trait enum deserializers
 *
 * @tparam T type of the trait enum
 */
abstract class TraitEnumDeserializer[T <: TraitEnum] extends JsonDeserializer[T] {

  protected val caseObjects: List[T]

  override def deserialize(p: JsonParser, ctxt: DeserializationContext): T = {
    val stringValue = p.getValueAsString
    caseObjects.find(_.jsonValue == stringValue) match {
      case Some(value) => value
      case None => throw new IllegalArgumentException(s"Undefined deserializer for value: $stringValue")
    }
  }

}
