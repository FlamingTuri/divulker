package it.unibo.models

class KeyComparator[T <: Key] extends Ordering[T] {
  override def compare(x: T, y: T): Int = x.getKey compare y.getKey
}
