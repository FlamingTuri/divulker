package it.unibo.models.general.aggregator

import com.fasterxml.jackson.annotation.{JsonGetter, JsonProperty}
import com.fasterxml.jackson.module.scala.JsonScalaEnumeration
import it.unibo.models.aggregator.SourceTemplate
import it.unibo.models.service.{ServiceName, ServiceNameType}

import scala.collection.mutable

case class SourceWithServices(override val name: String,
                              @JsonProperty(value = "date", required = true)
                              private var _date: String,
                              @JsonScalaEnumeration(classOf[ServiceNameType])
                              servicesWithSource: mutable.Set[ServiceName.Value])
  extends SourceTemplate(name, _date)
