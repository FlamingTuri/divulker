package it.unibo.models.general.aggregator

import com.fasterxml.jackson.module.scala.JsonScalaEnumeration
import it.unibo.models.service.{ServiceName, ServiceNameType}
import it.unibo.models.ws.messages.Entries

import scala.collection.mutable

case class GeneralAggregatorResponse(@JsonScalaEnumeration(classOf[ServiceNameType])
                                     aggregatorServiceName: ServiceName.Value,
                                     @JsonScalaEnumeration(classOf[ServiceNameType])
                                     distros: mutable.Set[ServiceName.Value],
                                     data: Entries[AggregatedVulnerabilityData]) {

}
