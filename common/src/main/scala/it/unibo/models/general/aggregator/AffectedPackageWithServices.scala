package it.unibo.models.general.aggregator

import com.fasterxml.jackson.module.scala.JsonScalaEnumeration
import it.unibo.models.Key
import it.unibo.models.service.{ServiceName, ServiceNameType}

import scala.collection.mutable

case class AffectedPackageWithServices(affectedPackage: String,
                                       @JsonScalaEnumeration(classOf[ServiceNameType])
                                       servicesWithAffectedPackages: mutable.Set[ServiceName.Value]) extends Key {

  override def getKey: String = affectedPackage

}
