package it.unibo.models.ws.messages.operations

import it.unibo.models.TraitEnumDeserializer

class OperationDeserializer extends TraitEnumDeserializer[Operation] {

  override protected val caseObjects: List[Operation] = List(Subscribe, UnSubscribe)

}
