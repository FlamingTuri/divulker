package it.unibo.models.ws.messages.comparator

import com.fasterxml.jackson.annotation.{JsonGetter, JsonProperty}
import com.fasterxml.jackson.module.scala.JsonScalaEnumeration
import it.unibo.models.aggregator.GatheredData
import it.unibo.models.service.{ServiceName, ServiceNameType}
import it.unibo.models.ws.messages.Entries

case class DataComparatorResponseMessage(@JsonProperty("aggregatorServiceName")
                                         @JsonScalaEnumeration(classOf[ServiceNameType])
                                         private val _aggregatorServiceName: ServiceName.Value,
                                         @JsonProperty("data")
                                         private val _data: Entries[GatheredData]) {

  @JsonGetter
  def aggregatorServiceName: ServiceName.Value = _aggregatorServiceName

  @JsonGetter
  def data: Entries[GatheredData] = _data

}

