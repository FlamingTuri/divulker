package it.unibo.models.ws.messages

import com.fasterxml.jackson.annotation.{JsonGetter, JsonProperty}
import it.unibo.models.service.ServiceKind

class WsResponseMessage[N, T](@JsonProperty(value = "dataSourceName", required = true)
                              private val _dataSourceName: N,
                              @JsonProperty(value = "serviceKind", required = true)
                              private val _serviceKind: ServiceKind.Value,
                              @JsonProperty(value = "data", required = true)
                              private val _data: T) {

  @JsonGetter
  def dataSourceName: N = _dataSourceName

  @JsonGetter
  def serviceKind: ServiceKind.Value = _serviceKind

  @JsonGetter
  def data: T = _data

}
