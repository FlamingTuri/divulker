package it.unibo.models.ws.messages.general

import com.fasterxml.jackson.core.`type`.TypeReference


object GeneralSourceOperation extends Enumeration {
  type GeneralSourceOperation = Value
  val Subscribe, Unsubscribe, AddCve, RemoveCve = Value
}

class GeneralSourceOperationType extends TypeReference[GeneralSourceOperation.type]
