package it.unibo.models.ws.messages.distro

import com.fasterxml.jackson.core.`type`.TypeReference

object DistroSourceOperation extends Enumeration {
  type DistroSourceOperation = Value
  val Subscribe, Unsubscribe = Value
}

class DistroSourceOperationType extends TypeReference[DistroSourceOperation.type]
