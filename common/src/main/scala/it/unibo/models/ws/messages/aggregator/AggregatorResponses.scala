package it.unibo.models.ws.messages.aggregator

import com.fasterxml.jackson.annotation.{JsonGetter, JsonProperty}
import com.fasterxml.jackson.module.scala.JsonScalaEnumeration
import it.unibo.models.CustomTypes.GatheredDataType
import it.unibo.models.aggregator.GatheredData
import it.unibo.models.service.{ServiceName, ServiceNameType}
import it.unibo.models.ws.messages.Entries

object AggregatorResponses {

  def apply(aggregatorServiceName: ServiceName.Value,
            data: Entries[GatheredData]): AggregatorResponseMessage = {
    new AggregatorResponseMessage(aggregatorServiceName, data)
  }

  def apply[D <: GatheredDataType](aggregatorServiceName: ServiceName.Value,
                                   data: Entries[D]): AggregatorResponseMessageTemplate[D] = {
    new AggregatorResponseMessageTemplate[D](aggregatorServiceName, data)
  }

  class AggregatorResponseMessageTemplate[D <: GatheredDataType](@JsonProperty("aggregatorServiceName")
                                                                 @JsonScalaEnumeration(classOf[ServiceNameType])
                                                                 private val _aggregatorServiceName: ServiceName.Value,
                                                                 @JsonProperty("data")
                                                                 private val _data: Entries[D]) {

    @JsonGetter
    def aggregatorServiceName: ServiceName.Value = _aggregatorServiceName

    @JsonGetter
    def data: Entries[D] = _data
  }

  class AggregatorResponseMessage(@JsonProperty("aggregatorServiceName")
                                  @JsonScalaEnumeration(classOf[ServiceNameType])
                                  private val _aggregatorServiceName: ServiceName.Value,
                                  @JsonProperty("data")
                                  private val _data: Entries[GatheredData])
    extends AggregatorResponseMessageTemplate[GatheredData](_aggregatorServiceName, _data) {

  }

}
