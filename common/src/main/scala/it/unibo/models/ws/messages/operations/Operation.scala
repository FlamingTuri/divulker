package it.unibo.models.ws.messages.operations

import com.fasterxml.jackson.annotation.JsonValue
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import it.unibo.models.TraitEnum

@JsonDeserialize(using = classOf[OperationDeserializer])
sealed trait Operation extends TraitEnum

case object Subscribe extends Operation {
  @JsonValue override val jsonValue: String = "Subscribe"
}

case object UnSubscribe extends Operation {
  @JsonValue override val jsonValue: String = "UnSubscribe"
}

@JsonDeserialize(using = classOf[GeneralSourceOperationDeserializer])
sealed trait GeneralSourceOperation extends Operation

case object AddCve extends GeneralSourceOperation {
  @JsonValue override val jsonValue: String = "AddCve"
}

case object RemoveCve extends GeneralSourceOperation {
  @JsonValue override val jsonValue: String = "RemoveCve"
}
