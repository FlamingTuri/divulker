package it.unibo.models.ws.messages.comparator

import com.fasterxml.jackson.core.`type`.TypeReference

object DataComparatorOperation extends Enumeration {
  type DataComparatorOperation = Value
  val Subscribe, Unsubscribe = Value
}

class DataComparatorOperationType extends TypeReference[DataComparatorOperation.type]
