package it.unibo.models.ws.messages.aggregator

import com.fasterxml.jackson.core.`type`.TypeReference

object AggregatorOperation extends Enumeration {
  type AggregatorOperation = Value
  val Subscribe, Unsubscribe = Value
}

class AggregatorOperationType extends TypeReference[AggregatorOperation.type]
