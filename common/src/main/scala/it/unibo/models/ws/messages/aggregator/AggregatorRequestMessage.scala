package it.unibo.models.ws.messages.aggregator

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.module.scala.JsonScalaEnumeration
import it.unibo.models.ws.messages.WsRequestMessage
import it.unibo.models.ws.messages.operations.Operation

case class AggregatorRequestMessage(@JsonProperty(value = "operation", required = true)
                                    @JsonScalaEnumeration(classOf[AggregatorOperationType])
                                    private val _operation: Operation)
  extends WsRequestMessage[Operation](_operation)
