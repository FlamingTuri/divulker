package it.unibo.models.ws.messages.general

import com.fasterxml.jackson.annotation.{JsonGetter, JsonProperty}
import com.fasterxml.jackson.module.scala.JsonScalaEnumeration
import it.unibo.models.GeneralCve
import it.unibo.models.service.{ServiceKind, ServiceKindType}
import it.unibo.models.ws.messages.{Entries, WsResponseMessage}

case class GeneralSourceResponseMessage[N, C <: GeneralCve](@JsonProperty(value = "dataSourceName", required = true)
                                                            private val _dataSourceName: N,
                                                            @JsonProperty(value = "serviceKind", required = true)
                                                            @JsonScalaEnumeration(classOf[ServiceKindType])
                                                            private val _serviceKind: ServiceKind.Value,
                                                            @JsonProperty(value = "distroName", required = true)
                                                            private val _distroName: String,
                                                            @JsonProperty(value = "data", required = true)
                                                            private val _data: Entries[C])
  extends WsResponseMessage[N, Entries[C]](_dataSourceName, _serviceKind, _data) {

  @JsonGetter
  def distroName: String = _distroName

}
