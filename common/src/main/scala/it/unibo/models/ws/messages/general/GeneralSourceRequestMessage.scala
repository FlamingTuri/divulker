package it.unibo.models.ws.messages.general

import com.fasterxml.jackson.annotation.{JsonGetter, JsonProperty}
import com.fasterxml.jackson.module.scala.JsonScalaEnumeration
import it.unibo.models.ws.messages.WsRequestMessage
import it.unibo.models.ws.messages.operations.GeneralSourceOperation

import scala.collection.mutable.ListBuffer


case class GeneralSourceRequestMessage[
  O >: GeneralSourceOperation
](@JsonProperty(value = "operation", required = true)
  private val _operation: O,
  @JsonProperty(value = "distroName", required = true)
  private val _distroName: String,
  @JsonProperty("cveIds")
  private val _cveIds: ListBuffer[String] = ListBuffer()
 ) extends WsRequestMessage[O](_operation) {

  @JsonGetter()
  def distroName: String = _distroName

  @JsonGetter()
  def cveIds: ListBuffer[String] = _cveIds

}
