package it.unibo.models.ws.messages.comparator

import com.fasterxml.jackson.annotation.{JsonGetter, JsonProperty}
import com.fasterxml.jackson.module.scala.JsonScalaEnumeration
import it.unibo.models.service.ServiceName
import it.unibo.models.ws.messages.WsRequestMessage
import it.unibo.models.ws.messages.operations.Operation

case class DataComparatorRequestMessage(@JsonProperty(value = "operation", required = true)
                                        @JsonScalaEnumeration(classOf[DataComparatorOperationType])
                                        private val _operation: Operation,
                                        @JsonProperty("subscriptions")
                                        private val _subscriptions: Iterable[ServiceName.Value] = Iterable())
  extends WsRequestMessage[Operation](_operation) {

  @JsonGetter
  def subscriptions: Iterable[ServiceName.Value] = _subscriptions

}
