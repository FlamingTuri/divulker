package it.unibo.models.ws.messages

import com.fasterxml.jackson.annotation.{JsonGetter, JsonProperty}

class WsRequestMessage[O](@JsonProperty(value = "operation", required = true)
                          private val _operation: O) {

  @JsonGetter()
  def operation: O = _operation
}
