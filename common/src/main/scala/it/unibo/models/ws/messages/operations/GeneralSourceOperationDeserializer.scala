package it.unibo.models.ws.messages.operations

import it.unibo.models.TraitEnumDeserializer

class GeneralSourceOperationDeserializer extends TraitEnumDeserializer[Operation] {

  override protected val caseObjects: List[Operation] = {
    List(Subscribe, UnSubscribe, AddCve, RemoveCve)
  }

}
