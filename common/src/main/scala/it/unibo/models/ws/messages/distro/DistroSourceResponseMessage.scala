package it.unibo.models.ws.messages.distro

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.module.scala.JsonScalaEnumeration
import it.unibo.models.service.{ServiceKind, ServiceKindType, ServiceName, ServiceNameType}
import it.unibo.models.ws.messages.{Entries, WsResponseMessage}

case class DistroSourceResponseMessage[T](@JsonProperty(value = "dataSourceName", required = true)
                                          @JsonScalaEnumeration(classOf[ServiceNameType])
                                          private val _dataSourceName: ServiceName.Value,
                                          @JsonProperty(value = "serviceKind", required = true)
                                          @JsonScalaEnumeration(classOf[ServiceKindType])
                                          private val _serviceKind: ServiceKind.Value,
                                          @JsonProperty(value = "data", required = true)
                                          private val _data: Entries[T])
  extends WsResponseMessage[ServiceName.Value, Entries[T]](_dataSourceName, _serviceKind, _data) {

}
