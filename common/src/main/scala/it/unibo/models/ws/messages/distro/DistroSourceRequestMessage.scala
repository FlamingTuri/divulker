package it.unibo.models.ws.messages.distro

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.module.scala.JsonScalaEnumeration
import it.unibo.models.ws.messages.WsRequestMessage
import it.unibo.models.ws.messages.operations.Operation

case class DistroSourceRequestMessage(@JsonProperty(value = "operation", required = true)
                                      private val _operation: Operation)
  extends WsRequestMessage[Operation](_operation) {

}
