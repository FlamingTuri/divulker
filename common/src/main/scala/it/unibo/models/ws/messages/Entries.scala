package it.unibo.models.ws.messages

import com.fasterxml.jackson.annotation.{JsonGetter, JsonProperty, JsonSetter}
import it.unibo.models.ws.messages.Entries.EntriesImpl
import it.unibo.updates.operation.result.UpdateResult
import it.unibo.utils.Logger

class Entries[T]() {

  @JsonProperty("newEntries")
  protected var _newEntries: Iterable[T] = Iterable()

  @JsonProperty("updatedEntries")
  protected var _updatedEntries: Iterable[T] = Iterable()

  @JsonProperty("deletedEntries")
  protected var _deletedEntries: Iterable[T] = Iterable()

  @JsonGetter
  def newEntries: Iterable[T] = _newEntries

  @JsonGetter
  def updatedEntries: Iterable[T] = _updatedEntries

  @JsonGetter
  def deletedEntries: Iterable[T] = _deletedEntries

  /** Checks whether at least one among newEntries, updatedEntries and deletedEntries is not empty.
   *
   * @return whether at least one among newEntries, updatedEntries and deletedEntries is not empty
   */
  def nonEmpty: Boolean = newEntries.nonEmpty || updatedEntries.nonEmpty || deletedEntries.nonEmpty

  /** Logging utility
   *
   * @param message the message to print before the logging
   */
  def printInfo(message: String = ""): Unit = {
    val newCount = newEntries.size
    val updatedCount = updatedEntries.size
    val deletedCount = deletedEntries.size
    Logger.printOut(s"$message new: $newCount updated: $updatedCount deleted: $deletedCount")
  }

  /** Split the data contained in this object over multiple ones.
   *
   * @param groups the number of elements over which the data will be spread
   * @return an iterable containing the data of this object spread over its elements
   */
  def split(groups: Int = 3): Iterable[Entries[T]] = {
    val entriesList = (1 to groups).map(_ => EntriesImpl[T]()).toList
    splitIterables(entriesList, groups, newEntries, _.setNewEntries)
    splitIterables(entriesList, groups, updatedEntries, _.setUpdatedEntries)
    splitIterables(entriesList, groups, deletedEntries, _.setDeletedEntries)
    entriesList
  }

  private def splitIterables(data: List[EntriesImpl[T]], groups: Int, values: Iterable[T],
                             assign: EntriesImpl[T] => Iterable[T] => Unit): Unit = {
    // divides the data in at most n groups
    val groupSize = math.ceil(values.size.toDouble / groups).toInt
    newEntries.grouped(groupSize).zipWithIndex.foreach { case (x, idx) => assign(data(idx))(x) }
  }

}

object Entries {

  def apply[T](updateResult: UpdateResult[T]): Entries[T] = {
    EntriesImpl[T]().setNewEntries(updateResult.newEntries).setUpdatedEntries(updateResult.updatedEntries)
  }

  def newEntries[T](newEntries: Iterable[T]): Entries[T] = {
    EntriesImpl[T]().setNewEntries(newEntries)
  }

  def updatedEntries[T](updatedEntries: Iterable[T]): Entries[T] = {
    EntriesImpl[T]().setUpdatedEntries(updatedEntries)
  }

  case class EntriesImpl[T]() extends Entries[T] {

    def setNewEntries(newEntries: Iterable[T]): EntriesImpl[T] = {
      _newEntries = newEntries
      this
    }

    @JsonSetter
    def setUpdatedEntries(updatedEntries: Iterable[T]): EntriesImpl[T] = {
      _updatedEntries = updatedEntries
      this
    }

    @JsonSetter
    def setDeletedEntries(deletedEntries: Iterable[T]): EntriesImpl[T] = {
      _deletedEntries = deletedEntries
      this
    }

  }

}
