package it.unibo.models

import com.fasterxml.jackson.annotation.JsonIgnore

trait Key {

  /** Gets the object identifier */
  @JsonIgnore
  def getKey: String
}
