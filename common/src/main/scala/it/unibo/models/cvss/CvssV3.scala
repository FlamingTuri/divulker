package it.unibo.models.cvss

case class CvssV3(version: String = "NONE",
                  vectorString: String = "NONE",
                  attackVector: AttackVector,
                  attackComplexity: String = "NONE",
                  privilegesRequired: String = "NONE",
                  userInteraction: String = "NONE",
                  scope: String = "NONE",
                  confidentialityImpact: String = "NONE",
                  availabilityImpact: String = "NONE",
                  baseScore: Double = 0,
                  baseSeverity: String = "NONE") {

}
