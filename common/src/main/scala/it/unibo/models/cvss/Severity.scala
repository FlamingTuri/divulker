package it.unibo.models.cvss

import com.fasterxml.jackson.annotation.JsonValue
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import it.unibo.models.{TraitEnum, TraitEnumDeserializer}

@JsonDeserialize(using = classOf[SeverityDeserializer])
sealed trait Severity extends TraitEnum

case object Low extends Severity {
  @JsonValue override val jsonValue: String = "LOW"
}

case object Medium extends Severity {
  @JsonValue override val jsonValue: String = "MEDIUM"
}

case object High extends Severity {
  @JsonValue override val jsonValue: String = "HIGH"
}

class SeverityDeserializer extends TraitEnumDeserializer[Severity] {

  override protected val caseObjects: List[Severity] = List(Low, Medium, High)

}
