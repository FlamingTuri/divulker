package it.unibo.models.cvss

case class BaseMetricV2(cvssV2: CvssV2,
                        severity: Severity,
                        exploitabilityScore: Double,
                        impactScore: Double,
                        obtainAllPrivilege: Boolean,
                        obtainUserPrivilege: Boolean,
                        obtainOtherPrivilege: Boolean,
                        userInteractionRequired: Boolean)
