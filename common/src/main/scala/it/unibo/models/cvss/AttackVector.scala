package it.unibo.models.cvss

import com.fasterxml.jackson.annotation.JsonValue
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import it.unibo.models.{TraitEnum, TraitEnumDeserializer}

@JsonDeserialize(using = classOf[AttackVectorDeserializer])
sealed trait AttackVector extends TraitEnum

case object Network extends AttackVector {
  @JsonValue override val jsonValue: String = "NETWORK"
}

case object Adjacent extends AttackVector {
  @JsonValue override val jsonValue: String = "ADJACENT"
}

case object Local extends AttackVector {
  @JsonValue override val jsonValue: String = "LOCAL"
}

case object Physical extends AttackVector {
  @JsonValue override val jsonValue: String = "PHYSICAL"
}

case object AdjacentNetwork extends AttackVector {
  @JsonValue override val jsonValue: String = "ADJACENT_NETWORK"
}

class AttackVectorDeserializer extends TraitEnumDeserializer[AttackVector] {

  override protected val caseObjects: List[AttackVector] = {
    List(Network, Adjacent, AdjacentNetwork, Local, Physical)
  }

}
