package it.unibo.models.cvss

case class BaseMetricV3(cvssV3: CvssV3,
                        exploitabilityScore: Double,
                        impactScore: Double)
