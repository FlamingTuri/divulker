package it.unibo.models.cvss

/** The vulnerability impact metric of the NVD.
 *
 * @param baseMetricV2 metric using CVSS v2
 * @param baseMetricV3 metric using CVSS v3
 */
case class Impact(baseMetricV2: Option[BaseMetricV2] = None,
                  baseMetricV3: Option[BaseMetricV3] = None)
