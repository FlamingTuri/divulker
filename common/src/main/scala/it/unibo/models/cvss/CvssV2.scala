package it.unibo.models.cvss

case class CvssV2(version: String = "NONE",
                  vectorString: String = "NONE",
                  accessVector: String = "NONE",
                  accessComplexity: String = "NONE",
                  authentication: String = "NONE",
                  confidentialityImpact: String = "NONE",
                  integrityImpact: String = "NONE",
                  availabilityImpact: String = "NONE",
                  baseScore: Double = 0)
