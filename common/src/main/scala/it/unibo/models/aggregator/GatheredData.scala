package it.unibo.models.aggregator

import com.fasterxml.jackson.annotation.JsonProperty

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

case class GatheredData(@JsonProperty(value = "cveId", required = true)
                        private val _cveId: String,
                        @JsonProperty(value = "disclosures", required = true)
                        private val _disclosures: ListBuffer[Source] = ListBuffer(),
                        @JsonProperty(value = "fixes", required = true)
                        private val _fixes: mutable.ListMap[String, ListBuffer[Source]] = mutable.ListMap())
  extends GatheredDataTemplate[Source](_cveId, _disclosures, _fixes) {

}
