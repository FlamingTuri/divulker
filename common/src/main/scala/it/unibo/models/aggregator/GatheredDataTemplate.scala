package it.unibo.models.aggregator

import com.fasterxml.jackson.annotation.{JsonGetter, JsonIgnore, JsonProperty}
import it.unibo.models.Key

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

class GatheredDataTemplate[S <: SourceTemplate](@JsonProperty(value = "cveId", required = true)
                                                private val _cveId: String,
                                                @JsonProperty(value = "disclosures", required = true)
                                                private val _disclosures: ListBuffer[S],
                                                @JsonProperty(value = "fixes", required = true)
                                                private val _fixes: mutable.ListMap[String, ListBuffer[S]] = mutable.ListMap[String, ListBuffer[S]]())
  extends Key {

  /** Gets CVE ID of this vulnerability.
   *
   * @return the CVE ID
   */
  @JsonGetter
  def cveId: String = _cveId

  /** Gets the disclosures of this vulnerability.
   *
   * @return the disclosures
   */
  @JsonGetter
  def disclosures: ListBuffer[S] = _disclosures

  /** Gets the vulnerability fixes stored as a map where the key is the package name,
   * the value is a list of sources which contain the fix release date.
   *
   * @return the fixes
   */
  @JsonGetter
  def fixes: mutable.ListMap[String, ListBuffer[S]] = _fixes

  /** Get the affected packages names.
   *
   * @return the affected packages names
   */
  @JsonIgnore
  def affectedPackages: collection.Set[String] = fixes.keySet

  override def getKey: String = cveId
}
