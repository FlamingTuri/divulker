package it.unibo.models.aggregator

import com.fasterxml.jackson.annotation.JsonProperty


case class Source(@JsonProperty(value = "name", required = true)
                  private val _name: String,
                  @JsonProperty(value = "date", required = true)
                  protected var _date: String) extends SourceTemplate(_name, _date)
