package it.unibo.models.aggregator

import com.fasterxml.jackson.annotation.{JsonGetter, JsonProperty, JsonSetter}

/** Template used to create a new disclosure/fix source.
 *
 * @param _name the source name
 * @param _date the source release date
 */
class SourceTemplate(@JsonProperty(value = "name", required = true)
                     private val _name: String,
                     @JsonProperty(value = "date", required = true)
                     private var _date: String) {

  /** Get the source name.
   *
   * @return the source name
   */
  @JsonGetter
  def name: String = _name

  /** Get the source date.
   *
   * @return the source date
   */
  @JsonGetter
  def date: String = _date

  /** Sets the source date.
   *
   * @param date the new source date
   */
  @JsonSetter
  def date_=(date: String): Unit = _date = date

}
