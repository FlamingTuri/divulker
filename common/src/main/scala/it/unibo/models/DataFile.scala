package it.unibo.models

class DataFile(private val extension: String,
               private val startName: String,
               private val endName: String = "") {

  def getName[T](identifier: T): String = s"$startName$identifier$endName"

  def getFile[T](identifier: T): String = s"${getName(identifier)}$extension"

}
