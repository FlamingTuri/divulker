package it.unibo.models

import com.fasterxml.jackson.annotation.{JsonGetter, JsonIgnore, JsonSetter}

abstract class GeneralCve extends Key {

  @JsonIgnore
  protected var _cveId: String

  @JsonGetter()
  def cveId: String = _cveId

  @JsonGetter()
  def publishedDate: String

  @JsonSetter("publishedDate")
  def publishedDate_=(publishedDate: String): Unit

  override def getKey: String = cveId
}
