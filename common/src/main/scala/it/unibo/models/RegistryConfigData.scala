package it.unibo.models

import com.fasterxml.jackson.annotation.JsonProperty

case class RegistryConfigData(@JsonProperty(required = true)
                              host: String,
                              @JsonProperty(required = true)
                              port: Int) {

}
