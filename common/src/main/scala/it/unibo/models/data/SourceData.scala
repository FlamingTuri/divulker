package it.unibo.models.data

trait SourceData[T] {

  /** Get the map as an [[Iterable]] of [[T]]
   *
   * @return the map as an [[Iterable]] of [[T]]
   */
  def getAsIterable: Iterable[T]

}
