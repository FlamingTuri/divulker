package it.unibo.network

import java.net.{DatagramSocket, InetAddress, NetworkInterface}

import scala.collection.JavaConverters._

/** Class necessary since InetAddress.getLocalHost.getHostAddress
 * on linux systems could return 127.0.0.1 and not the local ip address
 */
object NetworkUtils {

  private val _hostAddresses = getHostAddresses
  private val _hostAddress = getHostAddress

  private def getHostAddresses: List[String] = {
    val networkInterfaces = NetworkInterface.getNetworkInterfaces
    networkInterfaces.asScala
      // remove loopback interfaces
      .filterNot(networkInterface => networkInterface.isLoopback)
      .flatMap(networkInterface => networkInterface.getInetAddresses.asScala)
      // if every condition is false
      .filterNot { inetAddress =>
        inetAddress.isAnyLocalAddress || inetAddress.isLoopbackAddress ||
          inetAddress.isMulticastAddress || inetAddress.isLinkLocalAddress ||
          inetAddress.isMCGlobal || inetAddress.isMCLinkLocal ||
          inetAddress.isMCNodeLocal || inetAddress.isMCOrgLocal ||
          inetAddress.isMCSiteLocal
      }.map(inetAddress => inetAddress.getHostAddress)
      .toList
  }

  private def getHostAddress: String = {
    val socket = new DatagramSocket()
    socket.connect(InetAddress.getByName("8.8.8.8"), 80)
    val hostAddress = socket.getLocalAddress.getHostAddress
    socket.close()
    // on mac os hostAddress will be 0.0.0.0
    if (hostAddress == "0.0.0.0" || hostAddress == "127.0.0.1") {
      InetAddress.getLocalHost.getHostAddress
    } else {
      hostAddress
    }
  }

  /** Gets the system's host addresses, localhost excluded
   *
   * @return the system's host addresses
   */
  def hostAddresses: List[String] = _hostAddresses

  /** Gets the system's default host address
   *
   * @return the system's default host address
   */
  def hostAddress: String = _hostAddress

}
