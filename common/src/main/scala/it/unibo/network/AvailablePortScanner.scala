package it.unibo.network

import java.io.IOException
import java.net.ServerSocket

import it.unibo.utils.Logger

/** Class used to check for system's port availability */
class AvailablePortScanner {

  private final val MinPortNumber = 2
  private final val MaxPortNumber = 65535

  /** Checks if the specified port is between the range 2 - 65535 (extremes included)
   *
   * @param port the port to check
   */
  private def isPortValid(port: Int): Unit = {
    if (port < MinPortNumber || port > MaxPortNumber) {
      throw new IllegalArgumentException("Invalid port number: " + port)
    }
  }

  /** Creates a new server socket
   *
   * @param port      the port where should be created, if it is 0 it will be randomly chosen between the available ones
   * @param onSuccess the callback to call after the deploy is succeeded
   * @param onFailure the callback to call after the deploy is failed
   * @tparam T the type returned by the callbacks
   * @return the value returned by the callbacks
   */
  private def createServerSocket[T](port: Int, onSuccess: ServerSocket => T, onFailure: () => T): T = {
    var serverSocket: ServerSocket = null
    try {
      serverSocket = new ServerSocket(port)
      serverSocket.setReuseAddress(true)
      onSuccess(serverSocket)
    } catch {
      case e: IOException =>
        /* failed opening the socket */
        onFailure()
    }
  }

  /** Closes the server socket connection
   *
   * @param serverSocket the server socket to close
   */
  private def close(serverSocket: ServerSocket): Unit = {
    if (serverSocket != null) {
      try {
        serverSocket.close()
      } catch {
        case exception: IOException => Logger.printErr(s"IOException on close() $exception")
      }
    }
  }

  /** Finds a random free port
   *
   * @return the free port number
   */
  def findFreePort: Int = createServerSocket(0, serverSocket => {
    val freePort = serverSocket.getLocalPort
    close(serverSocket)
    freePort
  }, () => throw new IllegalStateException("Could not find a free TCP/IP port"))

  /** Finds a free port starting from the specified one
   *
   * @param startingPort the first port used when starting the availability check
   * @return the first free port number found
   */
  def findFreePort(startingPort: Int): Int = {
    var port = startingPort
    while (!isPortAvailable(port) && port <= MaxPortNumber) {
      port += 1
    }
    isPortValid(port)
    port
  }

  /** Checks if the specified port is available
   *
   * @param port the port to check
   * @return true if the port is available, false otherwise
   */
  def isPortAvailable(port: Int): Boolean = {
    isPortValid(port)
    createServerSocket(port, (serverSocket: ServerSocket) => {
      close(serverSocket)
      true
    }, () => false)
  }

}
