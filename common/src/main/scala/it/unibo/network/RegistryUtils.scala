package it.unibo.network

import io.vertx.core.buffer.Buffer
import io.vertx.scala.core.file.FileSystem
import io.vertx.scala.core.http.HttpServer
import io.vertx.scala.core.{Future, Promise}
import io.vertx.scala.ext.web.client.WebClient
import it.unibo.constants.{Constants, RegistryPaths}
import it.unibo.jackson.MarshallableImplicit._
import it.unibo.models.RegistryConfigData
import it.unibo.models.service.{ServiceInfo, ServiceKind, ServiceName}
import it.unibo.utils.Resources
import it.unibo.vertx.FileSystemUtils
import it.unibo.vertx.VertxHelpers._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}

object RegistryUtils {

  private var registryConfig: RegistryConfigData = _
  val registryLocationConfigName: String = "registryLocationConfig.json"

  /** Reads registry host and port from resources
   *
   * @return the registry config
   */
  def registryConfigData: RegistryConfigData = {
    val stringResource = Resources.readAsString(registryLocationConfigName)
    stringResource.fromJson[RegistryConfigData]()
  }

  def readRegistryConfig(fs: FileSystem): Future[RegistryConfigData] = {
    val fileSystemUtils = FileSystemUtils(fs)
    val configPath = Constants.pathInsideSaveDirectory(registryLocationConfigName).toString
    if (fileSystemUtils.checkFilesExistence(configPath)) {
      fileSystemUtils.loadFile(configPath)
        .compose(h => h.toString().fromJson[RegistryConfigData]())
    } else {
      val config = registryConfigData
      fileSystemUtils.saveData(configPath, Buffer.buffer(config.toPrettyJson))
      Future.succeededFuture(config)
    }
  }

  def getRegistryConfig(fs: FileSystem): Future[Unit] = {
    if (registryConfig == null) {
      readRegistryConfig(fs).compose { rc =>
        registryConfig = rc
        Future.succeededFuture()
      }
    } else {
      Future.succeededFuture()
    }
  }

  /** Subscribes the service to the registry, sending its name, type and web socket path
   *
   * @param fs            used to read registry config
   * @param webClient     the client used to perform the http request
   * @param httpServer    the service's http server
   * @param sourceName    the name of the service
   * @param serviceType   the type of service
   * @param websocketPath the websocket path used by the service to receive requests
   * @return a future representing the subscription result
   */
  def subscribe(fs: FileSystem, webClient: WebClient, httpServer: HttpServer,
                sourceName: ServiceName.Value, serviceType: ServiceKind.Value,
                websocketPath: String): Future[Unit] = {
    val promise = Promise.promise[Unit]()
    val port = httpServer.actualPort
    // retrieve service host address
    val hostAddress = NetworkUtils.hostAddress
    val sourceData = ServiceInfo(sourceName, serviceType, hostAddress, port, websocketPath)
    getRegistryConfig(fs).compose { _ =>
      val registryPort = registryConfig.port
      val registryHost = registryConfig.host
      webClient.post(registryPort, registryHost, "/registry/sources")
        .sendJsonFuture(Option(Buffer.buffer(sourceData.toJson)))
        .onComplete {
          case Success(_) => promise.complete()
          case Failure(exception) =>
            // TODO: uncomment later, for now I don't want to flood the terminal
            //  with errors if registry is not up
            //Logger.printErr(exception)
            promise.fail(exception)
        }
      promise.future()
    }
  }

  def checkIfRegistryIsUp(webClient: WebClient): Future[Boolean] = {
    val promise = Promise.promise[Boolean]()
    val registryPort = registryConfig.port
    val registryHost = registryConfig.host
    webClient.get(registryPort, registryHost, RegistryPaths.SourcesPath)
      .sendFuture()
      .onComplete {
        case Success(_) => promise.complete(true)
        case Failure(_) => promise.complete(false)
      }
    promise.future()
  }

}
