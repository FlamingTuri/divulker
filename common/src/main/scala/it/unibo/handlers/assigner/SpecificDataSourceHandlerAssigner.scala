package it.unibo.handlers.assigner

import it.unibo.handlers.SpecificRetrieverWsHandler
import it.unibo.models.CustomTypes.GatheredDataType

/** Utility class used to assign the correct websocket text message handler
 * to a distro specific source.
 *
 * @tparam D the type of the data stored in the verticle
 */
abstract class SpecificDataSourceHandlerAssigner[D <: GatheredDataType]
  extends HandlerAssigner[SpecificRetrieverWsHandler[D]]
