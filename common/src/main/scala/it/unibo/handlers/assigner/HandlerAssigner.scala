package it.unibo.handlers.assigner

import it.unibo.handlers.WsHandler
import it.unibo.models.CustomTypes.ServicesData
import it.unibo.models.service.{ServiceInfo, ServiceName}
import it.unibo.verticle.SourceVerticle

import scala.collection.mutable.ListBuffer

/** Utility class used to assign the correct websocket text message handler to a source.
 *
 * @tparam W the web socket handler type
 */
abstract class HandlerAssigner[W <: WsHandler[_ <: SourceVerticle[_]]] {

  /** Assigns the handler specified by the matcher to the list
   *
   * @param list        the list that tracks the verticle's services info and their associated handlers
   * @param serviceInfo the current service info
   */
  def assignHandler(list: ListBuffer[ServicesData[W]],
                    serviceInfo: ServiceInfo): Unit = {
    val handler = serviceMatcher(serviceInfo.name)
    list += ((serviceInfo, handler))
  }

  /** Strategy used to get the correct web socket message handler for the specified service
   *
   * @param serviceName the service name used to determine which handler is able to manage the incoming data
   * @return the websocket message handler
   */
  protected def serviceMatcher(serviceName: ServiceName.Value): W

}
