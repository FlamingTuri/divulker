package it.unibo.handlers

import it.unibo.models.service.ServiceName

/** Trait that stores information about the distro name. */
trait DistroNameInfo {

  /** Gets the distro name.
   *
   * @return the distro name
   */
  protected def distroName: ServiceName.Value

}
