package it.unibo.handlers

import it.unibo.models.CustomTypes.GatheredDataType
import it.unibo.models.ws.messages.distro.DistroSourceRequestMessage
import it.unibo.models.ws.messages.operations.Subscribe

/** Trait that should be implemented by distro specific data sources
 * to handle websocket text messages
 *
 * @tparam D the type of the verticle's data
 */
trait SpecificDataSourceWsHandler[D <: GatheredDataType] extends SpecificRetrieverWsHandler[D]
  with DistroNameInfo {

  override protected val wsRequestMessage: DistroSourceRequestMessage = {
    DistroSourceRequestMessage(Subscribe)
  }

}
