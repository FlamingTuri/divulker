package it.unibo.handlers

import it.unibo.models.CustomTypes.GatheredDataType
import it.unibo.models.ws.messages.Entries
import it.unibo.verticle.aggregator.SpecificDataAggregatorVerticle

/** Trait that should be implemented by distro specific data aggregator sources
 * to handle websocket text messages
 *
 * @tparam D the type of the verticle's data
 */
trait SpecificRetrieverWsHandler[D <: GatheredDataType]
  extends NotifySubscribersWsHandler[D, SpecificDataAggregatorVerticle[D]] {

  override def notifySubscribers(entries: Entries[D]): Unit = {
    if (entries.nonEmpty) {
      verticle.notifyAggregatorSubscribers(entries)
    }
  }

}
