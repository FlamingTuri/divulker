package it.unibo.handlers

import it.unibo.models.ws.messages.Entries
import it.unibo.verticle.SourceVerticle

trait NotifySubscribersWsHandler[T, V <: SourceVerticle[_]] extends WsHandler[V] {

  /** Notifies the subscribers with the result of the parsing of received data
   *
   * @param entries the entries to send
   */
  def notifySubscribers(entries: Entries[T]): Unit

}
