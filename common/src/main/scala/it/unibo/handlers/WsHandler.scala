package it.unibo.handlers

import java.util.concurrent.TimeUnit

import io.vertx.scala.core.Vertx
import io.vertx.scala.core.http.WebSocket
import it.unibo.jackson.MarshallableImplicit._
import it.unibo.models.service.ServiceName
import it.unibo.models.ws.messages.WsRequestMessage
import it.unibo.updates.PendingUpdate
import it.unibo.utils.Logger
import it.unibo.verticle.SourceVerticle
import it.unibo.vertx.VertxHelpers._
import it.unibo.vertx.WebSocketHelpers._

/** Interface used to setup message handling logic from data sent by different sources
 *
 * @tparam V the verticle type
 */
trait WsHandler[V <: SourceVerticle[_]] {

  //protected val vertx: Vertx = VertxSingleton.getInstance
  protected val wsRequestMessage: WsRequestMessage[_]
  protected var webSocket: WebSocket = _
  protected var verticle: V = _
  protected val sourceName: ServiceName.Value

  protected implicit def enumToString(value: ServiceName.Value): String = value.toString

  /** Setups the text message handler and subscribes for service updates
   *
   * @param webSocket      the other websocket endpoint
   * @param verticle       the verticle reference
   * @param vertx          a vertx instance
   * @param pendingUpdates the queue of updates that are waiting to be processed
   */
  def setup(webSocket: WebSocket, verticle: V, vertx: Vertx, pendingUpdates: PendingUpdate): Unit = {
    this.webSocket = webSocket
    this.verticle = verticle
    Logger.printOut(sourceName)
    webSocket.textMessageHandler { text =>
      // TODO: setting a 5 minutes estimated execution time is a temporary solution
      //  should be replaced with worker verticles or vertx.runOnContext()
      val executor = vertx.createSharedWorkerExecutor(sourceName, 4, 5, TimeUnit.MINUTES)
      pendingUpdates.add(sourceName).compose { _ =>
        executor.executeBlocking { () =>
          try {
            textMessageHandler(webSocket, text)
          } catch {
            case e: Throwable =>
              Logger.printErr(s"$sourceName $e")
              e.printStackTrace()
          }
        }
      }.onComplete { _ =>
        executor.close
        pendingUpdates.remove()
      }
    }
    webSocket.defaultExceptionHandler()
    webSocket.defaultCloseHandler()
    // subscribe for updates
    sendTextMessage(wsRequestMessage.toJson)
  }

  /** Handles the text messages sent by the other websocket endpoint
   *
   * @param webSocket the other websocket endpoint
   * @param text      the received text
   */
  protected def textMessageHandler(webSocket: WebSocket, text: String): Unit

  /** Sends a text message to the other websocket endpoint
   *
   * @param text the text to send
   */
  def sendTextMessage(text: String): Unit = webSocket.writeTextMessageFramed(text)

}
