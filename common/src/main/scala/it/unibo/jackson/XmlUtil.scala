package it.unibo.jackson

import com.fasterxml.jackson.databind.{DeserializationFeature, ObjectMapper}
import com.fasterxml.jackson.dataformat.xml.XmlMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule

import scala.reflect.ClassTag
import scala.xml.Elem

/** Utilities used to setup jackson xml mapper */
object XmlUtil {

  /** Xml mapper instance */
  val xmlMapper: ObjectMapper = new XmlMapper()
    .registerModule(DefaultScalaModule)
    .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)

  /** Implicit used to convert a scala xml element to string
   *
   * @param xml a scala xml element
   * @return the string representation of the scala xml element
   */
  implicit def xmlElemToString(xml: Elem): String = xml.toString

  /** Converts the xml string to a scala object
   *
   * @param xml the xml string to convert
   * @param tag the class tag used for the conversion
   * @tparam T the type of the scala object
   * @return the scala object resulting from the conversion
   */
  def fromXml[T: ClassTag](xml: String)(implicit tag: ClassTag[T]): T = {
    xmlMapper.readValue(xml, tag.runtimeClass.asInstanceOf[Class[T]])
  }

}
