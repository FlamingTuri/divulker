package it.unibo.jackson

import com.fasterxml.jackson.databind.{DeserializationFeature, ObjectMapper}
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper

/** Utilities used to setup jackson json mapper */
object JsonUtil {

  /** Json mapper instance */
  val jsonMapper = new ObjectMapper() with ScalaObjectMapper
  jsonMapper.registerModule(DefaultScalaModule)
    .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

  /** Converts the specified value to json string
   *
   * @param value the value to convert
   * @return the value converted to json format
   */
  def toJson(value: Map[Symbol, Any]): String = toJson(value map { case (k, v) => k.name -> v })

  /** Converts the specified value to json string
   *
   * @param value the value to convert
   * @return the value converted to json format
   */
  def toJson(value: Any): String = jsonMapper.writeValueAsString(value)

  /** Converts the specified value to json string using the default pretty printer
   *
   * @param value the value to convert
   * @return the value converted to json format using the default pretty printer
   */
  def toPrettyJson(value: Any): String = {
    jsonMapper.writerWithDefaultPrettyPrinter().writeValueAsString(value)
  }

  /** Converts the json string to map
   *
   * @param json the json string to convert
   * @tparam V the type of the map values
   * @return the map resulting from the conversion
   */
  def toMap[V: Manifest](json: String): Map[String, V] = fromJson[Map[String, V]](json)

  /** Converts the json string to a scala object
   *
   * @param json the json string to convert
   * @tparam T the type of the scala object
   * @return the scala object resulting from the conversion
   */
  def fromJson[T: Manifest](json: String): T = jsonMapper.readValue[T](json)

}
