package it.unibo.jackson

/** Implicit used to marshall/unmarshall scala objects */
object MarshallableImplicit {

  /** Implicit class with string parsing utilities
   *
   * @param unMarshallMe the string to parse
   */
  implicit class Unmarshallable(unMarshallMe: String) {

    /** Parses the string as map
     *
     * @tparam V the type of the map values
     * @return the parsed map
     */
    def toMapOf[V: Manifest](): Map[String, V] = JsonUtil.toMap[V](unMarshallMe)

    /** Parses the json string as the specified object type
     *
     * @tparam T the object type
     * @return the json parsing result
     */
    def fromJson[T: Manifest](): T = JsonUtil.fromJson[T](unMarshallMe)

    /** Parses the xml string as the specified object type
     *
     * @tparam T the object type
     * @return the xml parsing result
     */
    def fromXml[T: Manifest](): T = XmlUtil.fromXml[T](unMarshallMe)
  }

  /** Implicit class used to parse the string version of an object
   *
   * @param unMarshallMe the object which string version should be parsed
   * @tparam U the object type
   */
  implicit class GenericUnmarshallable[U](unMarshallMe: U)
    extends Unmarshallable(unMarshallMe.toString) {
  }

  /** Implicit class used to stringify an object
   *
   * @param marshallMe the object to json stringify
   * @tparam T the object type
   */
  implicit class Marshallable[T](marshallMe: T) {

    /** Converts the object to json string
     *
     * @return the object's json representation
     */
    def toJson: String = JsonUtil.toJson(marshallMe)

    /** Converts the object to json string
     *
     * @return the object's pretty json representation
     */
    def toPrettyJson: String = JsonUtil.toPrettyJson(marshallMe)

    /** Converts the object to json string and inserts it into a json array
     *
     * @return the object's json representation inserted into a json array
     */
    def toJsonArray: String = s"[$toJson]"

    /** Attempts to converts the object to json string.
     * If the object conversion fails due to its size, it will be split in multiple ones.
     *
     * @param splittingStrategy the strategy used to split the object in multiple ones
     * @return the object json representation, eventually split over multiple strings
     */
    def tryToJson(splittingStrategy: T => Iterable[T]): Iterable[String] = {
      try {
        Array(toJson)
      } catch {
        case e: OutOfMemoryError =>
          // jackson could not convert the data cause its too big, retry with half dataset
          e.printStackTrace()
          splittingStrategy(marshallMe).flatMap(_.tryToJson(splittingStrategy))
        case e: Throwable =>
          e.printStackTrace()
          throw e
      }
    }

  }

}
