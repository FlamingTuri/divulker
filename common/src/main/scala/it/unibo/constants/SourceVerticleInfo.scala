package it.unibo.constants

import it.unibo.models.service.{ServiceKind, ServiceName}

trait SourceVerticleInfo extends Constants {
  /** The service name */
  val serviceName: ServiceName.Value
  /** The service kind */
  val serviceKind: ServiceKind.Value
  /** The path accepted for websocket messages */
  val WebSocketPath: String
}
