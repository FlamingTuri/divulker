package it.unibo.constants

object DistroDataComparatorPaths {
  // TODO: I'm not happy with the route configuration, it should be rethought from scratch

  val BasePath = "/distro-data-comparator"
  val DistrosPath = s"$BasePath/distros"
  val DistroVulnerabilityWindowPath = s"$BasePath/:distro/vulnerability-window"
  val VulnerabilityWindowPath = s"$BasePath/vulnerability-window-by-year"
  val DistroPackagesVulnerabilityWindowPath = s"$BasePath/:distro/packages-vulnerability-window-by-year"

  val PackagesOccurrences = s"$BasePath/packages-occurrences"
  val PackagesOccurrencesInYear = s"$BasePath/packages-occurrences-in-year"
  val PackagesOccurrencesInMonth = s"$BasePath/packages-occurrences-in-month"
  val PackagesOccurrencesInWeek = s"$BasePath/packages-occurrences-in-week"
  val DistrosOccurrences = s"$BasePath/distros-occurrences"
  val DistrosOccurrencesInYear = s"$BasePath/distros-occurrences-in-year"
  val DistrosOccurrencesInMonth = s"$BasePath/distros-occurrences-in-month"
  val DistrosOccurrencesInWeek = s"$BasePath/distros-occurrences-in-week"

  val DistrosCveAverageVulnerabilityWindow = s"$BasePath/distros/cve/average-vulnerability-window"
  val DistrosCveCount = s"$BasePath/distros/cve/count"
  val DistrosDisclosuresOccurrences = s"$BasePath/distros/disclosures/occurrences"
  val DistrosFixesOccurrences = s"$BasePath/distros/fixes/occurrences"
  val PackagesCveAverageVulnerabilityWindow = s"$BasePath/packages/cve/average-vulnerability-window"
  val PackagesCveCount = s"$BasePath/packages/cve/count"
  val PackagesDisclosuresOccurrences = s"$BasePath/packages/disclosures/occurrences"
  val PackagesFixesOccurrences = s"$BasePath/packages/fixes/occurrences"

  val DistroOccurrencesInYearByFourMonths = s"$BasePath/distro-occurrences-in-year-by-four-months"
  val TopDistroPackages = s"$BasePath/:distro/packages/top-vulnerable/:count"
  val DistroWoVInYearRange = s"$BasePath/:distro/wov-in-disclosure-year-range"
  val DistroNumberByYear = s"$BasePath/:distro/disclosures-number-by-year"
  val DistroNumberByYearSum = s"$BasePath/:distro/disclosures-number-by-year-sum"
}
