package it.unibo.constants

object RegistryPaths {

  val BasePath = "/registry"
  val SourcesPath = s"$BasePath/sources"
}
