package it.unibo.constants

abstract class DataRetrieverConfig extends SourceVerticleInfo {
  /** The site hosting the data to download */
  val DataHost: String
  /** Name of the directory where the files will be saved */
  protected val SourceSpecificSaveDirName: String

  /** Source specific save directory */
  protected def SourceSpecificSaveDir: String = pathInsideSaveDirectory(SourceSpecificSaveDirName)

}
