package it.unibo.constants

import java.nio.file.{Path, Paths}

trait Constants {

  /** User home folder */
  private final val UserHomeFolder = System.getProperty("user.home")
  /** Name of the project directory save folder */
  private final val ProjectSaveDirectoryName = ".divulker"
  /** Project directory save folder. Everything should be saved inside it. */
  private final val ProjectSaveDirectory: Path = Paths.get(UserHomeFolder, ProjectSaveDirectoryName)

  /** Combines the project save folder with the specified relative path
   *
   * @param relativePath the relative path to combine
   * @return the resulting absolute path
   */
  def pathInsideSaveDirectory(relativePath: String): Path = ProjectSaveDirectory.resolve(relativePath)

  /** Converts a path to string
   *
   * @param path the path
   * @return the path to string
   */
  implicit def pathToString(path: Path): String = path.toString
}

object Constants extends Constants
