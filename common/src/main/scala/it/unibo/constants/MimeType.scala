package it.unibo.constants

/** Mime types used in http requests/responses */
object MimeType {

  /** application/json mime type */
  val ApplicationJson = "application/json"

  /** text/plain mime type */
  val TextPlain = "text/plain"
}
