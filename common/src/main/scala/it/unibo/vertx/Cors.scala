package it.unibo.vertx

import io.vertx.core.http.HttpMethod
import io.vertx.scala.ext.web.handler.CorsHandler

import scala.collection.mutable

/** Utility to setup a CORS handler. */
object Cors {

  // TODO: configure actual origin address url
  private val allowedOriginPattern = "https?://localhost:\\d{1,5}"
  private val allowedHeaders = mutable.Set[String](
    "Access-Control-Allow-Origin",
    //"Content-Type"
  )
  private val allowedMethods = mutable.Set[HttpMethod](
    HttpMethod.GET,
    HttpMethod.POST
  )

  /** Creates a new CorsHandler. It should be added to router.routes()
   * after the body handler and before the routes declarations.
   *
   * @return the cors handler with the specified headers and methods
   */
  def corsHandler: CorsHandler = {
    CorsHandler.create(allowedOriginPattern)
      .allowedHeaders(allowedHeaders)
      .allowedMethods(allowedMethods)
  }

}
