package it.unibo.vertx

import io.vertx.scala.core.{Future, Promise}
import io.vertx.scala.ext.web.client.HttpResponse

import scala.concurrent.ExecutionContext.Implicits.global
import scala.reflect.runtime.universe.TypeTag
import scala.util.{Failure, Success}

object HttpResponseFix {

  /** Transforms a scala HttpResponse[U] future to a vertx one.
   * Avoids java.lang.NoSuchMethodException:
   * io.vertx.scala.ext.web.client.HttpResponse$.apply(io.vertx.ext.web.client.HttpResponse)
   *
   * @param scalaFuture the scala future to convert
   * @param tag         implicit TypeTag used for type [[T]]
   * @tparam T the scala future type
   * @return the equivalent vertx future
   */
  implicit def fixHttpResponseFuture[T](scalaFuture: scala.concurrent.Future[HttpResponse[T]])
                                       (implicit tag: TypeTag[T]): Future[HttpResponseFix[T]] = {
    val promise = Promise.promise[HttpResponseFix[T]]()
    scalaFuture.onComplete {
      case Success(value) => promise.complete(new HttpResponseFix[T](value))
      case Failure(exception) => promise.fail(exception)
    }
    promise.future()
  }

  /** Class used to prevent vertx throwing exception upon receiving an http response.
   *
   * @param _httpResponse the [[HttpResponse]] instance
   * @tparam T the type of the [[HttpResponse]]
   */
  protected class HttpResponseFix[T](private val _httpResponse: HttpResponse[T]) {

    /** Gets the http response value.
     *
     * @return the http response value
     */
    def httpResponse: HttpResponse[T] = _httpResponse

  }

}
