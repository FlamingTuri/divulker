package it.unibo.vertx

import io.vertx.scala.core.{CompositeFuture, Future, Promise}
import it.unibo.vertx.VertxHelpers._

import scala.collection.mutable
import scala.reflect.runtime.universe.TypeTag

object CompositeFutureHelpers {

  /** Transforms a composite future to vertx Future.
   *
   * @param compositeFuture the composite future
   * @return a future with the same result as the composite future
   */
  implicit def toFuture(compositeFuture: CompositeFuture): Future[Unit] = {
    val promise = Promise.promise[Unit]()
    compositeFuture.compose(_ => promise.complete()).otherwise(e => promise.fail(e))
    promise.future()
  }

  /** Utility class used as a shortcut to create a composite future all
   * from a generic future iterable.
   *
   * @param futures the futures
   * @param tag     implicit TypeTag used for type [[T]]
   * @tparam T the type of the futures
   */
  implicit class CompositeFutureFix[T](futures: Iterable[Future[T]])(implicit tag: TypeTag[T]) {

    /** Return a composite future, succeeded when all futures are succeeded,
     * failed when any future is failed.
     *
     * @return the composite future
     */
    def onAll(): CompositeFuture = {
      val tmp = futures.map(futureToPromise).to[mutable.Buffer]
      CompositeFuture.all(tmp.map(_.future()))
    }
  }

}
