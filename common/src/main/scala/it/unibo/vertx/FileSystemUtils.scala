package it.unibo.vertx

import io.vertx.core.buffer.Buffer
import io.vertx.scala.core.Future
import io.vertx.scala.core.file.FileSystem
import it.unibo.utils.Logger
import it.unibo.vertx.VertxHelpers._

import scala.reflect.io.Path

case class FileSystemUtils(fs: FileSystem) {

  /** Checks whether the specified paths exists.
   *
   * @param paths the path to check
   * @return true if all the paths exists
   */
  def checkFilesExistence(paths: String*): Boolean = paths.forall(fs.existsBlocking)

  /** Reads a file content.
   *
   * @param filePath the path of the file
   * @return a future with the file content
   */
  def loadFile(filePath: String): Future[Buffer] = {
    Logger.printOut(s"loading data from file $filePath")
    // TODO: missing failure handling
    fs.existsFuture(filePath).compose(_ => fs.readFileFuture(filePath))
  }

  /** Saves the specified buffer to the specified path.
   *
   * @param filePath the path where the buffer data will be saved
   * @param buffer   the buffer storing the data to save
   * @return a future that will be completed when the save process is ended
   */
  def saveData(filePath: String, buffer: Buffer): Future[Unit] = {
    Logger.printOut(s"save data $filePath")
    val parentDirectory = Path(filePath).parent.toString
    fs.mkdirsFuture(parentDirectory).compose(_ => fs.writeFileFuture(filePath, buffer))
  }

}
