package it.unibo.vertx

import io.vertx.scala.core.Vertx

/** Singleton that returns a vertx instance. */
object VertxSingleton {

  private val vertx: Vertx = Vertx.vertx

  /** Gets the vertx instance.
   *
   * @return the vertx instance
   */
  def getInstance: Vertx = vertx
}
