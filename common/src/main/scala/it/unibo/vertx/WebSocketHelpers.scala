package it.unibo.vertx

import io.vertx.core.AsyncResult
import io.vertx.core.buffer.Buffer
import io.vertx.scala.core.http.{HttpClient, WebSocket, WebSocketBase}
import io.vertx.scala.core.{Future, Promise, Vertx}
import it.unibo.jackson.MarshallableImplicit._
import it.unibo.models.service.ServiceInfo
import it.unibo.utils.Logger

/** WebSocket utilities */
object WebSocketHelpers {

  //private val vertx = VertxSingleton.getInstance
  val MaxMessageSize: Int = Int.MaxValue
  val Threshold: Int = 4096

  /** Attempts to establish a websocket connection.
   *
   * @param httpClient            the httpClient used to perform the connection
   * @param serviceInfo           the info used to setup the websocket connection
   * @param onConnectionSucceeded the function to run when the connection is successfully established
   * @return a future which result reflects whether the connection has been successfully established
   */
  def wsConnect(httpClient: HttpClient, serviceInfo: ServiceInfo,
                onConnectionSucceeded: AsyncResult[WebSocket] => Unit): Future[Unit] = {
    wsConnect(httpClient, serviceInfo.port, serviceInfo.host, serviceInfo.wsPath, onConnectionSucceeded)
  }

  /** Attempts to establish a websocket connection.
   *
   * @param httpClient            the httpClient used to perform the connection
   * @param port                  the port of the host
   * @param host                  the address of the host
   * @param wsPath                the websocket path
   * @param onConnectionSucceeded the function to run when the connection is successfully established
   * @return a future which result reflects whether the connection has been successfully established
   */
  def wsConnect(httpClient: HttpClient, port: Int, host: String, wsPath: String,
                onConnectionSucceeded: AsyncResult[WebSocket] => Unit): Future[Unit] = {
    val connectionPromise = Promise.promise[Unit]()
    httpClient.webSocket(port, host, wsPath, { res: AsyncResult[WebSocket] =>
      if (res.succeeded) {
        Logger.printOut(s"Successfully connected to $host:$port$wsPath")
        connectionPromise.complete()
        onConnectionSucceeded(res)
      } else {
        connectionPromise.fail(res.cause)
        Logger.printErr(res.cause)
      }
    })
    connectionPromise.future()
  }

  /** Max message size represented as in vertx message buffer overflow error.
   *
   * @return the max message size
   */
  def vertxMaxMessageSize: Int = {
    // double conversion is necessary or the integer will overflow
    ((MaxMessageSize.toDouble + 1) / math.pow(2, 13)).toInt
  }

  /** Same as [[vertxMaxMessageSize]] but with a threshold applied,
   * to avoid max message size overflow error.
   *
   * @return the max message size
   */
  def thresholdedVertxMaxMessageSize: Int = vertxMaxMessageSize - Threshold

  /** Split the given iterable into multiple groups if its json representation
   * exceeds the receiver max message size.
   *
   * @param iterable the iterable to spilt
   * @tparam T the iterable type
   * @return an iterator containing the split iterable
   */
  def groupByWebsocketSize[T](iterable: Iterable[T]): Iterable[Iterable[T]] = {
    try {
      groupByWebsocketSize(iterable, 0)
    } catch {
      case e: OutOfMemoryError =>
        // hoping this never happens
        Logger.printErr(s"out of memory with size ${iterable.size}")
        // jackson could not convert the data cause its too big, retry with half dataset
        iterable.grouped(math.ceil(iterable.size.toDouble / 3).toInt).toList
          .flatMap(groupByWebsocketSize[T])
      case e: Exception =>
        Logger.printErr(s"error in groupByWebsocketSize with size ${iterable.size}")
        throw new Exception(e)
    }
  }

  /** Split the given iterable into multiple groups if its json representation
   * exceeds the receiver max message size.
   *
   * @param iterable     the iterable to spilt
   * @param tuningFactor value used to reduce the number of elements per group
   * @tparam T the iterable type
   * @return the splitting result
   */
  @scala.annotation.tailrec
  protected def groupByWebsocketSize[T](iterable: Iterable[T],
                                        tuningFactor: Int): Iterable[Iterable[T]] = {
    // TODO: this method assumes that the Int.MaxValue is the receiver max message size
    //  should be used the real receiver max message size instead
    val iterableSize = iterable.size.toDouble
    if (iterableSize > 0) {
      // computes the average space occupied by the list elements
      val bufferSize = getBufferSize(iterable).toDouble
      val wsMessageSize = thresholdedVertxMaxMessageSize
      // the tuning factor reduces the number of elements per group
      // +1 avoids division by 0
      val groupSize = (iterableSize / ((bufferSize / wsMessageSize) + 1)) - tuningFactor
      //Logger.printOut(s"size $groupSize tuning $tuningFactor")
      //Logger.printOut(s"groups ${iterableSize / groupSize} ${iterableSize % groupSize}")
      val groups = iterable.grouped(math.ceil(groupSize).toInt).toIterable
      // checks if a group exceeds the ws max message size
      // TODO: if just one iterable exceeds the size, everything is recomputed from
      //  zero, probably it is better to recompute only for such elements
      groups.find(group => getBufferSize(group) > wsMessageSize) match {
        case Some(group) =>
          // gets the buffer size of the element
          val groupBufferSize = getBufferSize(group)
          // computes how much the element exceeds the ws max buffer size
          val exceededBufferSize = groupBufferSize - wsMessageSize
          // computes how many group elements should be removed in the next step
          // ${element.size} : $groupBufferSize = ${exceededElementsNumber} : $exceededBufferSize"
          val exceededElementsNumber = group.size.toLong * exceededBufferSize / groupBufferSize
          // +1 avoids infinite recursion when exceededElementsNumber is between 1 and 0
          groupByWebsocketSize(iterable, tuningFactor + exceededElementsNumber.toInt + 1)
        case None => groups
      }
    } else {
      Iterable[Iterable[T]]()
    }
  }

  /** Converts the object to its json value and then gets its buffer length.
   *
   * @param obj the object
   * @tparam T the object type
   * @return the length it occupies when sent as a buffer
   */
  protected def getBufferSize[T](obj: T): Int = Buffer.buffer(obj.toJson).length

  /** Extensions for websocket classes.
   *
   * @param webSocket the websocket
   * @tparam W type of the websocket
   */
  implicit class WebSocketExtensions[W <: WebSocketBase](webSocket: W) {

    private val remoteAddress = webSocket.remoteAddress()

    /** Gets remote address host and port in a pretty format.
     *
     * @return remote address host and port in a pretty format
     */
    def remoteAddressInfo: String = s"${remoteAddress.host()} ${remoteAddress.port()}"

    /** Setups an asynchronous web socket text message handler.
     *
     * @param vertx   the vertx instance that will run
     * @param handler the handler that will be run upon receiving a text message
     */
    def textMessageHandlerAsync(vertx: Vertx, handler: (W, String) => Unit): Unit = {
      webSocket.textMessageHandler(text => {
        vertx.executeBlocking(() => handler(webSocket, text))
      })
    }

    /** Setups an exception handler that will log [[remoteAddressInfo]]
     * and the exception occurred.
     */
    def defaultExceptionHandler(): Unit = {
      webSocket.exceptionHandler { e =>
        Logger.printErr(remoteAddressInfo)
        Logger.printErr(e)
      }
    }

    /** Setups a close handler that will log [[remoteAddressInfo]]
     * when the websocket connection is closed.
     */
    def defaultCloseHandler(): Unit = {
      webSocket.closeHandler(_ => Logger.printOut(s"connection $remoteAddressInfo was closed"))
    }

    /** Sends the specified text over multiple frames if it would exceed Int.MaxValue.
     *
     * @param text the text to send
     */
    def writeTextMessageFramed(text: String): Unit = {
      val textPartitions = text.grouped(MaxMessageSize)
      val lastIdx = textPartitions.size - 1
      if (lastIdx == 0) {
        // grouped result produced just 1 partition,
        // so the message won't be split in multiple frames
        webSocket.writeTextMessage(text)
      } else {
        import io.vertx.scala.core.http.WebSocketFrame.{continuationFrame, textFrame}
        textPartitions.zipWithIndex.map {
          case (text, 0) => textFrame(text, false)
          case (text, idx) if idx != lastIdx => continuationFrame(Buffer.buffer(text), false)
          case (text, _) => continuationFrame(Buffer.buffer(text), true)
        }.foreach(webSocket.writeFrame)
      }
    }

  }

}
