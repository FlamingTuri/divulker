package it.unibo.vertx

import io.vertx.scala.core.{Future, Promise}
import io.vertx.scala.ext.web.RoutingContext
import it.unibo.jackson.MarshallableImplicit._
import it.unibo.utils.Logger

import scala.concurrent.ExecutionContext.Implicits.global
import scala.reflect.runtime.universe.TypeTag
import scala.util.{Failure, Success}

/** Utilities to interact with vertx API. */
object VertxHelpers {

  /** Converts a scala future to a vertx future.
   *
   * @param scalaFuture the scala future to convert
   * @param tag         implicit TypeTag used for type [[T]]
   * @tparam T the scala future type
   * @return the equivalent vertx future
   */
  implicit def toVertxFuture[T](scalaFuture: scala.concurrent.Future[T])
                               (implicit tag: TypeTag[T]): Future[T] = {
    val promise = Promise.promise[T]()
    scalaFuture.onComplete {
      case Success(value) => promise.complete(value)
      case Failure(exception) => promise.fail(exception)
    }
    promise.future()
  }

  /** Utility to transform a Future[T] in a Promise[T].
   *
   * @param future the future to transform
   * @tparam T the future type
   * @return the corresponding promise
   */
  def futureToPromise[T](future: Future[T])(implicit tag: TypeTag[T]): Promise[T] = {
    val promise = Promise.promise[T]()
    future.onComplete { res =>
      if (res.succeeded()) {
        promise.complete()
      } else {
        promise.fail(res.cause())
      }
    }
    promise
  }

  implicit def objectToVertxFuture[T](obj: T)(implicit tag: TypeTag[T]): io.vertx.core.Future[T] = {
    io.vertx.core.Future.succeededFuture(obj)
  }

  implicit def objectToScalaFuture[T](obj: T)(implicit tag: TypeTag[T]): Future[T] = {
    Future.succeededFuture(obj)
  }

  /** Failure handler that prints the failure cause and returns a failed future.
   *
   * @param exception the failure cause
   * @param tag       implicit TypeTag used for type [[T]]
   * @tparam T the scala future type
   * @return a failed future with the specified cause
   */
  def defaultFailHandler[T](exception: Throwable)(implicit tag: TypeTag[T]): Future[T] = {
    Logger.printErr(exception, 4)
    exception.printStackTrace()
    Future.failedFuture[T](exception)
  }

  /** Logging utility to use when a route failure occurs.
   *
   * @param routingContext the route's [[RoutingContext]]
   */
  def routerFailureHandler(routingContext: RoutingContext): Unit = {
    val request = routingContext.request()
    val remoteAddress = request.remoteAddress()
    val errorInfo =
      s"""Error detected while handling a request
         |Remote address: ${remoteAddress.host()} ${remoteAddress.port()} ${remoteAddress.path()}
         |Method: ${request.rawMethod()}
         |Path: ${request.path()} ${request.absoluteURI()}
         |Body: ${routingContext.getBodyAsString()}
         |Error: ${routingContext.failure().getMessage}""".stripMargin
    Logger.printErr(errorInfo)
    routingContext.failure().printStackTrace()
    routingContext.response().setStatusCode(500).end(new Exception(errorInfo).toJson)
  }

}
