package it.unibo.vertx

import io.vertx.scala.core.{Context, Future, Promise, Vertx}

import scala.reflect.runtime.universe.TypeTag

object VertxExtensions {

  /** Vertx instance extensions.
   *
   * @param vertx a vertx instance
   */
  implicit class VertxExtension(vertx: Vertx) {

    /** Like [[Vertx.setTimer]] but returns a succeeded future after the delay has elapsed.
     *
     * @param delay the time to wait
     * @return a succeeded future after the delay has elapsed
     */
    def setTimer(delay: Long): Future[Long] = {
      val promise = Promise.promise[Long]()
      vertx.setTimer(delay, promise.complete(_))
      promise.future()
    }

    /** Sets a timer that will run the specified function after a delay.
     * After the function is completed the process will restart.
     * It differs from [[Vertx.setPeriodic]], since the latter won't check
     * the function completion before restarting the timer.
     *
     * @param delay    the time to wait
     * @param function the function called periodically with the timer id
     * @param tag      implicit TypeTag used for type [[T]]
     * @tparam T the type returned by the future
     */
    def repeatAfterDelay[T](delay: Long, function: Long => Future[T])(implicit tag: TypeTag[T]): Unit = {
      setTimer(delay).compose(function).onComplete(_ => repeatAfterDelay(delay, function))
    }

    /** Gets the vertx context and logs useful information about it.
     *
     * @return the context
     */
    def getContextAndPrintInfo: Context = {
      val context = vertx.getOrCreateContext()
      if (context.isEventLoopContext()) {
        println("Context attached to Event Loop")
      }
      if (context.isWorkerContext()) {
        println("Context attached to Worker Thread")
      }
      if (context.isMultiThreadedWorkerContext()) {
        println("Context attached to Worker Thread - multi threaded worker")
      }
      if (!Context.isOnVertxThread()) {
        println("Context not attached to a thread managed by vert.x")
      }
      context
    }

  }

}
