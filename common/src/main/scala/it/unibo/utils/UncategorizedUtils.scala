package it.unibo.utils

object UncategorizedUtils {

  /** Applies the function if the specified condition is true
   *
   * @param condition the condition to verify
   * @param apply     the function to apply
   * @return the condition result
   */
  def applyIfTrue(condition: Boolean, apply: () => Unit): Boolean = {
    if (condition) {
      apply()
    }
    condition
  }

}
