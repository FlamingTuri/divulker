package it.unibo.utils

import java.io.{File, FileWriter}

import it.unibo.constants.Constants.{pathInsideSaveDirectory, pathToString}

/** Logging utility */
object Logger {

  private def printFormat[T](message: T, stackTraceIdx: Int = 3): String = {
    val stackTrace = Thread.currentThread.getStackTrace
    val caller = stackTrace(stackTraceIdx)
    val callerClassName = caller.getClassName
    val callerLineNumber = caller.getLineNumber
    try {
      s"[$callerClassName] ${message.toString} (line $callerLineNumber)"
    } catch {
      case e: Exception => throw e
    }
  }

  /** Prints on stdout.
   *
   * Format: [$callerClassName] ${message.toString} (line $callerLineNumber)
   *
   * @param message       the message to print on stdout
   * @param stackTraceIdx the caller index on the stacktrace used to print
   *                      the caller class name and the caller line number
   * @tparam T the type of the message
   */
  def printOut[T](message: T, stackTraceIdx: Int = 3): Unit = {
    try {
      println(printFormat(message, stackTraceIdx))
    } catch {
      case e: Exception =>
        printErr("Logger error")
        e.printStackTrace()
    }
  }

  /** Prints on stderr.
   *
   * Format: [$callerClassName] ${message.toString} (line $callerLineNumber)
   *
   * @param message       the message to print on stderr
   * @param stackTraceIdx the caller index on the stacktrace used to print
   *                      the caller class name and the caller line number
   * @tparam T the type of the message
   */
  def printErr[T](message: T, stackTraceIdx: Int = 3): Unit = {
    try {
      System.err.println(printFormat(message, stackTraceIdx))
    } catch {
      case e: Exception =>
        printErr("Logger error")
        e.printStackTrace()
    }
  }

  def printFile[T](message: T): Unit = {
    printFile(message, pathInsideSaveDirectory("log.txt"))
  }

  /** Logs a message on a file.
   *
   * @param message  the message to print on file
   * @param filePath the file path where the output will be written
   * @tparam T the type of the message
   */
  def printFile[T](message: T, filePath: String): Unit = {
    val file = new File(filePath)
    if (!file.exists) {
      new File(file.getParent).mkdirs()
      file.createNewFile()
    }
    val fr = new FileWriter(file, true)
    fr.write(s"${printFormat(message)}\n")
    fr.close()
  }

}
