package it.unibo.utils

import io.vertx.scala.core.{Future, Promise}

case class AttemptExecution(private val applyMatching: () => Future[_] = () => Future.succeededFuture()) {

  /** Attempts the execution of the given function. If the function fails with a specific error
   * it is re-executed with a tail recursive approach. This method prevents StackOverflowException
   * when a function that must be executed can fail an undetermined number of times.
   *
   * @param element         the element accepted by the function
   * @param function        the function to run
   * @param retryExceptions the exception that if thrown will re-run the function
   * @param format          the format used to print the element info when an error occurs
   * @param log             whether logging should be performed on exception
   * @tparam T the type of the element
   * @return a future representing the operation result
   */
  def process[T](element: T, function: T => Future[Unit], retryExceptions: Seq[Class[_ <: Throwable]] = Seq(),
                 format: T => String = (e: T) => s"$e", log: Boolean = true): Future[Unit] = {
    val promise = Promise.promise[Unit]()
    process(promise, element, function, retryExceptions, format, log)
    promise.future()
  }

  private def process[T](promise: Promise[Unit], element: T, function: T => Future[Unit],
                         retryExceptions: Seq[Class[_ <: Throwable]], format: T => String,
                         log: Boolean): Unit = {
    function(element).compose { _ =>
      promise.complete()
      Future.succeededFuture[Unit]()
    }.otherwise { e =>
      if (log) {
        Logger.printErr(s"error with ${format(element)} cause: $e")
      }
      val matcher = retry(promise, element, function, retryExceptions, format, log) orElse default(promise)
      applyMatching().compose(_ => Future.succeededFuture(matcher(e)))
    }
  }

  private def retry[T](promise: Promise[Unit], element: T, function: T => Future[Unit],
                       retryExceptions: Seq[Class[_ <: Throwable]],
                       format: T => String, log: Boolean): PartialFunction[Throwable, Unit] = {
    // TODO: check if there is a way without using get class
    case e if retryExceptions.exists(_.isAssignableFrom(e.getClass)) =>
      if (log) {
        Logger.printOut(s"retry ${format(element)}")
      }
      process(promise, element, function, retryExceptions, format, log)
  }

  private def default(promise: Promise[Unit]): PartialFunction[Throwable, Unit] = {
    case _ => promise.complete()
  }

}
