package it.unibo.utils

/** Utility class used to measure application execution time */
case class ExecutionTime() {

  private var start: Long = 0
  private var stop: Long = 0

  /** Sets the start execution time.
   *
   * @return a reference to this to enable fluency
   */
  def startExecution(): ExecutionTime = {
    start = System.currentTimeMillis()
    this
  }

  /** Sets the stop execution time.
   *
   * @return a reference to this to enable fluency
   */
  def stopExecution(): ExecutionTime = {
    stop = System.currentTimeMillis()
    this
  }

  /** Gets the elapsed execution time
   *
   * @return the elapsed execution time
   */
  def getElapsedTime: Long = stop - start

  /** Pretty prints the elapsed execution time   */
  def printElapsedTime(): Unit = Logger.printOut(s"Elapsed $getElapsedTime milliseconds")
}
