package it.unibo.utils

import io.vertx.scala.core.{Future, Promise, Vertx}
import it.unibo.vertx.VertxExtensions._

import scala.util.Random

object CollectionUtils {

  implicit class TraversableWithContains[A](traversable: TraversableOnce[A]) {

    /** Checks if the traversable contains a value
     *
     * @param value the value
     * @return true if the traversable contains the value
     */
    def contains(value: A): Boolean = traversable.exists(_ == value)

    /** Checks if the traversable contains at least one element of the iterable
     *
     * @param iterable the iterable
     * @return true if the traversable contains at least one element of the iterable
     */
    def containsAtLeastOne(iterable: Iterable[A]): Boolean = traversable.exists(iterable.contains)

  }

  implicit class IterableExtensions[T](iterable: Iterable[T]) {

    private var attemptExecution: AttemptExecution = _
    private val random = new Random()

    private def getDelay(min: Int, max: Int): Long = random.nextDouble().toLong * (max - min) + min

    /** Applies the specified asynchronous function to all the collection elements.
     * Each collection element will be processed as soon as the asynchronous function
     * of previous one is completed
     *
     * @param function        the asynchronous function to apply
     * @param retryExceptions an exception matcher to perform recovery operations,
     *                        if nothing matches the next element will be processed
     * @param delayOptions    if defined is used to set a random delay between each element processing,
     *                        where the delay boundaries are between the second and the third of Tuple3
     * @param format          the format used to log errors
     * @param log             whether to log errors
     * @return a future that will be completed when all the collection elements has been processed
     */
    def foreachSynchronous(function: T => Future[Unit], retryExceptions: Seq[Throwable] = Seq(),
                           delayOptions: Option[(Vertx, Int, Int)] = Option.empty,
                           format: T => String = (e: T) => s"$e",
                           log: Boolean = true): Future[Unit] = {
      val promise = Promise.promise[Unit]()
      attemptExecution = delayOptions match {
        case Some(value) => AttemptExecution(() => value._1.setTimer(getDelay(value._2, value._3)))
        case None => AttemptExecution()
      }
      val iterator = iterable.toIterator
      processNext(promise, iterator, function, retryExceptions.map(_.getClass), format, log)
      promise.future()
    }

    private def processNext(promise: Promise[Unit], iterator: Iterator[T], function: T => Future[Unit],
                            retryExceptions: Seq[Class[_ <: Throwable]], format: T => String,
                            log: Boolean): Unit = {
      if (iterator.hasNext) {
        val current = iterator.next()
        attemptExecution.process(current, function, retryExceptions, format, log).onComplete { _ =>
          processNext(promise, iterator, function, retryExceptions, format, log)
        }
      } else {
        promise.complete()
      }
    }

  }

}
