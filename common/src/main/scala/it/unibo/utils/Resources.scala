package it.unibo.utils

import scala.io.Source

/** Utility to read from project resources */
object Resources {

  /** Reads a resource and returns its content as a string
   *
   * @param resource the resource name
   * @return the resource's content as a string
   */
  def readAsString(resource: String): String = {
    Source.fromResource(resource).mkString
  }

}
