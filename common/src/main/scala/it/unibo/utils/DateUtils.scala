package it.unibo.utils

import java.util.concurrent.TimeUnit
import java.util.{Calendar, Date, Locale}

object DateUtils {

  /** Gets a calendar instance with Locale.US time.
   *
   * @return the calendar instance
   */
  def getCalendarInstance: Calendar = Calendar.getInstance(Locale.US)

  /** Gets the string representation of the current time using Locale.US.
   *
   * @return the string representation of the current time
   */
  def getCurrentTime: String = getCalendarInstance.getTime.toString

  /** Returns true if the specified amount of time is elapsed from the specified date.
   *
   * @param calendar      the calendar instance representing the previous time measurement
   * @param delay         the amount of time that needs to elapse
   * @param calendarField the calendar field used to measure the time difference
   * @return true if the amount of time is elapsed
   */
  def isTimeElapsed(calendar: Calendar, delay: Int, calendarField: Int = Calendar.HOUR_OF_DAY): Boolean = {
    calendar.add(calendarField, delay)
    val result = calendar.compareTo(getCalendarInstance)
    calendar.add(calendarField, -delay)
    result <= 0
  }

  /** Checks if the difference between the two dates is greater than the specified number of hours.
   *
   * @param date1 the first date
   * @param date2 the first date
   * @param hours he amount of hours
   * @return true if the two dates difference is greater than the specified amount of hours
   */
  def isTimeElapsed(date1: Date, date2: Date, hours: Int): Boolean = {
    val diffInMillis = math.abs(date1.getTime - date2.getTime)
    val diff = TimeUnit.HOURS.convert(diffInMillis, TimeUnit.MILLISECONDS)
    diff >= hours
  }

}
