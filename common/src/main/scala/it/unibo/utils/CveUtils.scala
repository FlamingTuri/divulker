package it.unibo.utils

import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException

object CveUtils {

  private val cveRegex = "CVE-(\\d{4})-(\\d{4,})".r

  /** Returns the year of a CVE id.
   * Ex. CVE-2019-0001 returns 2019
   *
   * @param cveId a CVE id
   * @return the year derived from the CVE id
   */
  def getCveIdYear(cveId: String): Int = {
    cveId match {
      case cveRegex(year, _) => year.toInt
      case value => throw new ValueException(s"could not parse $value")
    }
  }

  /** Returns the numeric portion of a CVE id.
   * Ex. CVE-2019-0001 returns 1
   *
   * @param cveId a CVE id
   * @return the year derived from the CVE id
   */
  def getCveIdNumericPortion(cveId: String): String = {
    cveId match {
      case cveRegex(_, numericPortion) => numericPortion
      case value => throw new ValueException(s"could not parse $value")
    }
  }

  /** Replace the numeric portion with a number of "x" equal to the mask size.
   * I.E with default mask size: 12 -> xxx, 123 -> xxx, 1234 -> 1xxx
   *
   * @param numericPortion the numeric portion
   * @param maskSize       the mask size
   * @return the masked numeric portion
   */
  def maskNumericPortion(numericPortion: String, maskSize: Int = 3): String = {
    val mask = "x" * maskSize
    if (maskSize >= numericPortion.length) {
      mask
    } else {
      s"${numericPortion.dropRight(maskSize)}$mask"
    }
  }

}
