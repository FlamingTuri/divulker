package it.unibo.updates.collections

import scala.collection.mutable

object BufferUpdateUtils {

  implicit class BufferUpdateExtensions[T](private val list: mutable.Buffer[T])
    extends IterableUpdate[T](list) {

    override def addNewEntry(newEntry: T): Unit = list += newEntry

    override def removeEntries(entries: mutable.Iterable[T]): Unit = list --= entries

  }

}
