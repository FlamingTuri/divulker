package it.unibo.updates.collections

import it.unibo.updates.collections.BufferUpdateUtils._
import it.unibo.updates.operation.result.OperationResultUtil.OperationResult
import it.unibo.updates.operation.result.{OperationResultUtil, Result, UpdateResult}
import it.unibo.utils.CveUtils

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

object MapUpdateUtils {

  implicit class MapExtension[K, V](map: mutable.Map[K, V])
    extends IterableUpdate[(K, V)](map) {

    override def addNewEntry(newEntry: (K, V)): Unit = map += newEntry

    override def removeEntries(entries: mutable.Iterable[(K, V)]): Unit = ???

    def insertOrUpdate(key: K, newValue: V, updateStrategy: (V, V) => Unit): Unit = {
      map.get(key) match {
        case Some(value) => updateStrategy(value, newValue)
        case None => map += (key -> newValue)
      }
    }
  }

  implicit class MapOfMapExtensions[T](map: mutable.Map[Int, mutable.ListMap[String, ListBuffer[T]]])
    extends MapExtension[Int, mutable.ListMap[String, ListBuffer[T]]](map) {

    /** Inserts the updates if missing or otherwise attempts the update of the existing ones,
     * based on the specified update strategy.
     *
     * @param updates             the entries that should be considered as a more recent
     *                            version of the current data
     * @param updateEntryStrategy the strategy used to update the existing entry
     * @return the operation result
     */
    def insertOrUpdate(updates: Iterable[T], getCveId: T => String,
                       updateEntryStrategy: (T, T) => Boolean): UpdateResult[T] = {
      insertOrUpdate[T](updates, getCveId, getCveId, updateEntryStrategy, (e: T) => e)
    }

    /** Inserts the updates if missing or otherwise attempts the update of the existing ones,
     * based on the specified update strategy.
     *
     * @param updates             the entries that should be considered as a more recent
     *                            version of the current data
     * @param getUpdateCveId      the strategy used to get the CVE ID from an [[E]] entry
     * @param updateEntryStrategy the strategy used to update the existing entry
     * @param updateToMapEntry    the strategy used to get an entry of type [[T]] from type [[E]]
     *                            used when the entry is missing
     * @return the operation result
     */
    def insertOrUpdate[E](updates: Iterable[E], getUpdateCveId: E => String,
                          getMapEntryCveId: T => String,
                          updateEntryStrategy: (T, E) => Boolean,
                          updateToMapEntry: E => T): UpdateResult[T] = {
      val updateResult = new UpdateResult[T]()
      updates.map(insertOrUpdate(_, getUpdateCveId, getMapEntryCveId, updateEntryStrategy, updateToMapEntry))
        .foreach(updateResult.addEntry)
      updateResult
    }

    /** Inserts the update if missing or otherwise attempts the update of the existing entry
     * with the same CVE ID, based on the specified update strategy.
     *
     * @param update              the entry that should be considered as a more recent
     *                            version of the current data
     * @param getMapEntryCveId    the strategy used to get the CVE ID from an [[E]] entry
     * @param updateEntryStrategy the strategy used to update the existing entry
     * @param updateToMapEntry    the strategy used to get an entry of type [[T]] from type [[E]]
     *                            used when the entry is missing
     * @return the operation result
     */
    def insertOrUpdate[E](update: E, getUpdateCveId: E => String,
                          getMapEntryCveId: T => String,
                          updateEntryStrategy: (T, E) => Boolean,
                          updateToMapEntry: E => T): OperationResult[T] = {
      val cveId = getUpdateCveId(update)
      val cveIdYear = CveUtils.getCveIdYear(cveId)
      val cveIdNumericPortion = CveUtils.getCveIdNumericPortion(cveId)
      val maskedNumericPortion = CveUtils.maskNumericPortion(cveIdNumericPortion)
      map.get(cveIdYear) match {
        case Some(numericPortionMap) =>
          numericPortionMap.get(maskedNumericPortion) match {
            case Some(cveList) =>
              cveList.insertOrUpdate(update,
                (current: T, update: E) => getMapEntryCveId(current) == cveId,
                updateEntryStrategy, updateToMapEntry)
            case None =>
              val newEntry = updateToMapEntry(update)
              numericPortionMap += (maskedNumericPortion -> ListBuffer(newEntry))
              OperationResultUtil(Result.New, newEntry)
          }
        case None =>
          val newEntry = updateToMapEntry(update)
          map += (cveIdYear -> mutable.ListMap(maskedNumericPortion -> ListBuffer(newEntry)))
          OperationResultUtil(Result.New, newEntry)
      }
    }

    /** Get how many elements of type [[T]] are present in the map
     *
     * @return how many elements of type [[T]] are present in the map
     */
    def countElements: Int = {
      map.values.flatMap(_.values).map(_.size).sum
    }

    /** Get the map as an [[Iterable]] of [[T]]
     *
     * @return the map as an [[Iterable]] of [[T]]
     */
    def getAsIterable: Iterable[T] = {
      map.values.flatMap(_.values).flatten
    }

  }

}
