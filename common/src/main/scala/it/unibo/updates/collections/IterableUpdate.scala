package it.unibo.updates.collections

import it.unibo.updates.operation.result
import it.unibo.updates.operation.result.OperationResultUtil.OperationResult
import it.unibo.updates.operation.result.{DeleteUpdateResult, OperationResultUtil, Result, UpdateResult}
import it.unibo.utils.CollectionUtils.TraversableWithContains

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

abstract class IterableUpdate[T](private val iterable: mutable.Iterable[T]) {

  /** Insert the new entry in the collection.
   *
   * @param newEntry the entry to add
   */
  def addNewEntry(newEntry: T): Unit

  /** Removes the entries from the collection.
   *
   * @param entries the entries to remove
   */
  def removeEntries(entries: mutable.Iterable[T]): Unit

  /** Inserts the entry data if missing or otherwise attempts the update of the existing one,
   * based on the specified update strategy
   *
   * @param entry           the entry to insert or update
   * @param compareStrategy the strategy used to determine when two entries
   *                        should be considered equal
   * @param updateStrategy  the strategy used to update the existing entry
   * @return the operation result
   */
  def insertOrUpdate(entry: T, compareStrategy: (T, T) => Boolean,
                     updateStrategy: (T, T) => Boolean): OperationResult[T] = {
    insertOrUpdate(entry, compareStrategy, updateStrategy, (e: T) => e)
  }

  /** Inserts the entry data if missing or otherwise attempts the update of the existing one,
   * based on the specified update strategy
   *
   * @param entry            the entry to insert or update
   * @param compareStrategy  the strategy used to determine when two entries
   *                         should be considered equal
   * @param updateStrategy   the strategy used to update the existing entry
   * @param newEntryStrategy the strategy used to insert the missing entry
   * @tparam A the type of the data
   * @return the operation result
   */
  def insertOrUpdate[A](entry: A, compareStrategy: (T, A) => Boolean,
                        updateStrategy: (T, A) => Boolean,
                        newEntryStrategy: A => T): OperationResult[T] = {
    iterable.find(current => compareStrategy(current, entry)) match {
      case Some(value) =>
        if (updateStrategy(value, entry)) {
          OperationResultUtil(Result.Update, value)
        } else {
          OperationResult(Result.None)
        }
      case None =>
        val newEntry = newEntryStrategy(entry)
        addNewEntry(newEntry)
        OperationResultUtil(Result.New, newEntry)
    }
  }

  /** Removes the elements that are not in the collection.
   *
   * @param data            the data to keep
   * @param compareStrategy the strategy used to determine when two entries
   *                        should be considered equal
   * @tparam A the type of the data iterable
   * @return the removed entries
   */
  def remove[A](data: Iterable[A], compareStrategy: (T, A) => Boolean): mutable.Iterable[T] = {
    val removedEntries = iterable filterNot (current => data.exists(compareStrategy(current, _)))
    removeEntries(removedEntries)
    removedEntries
  }

  /** Removes the elements that are not in the collection.
   *
   * @param data the data to keep
   * @return the removed entries
   */
  def remove(data: Iterable[T]): mutable.Iterable[T] = {
    val removedEntries = iterable filterNot (data contains)
    removeEntries(removedEntries)
    removedEntries
  }

  /** Removes and updates the collection entries.
   *
   * @param data             the data to keep or update
   * @param compareStrategy  the strategy used to determine when two entries
   *                         should be considered equal
   * @param updateStrategy   the strategy used to update an already existing entry
   * @param newEntryStrategy the strategy used to insert a missing entry
   * @tparam A the type of the data
   * @return the operation result
   */
  def removeAndUpdate[A](data: Iterable[A],
                         compareStrategy: (T, A) => Boolean,
                         updateStrategy: (T, A) => Boolean,
                         newEntryStrategy: A => T): DeleteUpdateResult[T] = {
    val removedEntries = remove(data, compareStrategy)
    val temp = insertOrUpdate(data, compareStrategy, updateStrategy, newEntryStrategy)
    result.DeleteUpdateResult(temp.newEntries, temp.updatedEntries, removedEntries.to[ListBuffer])
  }

  /** Removes and updates the collection entries.
   *
   * @param data           the data to keep or update
   * @param updateStrategy the strategy used to update an already existing entry
   * @return the operation result
   */
  def removeAndUpdate(data: Iterable[T], updateStrategy: (T, T) => Boolean): DeleteUpdateResult[T] = {
    val removedEntries = remove(data)
    val temp = insertOrUpdate(data, updateStrategy)
    result.DeleteUpdateResult(temp.newEntries, temp.updatedEntries, removedEntries.to[ListBuffer])
  }

  /** Updates the collection entries with the given data.
   *
   * @param data             the data used to update the list
   * @param compareStrategy  the strategy used to determine when two entries
   *                         should be considered equal
   * @param updateStrategy   the strategy used to update an already existing entry
   * @param newEntryStrategy the strategy used to insert a missing entry
   * @tparam A the type of the data
   * @return the operation result
   */
  def insertOrUpdate[A](data: Iterable[A], compareStrategy: (T, A) => Boolean,
                        updateStrategy: (T, A) => Boolean, newEntryStrategy: A => T): UpdateResult[T] = {
    val updateResult = new UpdateResult[T]()
    data.map(insertOrUpdate[A](_, compareStrategy, updateStrategy, newEntryStrategy))
      .foreach(updateResult.addEntry)
    updateResult
  }

  /** Updates the entries with the given data.
   * The new entry strategy is implemented as (value) => value.
   *
   * @param dataIterable    the data that will update the list
   * @param compareStrategy the strategy used to determine when two entries
   *                        should be considered equal
   * @param updateStrategy  the strategy used to update an already existing entry
   * @return the operation result
   */
  def insertOrUpdate(dataIterable: Iterable[T],
                     compareStrategy: (T, T) => Boolean,
                     updateStrategy: (T, T) => Boolean): UpdateResult[T] = {
    insertOrUpdate[T](dataIterable, compareStrategy, updateStrategy, (update: T) => update)
  }

  /** Updates the entries with the given data.
   *
   * @param dataIterable   the data that will update the list
   * @param updateStrategy the strategy used to update an already existing entry
   * @return the operation result
   */
  def insertOrUpdate(dataIterable: Iterable[T],
                     updateStrategy: (T, T) => Boolean): UpdateResult[T] = {
    insertOrUpdate[T](dataIterable, (current: T, update: T) => current == update,
      updateStrategy, (update: T) => update)
  }

  /** Adds the entry if not already present in the collection.
   *
   * @param entry the entry to add
   * @return true if the data is successfully added to the collection
   */
  def addIfNotPresent(entry: T): Boolean = {
    val isNotPresent = !iterable.contains(entry)
    if (isNotPresent) {
      addNewEntry(entry)
    }
    isNotPresent
  }

  /** Adds the entry if not already present in the collection.
   *
   * @param entry           the entry to add
   * @param compareStrategy the strategy used to check if the element is already present
   * @return true if the data is successfully added to the collection
   */
  def addIfNotPresent(entry: T, compareStrategy: (T, T) => Boolean): Boolean = {
    val isNotPresent = !iterable.exists(compareStrategy(_, entry))
    if (isNotPresent) {
      addNewEntry(entry)
    }
    isNotPresent
  }

}
