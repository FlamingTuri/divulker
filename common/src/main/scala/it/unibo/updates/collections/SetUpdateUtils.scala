package it.unibo.updates.collections

import it.unibo.models.{Key, KeyComparator}

import scala.collection.mutable

object SetUpdateUtils {

  implicit class SetUpdateExtensions[T](private val set: mutable.Set[T]) extends IterableUpdate[T](set) {

    override def addNewEntry(newEntry: T): Unit = set += newEntry

    override def removeEntries(entries: mutable.Iterable[T]): Unit = set --= entries
  }

  implicit class SortedSetUpdateExtensions[T <: Key](private val dataList: mutable.SortedSet[T]) {

    private val comparator = new KeyComparator[T]

    def myUpdate(updatesList: Iterable[T],
                 updateStrategy: (T, T) => Boolean): (mutable.SortedSet[T], mutable.SortedSet[T]) = {
      val updatedEntries = mutable.SortedSet[T]()(comparator)
      val newEntries = mutable.SortedSet[T]()(comparator)
      updatesList.foreach { update =>
        dataList.find(current => current == update) match {
          case Some(value) =>
            if (updateStrategy(value, update) && !newEntries.contains(value)) {
              updatedEntries += value
            }
          case None =>
            dataList += update
            newEntries += update
        }
      }
      (updatedEntries, newEntries)
    }

    def removeAndUpdate(updatesList: Iterable[T],
                        updateStrategy: (T, T) => Boolean): (mutable.SortedSet[T], mutable.SortedSet[T], mutable.SortedSet[T]) = {
      val removedEntries = dataList filterNot (updatesList.toList contains)
      dataList --= removedEntries
      val (updatedEntries, newEntries) = myUpdate(updatesList, updateStrategy)
      (removedEntries, updatedEntries, newEntries)
    }
  }

}
