package it.unibo.updates.strategies

import it.unibo.models.aggregator.Source

import scala.collection.mutable.ListBuffer

object SourceStrategies {

  /** Updates the source with the same name of the update.
   *
   * @param currentSources the source list
   * @param update the received update
   * @return true if a source was updated
   */
  def updateSourcesStrategy(currentSources: ListBuffer[Source], update: Source): Boolean = {
    currentSources.find(_.name == update.name) match {
      case Some(value) => updateSourceStrategy(value, update)
      case None =>
        currentSources += update
        true
    }
  }

  /** Updates the current source date with the update one if they differ.
   *
   * @param current the current source
   * @param update the received update
   * @return true if the date is updated
   */
  def updateSourceStrategy(current: Source, update: Source): Boolean = {
    val updated = current.date != update.date
    if (updated) {
      current.date = update.date
    }
    updated
  }

}
