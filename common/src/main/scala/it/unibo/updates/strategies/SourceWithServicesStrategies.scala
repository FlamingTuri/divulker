package it.unibo.updates.strategies

import it.unibo.models.aggregator.Source
import it.unibo.models.general.aggregator.SourceWithServices
import it.unibo.models.service.ServiceName

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

object SourceWithServicesStrategies {

  def updateSourceStrategy(currentSources: ListBuffer[SourceWithServices], update: Source,
                           aggregatorServiceName: ServiceName.Value): Boolean = {
    val updateSourceName = update.name
    currentSources.find(e => e.name == updateSourceName) match {
      case Some(value) =>
        val servicesWithSourceUpdated = value.servicesWithSource.add(aggregatorServiceName)
        val dateUpdated = value.date != update.date
        if (dateUpdated) {
          value.date = update.date
        }
        dateUpdated || servicesWithSourceUpdated
      case None =>
        currentSources += toSourceWithServices(update, aggregatorServiceName)
        true
    }
  }

  def toSourceWithServices(source: Source,
                           aggregatorServiceName: ServiceName.Value): SourceWithServices = {
    SourceWithServices(source.name, source.date, mutable.Set(aggregatorServiceName))
  }

  def compareStrategy(current: SourceWithServices, update: SourceWithServices): Boolean = {
    current.name == update.name
  }

  def updateStrategy(current: SourceWithServices, update: SourceWithServices): Boolean = {
    val updated = current.date != update.date
    if (updated) {
      current.date = update.date
    }
    val prevSize = current.servicesWithSource.size
    current.servicesWithSource ++= update.servicesWithSource
    updated || (prevSize != current.servicesWithSource.size)
  }

}
