package it.unibo.updates.strategies

import it.unibo.models.CustomTypes.GatheredDataType
import it.unibo.models.GeneralCve
import it.unibo.models.aggregator.GatheredData
import it.unibo.updates.strategies.SourceStrategies.updateSourcesStrategy

object GatheredDataStrategies {

  /** Strategy check whether two vulnerabilities have the same CVE ID.
   *
   * @param current the first one
   * @param update the second one
   * @return whether the two vulnerabilities have the same CVE ID
   */
  def compareStrategy(current: GatheredData, update: GatheredData): Boolean = {
    current.cveId == update.cveId
  }

  /** Updates the current vulnerability with more updated data.
   *
   * @param current the vulnerability to update
   * @param update the vulnerability which data are more up to date
   * @return whether the vulnerability was updated
   */
  def updateStrategy(current: GatheredData, update: GatheredData): Boolean = {
    val disclosuresUpdated = update.disclosures
      .map(updateSourcesStrategy(current.disclosures, _)).exists(e => e)
    val fixesUpdated = update.fixes.map { updateFix =>
      val packageName = updateFix._1
      val updateFixSources = updateFix._2
      // updates the fixes related to the the current package, or if the package was not
      // tracked by the vulnerability its new fix information will be added
      current.fixes.get(packageName) match {
        case Some(value) => updateFixSources.map(updateSourcesStrategy(value, _)).exists(e => e)
        case None =>
          current.fixes += (packageName -> updateFixSources)
          true
      }
    }.exists(e => e)
    disclosuresUpdated || fixesUpdated
  }

  /** Strategy check whether two vulnerabilities have the same CVE ID.
   *
   * @param current the first one
   * @param update the second one
   * @return whether the two vulnerabilities have the same CVE ID
   */
  def compareStrategy[D <: GatheredDataType, G <: GeneralCve](current: D, update: G): Boolean = {
    current.cveId == update.cveId
  }

}
