package it.unibo.updates

import io.vertx.scala.core.{Future, Promise}
import it.unibo.utils.Logger

import scala.collection.mutable

/** Class used as a lock mechanism to prevent conflicts when updating variables upon receiving data
 * from asynchronous sources. Each time an update should be done, the add method should be called.
 * The update should be processed only when the promise returned by the add method will be completed.
 */
class PendingUpdate {

  // TODO: it could be useful to define priority between the received updates,
  //  if so the collection should be sorted each time a new element is added:
  //  for obvious reason the current processed element should be excluded.
  //  To do so I suggest using java collections, since scala sort always creates a sorted copy
  //  of the collection and technically an update request could be lost in the process
  //  of collection replacement.
  private val pendingUpdates: mutable.Queue[(String, Promise[Unit])] = mutable.Queue()

  /** Creates a new promise representing whether an update can be processed without conflicts
   * and puts it at the end of the update queue. The promise will be completed when it reaches
   * the queue head. When the promise is completed the code performing the update can be run
   * without conflicts.
   *
   * @param id id used for logging purposes
   * @return the promise representing whether the update can be processed without conflicts
   */
  def add(id: String = ""): Future[Unit] = {
    Logger.printOut(s"added $id, pending queue size ${pendingUpdates.length}")
    val promise = Promise.promise[Unit]()
    pendingUpdates += ((id, promise))
    completeFirst()
    promise.future()
  }

  /** Removes the head of the update queue. This method should be called after the update process
   * has been completed, to unlock the next one.
   */
  def remove(): Unit = {
    val removed = pendingUpdates.dequeue()
    Logger.printOut(s"removed ${removed._1}, pending queue size ${pendingUpdates.length}")
    completeFirst()
  }

  private def completeFirst(): Unit = {
    pendingUpdates.headOption match {
      case Some(value) =>
        Logger.printOut(s"head ${value._1}")
        if (!value._2.future().isComplete()) {
          value._2.complete()
        }
      case None =>
    }
  }

}
