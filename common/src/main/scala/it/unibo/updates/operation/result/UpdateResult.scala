package it.unibo.updates.operation.result

import it.unibo.updates.operation.result.OperationResultUtil.OperationResult
import it.unibo.utils.Logger

import scala.collection.mutable.ListBuffer

/** Class used to represent the result of an insert or update operation.
 *
 * @param _newEntries the new entries
 * @param _updatedEntries the updated entries
 * @tparam T the type of the entries
 */
class UpdateResult[T](private val _newEntries: ListBuffer[T] = ListBuffer[T](),
                      private val _updatedEntries: ListBuffer[T] = ListBuffer[T]()) {

  /** Gets the new entries
   *
   * @return the new entries
   */
  def newEntries: ListBuffer[T] = _newEntries

  /** Gets the updated entries
   *
   * @return the updated entries
   */
  def updatedEntries: ListBuffer[T] = _updatedEntries

  /** Adds a value to the updated entries list if not already present and
   * if not present in the new entries list.
   *
   * @param value the value to add
   */
  def addUpdatedEntry(value: T): Unit = {
    if (!newEntries.contains(value) && !updatedEntries.contains(value)) {
      _updatedEntries += value
    }
  }

  /** Adds a value to the new entries list if not already present.
   *
   * @param newEntry the value to add
   */
  def addNewEntry(newEntry: T): Unit = {
    if (!newEntries.contains(newEntry)) {
      _newEntries += newEntry
    }
  }

  /** Adds the result to the correct collection
   *
   * @param operationResult the operation result
   */
  def addEntry(operationResult: OperationResult[T]): Unit = operationResult match {
    case OperationResult(Result.Update, Some(value)) => addUpdatedEntry(value)
    case OperationResult(Result.New, Some(value)) => addNewEntry(value)
    case _ =>
  }

  /** Logging utility to print the entries number */
  def printEntriesNumber(): Unit = {
    Logger.printOut(
      s"""entries:
         |new: ${newEntries.size}
         |updated: ${updatedEntries.size}""".stripMargin)
  }

}
