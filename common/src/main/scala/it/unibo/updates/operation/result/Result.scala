package it.unibo.updates.operation.result

object Result extends Enumeration {
  type Result = Value
  val None, Update, New = Value
}
