package it.unibo.updates.operation.result

import it.unibo.utils.Logger

import scala.collection.mutable.ListBuffer

case class DeleteUpdateResult[T](private val _newEntries: ListBuffer[T] = ListBuffer[T](),
                                 private val _updatedEntries: ListBuffer[T] = ListBuffer[T](),
                                 private val _deletedEntries: ListBuffer[T] = ListBuffer[T]())
  extends UpdateResult[T](_newEntries, _updatedEntries) {

  /** Gets the deleted entries
   *
   * @return the deleted entries
   */
  def deletedEntries: ListBuffer[T] = _deletedEntries

  override def printEntriesNumber(): Unit = {
    Logger.printOut(
      s"""entries:
         |new: ${newEntries.size}
         |updated: ${updatedEntries.size}
         |deleted: ${deletedEntries.size}""".stripMargin)
  }

}
