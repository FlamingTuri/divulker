package it.unibo.updates.operation.result

object OperationResultUtil {

  def apply[T](result: Result.Value, v: T): OperationResult[T] = {
    OperationResult(result, Option(v))
  }

  case class OperationResult[T](result: Result.Value, v: Option[T] = Option.empty)

}
