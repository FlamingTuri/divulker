package it.unibo.handler.specific

import io.vertx.scala.core.http.WebSocket
import it.unibo.handler.GeneralDataAggregatorInfo
import it.unibo.handlers.NotifySubscribersWsHandler
import it.unibo.jackson.MarshallableImplicit._
import it.unibo.models.aggregator.GatheredData
import it.unibo.models.general.aggregator.AggregatedVulnerabilityData
import it.unibo.models.service.ServiceKind
import it.unibo.models.ws.messages.Entries
import it.unibo.models.ws.messages.aggregator.AggregatorResponses.AggregatorResponseMessage
import it.unibo.models.ws.messages.distro.DistroSourceRequestMessage
import it.unibo.models.ws.messages.general.GeneralSourceRequestMessage
import it.unibo.models.ws.messages.operations.{AddCve, Subscribe}
import it.unibo.updates.collections.MapUpdateUtils.MapOfMapExtensions
import it.unibo.updates.strategies.VulnerabilityDataStrategies
import it.unibo.utils.Logger
import it.unibo.verticle.GeneralDataAggregatorVerticle

import scala.collection.mutable.ListBuffer

abstract class SpecificAggregatorWsHandler extends NotifySubscribersWsHandler[AggregatedVulnerabilityData, GeneralDataAggregatorVerticle] with GeneralDataAggregatorInfo {

  override protected val wsRequestMessage: DistroSourceRequestMessage = {
    DistroSourceRequestMessage(Subscribe)
  }

  override protected def textMessageHandler(webSocket: WebSocket, text: String): Unit = {
    val result = text.fromJson[AggregatorResponseMessage]()
    val aggregatorServiceName = result.aggregatorServiceName
    val verticleData = verticle.data
    Logger.printOut("aggregator service name " + aggregatorServiceName)
    val operationResult = verticleData.insertOrUpdate(result.data.newEntries ++ result.data.updatedEntries,
      (update: GatheredData) => update.cveId, mapEntry => mapEntry.cveId,
      (current, update: GatheredData) => {
        VulnerabilityDataStrategies.updateStrategy(current, update, aggregatorServiceName)
      }, (update: GatheredData) => VulnerabilityDataStrategies.newEntryStrategy(update, aggregatorServiceName))
    Logger.printOut(verticleData.countElements)
    operationResult.printEntriesNumber()
    addCveToGeneralSources(operationResult.newEntries)
    notifySubscribers(Entries(operationResult))
  }

  protected def addCveToGeneralSources(entries: ListBuffer[AggregatedVulnerabilityData]): Unit = {
    val generalCveDataSourcesServices = getGeneralRetrievers
    if (entries.nonEmpty && generalCveDataSourcesServices.nonEmpty) {
      val cveIds = entries.map(_.cveId)
      val request = GeneralSourceRequestMessage(AddCve, distroName, cveIds).toJson
      generalCveDataSourcesServices.foreach(_._2.sendTextMessage(request))
    }
  }

  private def getGeneralRetrievers = {
    verticle.services.filter(_._1.serviceType == ServiceKind.GeneralRetriever)
  }

  /** Notifies the data aggregator subscribers with the received entries
   *
   * @param entries the entries to send
   */
  override def notifySubscribers(entries: Entries[AggregatedVulnerabilityData]): Unit = {
    if (entries.nonEmpty) {
      verticle.notifyAggregatorSubscribers(entries)
    }
  }

}
