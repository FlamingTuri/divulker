package it.unibo.handler.general

import io.vertx.scala.core.http.WebSocket
import it.unibo.handler.GeneralDataAggregatorInfo
import it.unibo.handlers.NotifySubscribersWsHandler
import it.unibo.jackson.MarshallableImplicit._
import it.unibo.models.GeneralCve
import it.unibo.models.general.aggregator.AggregatedVulnerabilityData
import it.unibo.models.service.ServiceName
import it.unibo.models.ws.messages.Entries
import it.unibo.models.ws.messages.general.{GeneralSourceRequestMessage, GeneralSourceResponseMessage}
import it.unibo.models.ws.messages.operations.{Operation, Subscribe}
import it.unibo.updates.collections.MapUpdateUtils.MapOfMapExtensions
import it.unibo.updates.strategies.VulnerabilityDataStrategies
import it.unibo.verticle.GeneralDataAggregatorVerticle
import it.unibo.vertx.WebSocketHelpers.WebSocketExtensions

abstract class GeneralRetrieverWsHandler[G <: GeneralCve]()(implicit manifest: Manifest[G])
  extends NotifySubscribersWsHandler[AggregatedVulnerabilityData, GeneralDataAggregatorVerticle] with GeneralDataAggregatorInfo {

  override protected val wsRequestMessage: GeneralSourceRequestMessage[Operation] = {
    GeneralSourceRequestMessage(Subscribe, distroName.toString)
  }

  /** Convert the published date to integer value with yyyy-MM-dd format.
   *
   * @param publishedDate the published date
   * @return the published date with yyyy-MM-dd format
   */
  protected def parsePublishedDate(publishedDate: String): String

  override protected def textMessageHandler(webSocket: WebSocket, text: String): Unit = {
    val res = text.fromJson[GeneralSourceResponseMessage[ServiceName.Value, G]]()
    val receivedData = res.data
    val receivedEntries = receivedData.newEntries ++ receivedData.updatedEntries
    val operationResult = verticle.data.insertOrUpdate(receivedEntries,
      (update: G) => update.cveId, entry => entry.cveId,
      (current: AggregatedVulnerabilityData, update: G) => updateStrategy(current, update),
      (update: G) => newEntryStrategy(update))
    receivedData.printInfo()
    operationResult.printEntriesNumber()
    notifySubscribers(Entries(operationResult))
  }

  protected def updateStrategy(current: AggregatedVulnerabilityData, update: G): Boolean = {
    VulnerabilityDataStrategies.updateStrategy(current, update, sourceName, parsePublishedDate)
  }

  protected def newEntryStrategy(update: G): AggregatedVulnerabilityData = {
    VulnerabilityDataStrategies.newEntryStrategy(update, sourceName, parsePublishedDate)
  }

  /** Sends to the other endpoint the request.
   *
   * @param request the request to send
   */
  def send(request: GeneralSourceRequestMessage[Operation]): Unit = {
    webSocket.writeTextMessageFramed(request.toJson)
  }

  override def notifySubscribers(entries: Entries[AggregatedVulnerabilityData]): Unit = {
    if (entries.nonEmpty) {
      verticle.notifyAggregatorSubscribers(entries)
    }
  }

}
