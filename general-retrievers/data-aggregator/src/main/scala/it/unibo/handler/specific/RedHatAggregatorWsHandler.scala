package it.unibo.handler.specific

import it.unibo.models.service.ServiceName

case class RedHatAggregatorWsHandler() extends SpecificAggregatorWsHandler {

  override protected val sourceName: ServiceName.Value = ServiceName.RedHatDataAggregator

}
