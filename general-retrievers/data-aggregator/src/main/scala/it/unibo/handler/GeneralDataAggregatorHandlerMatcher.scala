package it.unibo.handler

import it.unibo.handler.general.{GeneralRetrieverMitreWsHandler, GeneralRetrieverNvdWsHandler}
import it.unibo.handler.specific.{DebianAggregatorWsHandler, RedHatAggregatorWsHandler}
import it.unibo.handlers.WsHandler
import it.unibo.handlers.assigner.HandlerAssigner
import it.unibo.models.service.ServiceName
import it.unibo.verticle.GeneralDataAggregatorVerticle

case class GeneralDataAggregatorHandlerMatcher() extends HandlerAssigner[WsHandler[GeneralDataAggregatorVerticle]] {

  override protected def serviceMatcher(serviceName: ServiceName.Value): WsHandler[GeneralDataAggregatorVerticle] = {
    serviceName match {
      case ServiceName.MitreService => GeneralRetrieverMitreWsHandler()
      case ServiceName.NvdService => GeneralRetrieverNvdWsHandler()
      case ServiceName.DebianDataAggregator => DebianAggregatorWsHandler()
      case ServiceName.RedHatDataAggregator => RedHatAggregatorWsHandler()
      case _ => ???
    }
  }

}
