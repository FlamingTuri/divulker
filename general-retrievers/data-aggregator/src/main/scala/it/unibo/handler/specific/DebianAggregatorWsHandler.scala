package it.unibo.handler.specific

import it.unibo.models.service.ServiceName

case class DebianAggregatorWsHandler() extends SpecificAggregatorWsHandler {

  override protected val sourceName: ServiceName.Value = ServiceName.DebianDataAggregator

}
