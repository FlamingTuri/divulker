package it.unibo.handler

import it.unibo.handlers.DistroNameInfo
import it.unibo.models.service.ServiceName

trait GeneralDataAggregatorInfo extends DistroNameInfo {

  override protected def distroName: ServiceName.Value = ServiceName.GeneralDataAggregator

}
