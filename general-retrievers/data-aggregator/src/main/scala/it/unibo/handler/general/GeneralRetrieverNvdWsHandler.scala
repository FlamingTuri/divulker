package it.unibo.handler.general

import it.unibo.models.general.aggregator.AggregatedVulnerabilityData
import it.unibo.models.service.ServiceName
import it.unibo.nvd.NvdCve
import it.unibo.parsers.NvdCveDateParser

case class GeneralRetrieverNvdWsHandler() extends GeneralRetrieverWsHandler[NvdCve] {

  override protected val sourceName: ServiceName.Value = ServiceName.NvdService

  override protected def parsePublishedDate(publishedDate: String): String = {
    NvdCveDateParser.parseDate(publishedDate)
  }

  override protected def updateStrategy(current: AggregatedVulnerabilityData, update: NvdCve): Boolean = {
    // defining a more fine grained update is very bothersome
    val impactUpdated = current.impact != update.impact
    if (impactUpdated) {
      current.impact = update.impact
    }
    super.updateStrategy(current, update) || impactUpdated
  }

  override protected def newEntryStrategy(update: NvdCve): AggregatedVulnerabilityData = {
    val newEntry = super.newEntryStrategy(update)
    newEntry.impact = update.impact
    newEntry
  }

}
