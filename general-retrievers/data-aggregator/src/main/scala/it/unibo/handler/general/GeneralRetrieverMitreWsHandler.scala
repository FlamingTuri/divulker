package it.unibo.handler.general

import it.unibo.mitre.CvrfVulnerability
import it.unibo.models.service.ServiceName
import it.unibo.parsers.CvrfVulnerabilityDateParser

case class GeneralRetrieverMitreWsHandler() extends GeneralRetrieverWsHandler[CvrfVulnerability] {

  override protected val sourceName: ServiceName.Value = ServiceName.MitreService

  override protected def parsePublishedDate(publishedDate: String): String = {
    CvrfVulnerabilityDateParser.parseDate(publishedDate)
  }

}
