package it.unibo.launcher

import it.unibo.verticle.{GeneralDataAggregatorVerticle, VerticleLauncher}

object GeneralDataAggregatorLauncher extends App with VerticleLauncher {
  deployVerticle[GeneralDataAggregatorVerticle]()
}
