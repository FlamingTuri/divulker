package it.unibo.constants

import it.unibo.models.service.{ServiceKind, ServiceName}

case class GeneralAggregatorSourceInfo() extends SourceVerticleInfo {
  /** The service name */
  override val serviceName: ServiceName.Value = ServiceName.GeneralDataAggregator
  /** The service type */
  override val serviceKind: ServiceKind.Value = ServiceKind.GeneralAggregator
  /** The path accepted for websocket messages */
  override val WebSocketPath: String = "/general-aggregator"
}
