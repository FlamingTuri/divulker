package it.unibo.verticle

import io.vertx.scala.core.http.ServerWebSocket
import it.unibo.constants.{GeneralAggregatorSourceInfo, RegistryPaths, SourceVerticleInfo}
import it.unibo.handler.GeneralDataAggregatorHandlerMatcher
import it.unibo.handlers.WsHandler
import it.unibo.jackson.MarshallableImplicit._
import it.unibo.models.CustomTypes.YearMaskVulnerabilities
import it.unibo.models.general.aggregator.{GeneralAggregatorResponse, AggregatedVulnerabilityData}
import it.unibo.models.service.{ServiceInfo, ServiceKind, ServiceName}
import it.unibo.models.ws.messages.Entries
import it.unibo.models.ws.messages.aggregator.AggregatorRequestMessage
import it.unibo.models.ws.messages.operations.{Subscribe, UnSubscribe}
import it.unibo.updates.collections.MapUpdateUtils.MapOfMapExtensions
import it.unibo.utils.Logger
import it.unibo.verticle.aggregator.{DataAggregatorVerticle, WsConnectionUtility}

import scala.collection.mutable

class GeneralDataAggregatorVerticle extends DataAggregatorVerticle[
  AggregatedVulnerabilityData, YearMaskVulnerabilities[AggregatedVulnerabilityData],
  GeneralDataAggregatorVerticle, WsHandler[GeneralDataAggregatorVerticle]] {

  override protected val registerRequestUri: String = RegistryPaths.SourcesPath +
    s"?sourceKind=DistroAggregator,GeneralRetriever"
  override protected val verticleInfo: SourceVerticleInfo = GeneralAggregatorSourceInfo()
  val distros: mutable.Set[ServiceName.Value] = mutable.Set()
  override val data: YearMaskVulnerabilities[AggregatedVulnerabilityData] = mutable.SortedMap()
  override protected val handlerAssigner: GeneralDataAggregatorHandlerMatcher = GeneralDataAggregatorHandlerMatcher()
  override protected val serviceCondition: ServiceInfo => Boolean = _ => true

  override protected def getWsConnectionUtility: WsConnectionUtility[GeneralDataAggregatorVerticle] = {
    WsConnectionUtility(this, httpClient, vertx)
  }

  override def additionalOperation(serviceInfo: ServiceInfo): Unit = {
    serviceInfo.serviceType match {
      case ServiceKind.DistroAggregator => distros += serviceInfo.name
      case _ =>
    }
  }

  override protected def wsTextMessageHandler(serverWebSocket: ServerWebSocket, textMessage: String): Unit = {
    val requestMessage = textMessage.fromJson[AggregatorRequestMessage]()
    Logger.printOut(requestMessage)
    requestMessage.operation match {
      case Subscribe =>
        subscribers += serverWebSocket
        // TODO: the subscriber should specify which distro he is interested into
        //  the data should be filtered to contain only the cves related to such distros
        val dataList = data.getAsIterable
        Logger.printOut(dataList.size)
        notifyAggregatorSubscribers(Entries.newEntries(dataList), List(serverWebSocket))
      case UnSubscribe =>
        subscribers -= serverWebSocket
      case _ =>
    }
  }

  override protected def wsCloseHandler(serverWebSocket: ServerWebSocket): Unit = {
    subscribers -= serverWebSocket
  }

  override protected def getResponse(dataToSend: Entries[AggregatedVulnerabilityData]): String = {
    GeneralAggregatorResponse(verticleInfo.serviceName, distros, dataToSend).toJson
  }

}
