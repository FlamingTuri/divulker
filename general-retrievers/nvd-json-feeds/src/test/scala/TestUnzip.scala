import java.nio.file.Paths

import io.vertx.scala.core.Vertx
import it.unibo.utils.UnzipUtil
import org.scalatest.funsuite.AnyFunSuite

import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}

class TestUnzip extends AnyFunSuite {

  test("Test nvdcve unzip") {
    val res = getClass.getResource("nvdcve-1.1-2019.json.zip")
    val file = Paths.get(res.toURI).toFile
    val absolutePath = file.getAbsolutePath

    val unzipUtil = UnzipUtil()
    val vertx = Vertx.vertx()
    val fs = vertx.fileSystem()

    assert(fs.existsBlocking(absolutePath))
    fs.readFileFuture(absolutePath).onComplete {
      case Success(buffer) =>
        try {
          val result = unzipUtil.unzipInMemory(buffer)
          assertResult(1)(result.size)
        } catch {
          case e: Throwable =>
            e.printStackTrace()
            fail()
        } finally {
          vertx.close()
        }
      case Failure(exception) =>
        exception.printStackTrace()
        vertx.close()
        fail()
    }
  }

}
