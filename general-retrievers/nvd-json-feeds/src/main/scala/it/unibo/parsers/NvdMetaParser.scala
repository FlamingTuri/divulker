package it.unibo.parsers

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter._

import io.vertx.core.buffer.Buffer

trait NvdMetaParser {

  def getLocalDateTimeFromMetaFile(mvdMetaFileContent: String): LocalDateTime

  def areLocalDataOutdated(downloadedNvdMeta: Buffer, localNvdMeta: Buffer): Boolean
}

object NvdMetaParser {

  def apply(): NvdMetaParser = NvdMetaParserImpl()

  private case class NvdMetaParserImpl() extends NvdMetaParser {

    override def getLocalDateTimeFromMetaFile(nvdMetaFileContent: String): LocalDateTime = {
      nvdMetaFileContent.split('\n').find(line => line.contains("lastModifiedDate:")) match {
        case Some(value) =>
          val stringDate = value.stripPrefix("lastModifiedDate:").trim
          LocalDateTime.parse(stringDate, ISO_DATE_TIME)
        case None => throw new MatchError("received data are not correctly formatted")
      }
    }

    override def areLocalDataOutdated(downloadedNvdMeta: Buffer, localNvdMeta: Buffer): Boolean = {
      val downloadedMetaDate = getLocalDateTimeFromMetaFile(downloadedNvdMeta.toString())
      val localMetaDate = getLocalDateTimeFromMetaFile(localNvdMeta.toString())
      downloadedMetaDate.compareTo(localMetaDate) > 0
    }

  }

}
