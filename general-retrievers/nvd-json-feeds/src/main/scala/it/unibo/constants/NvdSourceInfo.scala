package it.unibo.constants

import it.unibo.models.service.{ServiceKind, ServiceName}

class NvdSourceInfo extends SourceVerticleInfo {
  /** The service name */
  override val serviceName: ServiceName.Value = ServiceName.NvdService
  /** The service type */
  override val serviceKind: ServiceKind.Value = ServiceKind.GeneralRetriever
  /** The path accepted for websocket messages */
  override val WebSocketPath: String = "/nvd"
}
