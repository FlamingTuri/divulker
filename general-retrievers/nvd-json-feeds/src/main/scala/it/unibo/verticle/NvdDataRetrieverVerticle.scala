package it.unibo.verticle

import java.time.Year

import it.unibo.constants.NvdSourceInfo
import it.unibo.nvd.NvdCve
import it.unibo.verticle.retriever.GeneralDataRetrieverVerticle
import it.unibo.verticle.retriever.configuration.DataRetrieverConfiguration


class NvdDataRetrieverVerticle extends GeneralDataRetrieverVerticle[NvdCve] {

  override protected val verticleInfo: NvdSourceInfo = new NvdSourceInfo()

  override protected def dataRetrieverConfigurations: List[DataRetrieverConfiguration] = {
    2002 to Year.now.getValue map getConfiguration toList
  }

  private def getConfiguration(year: Int): DataRetrieverConfiguration = {
    val serviceName = verticleInfo.serviceName
    val serviceKind = verticleInfo.serviceKind
    NvdDataRetrieverConfiguration(vertx, webClient, year, data, dataSubscriberManager, serviceName, serviceKind)
  }
}
