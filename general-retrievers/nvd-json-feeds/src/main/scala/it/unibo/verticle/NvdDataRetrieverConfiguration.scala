package it.unibo.verticle

import java.time.{LocalDateTime, ZoneId}
import java.util.Date

import io.vertx.core.buffer.Buffer
import io.vertx.scala.core.{Future, Vertx}
import io.vertx.scala.ext.web.client.WebClient
import it.unibo.data.subscriber.manager.GeneralDataSubscriberManager
import it.unibo.jackson.MarshallableImplicit._
import it.unibo.models.CustomTypes.YearMaskVulnerabilities
import it.unibo.models.service.{ServiceKind, ServiceName}
import it.unibo.models.ws.messages.Entries
import it.unibo.nvd.{NvdCve, NvdData}
import it.unibo.parsers.NvdMetaParser
import it.unibo.updates.GeneralSourceConfiguration
import it.unibo.updates.operation.result.UpdateResult
import it.unibo.utils.UnzipUtil
import it.unibo.verticle.retriever.configuration.LocalDataLoadWithPollingRetrieverConfiguration
import it.unibo.vertx.FileSystemUtils
import it.unibo.vertx.HttpResponseFix.fixHttpResponseFuture
import it.unibo.vertx.VertxHelpers._

case class NvdDataRetrieverConfiguration(override protected val vertx: Vertx,
                                         override protected val webClient: WebClient,
                                         private val year: Int,
                                         private val data: YearMaskVulnerabilities[NvdCve],
                                         private val dataSubscribersManager: GeneralDataSubscriberManager[NvdCve],
                                         override protected val serviceName: ServiceName.Value,
                                         override protected val serviceKind: ServiceKind.Value)
  extends LocalDataLoadWithPollingRetrieverConfiguration[NvdCve] with GeneralSourceConfiguration[NvdCve] {

  override protected val dataHost: String = "nvd.nist.gov"
  override protected val dataUri: String = s"/feeds/json/cve/1.1/nvdcve-1.1-$year.json.zip"
  protected val timestampUri: String = s"/feeds/json/cve/1.1/nvdcve-1.1-$year.meta"

  private val nvdMetaParser = NvdMetaParser()
  private val unzipUtil = UnzipUtil()

  private def toDate(localDateTime: LocalDateTime): Date = {
    Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant)
  }

  override def getActualTimestamp: Future[Date] = {
    webClient.get(dataHost, timestampUri).sendFuture.compose { value =>
      val buffer = value.httpResponse.body.get
      val localDateTime = nvdMetaParser.getLocalDateTimeFromMetaFile(buffer.toString())
      toDate(localDateTime)
    }
  }

  override def loadDownloadedData(buffer: Buffer): UpdateResult[NvdCve] = {
    val result = unzipUtil.unzipInMemory(buffer)
    result.map { entry =>
      val nvdData = entry.fromJson[NvdData]()
      generalSourceDataUpdate(data, nvdData.cveItems)
    }.fold(new UpdateResult[NvdCve]())((a, b) => {
      new UpdateResult(a.newEntries ++ b.newEntries, a.updatedEntries ++ b.updatedEntries)
    })
  }

  override def saveDownloadedData(buffer: Buffer): Future[Unit] = {
    // dirty hack to have all the data previous to 2002 in the same file, like it is offered on NVD
    val vulnerabilityList = if (year == 2002) {
      1999 to year flatMap (data(_).values.flatten)
    } else {
      data(year).values.flatten
    }
    super.saveDownloadedData(Buffer.buffer(vulnerabilityList.toJson))
  }

  override def notifySubscribers(operationResult: UpdateResult[NvdCve]): Unit = {
    operationResult.printEntriesNumber()
    dataSubscribersManager.groupUpdatesBySubscribers(operationResult.newEntries ++ operationResult.updatedEntries)
      .foreach(e => notifySubscribers(e._1, Entries.updatedEntries(e._2), e._3))
  }

  override protected val saveFolderName: String = "nvd"
  override protected val lastSaveTimestampFileName: String = s"nvdcve-1.1-$year.meta"
  override protected val savedDataFileName: String = s"nvdcve-1.1-$year.json"
  override protected val fsUtils: FileSystemUtils = FileSystemUtils(vertx.fileSystem())

  override def loadLocalData(buffer: Buffer): UpdateResult[NvdCve] = {
    val vulnerabilities = buffer.toString().fromJson[Array[NvdCve]]()
    generalSourceDataUpdate(data, vulnerabilities)
  }

}
