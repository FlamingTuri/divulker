package it.unibo.utils

import java.io._
import java.nio.file.Path
import java.util.zip.{ZipEntry, ZipInputStream}

import io.vertx.core.buffer.Buffer

trait UnzipUtil {

  /** Applies the specified strategy to each non folder file inside the zip archive
   *
   * @param zipInputStream          input stream of a zip archive
   * @param zipEntryParsingStrategy the strategy used to parse a zip entry
   * @tparam T type returned by the
   * @return a list with the result of each zipEntryParsingStrategy
   */
  def unzip[T](zipInputStream: InputStream,
               zipEntryParsingStrategy: (ZipInputStream, ZipEntry, Array[Byte]) => T): List[T]

  /**
   *
   * @param zipInputStream
   * @param destination
   */
  def unzipToDisk(zipInputStream: InputStream, destination: Path): Unit

  /** Unzips the archive represented by the buffer
   *
   * @param zipBuffer the vertx buffer of a zip file
   * @return a list with the content of each archive entry
   */
  def unzipInMemory(zipBuffer: Buffer): List[String]

  /** Unzips the archive returning each content
   *
   * @param zipInputStream input stream of a zip archive
   * @return a list with the content of each archive entry
   */
  def unzipInMemory(zipInputStream: InputStream): List[String]

  /** Unzips the file using a java like approach
   *
   * @param zipFile path to zip file
   */
  def unzipJavaImpl(zipFile: String): Unit
}

object UnzipUtil {

  def apply(): UnzipUtil = UnzipUtilImpl()

  private case class UnzipUtilImpl() extends UnzipUtil {

    private def toInputStream(buffer: Buffer): InputStream = {
      new ByteArrayInputStream(buffer.getByteBuf.array)
    }

    override def unzip[T](zipInputStream: InputStream,
                          zipEntryParsingStrategy: (ZipInputStream, ZipEntry, Array[Byte]) => T): List[T] = {
      val zis = new ZipInputStream(zipInputStream)
      val buffer = new Array[Byte](4096)
      val result = Stream.continually(zis.getNextEntry)
        .takeWhile(_ != null)
        // ignores zip directories
        .filterNot(_.isDirectory)
        .map(zipEntry => zipEntryParsingStrategy(zis, zipEntry, buffer))
        .toList
      zis.closeEntry()
      zis.close()
      result
    }

    override def unzipToDisk(zipInputStream: InputStream, destination: Path): Unit = {
      unzip(zipInputStream, (zis, zipEntry, buffer) => {
        val outPath = destination.resolve(zipEntry.getName)
        val outPathParent = outPath.getParent
        if (!outPathParent.toFile.exists) {
          outPathParent.toFile.mkdirs
        }
        val outFile = outPath.toFile
        val out = new FileOutputStream(outFile)
        // TODO
        Stream.continually(zis.read(buffer))
          .takeWhile(_ != -1)
          .foreach { dataRead =>
            //fs.writeFile("destination.json", buffer.toBuffer)
            //out.write(buffer, 0, _)
          }
        out.close()
      })
    }

    override def unzipInMemory(zipBuffer: Buffer): List[String] = {
      unzipInMemory(toInputStream(zipBuffer))
    }

    override def unzipInMemory(zipInputStream: InputStream): List[String] = {
      unzip(zipInputStream, (zis, zipEntry, buffer) => {
        val stringBuilder = new StringBuilder
        Stream.continually(zis.read(buffer))
          .takeWhile(_ != -1)
          .foreach(dataRead => stringBuilder.append(new String(buffer, 0, dataRead)))
        stringBuilder.mkString
      })
    }

    override def unzipJavaImpl(zipFile: String): Unit = {
      val buffer = new Array[Byte](1024)
      val zis = new ZipInputStream(new FileInputStream(zipFile))
      var zipEntry = zis.getNextEntry
      while (zipEntry != null) {
        val destinationFile = newFile(new File(zipFile).getParentFile, zipEntry)
        val fos = new FileOutputStream(destinationFile)
        var len = zis.read(buffer)
        while (len > 0) {
          fos.write(buffer, 0, len)
          len = zis.read(buffer)
        }
        fos.close()
        zipEntry = zis.getNextEntry
      }
      zis.closeEntry()
      zis.close()
    }

    /** Avoids Zip Slip Vulnerability
     *
     * @param destinationDir
     * @param zipEntry
     * @return
     */
    private def newFile(destinationDir: File, zipEntry: ZipEntry): File = {
      val destFile = new File(destinationDir, zipEntry.getName)

      val destDirPath = destinationDir.getCanonicalPath
      val destFilePath = destFile.getCanonicalPath

      if (!destFilePath.startsWith(destDirPath + File.separator)) {
        throw new IOException("Entry is outside of the target dir: " + zipEntry.getName)
      }

      destFile
    }

  }

}
