package it.unibo.launcher

import it.unibo.verticle.{NvdDataRetrieverVerticle, VerticleLauncher}

object NvdDataRetrieverLauncher extends App with VerticleLauncher {
  deployVerticle[NvdDataRetrieverVerticle]()
}
