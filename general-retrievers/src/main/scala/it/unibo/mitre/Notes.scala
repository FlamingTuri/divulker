package it.unibo.mitre

import com.fasterxml.jackson.dataformat.xml.annotation.{JacksonXmlElementWrapper, JacksonXmlProperty}

case class Notes(@JacksonXmlProperty(localName = "Note")
                 @JacksonXmlElementWrapper(useWrapping = false)
                 private val _notes: Array[Note]) {

  def items: Array[Note] = _notes

}
