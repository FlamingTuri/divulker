package it.unibo.mitre

import com.fasterxml.jackson.annotation.JsonGetter
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty


case class MitreCveItem(@JacksonXmlProperty(localName = "name", isAttribute = true)
                        @JsonGetter(value = "cveId")
                        private val _cveId: String) {

  private var _date: String = ""

  @JacksonXmlProperty(localName = "phase")
  protected def unpackDateFromNestedObject(phase: Phase): Unit = {
    _date = phase.date
  }

  @JsonGetter()
  def cveId: String = _cveId

  @JsonGetter()
  def date: String = _date
}

class Phase(@JacksonXmlProperty(localName = "date", isAttribute = true)
            private val _date: String) {

  @JsonGetter()
  val date: String = _date
}
