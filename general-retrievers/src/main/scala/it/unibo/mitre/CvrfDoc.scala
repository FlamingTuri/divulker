package it.unibo.mitre

import com.fasterxml.jackson.dataformat.xml.annotation.{JacksonXmlElementWrapper, JacksonXmlProperty}

class CvrfDoc {
  @JacksonXmlProperty(localName = "Vulnerability")
  @JacksonXmlElementWrapper(useWrapping = false)
  var vulnerabilities: Array[CvrfVulnerability] = _
}
