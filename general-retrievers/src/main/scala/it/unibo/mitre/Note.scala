package it.unibo.mitre

import com.fasterxml.jackson.dataformat.xml.annotation.{JacksonXmlProperty, JacksonXmlText}


case class Note() {

  @JacksonXmlText
  private var _text: String = _

  def text: String = _text

  @JacksonXmlProperty(localName = "Ordinal", isAttribute = true)
  private var _ordinal: String = _

  def ordinal: String = _ordinal

  @JacksonXmlProperty(localName = "Title", isAttribute = true)
  private var _title: String = _

  def title: String = _title

  @JacksonXmlProperty(localName = "Type", isAttribute = true)
  private var _type: String = _

  def typology: String = _type

  override def toString = s"Note($text, $ordinal, $title, $typology)"
}
