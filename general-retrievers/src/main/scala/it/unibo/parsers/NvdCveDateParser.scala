package it.unibo.parsers

import it.unibo.nvd.NvdCve

object NvdCveDateParser extends DateParser {

  private val dateRegex = "(\\d{4}-\\d{2}-\\d{2}).*".r

  def parseDate(nvdCve: NvdCve): String = {
    parseDate(nvdCve.publishedDate)
  }

  override def parseDate(date: String): String = {
    date match {
      case dateRegex(date) => date
      case value => Parser.throwParseError(value)
    }
  }

}
