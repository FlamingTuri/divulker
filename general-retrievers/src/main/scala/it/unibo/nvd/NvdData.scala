package it.unibo.nvd

import com.fasterxml.jackson.annotation.{JsonIgnoreProperties, JsonProperty}

import scala.collection.mutable.ListBuffer

@JsonIgnoreProperties(ignoreUnknown = true)
case class NvdData(@JsonProperty("CVE_data_type")
                   private var _cveDataType: String,
                   @JsonProperty("CVE_data_format")
                   private var _cveDataFormat: String,
                   @JsonProperty("CVE_data_version")
                   private var _cveDataVersion: String,
                   @JsonProperty("CVE_data_numberOfCVEs")
                   private var _cveDataNumberOfCve: String,
                   @JsonProperty("CVE_data_timestamp")
                   private var _cveDataTimestamp: String,
                   @JsonProperty("CVE_Items")
                   private var _cveItems: ListBuffer[NvdCve]) {

  def cveDataType: String = _cveDataType

  def cveDataFormat: String = _cveDataFormat

  def cveDataVersion: String = _cveDataVersion

  def cveDataNumberOfCve: String = _cveDataNumberOfCve

  def cveDataTimestamp: String = _cveDataTimestamp

  def cveItems: ListBuffer[NvdCve] = _cveItems
}
