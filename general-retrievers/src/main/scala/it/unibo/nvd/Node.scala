package it.unibo.nvd

import com.fasterxml.jackson.annotation.{JsonGetter, JsonProperty, JsonSetter}

case class Node() {

  @JsonProperty(value = "cpe_match", required = false)
  private var _cpeMatch: List[NvdCpe] = List()

  @JsonProperty(value = "children", required = false)
  private var _children: List[Node] = List()

  @JsonGetter("cpe_match")
  def cpeMatch: List[NvdCpe] = _cpeMatch

  @JsonSetter("cpe_match")
  def cpeMatch_=(list: List[NvdCpe]): Unit = _cpeMatch = list

  @JsonGetter()
  def children: List[Node] = _children

  @JsonSetter()
  def children_=(list: List[Node]): Unit = _children = list

}
