package it.unibo.nvd

import com.fasterxml.jackson.annotation.{JsonGetter, JsonIgnore, JsonProperty, JsonSetter}
import it.unibo.models.GeneralCve
import it.unibo.models.cvss.Impact

case class NvdCve(@JsonProperty(value = "cveId")
                  override protected var _cveId: String = "",
                  @JsonProperty(value = "publishedDate", required = true)
                  private var _publishedDate: String,
                  @JsonProperty(value = "impact")
                  protected var _impact: Impact = Impact(None, None) /*,
                  @JsonProperty("affectedProducts")
                  protected var _affectedProducts: mutable.Set[String] = mutable.Set()*/) extends GeneralCve {

  @JsonProperty(value = "cve", required = true)
  def unpackNameFromNestedObject(cve: Map[String, Object]): Unit = {
    _cveId = cve("CVE_data_meta").asInstanceOf[Map[String, String]]("ID")
  }

  //@JsonGetter("cpe")
  //def affectedProducts: mutable.Set[String] = _affectedProducts

  /*@JsonProperty(value = "configurations")
  def unpackName(configurations: Map[String, Object]): Unit = {
    val list = configurations("nodes").asInstanceOf[List[Map[String, Object]]]
    _affectedProducts = parseCpeProduct(list)
  }

  private def parseCpeProduct(list: List[Map[String, Object]]): mutable.Set[String] = {
    val result = mutable.Set[String]()
    list.foreach { e =>
      e.get("cpe_match") match {
        case Some(value) =>
          value.asInstanceOf[List[Map[String, Object]]]
            .map(cpeMatch => NvdCpe(cpeMatch("vulnerable").asInstanceOf[Boolean], cpeMatch("cpe23Uri").asInstanceOf[String]))
            // TODO removes the entries that have vulnerable property = false,
            //  however is not really clear how they should be treated,
            //  since the nvd database show them as vulnerable
            .filter(_.vulnerable)
            .foreach(result += _.affectedProduct)
        case None =>
      }
      e.get("children") match {
        case Some(value) => result ++= parseCpeProduct(value.asInstanceOf[List[Map[String, Object]]])
        case None =>
      }
    }
    result
  }*/

  @JsonGetter()
  override def publishedDate: String = _publishedDate

  @JsonSetter("publishedDate")
  override def publishedDate_=(publishedDate: String): Unit = _publishedDate = publishedDate

  @JsonGetter()
  def impact: Impact = _impact
}
