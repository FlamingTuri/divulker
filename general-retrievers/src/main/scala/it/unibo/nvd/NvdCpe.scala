package it.unibo.nvd

import it.unibo.models.cpe.Cpe23

case class NvdCpe(vulnerable: Boolean, cpe23Uri: String) {

  def affectedProduct: String = Cpe23.affectedProduct(cpe23Uri)

}
