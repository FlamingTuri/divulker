package it.unibo.updates

import io.vertx.scala.core.http.ServerWebSocket
import it.unibo.jackson.MarshallableImplicit._
import it.unibo.models.CustomTypes.YearMaskVulnerabilities
import it.unibo.models.GeneralCve
import it.unibo.models.service.{ServiceKind, ServiceName}
import it.unibo.models.ws.messages.Entries
import it.unibo.models.ws.messages.general.GeneralSourceResponseMessage
import it.unibo.updates.collections.MapUpdateUtils.MapOfMapExtensions
import it.unibo.updates.operation.result.UpdateResult
import it.unibo.vertx.WebSocketHelpers.WebSocketExtensions

import scala.collection.mutable

trait GeneralSourceConfiguration[D <: GeneralCve] {

  protected val serviceName: ServiceName.Value
  protected val serviceKind: ServiceKind.Value

  def generalSourceDataUpdate(data: YearMaskVulnerabilities[D],
                              vulnerabilities: Iterable[D]): UpdateResult[D] = {
    data.insertOrUpdate(vulnerabilities, entry => entry.cveId, (current, update) => {
      val updated = current.publishedDate != update.publishedDate
      if (updated) {
        current.publishedDate = update.publishedDate
      }
      updated
    })
  }

  protected def notifySubscribers(subscribers: mutable.Set[ServerWebSocket],
                                  entries: Entries[D],
                                  distroName: String): Unit = {
    if (subscribers.nonEmpty) {
      val response = GeneralSourceResponseMessage(serviceName, serviceKind, distroName, entries)
      val jsonResponse = response.toJson
      subscribers.foreach(_.writeTextMessageFramed(jsonResponse))
    }
  }

}
