application {
    mainClassName = "it.unibo.Launcher"
    extra.set("mainClassName", mainClassName)
}

fun downloadLibFromUrl(libName: String, libUrl: String) {
    val gradleLibSaveDir = "${System.getProperty("user.home")}/.gradle/caches/modules-2/files-2.1"
    val folder = File(gradleLibSaveDir)
    if (!folder.exists()) {
        folder.mkdirs()
    }
    val file = File("$gradleLibSaveDir/$libName")
    if (!file.exists()) {
        ant.invokeMethod("get", mapOf("src" to libUrl, "dest" to file))
    }
    dependencies.add("implementation", files(file.absolutePath))
}

dependencies {
    implementation("com.squareup.okhttp3:okhttp:4.4.1")
    // TODO: this library will be deprecated and should be replaced with the newer one.
    //   Such library will be released when I will come up with a better name and
    //   I will have some free time
    val version = "1.1.1"
    val libName = "multi-module-deployer-${version}.jar"
    val host = "github.com"
    val libraryUri = "/FlamingTuri/multi-module-deployer/releases/download/v$version/$libName"
    val url = "https://$host$libraryUri"
    downloadLibFromUrl(libName, url)
}
