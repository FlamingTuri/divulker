package it.unibo

import java.io.IOException

import io.vertx.core.AsyncResult
import io.vertx.core.buffer.Buffer
import io.vertx.ext.web.client.HttpResponse
import io.vertx.scala.core.Vertx
import it.unibo.constants.Constants.{pathInsideSaveDirectory, pathToString}
import it.unibo.models.RegistryConfigData
import it.unibo.models.service.ServiceName
import it.unibo.network.{AvailablePortScanner, RegistryUtils}
import it.unibo.utils.Resources
import it.unibo.vertx.FileSystemUtils
import multi.module.deployer.moduleconfig.ModuleConfig
import multi.module.deployer.moduleconfig.ModuleConfigFactory.httpModuleConfig
import okhttp3.{OkHttpClient, Request, Response}

/** Utilities for running and checking microservices deployment */
object LauncherUtils {

  val availablePortsScanner: AvailablePortScanner = new AvailablePortScanner()
  private val httpClient = new OkHttpClient()
  val sourcesPath: String = "/registry/sources"
  private val configData = {
    val configPath = pathInsideSaveDirectory(RegistryUtils.registryLocationConfigName)
    val vertx = Vertx.vertx()
    val fs = vertx.fileSystem()
    val jsonObject = (if (FileSystemUtils(fs).checkFilesExistence(configPath)) {
      fs.readFileBlocking(configPath)
    } else {
      Buffer.buffer(Resources.readAsString(RegistryUtils.registryLocationConfigName))
    }).toJsonObject
    vertx.close()
    RegistryConfigData(jsonObject.getString("host"), jsonObject.getInteger("port"))
  }

  /** Utility to convert a service name enum to string
   *
   * @param serviceName the service name
   * @return the service string representation
   */
  implicit def enumToString(serviceName: ServiceName.Value): String = serviceName.toString

  /** Returns the registry uri for the current service
   *
   * @param serviceName the service name
   * @return the service request uri
   */
  def serviceRequestUri(serviceName: String): String = s"$sourcesPath/$serviceName"

  /** Returns a module that runs the specified command and waits until
   * the specified service appears in the registry service list
   *
   * @param linuxCmd    the linux command used to run the service
   * @param windowsCmd  the windows command used to run the service
   * @param serviceName the service name
   * @return the module config
   */
  def waitServiceDeployment(linuxCmd: String, windowsCmd: String,
                            serviceName: String): ModuleConfig[AsyncResult[HttpResponse[Buffer]]] = {
    val requestUri = serviceRequestUri(serviceName)
    httpModuleConfig(linuxCmd, windowsCmd, configData.port, configData.host, requestUri)
      .setSuccessCondition(ar => ar.succeeded && (ar.result.statusCode == 200))
  }

  /** Checks whether the specified service is present in the registry service list
   *
   * @param serviceName the service name
   * @return whether the specified service is present in the registry service list
   */
  def isNotDeployed(serviceName: String): Boolean = {
    var isNotDeployed = false
    val url = s"http://${configData.host}:${configData.port}/registry/sources/$serviceName"
    val request = new Request.Builder().url(url).build()
    var response: Response = null
    try {
      response = httpClient.newCall(request).execute
      if (!response.isSuccessful) {
        throw new IOException("Unexpected code " + response)
      }
      // Get response body
      //System.out.println(response.body().string());
      isNotDeployed = false
    } catch {
      case e: IOException =>
        //e.printStackTrace();
        isNotDeployed = true
    } finally {
      if (response != null) {
        response.close()
      }
    }
    isNotDeployed
  }

}
