package it.unibo

import java.nio.file.Paths

import it.unibo.LauncherUtils._
import it.unibo.models.service.ServiceName
import multi.module.deployer.MultiModuleDeployer
import multi.module.deployer.moduleconfig.ModuleConfigFactory.{httpModuleConfig, noDeploymentWaitModuleConfig, setupModuleConfig}

/** Utility to run the microservices in the correct order */
object Launcher extends App {

  val multiModuleDeployer = new MultiModuleDeployer(false)
  // this will work only if the working directory is /path/to/divulner
  val currentPath = Paths.get("").toAbsolutePath.toString
  val projectDirectoryRegex = "(.*divulker)(.launcher)?".r
  val projectDir = currentPath match {
    case projectDirectoryRegex(dir, _) => dir
    case _ => throw new IllegalStateException()
  }

  val gradleLinuxCmd = (gradleCmd: String) => s"cd '$projectDir' && ./gradlew $gradleCmd"
  val gradleWindowsCmd = (gradleCmd: String) => s"cd $projectDir && gradlew.bat $gradleCmd"
  var commonCmd = ""
  var linuxCmd = ""
  var windowsCmd = ""

  private def runIfNotDeployed(serviceName: ServiceName.Value, gradleCommand: String): Unit = {
    if (isNotDeployed(serviceName)) {
      val linuxCmd = gradleLinuxCmd(gradleCommand)
      val windowsCmd = gradleWindowsCmd(gradleCommand)
      val module = waitServiceDeployment(linuxCmd, windowsCmd, serviceName)
      multiModuleDeployer.add(module)
    }
  }


  commonCmd = "build"
  linuxCmd = gradleLinuxCmd(commonCmd)
  windowsCmd = gradleWindowsCmd(commonCmd)
  val build = setupModuleConfig(linuxCmd, windowsCmd)
  multiModuleDeployer.add(build)

  val registryPort = 8080
  val registryAddress = "127.0.0.1"
  if (availablePortsScanner.isPortAvailable(registryPort)) {
    commonCmd = ":registry:run"
    linuxCmd = gradleLinuxCmd(commonCmd)
    windowsCmd = gradleWindowsCmd(commonCmd)
    val registryConfig = httpModuleConfig(linuxCmd, windowsCmd, registryPort, registryAddress, sourcesPath)
    multiModuleDeployer.add(registryConfig)
  }

  // --- general ---
  runIfNotDeployed(ServiceName.MitreService, ":general-retrievers:mitre-cvrf-cve-list:run")
  runIfNotDeployed(ServiceName.NvdService, ":general-retrievers:nvd-json-feeds:run")

  // --- debian ---
  runIfNotDeployed(ServiceName.DebianCveService, ":debian:debian-cve:run")
  runIfNotDeployed(ServiceName.DsaService, ":debian:debian-security-advisory:run")
  runIfNotDeployed(ServiceName.DebianSnapshotInfoService, ":debian:snapshot-info:run")

  // --- red hat ---
  runIfNotDeployed(ServiceName.RedHatCveDatesService, ":red-hat:red-hat-cve-dates:run")
  runIfNotDeployed(ServiceName.RhsaReleaseDates, ":red-hat:red-hat-rhsa-release-dates:run")

  // --- aggregator ---
  runIfNotDeployed(ServiceName.DebianDataAggregator, ":debian:data-aggregator:run")
  runIfNotDeployed(ServiceName.RedHatDataAggregator, ":red-hat:data-aggregator:run")
  runIfNotDeployed(ServiceName.GeneralDataAggregator, ":general-retrievers:data-aggregator:run")

  // --- comparator ---
  runIfNotDeployed(ServiceName.DistroDataComparatorService, ":distro-data-comparator:run")

  // --- web gui ---
  if (availablePortsScanner.isPortAvailable(4200)) {
    commonCmd = s"$projectDir cd web-gui && ng serve -o --poll=5000"
    // --host 0.0.0.0"; //--ssl=true --sslCert=cert/cert.pem --sslKey=cert/key.pem
    val webGuiConfig = noDeploymentWaitModuleConfig(commonCmd, commonCmd)
    multiModuleDeployer.add(webGuiConfig)
  }
  // deploys the modules
  multiModuleDeployer.deploy()

}
