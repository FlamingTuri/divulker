plugins {
    java
    idea
}

allprojects {
    apply(plugin = "idea")

    version = "1.1.0"

    repositories {
        mavenCentral()
        jcenter()
    }

    tasks.create<Delete>("cleanBin") {
        group = "build"
        val folderToRemove = "bin"
        description = "removes $folderToRemove directories"
        delete = setOf(folderToRemove)
    }

    tasks.withType<JavaCompile>().configureEach {
        options.compilerArgs.add("-XX:ReservedCodeCacheSize=393216") // 2^18+2^17
    }
}

subprojects {
    apply(plugin = "java")
    apply(plugin = "scala")

    java {
        val javaVersion = JavaVersion.VERSION_1_8
        sourceCompatibility = javaVersion
        targetCompatibility = javaVersion
    }

    extra.apply {
        set("scalaVersion", "2.12")
        set("vertxVersion", "3.9.0")
        set("jacksonVersion", "2.11.0")
    }

    dependencies {
        val scalaVersion: String? by project.extra
        implementation("org.scala-lang", "scala-library", "$scalaVersion.11")
        implementation("org.scala-lang.modules", "scala-xml_$scalaVersion", "1.2.0")

        val jacksonVersion: String? by project.extra
        implementation("com.fasterxml.jackson.core", "jackson-databind", jacksonVersion)
        implementation("com.fasterxml.jackson.module", "jackson-module-scala_$scalaVersion", jacksonVersion)
        implementation("com.fasterxml.jackson.dataformat", "jackson-dataformat-xml", jacksonVersion)

        val vertxVersion: String? by project.extra
        implementation("io.vertx", "vertx-lang-scala_$scalaVersion", vertxVersion)
        implementation("io.vertx", "vertx-web-scala_$scalaVersion", vertxVersion)
        implementation("io.vertx", "vertx-web-client-scala_$scalaVersion", vertxVersion)

        testImplementation("org.scalatest", "scalatest_$scalaVersion", "3.2.0-M2")
        testImplementation("junit", "junit", "4.12")
    }

    tasks {
        val scalaOptions = listOf(
            "-encoding", "utf8",
            "-Xfatal-warnings",
            "-deprecation",
            "-unchecked",
            "-feature",
            "-language:implicitConversions",
            "-language:higherKinds",
            "-language:existentials",
            "-language:postfixOps"
        )

        withType<ScalaCompile>().configureEach {
            scalaCompileOptions.additionalParameters = scalaOptions
        }

        withType<ScalaDoc>().configureEach {
            scalaDocOptions.additionalParameters = scalaOptions
        }

        val scalaTest = register("scalaTest") {
            group = "Verification"
            description = "Runs the scala tests"
            dependsOn(listOf("build"))
            // JavaExec tasks can't declare main, args and classpath inside doLast block and so
            // they run even when not being called. This exploit avoids this annoying behaviour
            doLast {
                // necessary for running scala tests without adding
                // @RunWith(classOf[JUnitRunner]) to each test class
                create<JavaExec>("scalaTestExec") {
                    main = "org.scalatest.tools.Runner"
                    args = listOf("-R", "build/classes/scala/test", "-o")
                    // runtimeClasspath is used instead of compileClasspath
                    // so that test classes can read inside test/resources folder
                    classpath = sourceSets.test.get().runtimeClasspath
                }.exec()
            }
        }

        // running ./gradlew build implicitly runs test task
        /*test {
            // after test task runs scalaTest task
            finalizedBy(scalaTest)
        }*/
    }

}

configure(subprojects.filter { it.name == "common" }) {

    // creates jar library with classes declared inside test directory
    val testJar = tasks.register<Jar>("testJar") {
        archiveClassifier.set("test")
        from(sourceSets.test.get().output)
    }

    artifacts {
        testImplementation(testJar)
    }
}

val modulesWithoutCommon = listOf("common")

configure(subprojects.filterNot { modulesWithoutCommon.contains(it.name) }) {

    dependencies {
        implementation(project(":common"))
        // makes :common test classes available to module's ones
        testImplementation(project(":common", "runtimeElements"))
    }
}

val sources = listOf("general-retrievers", "debian", "red-hat", "arch")

configure(subprojects.filter { sources.contains(it.name) }.flatMap { it.subprojects }) {
    if (parent != null) {
        dependencies {
            implementation(project(parent!!.path))
        }
    }
}

configure(subprojects.filterNot { modulesWithoutCommon.contains(it.name) || sources.contains(it.name) }) {
    apply(plugin = "application")

    tasks.register<Jar>("fatJar") {
        group = "build"
        dependsOn("build", configurations.runtimeClasspath)
        description = "Creates runnable jar"

        archiveClassifier.set("all")
        duplicatesStrategy = DuplicatesStrategy.EXCLUDE

        // subprojects should set the project.extra mainClassName property value
        val mainClassName: String? by project.extra
        manifest {
            attributes(mapOf(
                "Implementation-Version" to rootProject.version,
                "Main-Class" to mainClassName
            ))
        }

        from(sourceSets.main.get().output)

        from(configurations.runtimeClasspath.get()
            .filter { it.name.endsWith("jar") }
            .map { zipTree(it) })
    }
}

val retrievers = listOf("debian-cve", "debian-security-advisory", "red-hat-cve-dates",
    "red-hat-rhsa-release-dates", "mitre-cvrf-cve-list", "nvd-json-feeds")
configure(subprojects.filter { retrievers.contains(it.name) }) {

    val run by tasks.getting(JavaExec::class) {

        val local = "downloadOutdated" //-PdownloadOutdated=true/false
        project.properties[local]?.let {
            environment(local, it)
        }

    }
}

tasks {
    create<Delete>("cleanSaveFolder") {
        group = "build"
        val folderToRemove = "${System.getProperty("user.home")}${File.separator}.divulker"
        description = "removes $folderToRemove"
        delete = setOf(folderToRemove)
    }

    test {
        useJUnitPlatform()
    }
}
