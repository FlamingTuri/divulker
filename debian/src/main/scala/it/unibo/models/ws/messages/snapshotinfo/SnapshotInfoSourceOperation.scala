package it.unibo.models.ws.messages.snapshotinfo

import com.fasterxml.jackson.core.`type`.TypeReference

object SnapshotInfoSourceOperation extends Enumeration {
  type SnapshotInfoSourceOperation = Value
  val Subscribe, AddPackages, Unsubscribe = Value
}

class SnapshotInfoSourceOperationType extends TypeReference[SnapshotInfoSourceOperation.type]
