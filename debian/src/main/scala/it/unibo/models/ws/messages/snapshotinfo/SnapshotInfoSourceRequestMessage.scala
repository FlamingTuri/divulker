package it.unibo.models.ws.messages.snapshotinfo

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.module.scala.JsonScalaEnumeration
import it.unibo.models.ws.messages.WsRequestMessage

case class SnapshotInfoSourceRequestMessage(@JsonProperty(value = "operation", required = true)
                                            @JsonScalaEnumeration(classOf[SnapshotInfoSourceOperationType])
                                            private val _operation: SnapshotInfoSourceOperation.Value,
                                            requestedPackagesVersions: Iterable[RequestedPackageVersions] = Iterable(),
                                            downloadMissing: Boolean = true)
  extends WsRequestMessage[SnapshotInfoSourceOperation.Value](_operation)
