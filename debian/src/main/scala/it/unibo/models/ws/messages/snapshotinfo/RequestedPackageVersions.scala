package it.unibo.models.ws.messages.snapshotinfo

import scala.collection.mutable

case class RequestedPackageVersions(packageName: String, packageVersions: mutable.Set[String])
