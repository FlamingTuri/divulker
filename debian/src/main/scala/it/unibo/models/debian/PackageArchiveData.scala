package it.unibo.models.debian

import com.fasterxml.jackson.annotation.{JsonGetter, JsonProperty, JsonSetter}

case class PackageArchiveData(archive: String,
                              @JsonProperty("date")
                              private var _date: String) {

  @JsonGetter
  def date: String = _date

  @JsonSetter("date")
  def date_=(newDate: String): Unit = _date = newDate

}
