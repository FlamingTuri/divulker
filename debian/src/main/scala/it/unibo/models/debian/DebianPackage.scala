package it.unibo.models.debian

import scala.collection.mutable.ListBuffer

case class DebianPackage(name: String, fixList: ListBuffer[DebianFix])

