package it.unibo.models.debian

object PackageCategory {

  private val prefix = "lib"

  /** Gets the value to be used as key in the map storing the information
   *
   * @param packageName the package name
   * @return the value to use as key
   */
  def getPackageCategory(packageName: String): String = {
    // lib packages are a lot, this helps by creating smaller lists and speed up find operation
    if (packageName.startsWith(prefix)) packageName.take(4) else packageName.take(1)
  }

}
