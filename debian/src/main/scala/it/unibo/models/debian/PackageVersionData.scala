package it.unibo.models.debian

import scala.collection.mutable

case class PackageVersionData(version: String, packageArchiveData: mutable.Set[PackageArchiveData])
