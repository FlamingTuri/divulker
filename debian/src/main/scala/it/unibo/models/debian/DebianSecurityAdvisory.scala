package it.unibo.models.debian

import java.time.LocalDate
import java.time.format.DateTimeFormatter

import com.fasterxml.jackson.annotation.{JsonGetter, JsonProperty, JsonSetter}
import it.unibo.models.Key
import it.unibo.parsers.{DateParser, Parser}

import scala.collection.mutable.ListBuffer

object DebianSecurityAdvisory extends DateParser {

  private val formatter = DateTimeFormatter.ofPattern("dd MMM yyyy")

  implicit def parseDate(dsa: DebianSecurityAdvisory): String = {
    parseDate(dsa.releaseDate)
  }

  override def parseDate(date: String): String = {
    try {
      LocalDate.parse(date, formatter).toString
    } catch {
      case e: Throwable => Parser.throwParseError(date)
    }
  }

}

case class DebianSecurityAdvisory(@JsonProperty(value = "dsaId", required = true)
                                  private val _dsaId: String,
                                  @JsonProperty(value = "releaseDate", required = true)
                                  private var _releaseDate: String,
                                  @JsonProperty(value = "affectedPackage")
                                  private var _affectedPackage: String,
                                  @JsonProperty(value = "relatedCveIds")
                                  private var _relatedCveIds: ListBuffer[String] = ListBuffer())
  extends Key {

  @JsonGetter("dsaId")
  def dsaId: String = _dsaId

  @JsonGetter("releaseDate")
  def releaseDate: String = _releaseDate

  @JsonSetter("releaseDate")
  def releaseDate_=(releaseDate: String): Unit = _releaseDate = releaseDate

  @JsonGetter("relatedCveIds")
  def relatedCveIds: ListBuffer[String] = _relatedCveIds

  @JsonSetter("relatedCveIds")
  def relatedCveIds_=(relatedCveIds: ListBuffer[String]): Unit = _relatedCveIds = relatedCveIds

  def addRelatedCve(cveId: String): Unit = _relatedCveIds += cveId

  @JsonGetter("affectedPackage")
  def affectedPackage: String = _affectedPackage

  override def getKey: String = dsaId
}
