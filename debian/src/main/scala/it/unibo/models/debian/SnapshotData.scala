package it.unibo.models.debian

import scala.collection.mutable.ListBuffer

case class SnapshotData(packageName: String, packageVersionDataList: ListBuffer[PackageVersionData])
