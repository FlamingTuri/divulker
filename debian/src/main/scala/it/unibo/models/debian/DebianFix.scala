package it.unibo.models.debian

case class DebianFix(debianRelease: String, fixedVersion: String)
