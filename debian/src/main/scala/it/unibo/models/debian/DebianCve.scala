package it.unibo.models.debian

import com.fasterxml.jackson.annotation.{JsonGetter, JsonProperty, JsonSetter}
import it.unibo.models.Key

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

case class DebianCve(@JsonProperty("cveId")
                     private val _cveId: String,
                     @JsonProperty("affectedPackages")
                     private var _affectedPackages: mutable.ListMap[String, ListBuffer[DebianFix]] = mutable.ListMap(),
                     @JsonProperty("securityAdvisory")
                     private var _relatedDsaList: ListBuffer[String] = ListBuffer(),
                     @JsonProperty("longTermSupportAdvisory")
                     private var _relatedDlaList: ListBuffer[String] = ListBuffer())
  extends Key {

  @JsonGetter()
  def cveId: String = _cveId

  @JsonGetter()
  def affectedPackages: mutable.ListMap[String, ListBuffer[DebianFix]] = _affectedPackages

  @JsonSetter("affectedPackages")
  def affectedPackages_=(affectedPackages: mutable.ListMap[String, ListBuffer[DebianFix]]): Unit = _affectedPackages = affectedPackages

  def addAffectedPackage(debianRelease: String, packageName: String, fixedVersion: String): Unit = {
    _affectedPackages.get(packageName) match {
      case Some(value) => value += DebianFix(debianRelease, fixedVersion)
      case None => _affectedPackages += (packageName -> ListBuffer(DebianFix(debianRelease, fixedVersion)))
    }
  }

  @JsonGetter()
  def securityAdvisory: ListBuffer[String] = _relatedDsaList

  @JsonSetter("securityAdvisory")
  def securityAdvisory_=(relatedDsa: ListBuffer[String]): Unit = _relatedDsaList = relatedDsa

  @JsonGetter()
  def longTermSupportAdvisory: ListBuffer[String] = _relatedDlaList

  @JsonSetter("longTermSupportAdvisory")
  def longTermSupportAdvisory_=(relatedDla: ListBuffer[String]): Unit = _relatedDlaList = relatedDla

  def addSecurityAdvisory(dsa: String): Unit = _relatedDsaList += dsa

  def addLongTermSupportAdvisory(dla: String): Unit = _relatedDlaList += dla

  override def getKey: String = cveId
}
