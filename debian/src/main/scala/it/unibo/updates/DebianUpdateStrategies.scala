package it.unibo.updates

import it.unibo.models.debian.{DebianCve, DebianSecurityAdvisory}
import it.unibo.utils.UncategorizedUtils

object DebianUpdateStrategies {

  /** Updates the current debian cve DSA and DLA.
   *
   * @param current the current debian cve
   * @param update the updated version of the debian cve
   * @return whether the current debian cve was updated
   */
  def debianCveUpdateStrategy(current: DebianCve, update: DebianCve): Boolean = {
    val dsaUpdated = UncategorizedUtils.applyIfTrue(current.securityAdvisory != update.securityAdvisory,
      () => current.securityAdvisory = update.securityAdvisory)
    val dlaUpdated = UncategorizedUtils.applyIfTrue(current.securityAdvisory != update.securityAdvisory,
      () => current.longTermSupportAdvisory = update.longTermSupportAdvisory)
    dsaUpdated || dlaUpdated
  }

  /** Updates the current debian security advisory.
   *
   * @param current the current debian security advisory
   * @param update the updated version of the debian security advisory
   * @return whether the current debian security advisory was updated
   */
  def dsaUpdateStrategy(current: DebianSecurityAdvisory, update: DebianSecurityAdvisory): Boolean = {
    val releaseDateUpdated = UncategorizedUtils.applyIfTrue(current.releaseDate != update.releaseDate,
      () => current.releaseDate = update.releaseDate)
    val relatedCveIdsUpdated = UncategorizedUtils.applyIfTrue(current.relatedCveIds != update.relatedCveIds,
      () => current.relatedCveIds = update.relatedCveIds)
    releaseDateUpdated || relatedCveIdsUpdated
  }
}
