package it.unibo.constants

trait DebianSecurityTrackerConstants {
  val Host: String = "salsa.debian.org"
  /** Security tracker repository uri */
  val DebianSecurityTrackerUri: String = "/security-tracker-team/security-tracker"
}

object DebianSecurityTrackerConstants extends DebianSecurityTrackerConstants