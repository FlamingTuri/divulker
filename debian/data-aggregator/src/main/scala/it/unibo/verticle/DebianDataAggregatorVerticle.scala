package it.unibo.verticle

import it.unibo.constants.{DebianDataTrackerSourceInfo, RegistryPaths}
import it.unibo.handlers.HandlerMatchers
import it.unibo.handlers.assigner.SpecificDataSourceHandlerAssigner
import it.unibo.models.DebianGatheredData
import it.unibo.models.service.ServiceName._
import it.unibo.models.service.{ServiceInfo, ServiceName}
import it.unibo.verticle.aggregator.SpecificDataAggregatorVerticle

class DebianDataAggregatorVerticle extends SpecificDataAggregatorVerticle[DebianGatheredData] {

  override protected val registerRequestUri: String = RegistryPaths.SourcesPath + "?sourceKind=DistroSpecificRetriever"
  override protected val verticleInfo: DebianDataTrackerSourceInfo = new DebianDataTrackerSourceInfo()
  protected val distroSpecificDataSources: Seq[ServiceName.Value] = {
    Seq(DsaService, DlaService, DebianCveService, DebianSnapshotInfoService)
  }
  override protected val serviceCondition: ServiceInfo => Boolean = serviceInfo => {
    distroSpecificDataSources.contains[ServiceName.Value](serviceInfo.name)
  }
  override val handlerAssigner: SpecificDataSourceHandlerAssigner[DebianGatheredData] = HandlerMatchers()

}
