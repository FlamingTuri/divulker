package it.unibo.handlers

import io.vertx.scala.core.http.WebSocket
import it.unibo.jackson.MarshallableImplicit._
import it.unibo.models.DebianGatheredData
import it.unibo.models.aggregator.Source
import it.unibo.models.debian.DebianCve
import it.unibo.models.service.ServiceName
import it.unibo.models.ws.messages.Entries
import it.unibo.models.ws.messages.distro.DistroSourceResponseMessage
import it.unibo.models.ws.messages.snapshotinfo.{RequestedPackageVersions, SnapshotInfoSourceOperation, SnapshotInfoSourceRequestMessage}

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

/** Message handler for DebianCveService */
class DebianRelatedCveHandler extends DebianSpecificDataSourceWsHandler {

  override protected val sourceName: ServiceName.Value = ServiceName.DebianCveService

  override protected def textMessageHandler(webSocket: WebSocket, text: String): Unit = {
    val response = text.fromJson[DistroSourceResponseMessage[DebianCve]]()
    val updates = response.data.newEntries ++ response.data.updatedEntries
    val operationResult = verticle.data.insertOrUpdate(updates, updatePolicy, newEntryPolicy)
    val updatedEntries = operationResult.updatedEntries
    val newEntries = operationResult.newEntries
    operationResult.printEntriesNumber()
    sendAffectedPackagesToSnapshotInfo(updatedEntries ++ newEntries)
    notifySubscribers(Entries(operationResult))
  }

  private def comparePolicy(current: DebianGatheredData, update: DebianCve): Boolean = {
    current.cveId == update.cveId
  }

  private def updatePolicy(current: DebianGatheredData, update: DebianCve): Boolean = {
    // TODO: the lts support advisories are ignored
    val affectedPackagesUpdated = update.affectedPackages
      .map { e =>
        val packageName = e._1
        e._2.map(current.addAffectedPackageVersion(packageName, _)).exists(r => r)
      }.exists(e => e)
    val securityAdvisoryUpdated = update.securityAdvisory
      .map(current.addAdvisoryId)
      .exists(e => e)
    affectedPackagesUpdated || securityAdvisoryUpdated
  }

  private def newEntryPolicy(update: DebianCve): DebianGatheredData = {
    // TODO: the lts support advisories are ignored
    val fixes = mutable.ListMap[String, ListBuffer[Source]]()
    update.affectedPackages.keySet.foreach(packageName => fixes += (packageName -> ListBuffer()))
    DebianGatheredData(update.cveId, ListBuffer(), fixes, update.affectedPackages, update.securityAdvisory)
  }

  protected def sendAffectedPackagesToSnapshotInfo(entries: ListBuffer[DebianGatheredData]): Unit = {
    val debianSnapshotInfoService = verticle.services.filter(_._1.name == ServiceName.DebianSnapshotInfoService)
    val requestedPackageVersionsList = ListBuffer[RequestedPackageVersions]()
    entries.map(_.affectedDebianPackages).foreach(_.foreach { e =>
      val packageName = e._1
      val debianFixes = e._2.map(_.fixedVersion)
      requestedPackageVersionsList.find(_.packageName == packageName) match {
        case Some(value) =>
          debianFixes.foreach(value.packageVersions += _)
        case None =>
          val requestedPackageVersions = RequestedPackageVersions(packageName, mutable.Set())
          debianFixes.foreach(requestedPackageVersions.packageVersions += _)
          requestedPackageVersionsList += requestedPackageVersions
      }
    })
    if (requestedPackageVersionsList.nonEmpty && debianSnapshotInfoService.nonEmpty) {
      val operation = SnapshotInfoSourceOperation.AddPackages
      val request = SnapshotInfoSourceRequestMessage(operation, requestedPackageVersionsList).toJson
      debianSnapshotInfoService.foreach(_._2.sendTextMessage(request))
    }
  }

}
