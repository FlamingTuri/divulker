package it.unibo.handlers

import io.vertx.scala.core.http.WebSocket
import it.unibo.jackson.MarshallableImplicit._
import it.unibo.models.aggregator.Source
import it.unibo.models.data.CveIdVulnerabilityData
import it.unibo.models.debian.DebianSecurityAdvisory
import it.unibo.models.service.ServiceName
import it.unibo.models.ws.messages.Entries
import it.unibo.models.ws.messages.distro.DistroSourceResponseMessage
import it.unibo.models.{DebianGatheredData, DebianSource}
import it.unibo.updates.collections.BufferUpdateUtils._
import it.unibo.updates.operation.result.{Result, UpdateResult}
import it.unibo.utils.Logger

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

/** Message handler for DsaService */
class DsaHandler extends DebianSpecificDataSourceWsHandler {

  override protected val sourceName: ServiceName.Value = ServiceName.DsaService

  override protected def textMessageHandler(webSocket: WebSocket, text: String): Unit = {
    val response = text.fromJson[DistroSourceResponseMessage[DebianSecurityAdvisory]]()
    val responseData = response.data
    val verticleData = verticle.data
    val updateResult = new UpdateResult[DebianGatheredData]()
    responseData.newEntries.foreach(updateStrategy(updateResult, verticleData, _))
    responseData.updatedEntries.foreach(updateStrategy(updateResult, verticleData, _))

    /*res.data.foreach { dsa =>
      // finds the gathered data that have a reference to the dsa id
      verticle.data.filter(p => p.advisoryIds.contains(dsa.dsaId)).foreach { element =>
        // adds the affected packages
        if (dsa.affectedPackage.map(element.affectedPackages.addIfNotPresent).exists(_)) {
          if (!newEntries.contains(element)) {
            addIfNotPresent(updatedEntries, element)
          }
        }
      }
    }*/
    val l = updateResult.newEntries.map(_.cveId)
    Logger.printOut(l.distinct.size != l.size)
    updateResult.printEntriesNumber()
    notifySubscribers(Entries(updateResult))
  }

  private def updateStrategy(updateResult: UpdateResult[DebianGatheredData],
                             verticleData: CveIdVulnerabilityData[DebianGatheredData],
                             dsa: DebianSecurityAdvisory): Unit = {
    val affectedPackage = dsa.affectedPackage
    dsa.relatedCveIds.foreach { cveId =>
      // finds if there is an entry with the dsa related cveId
      val operationResult = verticleData.insertOrUpdate(cveId, (id: String) => id,
        (value, id: String) => {
          val currentEntryFixes = value.fixes
          currentEntryFixes.get(affectedPackage) match {
            case Some(fixSources) =>
              val operationResult = fixSources.insertOrUpdate[String](sourceName.toString,
                (current: Source, update: String) => current.name == update,
                (source: Source, _: String) => {
                  // TODO: the date is always replaced, should be replaced only
                  //  if less than the current value
                  val releaseDate = DebianSecurityAdvisory.parseDate(dsa)
                  val dateUpdated = source.date != releaseDate
                  if (dateUpdated) {
                    source.date = releaseDate
                  }
                  // adds the information about the dsa id if not already present
                  val dsaAdded = value.addAdvisoryId(dsa.dsaId)
                  // if an update has happened add the value to the updatedEntries
                  dateUpdated || dsaAdded
                }, (_: String) => {
                  value.addAdvisoryId(dsa.dsaId)
                  DebianSource(sourceName, dsa)
                })
              operationResult.result != Result.None
            case None =>
              currentEntryFixes += (affectedPackage -> ListBuffer(DebianSource(sourceName, dsa)))
              true
          }
        }, (cveId: String) => {
          val fixSource = affectedPackage -> ListBuffer(DebianSource(sourceName, dsa))
          val fixes = mutable.ListMap[String, ListBuffer[Source]](fixSource)
          DebianGatheredData(cveId, ListBuffer(), fixes, mutable.ListMap(), ListBuffer(dsa.dsaId))
        })
      updateResult.addEntry(operationResult)
    }
  }
}
