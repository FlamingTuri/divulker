package it.unibo.handlers

import it.unibo.handlers.assigner.SpecificDataSourceHandlerAssigner
import it.unibo.models.DebianGatheredData
import it.unibo.models.service.ServiceName

case class HandlerMatchers() extends SpecificDataSourceHandlerAssigner[DebianGatheredData] {

  override protected def serviceMatcher(serviceName: ServiceName.Value): SpecificRetrieverWsHandler[DebianGatheredData] = {
    serviceName match {
      case ServiceName.DebianCveService => new DebianRelatedCveHandler()
      case ServiceName.DsaService => new DsaHandler()
      case ServiceName.DebianSnapshotInfoService => SnapshotInfoHandler()
      case _ => ???
    }
  }

}
