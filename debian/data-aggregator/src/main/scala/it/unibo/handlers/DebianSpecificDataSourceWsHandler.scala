package it.unibo.handlers

import it.unibo.models.DebianGatheredData

/** DebianSpecificDataSource message handler */
trait DebianSpecificDataSourceWsHandler extends SpecificDataSourceWsHandler[DebianGatheredData]
  with DebianInfo
