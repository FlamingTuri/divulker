package it.unibo.handlers

import io.vertx.scala.core.http.WebSocket
import it.unibo.jackson.MarshallableImplicit._
import it.unibo.models.DebianGatheredData
import it.unibo.models.aggregator.Source
import it.unibo.models.data.CveIdVulnerabilityData
import it.unibo.models.debian.SnapshotData
import it.unibo.models.service.ServiceName
import it.unibo.models.ws.messages.Entries
import it.unibo.models.ws.messages.distro.DistroSourceResponseMessage
import it.unibo.models.ws.messages.snapshotinfo.{SnapshotInfoSourceOperation, SnapshotInfoSourceRequestMessage}
import it.unibo.updates.collections.BufferUpdateUtils.BufferUpdateExtensions
import it.unibo.updates.strategies.SourceStrategies

import scala.collection.mutable.ListBuffer

/** Message handler for DebianSnapshotInfoService */
case class SnapshotInfoHandler() extends SpecificRetrieverWsHandler[DebianGatheredData]
  with DebianInfo {

  override protected val wsRequestMessage: SnapshotInfoSourceRequestMessage = {
    SnapshotInfoSourceRequestMessage(SnapshotInfoSourceOperation.Subscribe)
  }
  override protected val sourceName: ServiceName.Value = ServiceName.DebianSnapshotInfoService

  override protected def textMessageHandler(webSocket: WebSocket, text: String): Unit = {
    val response = text.fromJson[DistroSourceResponseMessage[SnapshotData]]()
    val updatedEntries = ListBuffer[DebianGatheredData]()
    val verticleData = verticle.data
    val receivedData = response.data
    receivedData.printInfo()
    receivedData.newEntries.foreach(updateStrategy(verticleData, _, updatedEntries))
    receivedData.updatedEntries.foreach(updateStrategy(verticleData, _, updatedEntries))
    notifySubscribers(Entries.updatedEntries(updatedEntries))
  }

  protected def updateStrategy(verticleData: CveIdVulnerabilityData[DebianGatheredData],
                               update: SnapshotData,
                               updatedEntries: ListBuffer[DebianGatheredData]): Unit = {
    val packageName = update.packageName
    // get only debian entries with update packageName
    val dataWithPackageName = verticleData.getAsIterable
      .filter(_.affectedDebianPackages.keySet.contains(packageName))
    update.packageVersionDataList.foreach { packageVersionData =>
      val archivePackageDataVersion = packageVersionData.version
      packageVersionData.packageArchiveData.foreach { packageArchiveData =>
        dataWithPackageName
          // get only the entries with the fixed version equal to the one of the update
          .filter(e => e.affectedDebianPackages(packageName).exists(_.fixedVersion == archivePackageDataVersion))
          .foreach { e =>
            val archive = packageArchiveData.archive
            val fixDate = packageArchiveData.date
            val source = Source(s"$sourceName$archive", fixDate)
            if (SourceStrategies.updateSourcesStrategy(e.fixes(packageName), source)) {
              // if the source was updated it is added to the updated entries list
              updatedEntries.addIfNotPresent(e)
            }
          }
      }
    }
  }

}
