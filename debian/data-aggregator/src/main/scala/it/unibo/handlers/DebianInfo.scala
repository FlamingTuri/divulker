package it.unibo.handlers

import it.unibo.models.service.ServiceName

trait DebianInfo extends DistroNameInfo {

  override protected def distroName: ServiceName.Value = ServiceName.DebianDataAggregator

}
