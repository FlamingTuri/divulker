package it.unibo.constants

import it.unibo.models.service.{ServiceKind, ServiceName}

class DebianDataTrackerSourceInfo extends SourceVerticleInfo {
  /** The service name */
  override val serviceName: ServiceName.Value = ServiceName.DebianDataAggregator
  /** The service type */
  override val serviceKind: ServiceKind.Value = ServiceKind.DistroAggregator
  /** The path accepted for websocket messages */
  override val WebSocketPath: String = "/debian-data-tracker"
}
