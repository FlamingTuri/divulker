package it.unibo.models

import com.fasterxml.jackson.annotation.JsonIgnore
import it.unibo.models.aggregator.{Source, SourceTemplate}
import it.unibo.models.debian.DebianSecurityAdvisory

object DebianSource {

  def apply(sourceName: String, dsa: DebianSecurityAdvisory): Source = Source(sourceName, dsa)

}

case class DebianSource(private val _name: String,
                        private var _date: String,
                        private val _dsaId: String) extends SourceTemplate(_name, _date) {

  @JsonIgnore
  def dsaId: String = _dsaId

}
