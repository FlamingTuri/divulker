package it.unibo.models

import com.fasterxml.jackson.annotation.{JsonIgnore, JsonProperty}
import it.unibo.models.aggregator.{GatheredDataTemplate, Source}
import it.unibo.models.debian.DebianFix
import it.unibo.updates.collections.BufferUpdateUtils._

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

case class DebianGatheredData(@JsonProperty(value = "cveId", required = true)
                              private val _cveId: String,
                              @JsonProperty(value = "disclosures", required = true)
                              private val _disclosures: ListBuffer[Source] = ListBuffer(),
                              @JsonProperty(value = "fixes", required = true)
                              private val _fixes: mutable.ListMap[String, ListBuffer[Source]] = mutable.ListMap(),
                              @JsonProperty(value = "affectedDebianPackages", required = true)
                              private val _affectedDebianPackages: mutable.ListMap[String, ListBuffer[DebianFix]] = mutable.ListMap(),
                              private var _advisoryIds: ListBuffer[String] = ListBuffer())
  extends GatheredDataTemplate[Source](_cveId, _disclosures, _fixes) {

  @JsonIgnore
  def advisoryIds: List[String] = _advisoryIds.toList

  /** Adds the dsa id if not already present in [[advisoryIds]] collection
   *
   * @param dsaId the dsa id to add
   * @return true if the dsa id is successfully added to [[advisoryIds]] collection
   */
  def addAdvisoryId(dsaId: String): Boolean = {
    _advisoryIds.addIfNotPresent(dsaId)
  }

  @JsonIgnore
  def affectedDebianPackages: mutable.ListMap[String, ListBuffer[DebianFix]] = _affectedDebianPackages

  /** Inserts a new affected package version
   *
   * @param packageName the package name
   * @param fix its fix
   * @return true if the vulnerability data was updated
   */
  def addAffectedPackageVersion(packageName: String, fix: DebianFix): Boolean = {
    var updated = false
    // updates the fixes if the package name does not exists
    _fixes.get(packageName) match {
      case Some(_) =>
      case None =>
        _fixes += (packageName -> ListBuffer())
        updated = true
    }
    // updates the affected debian packages with the debian fix information
    _affectedDebianPackages.get(packageName) match {
      case Some(value) =>
        updated = updated || value.addIfNotPresent(fix)
      case None =>
        _affectedDebianPackages += (packageName -> ListBuffer(fix))
        updated = true
    }
    updated
  }

}
