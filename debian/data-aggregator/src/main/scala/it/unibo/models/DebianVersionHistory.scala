package it.unibo.models

import scala.collection.immutable.ListMap


class DebianVersionHistory {

  val debianVersionsMap: ListMap[String, DebianVersionDates] = ListMap(
    "slink" -> DebianVersionDates("9 Mar 1999", "30 Oct 2000", "30 Oct 2000"),
    "potato" -> DebianVersionDates("15 Aug 2000", "30 Jun 2003", "30 Jun 2003"),
    "woody" -> DebianVersionDates("19 Jul 2002", "30 Jun 2006", "30 Jun 2006"),
    "sarge" -> DebianVersionDates("6 Jun 2005", "31 Mar 2008", "31 Mar 2008"),
    "etch" -> DebianVersionDates("8 Apr 2007", "15 Feb 2010", "15 Feb 2010"),
    "lenny" -> DebianVersionDates("14 Feb 2009", "6 Feb 2012", "6 Feb 2012"),
    "squeeze" -> DebianVersionDates("6 Feb 2011", "19 Jul 2014", "29 Feb 2016"),
    "wheezy" -> DebianVersionDates("4 May 2013", "26 Apr 2016", "31 May 2018"),
    "jessie" -> DebianVersionDates("26 Apr 2015", "17 Jun 2018", "30 Jun 2020"),
    // TODO: stable and lts dates should be updated with the real values (when available)
    "stretch" -> DebianVersionDates("17 Jun 2017", "30 Jun 2020", "30 Jun 2022"),
    "buster" -> DebianVersionDates("6 Jul 2019", "30 Jun 2022", "30 Jun 2024")
  )

  def compareVersion(debianVersion1: String, debianVersion2: String): Boolean = {
    val debianVersionsNames = debianVersionsMap.toArray.map(_._1)
    debianVersionsNames.indexOf(debianVersion1) > debianVersionsNames.indexOf(debianVersion2)
  }

  def getLatest(debianVersions: String*): String = {
    val debianVersionsNames = debianVersionsMap.toArray.map(_._1)
    val latestIdx = debianVersions.map(debianVersionsNames.indexOf)
      .fold(-1)((acc, curr) => if (acc < curr) curr else acc)
    if (latestIdx >= 0) {
      debianVersionsNames(latestIdx)
    } else {
      throw new IllegalArgumentException("No debian version matched")
    }
  }

}

case class DebianVersionDates(releaseDate: String,
                              stableEndOfSecuritySupportDate: String,
                              ltsEndOfSecuritySupportDate: String)
