package it.unibo.launcher

import it.unibo.verticle.{DebianDataAggregatorVerticle, VerticleLauncher}

object DebianDataAggregatorLauncher extends App with VerticleLauncher {
  deployVerticle[DebianDataAggregatorVerticle]()
}
