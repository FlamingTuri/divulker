package it.unibo.models

import com.fasterxml.jackson.databind.{MapperFeature, ObjectMapper, SerializationFeature}
import it.unibo.jackson.JsonUtil
import it.unibo.jackson.MarshallableImplicit._
import it.unibo.models.aggregator.{GatheredData, GatheredDataTemplate, Source}
import it.unibo.models.debian.DebianSecurityAdvisory.parseDate
import it.unibo.models.service.ServiceName
import org.scalatest.BeforeAndAfter
import org.scalatest.funsuite.AnyFunSuite

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

class TestDebianDataAggregatorModels extends AnyFunSuite with BeforeAndAfter {

  private val disclosureServiceName = ServiceName.DsaService.toString
  private val disclosureSource = Source(disclosureServiceName, "2019-10-17")
  private val debianDisclosureSource = DebianSource(disclosureServiceName, "2019-10-17", "something")

  private val fixServiceName = ServiceName.DsaService.toString
  private val fixSource = Source(fixServiceName, parseDate("14 Oct 2019"))
  private val debianFixSource = DebianSource(fixServiceName, parseDate("14 Oct 2019"), "something")

  private def jsonMapperTestConfiguration(jsonMapper: ObjectMapper, state: Boolean): Unit = {
    jsonMapper.configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, state)
      .configure(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS, state)
  }

  before {
    jsonMapperTestConfiguration(JsonUtil.jsonMapper, true)
  }

  after {
    jsonMapperTestConfiguration(JsonUtil.jsonMapper, false)
  }

  test("DebianSource to json should have the same value of Source to json") {
    assertResult(disclosureSource.toJson)(debianDisclosureSource.toJson)
    assertResult(fixSource.toJson)(debianFixSource.toJson)
  }

  test("DebianGatheredData to json should have the same value of GatheredData to json") {
    val cveId = "CVE-2019-14287"
    val disclosures = ListBuffer(disclosureSource)
    val fixes = mutable.ListMap("sudo" -> ListBuffer(fixSource))

    val gatheredDataTemplate = new GatheredDataTemplate(cveId, disclosures, fixes)
    val gatheredData = GatheredData(cveId, disclosures, fixes)
    val debianGatheredData = DebianGatheredData(cveId, disclosures, fixes)

    assertResult(gatheredDataTemplate.toJson)(debianGatheredData.toJson)
    assertResult(gatheredData.toJson)(debianGatheredData.toJson)
    assertResult(gatheredData)(debianGatheredData.toJson.fromJson[GatheredData]())
  }

}
