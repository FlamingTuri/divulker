package it.unibo.parsers

import it.unibo.jackson.MarshallableImplicit._
import it.unibo.models.debian.{DebianCve, DebianFix}
import org.scalatest.funsuite.AnyFunSuite

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

class TestCveListParser extends AnyFunSuite {

  private val parser = CveListParser()

  test("Parsing debian related cve list") {
    val text =
      s"""CVE-2017-18616
         |\tRESERVED
         |CVE-2017-18615 (The kama-clic-counter plugin before 3.5.0 for WordPress has XSS. ...)
         |\tNOT-FOR-US: Wordpress plugin
         |CVE-2019-16165 (GNU cflow through 1.6 has a use-after-free in the reference function i ...)
         |\t- cflow <unfixed> (unimportant; bug #939915)
         |\tNOTE: https://lists.gnu.org/archive/html/bug-cflow/2019-04/msg00001.html
         |\tNOTE: Crash in CLI tool, no security impact
         |CVE-2019-16275 (hostapd before 2.10 and wpa_supplicant before 2.10 allow an incorrect  ...)
         |\t{DSA-4538-1 DLA-1922-1}
         |\t- wpa 2:2.9-2 (bug #940080)
         |\t[stretch] - wpa <no-dsa> (Minor issue; can be fixed via point release)
         |\tNOTE: https://www.openwall.com/lists/oss-security/2019/09/11/7
         |\tNOTE: https://w1.fi/security/2019-7/
         |CVE-2019-16095 (Symonics libmysofa 0.7 has an invalid read in getDimension in hrtf/rea ...)
         |\t- libmysofa 0.8~dfsg0-1 (bug #939735)
         |\t[buster] - libmysofa 0.6~dfsg0-3+deb10u1
         |CVE-2019-16165 (GNU cflow through 1.6 has a use-after-free in the reference function i ...)
         |\t- cflow <unfixed> (unimportant; bug #939915)
         |\tNOTE: https://lists.gnu.org/archive/html/bug-cflow/2019-04/msg00001.html
         |\tNOTE: Crash in CLI tool, no security impact""".stripMargin

    val debianCveList = parser.fromPlainText(text)
    assertResult(2)(debianCveList.length)

    var expectedCve = DebianCve("CVE-2019-16275", mutable.ListMap("wpa" ->
      ListBuffer(DebianFix("sid", "2:2.9-2"))), ListBuffer("DSA-4538-1"), ListBuffer("DLA-1922-1"))
    assertResult(expectedCve)(debianCveList(0))

    expectedCve = DebianCve("CVE-2019-16095", mutable.ListMap("libmysofa" ->
      ListBuffer(DebianFix("sid", "0.8~dfsg0-1"), DebianFix("buster", "0.6~dfsg0-3+deb10u1"))))
    assertResult(expectedCve)(debianCveList(1))
  }

  test("Debian cve with no package should be ignored") {
    val text =
      s"""CVE-2020-8428 (fs/namei.c in the Linux kernel before 5.5 has a may_create_in_sticky u ...)
         |\t- linux 5.4.19-1
         |\t[jessie] - linux <not-affected> (Vulnerable code introduced later)
         |\tNOTE: Fixed by: https://git.kernel.org/linus/d0cb50185ae942b03c4327be322055d622dc79f6
         |CVE-2020-8315 (In Python (CPython) 3.6 through 3.6.10, 3.7 through 3.7.6, and 3.8 thr ...)
         |\t- python3.8 <not-affected> (Windows-specific)
         |\t- python3.7 <not-affected> (Windows-specific)
         |\tNOTE: https://bugs.python.org/issue39401
         |CVE-2019-20422 (In the Linux kernel before 5.3.4, fib6_rule_lookup in net/ipv6/ip6_fib ...)
         |\t- linux <not-affected> (Vulnerable code not present)
         |\tNOTE: https://git.kernel.org/linus/7b09c2d052db4b4ad0b27b97918b46a7746966fa
         |\tCVE-2020-6750 (GSocketClient in GNOME GLib through 2.62.4 may occasionally connect di ...)
         |\t- glib2.0 <unfixed> (bug #948554)
         |\t[buster] - glib2.0 <not-affected> (Vulnerable code introduced later, regreession from 2.60.0)
         |\t[stretch] - glib2.0 <not-affected> (Vulnerable code introduced later, regreession from 2.60.0)
         |\t[jessie] - glib2.0 <not-affected> (Vulnerable code introduced later, regreession from 2.60.0)
         |\tNOTE: https://gitlab.gnome.org/GNOME/glib/issues/1989""".stripMargin

    val debianCveList = parser.fromPlainText(text)
    assertResult(1)(debianCveList.length)
  }

  test("Debian vulnerabilities without an assigned CVE id should be ignored") {
    val text =
      s"""CVE-2009-XXXX [optipng array overflow]
         |\t- optipng 0.6.2.1-1 (low)
         |\tNOTE: https://secunia.com/advisories/34035/""".stripMargin

    val debianCveList = parser.fromPlainText(text)
    assertResult(0)(debianCveList.length)
  }

  test("Debian related cve json conversion") {
    val text =
      """CVE-2019-16165 (GNU cflow through 1.6 has a use-after-free in the reference function i ...)
        |\t- cflow <unfixed> (unimportant; bug #939915)
        |\tNOTE: https://lists.gnu.org/archive/html/bug-cflow/2019-04/msg00001.html
        |\tNOTE: Crash in CLI tool, no security impact
        |CVE-2019-16275 (hostapd before 2.10 and wpa_supplicant before 2.10 allow an incorrect  ...)
        |\t{DSA-4538-1 DLA-1922-1}
        |\t- wpa 2:2.9-2 (bug #940080)
        |\t[stretch] - wpa <no-dsa> (Minor issue; can be fixed via point release)
        |\tNOTE: https://www.openwall.com/lists/oss-security/2019/09/11/7
        |\tNOTE: https://w1.fi/security/2019-7/""".stripMargin

    parser.fromPlainText(text).foreach { debianCve =>
      assertResult(debianCve)(debianCve.toJson.fromJson[DebianCve]())
    }
  }

}
