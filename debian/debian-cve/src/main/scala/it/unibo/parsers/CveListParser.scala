package it.unibo.parsers

import it.unibo.models.debian.DebianCve

case class CveListParser() extends Parser[Array[DebianCve]] {

  private val debianUnstableRelease = "sid"
  private val cveIdRegex = "(?m)^(CVE-\\d{4}-(?:\\d{4,}|XXXX))"
  private val vulnerabilityWithoutCveIdRegex = "CVE-\\d{4}-XXXX".r
  private val removeCveRegex = cveIdRegex + ".*\\s+(RESERVED|REJECTED|NOT-FOR-US)(.*\\s)+$"
  private val advisoriesRegex = "\\{((DSA|DLA)(-\\d+)+)( (DSA|DLA)(-\\d+)+)*\\}"
  private val debianSidPackageRegex = "\\t- (.*?) .*".r //"\\t- (.*?) (<not-affected>|.*)( .*)?".r
  //private val debianStableOldStablePackageRegex = "\\t\\[.*\\] - (.*) (<not-affected>) .*".r
  private val debianReleasePackageRegex = "\\t\\[([a-z]+)\\] - (.*?) .*".r
  private val sidPackageAndVersionRegex = "\\t- ([A-Za-z0-9:.+-]+)(?:\\s([A-Za-z0-9:.+~-]+)\\s*)?.*".r
  private val debianReleasePackageAndVersionRegex = "\\t\\[([a-z]+)\\] - ([A-Za-z0-9:.+-]+)(?:\\s([A-Za-z0-9:.+~-]+)\\s*)?.*".r

  override def fromPlainText(text: String): Array[DebianCve] = {
    // splits without removing the matching characters
    text.split(s"(?=$cveIdRegex)")
      // removes RESERVED, REJECTED, NOT-FOR-US cve entries
      .filterNot(cve => cve.matches(removeCveRegex))
      // removes cve with XXXX identifier
      .filterNot(cve => vulnerabilityWithoutCveIdRegex.findFirstIn(cve).isDefined)
      .map(extractDebianCveData)
      .filter(_._2)
      .map(_._1)
  }

  private def extractDebianCveData(stringDebianCve: String): (DebianCve, Boolean) = {
    val cveId = cveIdRegex.r.findFirstIn(stringDebianCve).get
    val debianCve = DebianCve(cveId)
    // retrieves advisories between {...}
    advisoriesRegex.r.findFirstIn(stringDebianCve) match {
      case Some(value) =>
        // removes curly braces and split the advisories
        value.filterNot("{}".toSet)
          .split(' ')
          .foreach {
            case advisory if advisory.contains("DSA") =>
              debianCve.addSecurityAdvisory(advisory)
            case advisory if advisory.contains("DLA") =>
              debianCve.addLongTermSupportAdvisory(advisory)
            case _ =>
          }
      case None =>
    }

    val isEntryToIgnore = (line: String) => {
      line.contains("<not-affected>") ||
        line.contains("<no-dsa>") ||
        line.contains("<ignored>") ||
        (line.contains("unfixed") && line.contains("unimportant"))
    }
    // TODO: decide what to do with unimportant severity level
    sidPackageAndVersionRegex.findAllIn(stringDebianCve)
      .filterNot(isEntryToIgnore)
      .foreach {
        case sidPackageAndVersionRegex(packageName, fixVersion) =>
          debianCve.addAffectedPackage(debianUnstableRelease, packageName, fixVersion)
        case _ =>
      }

    debianReleasePackageRegex.findAllIn(stringDebianCve)
      .filterNot(isEntryToIgnore)
      .foreach {
        case debianReleasePackageAndVersionRegex(debianRelease, packageName, fixedVersion) =>
          debianCve.addAffectedPackage(debianRelease, packageName, fixedVersion)
        case _ =>
      }

    // CVEs without security advisories and with only sid entries will be filtered out
    //val packagesAreOnlySidRelated = debianCve.affectedPackages
    //  .forall(e => e.fixList.size == e.fixList.count(_.debianRelease == debianUnstableRelease))
    val affectedPackageCount = debianCve.affectedPackages.values.map(_.size).sum
    val keep = debianCve.longTermSupportAdvisory.nonEmpty ||
      debianCve.securityAdvisory.nonEmpty ||
      affectedPackageCount > 0

    /*if (keep) {
      if (debianCve.securityAdvisory.isEmpty) {
        debianCve.securityAdvisory += "fixed without DSA"
      }
    }*/

    (debianCve, keep)
  }

}
