package it.unibo.constants

import it.unibo.models.service.{ServiceKind, ServiceName}

case class DebianCveSourceInfo() extends SourceVerticleInfo {
  /** The service name */
  override val serviceName: ServiceName.Value = ServiceName.DebianCveService
  /** The service type */
  override val serviceKind: ServiceKind.Value = ServiceKind.DistroSpecificRetriever
  /** The path accepted for websocket messages */
  override val WebSocketPath: String = "/debian-cve"
}
