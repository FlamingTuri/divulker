package it.unibo.verticle

import io.vertx.core.buffer.Buffer
import io.vertx.scala.core.Vertx
import io.vertx.scala.ext.web.client.WebClient
import it.unibo.constants.DebianSecurityTrackerConstants
import it.unibo.constants.DebianSecurityTrackerConstants.DebianSecurityTrackerUri
import it.unibo.data.subscriber.manager.DistroSpecificDataSubscriberManager
import it.unibo.models.data.VulnerabilityData
import it.unibo.models.debian.DebianCve
import it.unibo.models.ws.messages.Entries
import it.unibo.parsers.{CveListParser, Parser}
import it.unibo.updates.DebianUpdateStrategies.debianCveUpdateStrategy
import it.unibo.updates.operation.result.UpdateResult
import it.unibo.verticle.retriever.configuration.LocalDataLoadWithPollingRetrieverConfiguration
import it.unibo.vertx.FileSystemUtils

case class DebianCveDataRetrieverConfiguration(override protected val vertx: Vertx,
                                               override protected val webClient: WebClient,
                                               private val data: VulnerabilityData[DebianCve],
                                               private val dataSubscribersManager: DistroSpecificDataSubscriberManager)
  extends LocalDataLoadWithPollingRetrieverConfiguration[DebianCve] {

  override protected val saveFolderName: String = "debian/cve"
  override protected val lastSaveTimestampFileName: String = "save-time.txt"
  override protected val savedDataFileName: String = "list.txt"
  override protected val fsUtils: FileSystemUtils = FileSystemUtils(vertx.fileSystem())

  override def loadLocalData(buffer: Buffer): UpdateResult[DebianCve] = loadDownloadedData(buffer)

  override protected val dataHost: String = DebianSecurityTrackerConstants.Host
  override protected val dataUri: String = s"$DebianSecurityTrackerUri/raw/master/data/CVE/list"
  protected val parser: Parser[Array[DebianCve]] = CveListParser()

  override def loadDownloadedData(buffer: Buffer): UpdateResult[DebianCve] = {
    val debianCveArray = parser.fromPlainText(buffer.toString())
    // TODO: some entries could be removed but now the notifySubscribers only supports
    //  updated and new entries notification
    data.insertOrUpdate(debianCveArray, debianCveUpdateStrategy)
  }

  override def notifySubscribers(operationResult: UpdateResult[DebianCve]): Unit = {
    dataSubscribersManager.notifySubscribers(Entries(operationResult))
  }

}
