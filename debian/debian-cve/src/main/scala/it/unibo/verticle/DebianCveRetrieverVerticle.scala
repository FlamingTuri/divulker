package it.unibo.verticle

import it.unibo.constants.DebianCveSourceInfo
import it.unibo.models.data.{CveIdVulnerabilityData, VulnerabilityData}
import it.unibo.models.debian.DebianCve
import it.unibo.verticle.retriever.SpecificDataRetrieverVerticle
import it.unibo.verticle.retriever.configuration.DataRetrieverConfiguration

class DebianCveRetrieverVerticle extends SpecificDataRetrieverVerticle[VulnerabilityData[DebianCve]](DebianCveSourceInfo()) {

  override val data: VulnerabilityData[DebianCve] = CveIdVulnerabilityData()

  override protected def dataRetrieverConfigurations: List[DataRetrieverConfiguration] = {
    List(DebianCveDataRetrieverConfiguration(vertx, webClient, data, dataSubscriberManager))
  }

  override protected def dataAsIterable: Iterable[_] = data.getAsIterable

}
