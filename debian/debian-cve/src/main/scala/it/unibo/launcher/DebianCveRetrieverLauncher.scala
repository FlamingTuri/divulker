package it.unibo.launcher

import it.unibo.verticle.{DebianCveRetrieverVerticle, VerticleLauncher}

object DebianCveRetrieverLauncher extends App with VerticleLauncher {
  deployVerticle[DebianCveRetrieverVerticle]()
}
