package it.unibo.parsers

import it.unibo.models.debian.DebianSecurityAdvisory
import it.unibo.utils.Logger.printErr

case class DsaListParser() extends Parser[Array[DebianSecurityAdvisory]] {

  // (?:) creates a non-capturing group
  private val dsaDate = "\\[((\\d\\d) ([A-Z][a-z][a-z]) (\\d{4}))\\]"
  private val dsaId = "(DSA-\\d+(?:-\\d+)?)"
  private val dsaHeaderRe = s"$dsaDate $dsaId\\s+(.*)".r
  private val affectedPackagesRe = "(.*?)(?:\\s+-\\s+(.*))?".r
  private val debianReleaseEndOfLifeRe = "(.*?)\\s+(end-of-life)".r
  private val relatedCvesRe = "\\{CVE-\\d{4}-\\d{4,}( CVE-\\d{4}-\\d{4,})*\\}".r
  private val releaseFixLinesRe = s"\\t\\[[a-z]+\\] - (.*?) .*".r

  /** Parse the given text
   *
   * @param text the text to parse
   * @return the parse result
   */
  override def fromPlainText(text: String): Array[DebianSecurityAdvisory] = {
    // splits without removing the matching characters
    text.split(s"(?=$dsaDate)")
      // converts each plaintext dsa into a scala object
      .map(parsePlaintextDsa)
  }

  private def parsePlaintextDsa(plaintextDsa: String): DebianSecurityAdvisory = {
    try {
      val plaintextDsaHeader = plaintextDsa.split('\n')(0)
      plaintextDsaHeader match {
        case dsaHeaderRe(releaseDate, _, _, _, id, packagesAndVulnerability) =>
          // parse the affected packages from the dsa header
          val affectedPackage = packagesAndVulnerability match {
            case debianReleaseEndOfLifeRe(affectedPackage, vulnerabilityType) => affectedPackage
            case affectedPackagesRe(affectedPackage, vulnerabilityType) => affectedPackage
            case _ => ""
          }
          val dsa = DebianSecurityAdvisory(id, releaseDate, affectedPackage)
          relatedCvesRe.findFirstIn(plaintextDsa) match {
            case Some(values) =>
              values.trim.filterNot("{}".toSet).split(' ').foreach(dsa.addRelatedCve)
            case None =>
          }
          dsa
        case header => throwParseError(header)
      }
    } catch {
      case e: Exception =>
        printErr(e)
        throw e
    }
  }

}
