package it.unibo.launcher

import it.unibo.verticle.{DebianSecurityAdvisoryRetrieverVerticle, VerticleLauncher}

object DebianSecurityAdvisoryRetrieverLauncher extends App with VerticleLauncher {
  deployVerticle[DebianSecurityAdvisoryRetrieverVerticle]()
}
