package it.unibo.verticle

import io.vertx.core.buffer.Buffer
import io.vertx.scala.core.Vertx
import io.vertx.scala.ext.web.client.WebClient
import it.unibo.constants.DebianSecurityTrackerConstants
import it.unibo.constants.DebianSecurityTrackerConstants.DebianSecurityTrackerUri
import it.unibo.data.subscriber.manager.DistroSpecificDataSubscriberManager
import it.unibo.models.DsaData
import it.unibo.models.debian.DebianSecurityAdvisory
import it.unibo.models.ws.messages.Entries
import it.unibo.parsers.{DsaListParser, Parser}
import it.unibo.updates.operation.result.UpdateResult
import it.unibo.verticle.retriever.configuration.LocalDataLoadWithPollingRetrieverConfiguration
import it.unibo.vertx.FileSystemUtils

case class DsaDataRetrieverConfiguration(override protected val vertx: Vertx,
                                         override protected val webClient: WebClient,
                                         private val data: DsaData,
                                         private val dataSubscribersManager: DistroSpecificDataSubscriberManager) extends LocalDataLoadWithPollingRetrieverConfiguration[DebianSecurityAdvisory] {

  override protected val saveFolderName: String = "debian/dsa"
  override protected val lastSaveTimestampFileName: String = "save-time.txt"
  override protected val savedDataFileName: String = "list.txt"
  override protected val fsUtils: FileSystemUtils = FileSystemUtils(vertx.fileSystem())

  override def loadLocalData(buffer: Buffer): UpdateResult[DebianSecurityAdvisory] = {
    loadDownloadedData(buffer)
  }

  override protected val dataHost: String = DebianSecurityTrackerConstants.Host
  override protected val dataUri: String = s"$DebianSecurityTrackerUri/raw/master/data/DSA/list"
  protected val parser: Parser[Array[DebianSecurityAdvisory]] = DsaListParser()

  override def loadDownloadedData(buffer: Buffer): UpdateResult[DebianSecurityAdvisory] = {
    val dsaArray = parser.fromPlainText(buffer.toString())
    // TODO: some entries could be removed but now the notifySubscribers only supports
    //  updated and new entries notification
    data.insertOrUpdate(dsaArray)
  }

  override def notifySubscribers(operationResult: UpdateResult[DebianSecurityAdvisory]): Unit = {
    operationResult.printEntriesNumber()
    dataSubscribersManager.notifySubscribers(Entries(operationResult))
  }

}
