package it.unibo.verticle

import it.unibo.constants.DebianSecurityAdvisoryConstants
import it.unibo.models.DsaData
import it.unibo.models.debian.DebianSecurityAdvisory
import it.unibo.verticle.retriever.SpecificDataRetrieverVerticle
import it.unibo.verticle.retriever.configuration.DataRetrieverConfiguration

import scala.collection.mutable.ListBuffer

class DebianSecurityAdvisoryRetrieverVerticle
  extends SpecificDataRetrieverVerticle[DsaData](DebianSecurityAdvisoryConstants()) {

  override val data: DsaData = DsaData()

  override protected def dataRetrieverConfigurations: List[DataRetrieverConfiguration] = {
    List(DsaDataRetrieverConfiguration(vertx, webClient, data, dataSubscriberManager))
  }

  override protected def dataAsIterable: Iterable[_] = data.toIterable
}
