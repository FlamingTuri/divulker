package it.unibo.models

import it.unibo.models.debian.DebianSecurityAdvisory
import it.unibo.updates.DebianUpdateStrategies.dsaUpdateStrategy
import it.unibo.updates.collections.BufferUpdateUtils._
import it.unibo.updates.operation.result.OperationResultUtil.OperationResult
import it.unibo.updates.operation.result.{OperationResultUtil, Result, UpdateResult}
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

case class DsaData() {

  private val dsaIdRegex = "DSA-(\\d+)(?:-\\d+)?".r
  val data: mutable.SortedMap[String, ListBuffer[DebianSecurityAdvisory]] = mutable.SortedMap()

  def insertOrUpdate(updates: Iterable[DebianSecurityAdvisory]): UpdateResult[DebianSecurityAdvisory] = {
    val result = new UpdateResult[DebianSecurityAdvisory]()
    updates.map(insertOrUpdate).foreach(result.addEntry)
    result
  }

  def insertOrUpdate(update: DebianSecurityAdvisory): OperationResult[DebianSecurityAdvisory] = {
    val key = getDsaNumericPortionMask(update.dsaId)
    data.get(key) match {
      case Some(value) =>
        value.insertOrUpdate[DebianSecurityAdvisory](update,
          (current: DebianSecurityAdvisory, update: DebianSecurityAdvisory) => current.dsaId == update.dsaId,
          dsaUpdateStrategy _, (e: DebianSecurityAdvisory) => e)
      case None =>
        data += (key -> ListBuffer(update))
        OperationResultUtil(Result.New, update)
    }
  }

  private def getDsaNumericPortionMask(dsaId: String): String = dsaId match {
    case dsaIdRegex(number) =>
      val maskSize = 3
      val mask = "x" * maskSize
      if (maskSize >= number.length) {
        mask
      } else {
        s"${number.dropRight(maskSize)}$mask"
      }
    case value => throw new ValueException(s"could not parse $value")
  }

  def toIterable: Iterable[DebianSecurityAdvisory] = data.values.flatten
}
