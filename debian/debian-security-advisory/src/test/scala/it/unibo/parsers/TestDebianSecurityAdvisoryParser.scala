package it.unibo.parsers

import it.unibo.jackson.MarshallableImplicit._
import it.unibo.models.debian.DebianSecurityAdvisory
import org.scalatest.BeforeAndAfter
import org.scalatest.funsuite.AnyFunSuite

import scala.collection.mutable.ListBuffer

class TestDebianSecurityAdvisoryParser extends AnyFunSuite with BeforeAndAfter {

  private val parser = DsaListParser()

  test("Multiple affected packages DSA") {
    val text =
      s"""[06 Apr 2009] DSA-1763-1 openssl openssl097 - denial of service
         |\t{CVE-2009-0590}
         |\t[etch] - openssl097 0.9.7k-3.1etch3
         |\t[etch] - openssl 0.9.8c-4etch5
         |\t[lenny] - openssl 0.9.8g-15+lenny1""".stripMargin

    val dsa = parser.fromPlainText(text)(0)
    assertResult("DSA-1763-1")(dsa.dsaId)
    assertResult("06 Apr 2009")(dsa.releaseDate)
    assertResult(ListBuffer("CVE-2009-0590"))(dsa.relatedCveIds)
    // TODO: handle this case
    //assertResult(ListBuffer("openssl", "openssl097"))(dsa.affectedPackage)
    assertResult(dsa.dsaId)(dsa.getKey)
  }

  test("DSA without vulnerability type") {
    val text =
      s"""[26 Sep 2010] DSA-2114-1 git-core
         |\t{CVE-2010-2542}
         |\t[lenny] - git-core 1:1.5.6.5-3+lenny3.1""".stripMargin

    val dsa = parser.fromPlainText(text)(0)
    assertResult("DSA-2114-1")(dsa.dsaId)
    assertResult("26 Sep 2010")(dsa.releaseDate)
    assertResult(ListBuffer("CVE-2010-2542"))(dsa.relatedCveIds)
    assertResult("git-core")(dsa.affectedPackage)
    assertResult(dsa.dsaId)(dsa.getKey)
  }

  test("debian release end of life DSA") {
    val text =
      s"""[18 Apr 2018] DSA-4205-1 jessie end-of-life
         |\tNOTE: end of security support for jessie""".stripMargin

    val dsa = parser.fromPlainText(text)(0)
    assertResult("DSA-4205-1")(dsa.dsaId)
    assertResult("18 Apr 2018")(dsa.releaseDate)
    assertResult(ListBuffer())(dsa.relatedCveIds)
    assertResult("jessie")(dsa.affectedPackage)
    assertResult(dsa.dsaId)(dsa.getKey)
  }

  test("DSA with different format for related CVE") {
    val text =
      s"""[17 Jan 2006] DSA-945-1 antiword - insecure temporary file
         |        {CVE-2005-3126}
         |        [woody] - antiword 0.32-2woody0
         |\tNOTE: fixed in testing at time of DSA
         |\tNOTE: sarge is also affected, but the uploaded version is greater
         |\tNOTE: than the fixed sid version.""".stripMargin

    val dsa = parser.fromPlainText(text)(0)
    assertResult("DSA-945-1")(dsa.dsaId)
    assertResult("17 Jan 2006")(dsa.releaseDate)
    assertResult(ListBuffer("CVE-2005-3126"))(dsa.relatedCveIds)
    assertResult("antiword")(dsa.affectedPackage)
    assertResult(dsa.dsaId)(dsa.getKey)
  }

  test("DSA list parser") {
    val list =
      s"""[14 Dec 2019] DSA-4584-1 spamassassin - security update
         |\t{CVE-2018-11805 CVE-2019-12420}
         |\t[stretch] - spamassassin 3.4.2-1~deb9u2
         |\t[buster] - spamassassin 3.4.2-1+deb10u1
         |[13 Dec 2019] DSA-4565-2 intel-microcode - security update
         |\t[stretch] - intel-microcode 3.20191115.2~deb9u1
         |\t[buster] - intel-microcode 3.20191115.2~deb10u1
         |[13 Dec 2019] DSA-4583-1 spip - security update
         |\t{CVE-2019-19830}
         |\t[buster] - spip 3.2.4-1+deb10u2""".stripMargin

    val dsaArray = parser.fromPlainText(list)
    assertResult(3)(dsaArray.length)

    val dsa1 = dsaArray(0)
    assertResult("DSA-4584-1")(dsa1.dsaId)
    assertResult("14 Dec 2019")(dsa1.releaseDate)
    assertResult(ListBuffer("CVE-2018-11805", "CVE-2019-12420"))(dsa1.relatedCveIds)
    assertResult("spamassassin")(dsa1.affectedPackage)
    assertResult(dsa1.dsaId)(dsa1.getKey)

    val dsa2 = dsaArray(1)
    assertResult("DSA-4565-2")(dsa2.dsaId)
    assertResult("13 Dec 2019")(dsa2.releaseDate)
    assertResult(ListBuffer())(dsa2.relatedCveIds)
    assertResult("intel-microcode")(dsa2.affectedPackage)
    assertResult(dsa2.dsaId)(dsa2.getKey)

    val dsa3 = dsaArray(2)
    assertResult("DSA-4583-1")(dsa3.dsaId)
    assertResult("13 Dec 2019")(dsa3.releaseDate)
    assertResult(ListBuffer("CVE-2019-19830"))(dsa3.relatedCveIds)
    assertResult("spip")(dsa3.affectedPackage)
    assertResult(dsa3.dsaId)(dsa3.getKey)
  }

  test("DSA json conversion") {
    val list =
      s"""[14 Dec 2019] DSA-4584-1 spamassassin - security update
         |\t{CVE-2018-11805 CVE-2019-12420}
         |\t[stretch] - spamassassin 3.4.2-1~deb9u2
         |\t[buster] - spamassassin 3.4.2-1+deb10u1
         |[13 Dec 2019] DSA-4565-2 intel-microcode - security update
         |\t[stretch] - intel-microcode 3.20191115.2~deb9u1
         |\t[buster] - intel-microcode 3.20191115.2~deb10u1
         |[13 Dec 2019] DSA-4583-1 spip - security update
         |\t{CVE-2019-19830}
         |\t[buster] - spip 3.2.4-1+deb10u2""".stripMargin

    val dsaArray = parser.fromPlainText(list)
    dsaArray.foreach { dsa =>
      val fromJsonDsa = dsa.toJson.fromJson[DebianSecurityAdvisory]()
      assertResult(dsa)(fromJsonDsa)
    }
  }
}
