import it.unibo.models.DebianVersion
import it.unibo.parsers.DebianVersionParser
import org.scalatest.funsuite.AnyFunSuite

/** Test taken from: [[https://git.dpkg.org/git/dpkg/dpkg.git/tree/lib/dpkg/t/t-version.c]] */
class TestDebianVersionParse extends AnyFunSuite {

  private var version: DebianVersion = _

  private implicit def toOption(value: String): Option[String] = Option(value)

  test("Test 0 versions") {
    version = DebianVersion(0, "0")
    assertResult(version)(DebianVersionParser.parsePackageVersion("0"))

    version = DebianVersion(0, "0")
    assertResult(version)(DebianVersionParser.parsePackageVersion("0:0"))

    version = DebianVersion(0, "0", "0")
    assertResult(version)(DebianVersionParser.parsePackageVersion("0:0-0"))

    version = DebianVersion(0, "0.0", "0.0")
    assertResult(version)(DebianVersionParser.parsePackageVersion("0:0.0-0.0"))
  }

  test("Test epoched versions") {
    version = DebianVersion(1, "0")
    assertResult(version)(DebianVersionParser.parsePackageVersion("1:0"))

    version = DebianVersion(5, "1")
    assertResult(version)(DebianVersionParser.parsePackageVersion("5:1"))
  }

  test("Test multiple hyphens") {
    version = DebianVersion(0, "0-0", "0")
    assertResult(version)(DebianVersionParser.parsePackageVersion("0:0-0-0"))

    version = DebianVersion(0, "0-0-0", "0")
    assertResult(version)(DebianVersionParser.parsePackageVersion("0:0-0-0-0"))
  }

  test("Test multiple colons") {
    version = DebianVersion(0, "0:0", "0")
    assertResult(version)(DebianVersionParser.parsePackageVersion("0:0:0-0"))

    version = DebianVersion(0, "0:0:0", "0")
    assertResult(version)(DebianVersionParser.parsePackageVersion("0:0:0:0-0"))
  }

  test("Test multiple hyphens and colons") {
    version = DebianVersion(0, "0:0-0", "0")
    assertResult(version)(DebianVersionParser.parsePackageVersion("0:0:0-0-0"))

    version = DebianVersion(0, "0-0:0", "0")
    assertResult(version)(DebianVersionParser.parsePackageVersion("0:0-0:0-0"))
  }

}
