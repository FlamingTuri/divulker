import it.unibo.parsers.SnapshotPageParser
import it.unibo.utils.Resources
import org.scalatest.funsuite.AnyFunSuite

class TestSnapshotData extends AnyFunSuite {

  private val parser = SnapshotPageParser()

  test("Test snapshot page with one archive package data parsing") {
    val text = Resources.readAsString("sudo1.8.19p1-2.html")
    val snapshotData = parser.fromHtml(text)
    assertResult("sudo")(snapshotData.packageName)
    val packageVersionDataList = snapshotData.packageVersionDataList
    assertResult(1)(packageVersionDataList.size)

    val packageVersionData = packageVersionDataList.head
    assertResult("1.8.19p1-2")(packageVersionData.version)
    val packageArchiveData = packageVersionData.packageArchiveData
    assertResult(1)(packageArchiveData.size)
    assertResult("debian")(packageArchiveData.head.archive)
    assertResult("2017-06-01")(packageArchiveData.head.date)
  }

  test("Test snapshot page with multiple archive package data parsing") {
    val text = Resources.readAsString("sudo1.8.27-1+deb10u1.html")
    val snapshotData = parser.fromHtml(text)
    assertResult("sudo")(snapshotData.packageName)
    val archivePackageDataList = snapshotData.packageVersionDataList
    assertResult(1)(archivePackageDataList.size)

    val packageVersionData = archivePackageDataList.head
    assertResult("1.8.27-1+deb10u1")(packageVersionData.version)

    val packageArchiveData = packageVersionData.packageArchiveData.toList
    assertResult(2)(packageArchiveData.size)
    assertResult("debian")(packageArchiveData.head.archive)
    assertResult("2019-10-19")(packageArchiveData.head.date)
    assertResult("debian-security")(packageArchiveData(1).archive)
    assertResult("2019-10-14")(packageArchiveData(1).date)
  }

}
