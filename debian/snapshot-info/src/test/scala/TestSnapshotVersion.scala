import it.unibo.parsers.SnapshotVersionsPageParser
import it.unibo.utils.Resources
import org.scalatest.funsuite.AnyFunSuite

class TestSnapshotVersion extends AnyFunSuite {

  private val parser = SnapshotVersionsPageParser()

  test("Test earliest package version page parse") {
    val text = Resources.readAsString("firefox-available-versions.html")
    val earliestFirefoxVersion = parser.fromHtml(text)
    assertResult(Some("1.4.99+1.5rc3.dfsg-2"))(earliestFirefoxVersion)
  }

}
