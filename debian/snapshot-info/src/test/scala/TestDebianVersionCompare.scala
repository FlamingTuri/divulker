import it.unibo.models.DebianVersion
import org.scalatest.funsuite.AnyFunSuite

/** Test taken from: [[https://git.dpkg.org/git/dpkg/dpkg.git/tree/lib/dpkg/t/t-version.c]] */
class TestDebianVersionCompare extends AnyFunSuite {

  private var a: DebianVersion = _
  private var b: DebianVersion = _

  private implicit def toOption(value: String): Option[String] = Option(value)

  test("Test version compare") {
    a = DebianVersion(0, "0", "0")
    b = DebianVersion(0, "0", "0")
    assertResult(0)(a.compareTo(b))

    a = DebianVersion(0, "0", "00")
    b = DebianVersion(0, "00", "0")
    //assertResult(0)(a.compareTo(b))

    a = DebianVersion(1, "2", "3")
    b = DebianVersion(1, "2", "3")
    assertResult(0)(a.compareTo(b))

    /* Test for epoch difference. */
    a = DebianVersion(0, "0", "0")
    b = DebianVersion(1, "0", "0")
    assertResult(-1)(a.compareTo(b))
    assertResult(1)(b.compareTo(a))

    /* Test for version component difference. */
    a = DebianVersion(0, "a", "0")
    b = DebianVersion(0, "b", "0")
    assertResult(-1)(a.compareTo(b))
    assertResult(1)(b.compareTo(a))

    /* Test for revision component difference. */
    a = DebianVersion(0, "0", "a")
    b = DebianVersion(0, "0", "b")
    assertResult(-1)(a.compareTo(b))
    assertResult(1)(b.compareTo(a))

    /* Test equality when revision is missing. */
    a = DebianVersion(0, "1.6", None)
    assertResult(0)(a.compareTo(a))
  }

}
