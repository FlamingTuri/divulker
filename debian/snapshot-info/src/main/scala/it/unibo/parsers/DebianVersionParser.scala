package it.unibo.parsers

import it.unibo.models.DebianVersion

object DebianVersionParser {

  private val versionWithRevisionRegex = "(?:(\\d):)?(.+)-(.+?)".r
  private val versionWithoutRevisionRegex = "(?:(\\d):)?(.+)".r

  def parsePackageVersion(string: String): DebianVersion = {
    val (epoch, upstreamVersion, debianRevision) = string match {
      case versionWithRevisionRegex(epoch, upstreamVersion, debianRevision) =>
        (parseEpoch(epoch), upstreamVersion, Option(debianRevision))
      case versionWithoutRevisionRegex(epoch, upstreamVersion) =>
        (parseEpoch(epoch), upstreamVersion, Option.empty)
      case value => Parser.throwParseError(value)
    }
    DebianVersion(epoch, upstreamVersion, debianRevision)
  }

  private def parseEpoch(epoch: String): Int = if (epoch == null) 0 else epoch.toInt

}
