package it.unibo.parsers

import it.unibo.jackson.MarshallableImplicit.Unmarshallable
import it.unibo.models.debian.{PackageArchiveData, PackageVersionData, SnapshotData}
import net.ruippeixotog.scalascraper.browser.JsoupBrowser
import net.ruippeixotog.scalascraper.dsl.DSL._
import net.ruippeixotog.scalascraper.scraper.ContentExtractors.{element, elementList, text}

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

case class SnapshotPageParser() extends Parser[ListBuffer[SnapshotData]] {

  private val headRegex = "(.+) \\((.+)\\) - snapshot.debian.org".r
  private val sourceFileRegex = "Seen in (debian.*) on (\\d{4}-\\d{2}-\\d{2}) .*".r
  private val toIgnore = "debian-debug"
  private val browser = JsoupBrowser()

  override def fromPlainText(text: String): ListBuffer[SnapshotData] = {
    text.fromJson[ListBuffer[SnapshotData]]()
  }

  def fromHtml(html: String): SnapshotData = {
    val doc = browser.parseString(html)

    val headText = doc >> text("head")
    val (packageName, version) = headText match {
      case headRegex(packageName, version) => (packageName, version)
      case value => Parser.throwParseError(value)
    }

    val archiveAvailabilityList = doc >> element("body dl dd") >> elementList("dd dl dd")
    val archivePackageDataList = archiveAvailabilityList.map(_ >> text("dd")).map {
      case sourceFileRegex(archive, date) => PackageArchiveData(archive, date)
      case value => Parser.throwParseError(value)
    }.filterNot(e => e.archive == toIgnore).to[mutable.Set]

    SnapshotData(packageName, ListBuffer(PackageVersionData(version, archivePackageDataList)))
  }

}
