package it.unibo.parsers

import it.unibo.jackson.MarshallableImplicit.Unmarshallable
import it.unibo.models.Response
import it.unibo.models.debian.SnapshotData
import net.ruippeixotog.scalascraper.browser.JsoupBrowser
import net.ruippeixotog.scalascraper.dsl.DSL._
import net.ruippeixotog.scalascraper.scraper.ContentExtractors.elementList

import scala.collection.mutable.ListBuffer

case class SnapshotVersionsPageParser() extends Parser[ListBuffer[SnapshotData]] {

  private val browser = JsoupBrowser()

  override def fromPlainText(text: String): ListBuffer[SnapshotData] = ???

  /** Retrieves the earliest package version available at https://snapshot.debian.org/
   *
   * @param html the source package html page obtained from /package/$packageName/
   * @return the earliest package version available
   */
  def fromHtml(html: String): Option[String] = {
    val doc = browser.parseString(html)
    // doc >> elementList("body ul") last
    doc >> elementList("body ul") find (e => !e.hasAttr("id")) match {
      case Some(value) => Option(value.children.last.text)
      case None => Option.empty
    }
  }


  /** Retrieves the earliest package version available at https://snapshot.debian.org
   *
   * @param jsonText json response obtained from /mr/package/<packageName>/
   * @return the earliest package version available
   */
  def fromJson(jsonText: String): Option[String] = {
    val result = jsonText.fromJson[Response]().result
    if (result.isEmpty) {
      Option.empty
    } else {
      Option(result.last.version)
    }
  }
}
