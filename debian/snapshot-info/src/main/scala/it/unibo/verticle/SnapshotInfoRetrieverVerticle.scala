package it.unibo.verticle

import io.vertx.core.buffer.Buffer
import io.vertx.scala.core.Future
import io.vertx.scala.core.http.ServerWebSocket
import io.vertx.scala.core.shareddata.SharedData
import it.unibo.constants.{SnapshotSourceInfo, SourceVerticleInfo}
import it.unibo.data.subscriber.manager.DistroSpecificDataSubscriberManager
import it.unibo.jackson.MarshallableImplicit._
import it.unibo.models.debian.{PackageCategory, SnapshotData}
import it.unibo.models.ws.messages.Entries
import it.unibo.models.ws.messages.snapshotinfo.{SnapshotInfoSourceOperation, SnapshotInfoSourceRequestMessage}
import it.unibo.updates.collections.BufferUpdateUtils._
import it.unibo.updates.collections.SetUpdateUtils._
import it.unibo.utils.CollectionUtils._
import it.unibo.utils.Logger
import it.unibo.vertx.VertxHelpers._

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

class SnapshotInfoRetrieverVerticle
  extends SourceVerticle[mutable.ListMap[String, ListBuffer[SnapshotData]]] {

  override protected val verticleInfo: SourceVerticleInfo = SnapshotSourceInfo()
  override val data: mutable.ListMap[String, ListBuffer[SnapshotData]] = mutable.ListMap()
  val dataSubscriberManager: DistroSpecificDataSubscriberManager = DistroSpecificDataSubscriberManager(verticleInfo)
  private var configuration: SnapshotInfoConfiguration = _
  private var sharedData: SharedData = _
  private val dataUpdateLock = "dataUpdateLock"
  private val dataSaveLock = "dataSaveLock"
  private val envDownloadMissingConfig = sys.env.getOrElse("downloadMissing", "true").toBoolean

  override protected def setupOperations(): Future[Unit] = {
    sharedData = vertx.sharedData()
    configuration = SnapshotInfoConfiguration(data, webClient, vertx)
    val saveDirName = configuration.saveDirName
    val savedDataFile = configuration.lastSaveDataFile
    val fileNames = s"${configuration.SavedDataFileName}.{1,4}\\.json"
    val fileRegex = s"$savedDataFile(.{1,4})\\.json".r
    fs.readDirFuture(saveDirName, fileNames).compose(_.collect {
      case fileName@fileRegex(packageCategory) =>
        fsUtils.loadFile(fileName).compose { e =>
          val snapshotData = e.toString().fromJson[ListBuffer[SnapshotData]]()
          (packageCategory, snapshotData)
        }
    }.foreachSynchronous(_.compose { e =>
      data += (e._1 -> e._2)
      Future.succeededFuture()
    })).otherwise(e => e.printStackTrace())
  }

  override protected def wsTextMessageHandler(serverWebSocket: ServerWebSocket, textMessage: String): Unit = {
    val request = textMessage.fromJson[SnapshotInfoSourceRequestMessage]()
    request.operation match {
      case SnapshotInfoSourceOperation.Subscribe =>
        retrieveMissing(serverWebSocket, request, true)
      case SnapshotInfoSourceOperation.AddPackages =>
        retrieveMissing(serverWebSocket, request, false)
      case SnapshotInfoSourceOperation.Unsubscribe =>
        dataSubscriberManager.unsubscribe(serverWebSocket)
    }
  }

  private def retrieveMissing(serverWebSocket: ServerWebSocket,
                              request: SnapshotInfoSourceRequestMessage,
                              subscribe: Boolean): Unit = {
    val requestedPackagesVersions = request.requestedPackagesVersions
    val requestedPackageNames = requestedPackagesVersions.map(_.packageName)
    Logger.printOut(s"received ${requestedPackagesVersions.size}")
    // the download procedure can take ages, for testing it can be useful to avoid it
    val downloadMissing = request.downloadMissing && envDownloadMissingConfig
    Future.succeededFuture(getIfTrueOrElseEmpty(downloadMissing, requestedPackageNames))
      .compose(_ => configuration.findExistingAndMissingData(requestedPackagesVersions))
      .compose { result =>
        Logger.printOut(s"sending existing data ${result._1.size}")
        // if a new client just subscribed send him all the already existing data
        if (subscribe) {
          dataSubscriberManager.subscribe(serverWebSocket, result._1)
        } else {
          val entries = Entries.newEntries(result._1)
          dataSubscriberManager.notifySubscribers(entries, serverWebSocket)
        }
        // returns the missing data list only if download missing is true,
        // used to avoid wasting a lot of time in waiting for retrieving all the
        // snapshot info data
        getIfTrueOrElseEmpty(downloadMissing, result._2)
      }.compose(configuration.downloadData)
      .compose { newData =>
        Logger.printOut(s"download ended ${newData.size}")
        val entries = Entries.newEntries(newData)
        dataSubscriberManager.notifySubscribers(entries)
        addNewSnapshotData(newData)
        newData
      }.compose(saveNewData)
      .otherwise(e => e.printStackTrace())
  }

  override protected def wsCloseHandler(serverWebSocket: ServerWebSocket): Unit = {
    dataSubscriberManager.unsubscribe(serverWebSocket)
  }

  /** If true returns the specified iterable, otherwise returns an empty one.
   *
   * @param condition the condition
   * @param iterable the iterable
   * @tparam T the iterable type
   * @return the specified iterable if true, otherwise returns an empty one
   */
  def getIfTrueOrElseEmpty[T](condition: Boolean, iterable: Iterable[T]): Iterable[T] = {
    if (condition) iterable else List[T]()
  }

  def addNewSnapshotData(newSnapshotData: ListBuffer[SnapshotData]): Future[ListBuffer[SnapshotData]] = {
    if (newSnapshotData.nonEmpty) {
      sharedData.getLocalLockFuture(dataUpdateLock).compose { lock =>
        newSnapshotData.foreach(addNewSnapshotData)
        lock.release()
        Future.succeededFuture(newSnapshotData)
      }
    } else {
      Future.succeededFuture(newSnapshotData)
    }
  }

  private def addNewSnapshotData(newSnapshotData: SnapshotData): Unit = {
    Logger.printOut(newSnapshotData)
    val packageName = newSnapshotData.packageName
    val packageCategory = PackageCategory.getPackageCategory(packageName)
    data.get(packageCategory) match {
      case Some(value) =>
        val packageVersionDataList = newSnapshotData.packageVersionDataList
          .filterNot(_.packageArchiveData.isEmpty)
        value.find(_.packageName == packageName) match {
          case Some(value) =>
            // TODO: result is ignored, probably it can be used like in the other microservices
            value.packageVersionDataList.insertOrUpdate(packageVersionDataList,
              (current, update) => current.version == update.version,
              (current, update) => {
                val result = current.packageArchiveData.insertOrUpdate(update.packageArchiveData,
                  (current, update) => current.archive == update.archive,
                  (current, update) => {
                    val dateUpdated = current.date != update.date
                    if (dateUpdated) {
                      current.date = update.date
                    }
                    dateUpdated
                  })
                result.newEntries.nonEmpty || result.updatedEntries.nonEmpty
              })
          case None => value += SnapshotData(packageName, packageVersionDataList)
        }
      case None => data += (packageCategory -> ListBuffer(newSnapshotData))
    }
  }

  def saveNewData(newData: ListBuffer[SnapshotData]): Unit = {
    if (newData.nonEmpty) {
      sharedData.getLocalLockFuture(dataSaveLock).compose { lock =>
        val lastSaveDataFile = s"${configuration.lastSaveDataFile}"
        data.foreach { e =>
          val saveFile = s"$lastSaveDataFile${e._1}.json"
          saveData(saveFile, Buffer.buffer(e._2.toPrettyJson))
        }
        lock.release()
      }
    }
  }

}
