package it.unibo.verticle

import java.net.ConnectException
import java.nio.file.Paths

import io.vertx.scala.core.{Future, Promise, Vertx}
import io.vertx.scala.ext.web.client.WebClient
import it.unibo.constants.Constants
import it.unibo.jackson.MarshallableImplicit._
import it.unibo.models.debian.{PackageCategory, SnapshotData}
import it.unibo.models.ws.messages.snapshotinfo.RequestedPackageVersions
import it.unibo.models.{DebianSnapshotVersion, Response}
import it.unibo.parsers.SnapshotPageParser
import it.unibo.utils.CollectionUtils._
import it.unibo.utils.Logger
import it.unibo.vertx.HttpResponseFix.fixHttpResponseFuture
import it.unibo.vertx.VertxHelpers._

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

case class SnapshotInfoConfiguration(data: mutable.ListMap[String, ListBuffer[SnapshotData]],
                                     webClient: WebClient,
                                     private val vertx: Vertx) extends Constants {

  val DataHost = "snapshot.debian.org"
  val DataUri = "/package"
  val SaveDirName = "debian/snapshot"
  val SavedDataFileName = "source-packages-"
  val LastSaveTimestampFileName = "save-time.txt"

  def saveDirName: String = pathInsideSaveDirectory(SaveDirName)

  def lastSaveTimestampFile: String = {
    pathInsideSaveDirectory(Paths.get(SaveDirName, LastSaveTimestampFileName).toString)
  }

  /** Gets the absolute path to the file storing the data
   *
   * @return the absolute path to the file
   */
  def lastSaveDataFile: String = {
    pathInsideSaveDirectory(Paths.get(SaveDirName, SavedDataFileName).toString)
  }

  private val snapshotInfoPageParser = SnapshotPageParser()

  /** Option used to set a random delay between 3000 and 6000 ms to avoid
   * snapshot info to refuse many requests.
   */
  private val delayOptions = Option(vertx, 3000, 6000)

  /** Among the requested packages versions finds the ones not present in the verticle data
   *
   * @param requestedPackagesVersions the requested packages versions
   * @return a pair containing in the first element the  package versions already present
   *         in verticle data, in the other the missing ones
   */
  def findExistingAndMissingData(requestedPackagesVersions: Iterable[RequestedPackageVersions]): (ListBuffer[SnapshotData], ListBuffer[RequestedPackageVersions]) = {
    val existing = ListBuffer[SnapshotData]()
    // for each requested package version checks if the verticle already has the data
    val toRetrieve = requestedPackagesVersions.map { e =>
      val packageName = e.packageName
      val packageVersions = e.packageVersions.filterNot(_ == null)
      val packageCategory = PackageCategory.getPackageCategory(packageName)
      val missingPackageVersions = data.get(packageCategory) match {
        case Some(value) =>
          value.find(_.packageName == packageName) match {
            case Some(value) =>
              // removes null versions
              val availableEntries = value.packageVersionDataList.filterNot(_.version == null)
              val availableVersions = availableEntries.map(_.version)
              val (existingVersions, missingVersions) = packageVersions.partition(availableVersions.contains)
              val existingPackageVersions = availableEntries.filter(e => existingVersions.contains(e.version))
              existing.prepend(SnapshotData(packageName, existingPackageVersions))
              missingVersions
            case None => packageVersions
          }
        case None => packageVersions
      }
      RequestedPackageVersions(packageName, missingPackageVersions)
    }.to[ListBuffer]
    (existing, toRetrieve)
  }

  /** Downloads and return the first time the specified
   * package versions appeared in debian repository.
   *
   * @param toRetrieve the package versions to retrieve
   * @return the package versions associated with the date they first appeared in debian repository.
   */
  def downloadData(toRetrieve: Iterable[RequestedPackageVersions]): Future[ListBuffer[SnapshotData]] = {
    val snapshotDataList = ListBuffer[SnapshotData]()
    toRetrieve.foreachSynchronous(current => {
      val currentPackageName = current.packageName
      val packageVersions = current.packageVersions.filterNot(_ == null).toList
      downloadVersions(snapshotDataList, currentPackageName, packageVersions)
    }, Seq(new java.net.ConnectException()), delayOptions).compose(_ => snapshotDataList)
  }

  private def downloadVersions(snapshotDataList: ListBuffer[SnapshotData],
                               packageName: String, packageVersions: List[String]): Future[Unit] = {
    val snapshotData = snapshotDataList.find(_.packageName == packageName) match {
      case Some(value) => value
      case None =>
        val value = SnapshotData(packageName, ListBuffer())
        snapshotDataList += value
        value
    }
    downloadAvailablePackageVersions(packageName).compose { available =>
      packageVersions.filter(available.contains).foreachSynchronous(
        downloadPackageVersionPage(snapshotData, packageName, _),
        Seq(new java.net.ConnectException()), delayOptions, v => s"$packageName $v")
    }
  }

  /** Downloads the list of the available versions from package archive
   *
   * @param packageName the package which version should be retrieved
   * @return the list of the available versions for the package
   */
  private def downloadAvailablePackageVersions(packageName: String): Future[List[DebianSnapshotVersion]] = {
    val promise = Promise.promise[List[DebianSnapshotVersion]]()
    downloadAvailablePackageVersions(promise, packageName)
    promise.future()
  }

  private def downloadAvailablePackageVersions(promise: Promise[List[DebianSnapshotVersion]],
                                               packageName: String): Unit = {
    val requestUri = s"/mr/package/$packageName/"
    webClient.get(DataHost, requestUri).sendFuture.compose { value =>
      val httpResponse = value.httpResponse
      if (httpResponse.statusCode() == 200) {
        Logger.printOut(s"success https://$DataHost$requestUri")
        promise.complete(httpResponse.body.get.toString().fromJson[Response]().result)
      } else {
        Logger.printErr(s"https://$DataHost$requestUri ${httpResponse.statusCode()}")
        promise.complete(List[DebianSnapshotVersion]())
      }
    }.otherwise(e => e match {
      case _: ConnectException => downloadAvailablePackageVersions(promise, packageName)
      case _ => promise.complete()
    })
  }

  /** Downloads the content of the package version page from debian snapshot,
   * parsing the information of the first time such version appeared in debian repositories.
   *
   * @param snapshotData          the snapshotData to update with the
   * @param packageName           the package name
   * @param currentPackageVersion the package version
   * @return a future that will be succeeded when the operation is completed
   */
  private def downloadPackageVersionPage(snapshotData: SnapshotData,
                                         packageName: String,
                                         currentPackageVersion: String): Future[Unit] = {
    // TODO: https://salsa.debian.org/snapshot-team/snapshot/raw/master/API
    //  currently does not offer a way to retrieve "first_seen" data with one http request,
    //  for this reason that information is directly parsed from the html page of package version
    val requestUri = s"/package/$packageName/$currentPackageVersion/"
    webClient.get(DataHost, requestUri).sendFuture.compose { value =>
      val httpResponse = value.httpResponse
      if (httpResponse.statusCode() == 200) {
        // parse the page content
        val result = snapshotInfoPageParser.fromHtml(httpResponse.body.get.toString)
        Logger.printOut(s"success https://$DataHost$requestUri")
        if (result.packageVersionDataList.nonEmpty) {
          snapshotData.packageVersionDataList ++= result.packageVersionDataList
        }
      } else {
        Logger.printErr(s"https://$DataHost$requestUri ${httpResponse.statusCode()}")
      }
      Future.succeededFuture[Unit]()
    }
  }

}
