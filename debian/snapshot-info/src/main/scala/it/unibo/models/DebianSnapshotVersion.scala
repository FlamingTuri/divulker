package it.unibo.models

case class DebianSnapshotVersion(version: String)
