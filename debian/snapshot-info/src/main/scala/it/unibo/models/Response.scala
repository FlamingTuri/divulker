package it.unibo.models

import com.fasterxml.jackson.annotation.JsonProperty

case class Response(@JsonProperty("package")
                    packageName: String,
                    result: List[DebianSnapshotVersion])

