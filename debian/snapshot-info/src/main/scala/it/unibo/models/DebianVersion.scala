package it.unibo.models

case class DebianVersion(epoch: Int, upstreamVersion: String, debianRevision: Option[String] = Option.empty)
  extends Comparable[DebianVersion] {

  override def compareTo(other: DebianVersion): Int = {
    // TODO: change the comparison between upstream version or debianRevision
    //  to be the same as the one in dpkg, now it works fine most of the times
    if (epoch > other.epoch) {
      1
    } else if (epoch < other.epoch) {
      -1
    } else if (upstreamVersion > other.upstreamVersion) {
      1
    } else if (upstreamVersion < other.upstreamVersion) {
      -1
    } else if (debianRevision.nonEmpty && other.debianRevision.isEmpty) {
      1
    } else if (debianRevision.isEmpty && other.debianRevision.nonEmpty) {
      -1
    } else if (debianRevision.isEmpty && other.debianRevision.isEmpty) {
      0
    } else if (debianRevision.get > other.debianRevision.get) {
      1
    } else if (debianRevision.get < other.debianRevision.get) {
      -1
    } else {
      0
    }
  }

}
