package it.unibo.launcher

import it.unibo.verticle.{SnapshotInfoRetrieverVerticle, VerticleLauncher}

object SnapshotInfoRetrieverLauncher extends App with VerticleLauncher {
  deployVerticle[SnapshotInfoRetrieverVerticle]
}
