package it.unibo.constants

import it.unibo.models.service.{ServiceKind, ServiceName}

case class SnapshotSourceInfo() extends SourceVerticleInfo {

  override val serviceName: ServiceName.Value = ServiceName.DebianSnapshotInfoService
  override val serviceKind: ServiceKind.Value = ServiceKind.DistroSpecificRetriever
  override val WebSocketPath: String = "/snapshot-info"
}
