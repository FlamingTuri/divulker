application {
    mainClassName = "it.unibo.launcher.SnapshotInfoRetrieverLauncher"
    extra.set("mainClassName", mainClassName)
}

dependencies {
    val scalaVersion: String? by project.extra
    implementation("net.ruippeixotog", "scala-scraper_$scalaVersion", "2.2.0")
}

// add to the task run the possibility to set custom properties
val run by tasks.getting(JavaExec::class) {

    val local = "downloadMissing" //-PdownloadMissing=true/false
    project.properties[local]?.let {
        environment(local, it)
    }

}
