import { OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { WebSocketEventListenerService } from './web-socket-event-listener.service';

export abstract class WebSocketComponent implements OnInit, OnDestroy {
  private wsService = new WebSocketEventListenerService<string>();
  protected messageSubscription: Subscription;
  protected errorSubscription: Subscription;

  protected abstract host: string;
  protected abstract port: number;
  protected abstract uri: string;

  ngOnInit(): void {
    this.wsService.connect(this.host, this.port, this.uri);
    const self = this;
    this.messageSubscription = this.wsService.wsMessage$.subscribe(data => {
      this.onMessage(self, data);
    });
  }

  abstract onMessage(self, data: string);

  ngOnDestroy() {
    if (this.messageSubscription) {
      this.messageSubscription.unsubscribe();
    }
    if (this.errorSubscription) {
      this.errorSubscription.unsubscribe();
    }
    if (this.wsService) {
      this.wsService.ngOnDestroy();
    }
  }

}
