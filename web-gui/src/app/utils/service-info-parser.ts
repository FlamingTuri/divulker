import { ActivatedRoute } from '@angular/router';
import { ServiceInfo } from '../models/service-info';
import { HttpUri } from '../models/http-uri/http-uri';

export class ServiceInfoParser {
  protected serviceInfo: ServiceInfo;
  protected host: string;
  protected port: number;
  protected uri: string;

  constructor(public activatedRoute: ActivatedRoute) {
    this.activatedRoute.queryParams.subscribe(params => {
      this.serviceInfo = JSON.parse(params['serviceInfo']);
      this.host = this.serviceInfo.host;
      this.port = this.serviceInfo.port;
      this.uri = this.serviceInfo.wsPath;
    });
  }

  getServiceUri(): HttpUri {
    return HttpUri.create(this.host, this.port).addPath('/distro-data-comparator');
  }

}
