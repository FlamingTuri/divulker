import { TestBed } from '@angular/core/testing';

import { WebSocketEventListenerService } from './web-socket-event-listener.service';

describe('WebSocketEventListenerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WebSocketEventListenerService = TestBed.get(WebSocketEventListenerService);
    expect(service).toBeTruthy();
  });
});
