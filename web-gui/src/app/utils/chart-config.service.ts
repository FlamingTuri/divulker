import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ChartConfigService {

  config: object = {};

  reset() {
    this.config = {};
  }

  getChartAsArray(): [string, any][] {
    return Object.entries(this.config);
  }

  private readonly distroKey = 'distro';

  get distro() {
    return this.config[this.distroKey];
  }

  set distro(distro: string) {
    this.config[this.distroKey] = distro;
  }

  private readonly distrosKey = 'distros';

  get distros() {
    return this.config[this.distrosKey];
  }

  set distros(selectedDistros: string[]) {
    this.config[this.distrosKey] = selectedDistros;
  }

  private readonly packagesKey = 'packages';

  get packages(): string[] {
    return this.config[this.packagesKey];
  }

  set packages(selectedPackages: string[]) {
    this.config[this.packagesKey] = selectedPackages;
  }

  private readonly yAxisKey = 'yAxis';

  get yAxis() {
    return this.config[this.yAxisKey];
  }

  set yAxis(yAxis: string) {
    this.config[this.yAxisKey] = yAxis;
  }

  private readonly fromCveIdYearKey = 'fromCveIdYear';

  get fromCveIdYear() {
    return this.config[this.fromCveIdYearKey];
  }

  set fromCveIdYear(fromCveIdYear: number) {
    this.config[this.fromCveIdYearKey] = fromCveIdYear;
  }

  private readonly toCveIdYearKey = 'toCveIdYear';

  get toCveIdYear() {
    return this.config[this.toCveIdYearKey];
  }

  set toCveIdYear(toCveIdYear: number) {
    this.config[this.toCveIdYearKey] = toCveIdYear;
  }

  private readonly fixedCveOnlyKey = 'fixedCveOnly';

  get fixedCveOnly() {
    return this.config[this.fixedCveOnlyKey];
  }

  set fixedCveOnly(bool: boolean) {
    this.config[this.fixedCveOnlyKey] = bool;
  }

  private readonly yearKey = 'year';

  get year() {
    return this.config[this.yearKey];
  }

  set year(year: number) {
    this.config[this.yearKey] = year;
  }

  private readonly monthKey = 'month';

  get month() {
    return this.config[this.monthKey];
  }

  set month(month: number) {
    this.config[this.monthKey] = month;
  }

  private readonly weekNumberKey = 'weekNumber';

  get weekNumber() {
    return this.config[this.weekNumberKey];
  }

  set weekNumber(weekNumber: number) {
    this.config[this.weekNumberKey] = weekNumber;
  }

}
