declare global {
  interface Array<T> {
    sortNumerically(): Array<T>;

    /**
     * Removes all the array elements
     *
     * @return the removed elements
     */
    clear(): Array<T>;

    isEmpty(): boolean;

    nonEmpty(): boolean;
  }
}

Array.prototype.sortNumerically = function() {
  console.log(this);
  return this.sort((a, b) => a - b);
};

Array.prototype.clear = function() {
  return this.splice(0, this.length);
};

Array.prototype.isEmpty = function() {
  return this.length === 0;
};

Array.prototype.nonEmpty = function() {
  return this.length > 0;
};

export {};
