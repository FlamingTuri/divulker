import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appStepper]'
})
export class StepperDirective {

  constructor(public viewContainerRef: ViewContainerRef) { }

}
