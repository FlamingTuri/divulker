declare global {
  interface Number {

    /** Gets an array from start to end
     *
     * @param start the starting value
     * @param end the ending value (included)
     */
    range(start: number, end: number): number[];

  }
}

Number.prototype.range = (start: number, end: number) => {
  return [...Array(end - start + 1)].map((_, i) => i + start);
};

export {};
