import { Injectable, OnDestroy } from '@angular/core';
import { Observable, Observer, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WebSocketService implements OnDestroy {

  private subject: Subject<MessageEvent>;
  private ws: WebSocket;

  ngOnDestroy() {
    this.close();
  }

  /**
   * Connects to the specified websocket
   * Url example "ws://127.0.0.1:9999/echo"
   *
   * @param url the websocket url
   */
  connect(url: string): Subject<MessageEvent> {
    if (!this.subject) {
      this.subject = this.create(url);
      console.log('Successfully connected to: ' + url);
    }
    return this.subject;
  }

  private create(url: string): Subject<MessageEvent> {
    this.ws = new WebSocket(url);

    // used to register to data received via websocket
    const observable = new Observable((obs: Observer<MessageEvent>) => {
      this.ws.onmessage = obs.next.bind(obs);
      this.ws.onerror = obs.error.bind(obs);
      this.ws.onclose = obs.complete.bind(obs);
      return this.ws.close.bind(this.ws);
    });
    // used to send data via websocket
    const observer = {
      next: (data: object) => {
        if (this.ws.readyState === WebSocket.OPEN) {
          this.ws.send(JSON.stringify(data));
        }
      }
    };
    return Subject.create(observer, observable);
  }

  /**
   * Parses a raw socket MessageEvent
   *
   * @param event the MessageEvent to parse
   */
  parseData(event: MessageEvent): Promise<any> {
    const data = event.data;

    if (data instanceof Blob) {
      return new Promise(resolve => {
        const reader = new FileReader();

        reader.onload = () => resolve(reader.result);

        reader.readAsText(data);
      });
    } else {
      return Promise.resolve(data);
    }
  }

  /**
   * Sends data via websocket
   *
   * @param data the data to send
   */
  send(data) {
    this.ws.send(data);
  }

  /**
   * Closes the websocket
   */
  close() {
    this.ws.close();
    this.subject = null;
  }

}
