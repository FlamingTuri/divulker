import { Injectable, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { WebSocketService } from './web-socket.service';

@Injectable({
  providedIn: 'root'
})
export class WebSocketEventListenerService<T> implements OnDestroy {

  private webSocketService = new WebSocketService();
  private wsMessageSource = new Subject<T>();
  private wsErrorSource = new Subject();
  wsMessage$ = this.wsMessageSource.asObservable();
  wsError$ = this.wsErrorSource.asObservable();
  private host: string;
  private port: number;
  private uri: string;

  /**
   * Connects to the service via websocket
   *
   * @param host the service host
   * @param port the service port
   * @param uri the websocket uri
   */
  connect(host: string, port: number, uri: string) {
    this.host = host;
    this.port = port;
    this.uri = uri;
    this.connectAndSetupHandlers();
  }

  private connectAndSetupHandlers() {
    const url = `ws://${this.host}:${this.port}${this.uri}`;
    const subject = this.webSocketService.connect(url);

    subject.subscribe(value => {
      this.notifyMessage(value);
    }, error => {
      console.log(error);
      this.notifyError();
    }, () => {
      console.log('disconnected from ' + url);
      this.notifyError();
      // this.reconnectAfterDelay();
    });
  }

  private reconnectAfterDelay(timeout: number = 5000) {
    this.webSocketService.close();
    // Try to reconnect in X milliseconds
    setTimeout(() => this.connectAndSetupHandlers(), timeout);
  }

  /**
   * Notifies new messages incoming from the websocket
   *
   * @param value the raw message received
   */
  private notifyMessage(value: any) {
    this.webSocketService.parseData(value).then(parsedValue => this.wsMessageSource.next(parsedValue));
  }

  private notifyError() {
    this.wsErrorSource.next();
  }

  ngOnDestroy() {
    this.webSocketService.close();
  }

}
