import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PackageComparatorComponent } from './package-comparator.component';

describe('PackageComparatorComponent', () => {
  let component: PackageComparatorComponent;
  let fixture: ComponentFixture<PackageComparatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PackageComparatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PackageComparatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
