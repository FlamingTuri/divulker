import { Component } from '@angular/core';
import { ServiceInfoParser } from '../utils/service-info-parser';
import { ActivatedRoute } from '@angular/router';
import { HttpRequestService } from '../utils/http-request.service';
import { ChartConfigService } from '../utils/chart-config.service';
import { StepperEntry } from '../models/stepper-entry';
import { DistroSelectionComponent } from '../steppers/distro-selection/distro-selection.component';
import { PackageSelectionComponent } from '../steppers/package-selection/package-selection.component';
import { YAxixMeasurementUnitComponent } from '../steppers/y-axix-measurement-unit/y-axix-measurement-unit.component';
import { XAxisMeasurementUnitComponent } from '../steppers/x-axis-measurement-unit/x-axis-measurement-unit.component';
import { YAxisParameter, YAxisParameterUtils } from '../models/y-axis-parameter';
import { ChartComponent } from '../chart/chart.component';
import { DistroPackagesOccurrences } from '../models/occurrences/packages/distro-packages-occurrences';
import { PackageVulnerabilityWindow } from '../models/package-vulnerability-window';
import { HttpUri } from '../models/http-uri/http-uri';
import { VulnerabilityWindow } from '../models/vulnerability-window';

@Component({
  selector: 'app-package-comparator',
  templateUrl: './package-comparator.component.html',
  styleUrls: ['./package-comparator.component.scss']
})
export class PackageComparatorComponent extends ServiceInfoParser {

  stepperEntries: StepperEntry[] = [
    {label: 'Distro selection', viewer: DistroSelectionComponent},
    {label: 'Packages comparison selection', viewer: PackageSelectionComponent, additionalData: 2},
    {label: 'Y axis unit of measurement', viewer: YAxixMeasurementUnitComponent},
    {label: 'X axis unit of measurement', viewer: XAxisMeasurementUnitComponent}
  ];

  constructor(public activatedRoute: ActivatedRoute,
              private httpRequestService: HttpRequestService,
              private chartConfigService: ChartConfigService) {
    super(activatedRoute);
  }

  onDataRequest(appChart: ChartComponent) {
    const chartConfig = this.chartConfigService;
    const yAxis = chartConfig.yAxis;
    const url = this.getServiceUri()
      .addPath('/packages');
    let requestUrl: string;
    const keys = YAxisParameterUtils.getAsKeyValue.map(e => e.key);
    switch (yAxis as YAxisParameter) {
      case keys[0]:
        this.showVulnerabilityWindow(appChart, url, 'vulnerability window', (e: VulnerabilityWindow) => e.averageDays);
        break;
      case keys[1]:
        this.showVulnerabilityWindow(appChart, url, 'vulnerability number', (e: VulnerabilityWindow) => e.cveNumber);
        break;
      case keys[2]:
      case keys[3]:
        requestUrl = url.addPath('-occurrences')
          .addQueryParam('distro', chartConfig.distro)
          .addQueryParam('yAxisMode', yAxis)
          .addQueryParam('packages', chartConfig.packages)
          .addQueryParam('year', chartConfig.year)
          .addQueryParam('month', chartConfig.month)
          .addQueryParam('weekNumber', chartConfig.weekNumber)
          .get();
        this.httpRequestService.get<DistroPackagesOccurrences>(requestUrl).then(result => {
          console.log(result);
          appChart.clearYAxis();
          appChart.yAxisLabel = 'occurrences';
          if (chartConfig.month && chartConfig.weekNumber) {
            appChart.xAxisLabel = 'week day';
          } else if (chartConfig.month) {
            appChart.xAxisLabel = 'day';
          } else {
            appChart.xAxisLabel = 'month';
          }
          const xLabels = Object.entries(result.packageOccurrences[0].occurrences)
            .map(([key, _]) => `${key}`);
          console.log(xLabels);
          appChart.setXAxisLabels(xLabels);
          appChart.clearYAxis();
          result.packageOccurrences.forEach(e => {
            const values = Object.entries(e.occurrences)
              .map(([_, v]) => v);
            console.log(values);
            appChart.addData(e.packageName, values);
          });
        });
        break;
    }
  }

  private showVulnerabilityWindow(appChart: ChartComponent, url: HttpUri, yAxisLabel: string,
                                  getParam: (VulnerabilityWindow) => number) {
    const chartConfig = this.chartConfigService;
    const requestUrl = url.addPath('/cve/average-vulnerability-window')
      .addQueryParam('distro', chartConfig.distro)
      .addQueryParam('packages', chartConfig.packages)
      .addQueryParam('fixedCveOnly', chartConfig.fixedCveOnly)
      .addQueryParam('fromCveIdYear', chartConfig.fromCveIdYear)
      .addQueryParam('toCveIdYear', chartConfig.toCveIdYear)
      .get();
    this.httpRequestService.get<PackageVulnerabilityWindow[]>(requestUrl)
      .then(result => {
        console.log(result);
        appChart.clearYAxis();
        appChart.yAxisLabel = yAxisLabel;
        appChart.xAxisLabel = 'cve id year';
        const xLabels = Object.entries(result[0].averageVulnerabilityWindowByYear)
          .map(([key, _]) => `${key}`);
        console.log(xLabels);
        appChart.setXAxisLabels(xLabels);
        appChart.clearYAxis();
        result.forEach(e => {
          const values = Object.entries(e.averageVulnerabilityWindowByYear)
            .map(([_, v]) => getParam(v));
          console.log(values);
          appChart.addData(e.packageName, values);
        });
      });
  }

}
