import { AfterViewInit, Component } from '@angular/core';
import { Chart, ChartConfiguration, ChartDataSets } from 'chart.js';
import 'chartjs-plugin-colorschemes';
import { Pair } from '../models/pair';
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements AfterViewInit {

  ariaLabel = 'asd';
  chart: Chart;

  yAxisValues: ChartDataSets[] = [];
  xAxisLabels: string[] = [];

  ngAfterViewInit() {
    const options: ChartConfiguration = {
      type: 'line',
      data: {
        labels: this.xAxisLabels,
        datasets: this.yAxisValues
      },
      options: {
        maintainAspectRatio: false,
        title: {
          display: true,
          fontSize: 16,
          text: ''
        },
        scales: {
          yAxes: [{
            scaleLabel: {
              display: true,
              labelString: ''
            }
          }],
          xAxes: [{
            scaleLabel: {
              display: true,
              labelString: ''
            }
          }]
        },
        plugins: {
          colorschemes: {
            scheme: 'brewer.Paired12'
          }
        }
      }
    };
    this.chart = new Chart('chart', options);
  }

  set chartTitle(title: string) {
    this.chart.options.title.text = title;
    this.chart.update();
  }

  set yAxisLabel(title: string) {
    this.chart.options.scales.yAxes[0].scaleLabel.labelString = title;
    this.chart.update();
  }

  get xAxisLabel(): string {
    return this.chart.options.scales.xAxes[0].scaleLabel.labelString;
  }

  set xAxisLabel(title: string) {
    this.chart.options.scales.xAxes[0].scaleLabel.labelString = title;
    this.chart.update();
  }

  setXAxisLabels(labels: string[]) {
    this.xAxisLabels.clear();
    this.xAxisLabels.push(...labels);
    this.chart.update();
  }

  addDataFillingMissingLabels(label: string, data: Pair<string, number>[]) {
    const labels = data.map(p => p.key);
    const missing = this.xAxisLabels.filter(value => !labels.includes(value))
      .map(l => new Pair(l, 0));
    data.push(...missing);
    this.addData(label, data.sort(this.pairComparator).map(e => e.value));
  }

  pairComparator(a: Pair<string, number>, b: Pair<string, number>) {
    // tslint:disable-next-line:radix
    return parseInt(a.key) - parseInt(b.key);
  }

  addData(label: string, data: number[]) {
    this.yAxisValues.push({
      label,
      data,
      lineTension: 0,
      fill: false
    });
    this.chart.update();
  }

  clearYAxis() {
    this.yAxisValues.clear();
  }

  saveAsCsv(name: string) {
    let result = `${this.xAxisLabel},${this.yAxisValues.map(e => e.label).join(',')}\n`;
    this.xAxisLabels.forEach((e, i) => {
      result += `${e},${this.yAxisValues.map(v => v.data[i]).join(',')}\n`;
    });
    const file = new File([result], `${name}.csv`, {type: 'text/csv;charset=utf-8'});
    saveAs(file);
  }

}
