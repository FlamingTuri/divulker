import { Component } from '@angular/core';
import { YAxisParameter, YAxisParameterUtils } from '../../models/y-axis-parameter';
import { ChartConfigService } from '../../utils/chart-config.service';
import { FormField } from '../form-field';

@Component({
  selector: 'app-y-axix-measurement-unit',
  templateUrl: './y-axix-measurement-unit.component.html',
  styleUrls: ['./y-axix-measurement-unit.component.scss']
})
export class YAxixMeasurementUnitComponent implements FormField {

  yAxisValues = YAxisParameterUtils.getAsKeyValue;
  selected: string;

  constructor(private chartConfigService: ChartConfigService) {
    const yAxisMode = this.chartConfigService.yAxis;
    if (yAxisMode === undefined) {
      this.selected = this.yAxisValues[0].key;
    } else {
      this.selected = yAxisMode;
    }
    this.chartConfigService.yAxis = this.selected;
  }

  getPreviousConfig(): { 'key': string, 'value': YAxisParameter } {
    return this.yAxisValues.find(e => e.key === this.selected);
  }

  onSelectionChange(value: { key: string, value: YAxisParameter }) {
    this.selected = value.key;
    this.chartConfigService.yAxis = this.selected;
  }

  setAdditionalData(additionalData: any): void {
  }

  isOk(): boolean {
    return true;
  }

}
