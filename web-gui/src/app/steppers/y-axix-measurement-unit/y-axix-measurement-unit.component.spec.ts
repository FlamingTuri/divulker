import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YAxixMeasurementUnitComponent } from './y-axix-measurement-unit.component';

describe('YAxixMeasurementUnitComponent', () => {
  let component: YAxixMeasurementUnitComponent;
  let fixture: ComponentFixture<YAxixMeasurementUnitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YAxixMeasurementUnitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YAxixMeasurementUnitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
