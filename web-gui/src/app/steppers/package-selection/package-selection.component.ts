import { Component } from '@angular/core';
import { ChartConfigService } from '../../utils/chart-config.service';
import { FormField } from '../form-field';
import { AddableField } from '../addable-field';

@Component({
  selector: 'app-package-selection',
  templateUrl: './package-selection.component.html',
  styleUrls: ['./package-selection.component.scss']
})
export class PackageSelectionComponent extends AddableField<string> implements FormField {

  constructor(private chartConfigService: ChartConfigService) {
    super();
    const config = this.chartConfigService;
    const packages = config.packages;
    if (packages !== undefined) {
      this.currentFieldsNumber = packages.length;
      this.fieldsValues = packages;
    }
    if (this.fieldsValues.nonEmpty()) {
      config.packages = this.fieldsValues;
    }
  }

  add() {
    super.add();
    this.chartConfigService.packages = this.fieldsValues;
  }

  remove(i: number) {
    super.remove(i);
    if (this.fieldsValues.isEmpty()) {
      delete this.chartConfigService.packages;
    }
  }

  setAdditionalData(additionalData: any): void {
    this.setMinFieldsNumber(additionalData);
  }

  isOk(): boolean {
    this.chartConfigService.packages = this.fieldsValues;
    const packages = this.chartConfigService.packages;
    if (this.fieldsValues) {
      const valid = packages.filter(p => !(p === undefined || p === null || p.trim() === ''));
      return valid.length === this.currentFieldsNumber;
    } else {
      return this.getMinFieldsNumber() === 0;
    }
  }

}
