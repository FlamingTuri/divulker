import { Component } from '@angular/core';
import { ChartConfigService } from '../../utils/chart-config.service';
import { YAxisParameter } from '../../models/y-axis-parameter';
import { FormField } from '../form-field';
import '../../utils/number-extensions';

@Component({
  selector: 'app-x-axis-measurement-unit',
  templateUrl: './x-axis-measurement-unit.component.html',
  styleUrls: ['./x-axis-measurement-unit.component.scss']
})
export class XAxisMeasurementUnitComponent implements FormField {

  readonly minYear = 1999;
  readonly maxYear = new Date().getFullYear();
  readonly yearsRange = Number.prototype.range(this.minYear, this.maxYear).map(e => `${e}`);
  readonly minMonth = 1;
  readonly maxMonth = 12;
  readonly monthsRange = [''].concat(Number.prototype.range(this.minMonth, this.maxMonth).map(e => `${e}`));
  readonly minWeekNumber = 1;
  readonly maxWeekNumber = 4;
  readonly weekNumbersRange = [''].concat(Number.prototype.range(this.minWeekNumber, this.maxWeekNumber).map(e => `${e}`));
  fixedCveOnlyChecked = false;

  constructor(private chartConfigService: ChartConfigService) {
    this.fixedCveOnlyChecked = this.chartConfigService.fixedCveOnly;
  }

  mode(): boolean {
    const yAxis = this.chartConfigService.yAxis;
    return yAxis === this.getEnumValueFromString(YAxisParameter.AvgVulnerabilityWindow) ||
      yAxis === this.getEnumValueFromString(YAxisParameter.VulnerabilityNumber);
  }

  fixedCveOnlyCheckboxChange() {
    this.chartConfigService.fixedCveOnly = this.fixedCveOnlyChecked;
  }

  getEnumValueFromString(value: string): YAxisParameter {
    const idx = Object.values(YAxisParameter).findIndex(v => v === value);
    return Object.keys(YAxisParameter)[idx] as YAxisParameter;
  }

  setAdditionalData(additionalData: any): void {
  }

  isOk(): boolean {
    return true;
  }

}
