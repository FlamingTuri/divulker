import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { XAxisMeasurementUnitComponent } from './x-axis-measurement-unit.component';

describe('XAxisMeasurementUnitComponent', () => {
  let component: XAxisMeasurementUnitComponent;
  let fixture: ComponentFixture<XAxisMeasurementUnitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XAxisMeasurementUnitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XAxisMeasurementUnitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
