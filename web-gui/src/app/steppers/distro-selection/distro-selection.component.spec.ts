import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DistroSelectionComponent } from './distro-selection.component';

describe('DistroSelectionComponent', () => {
  let component: DistroSelectionComponent;
  let fixture: ComponentFixture<DistroSelectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DistroSelectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DistroSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
