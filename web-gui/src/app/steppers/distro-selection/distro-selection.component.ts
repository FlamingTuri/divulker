import { Component } from '@angular/core';
import '../../utils/number-extensions';
import { ChartConfigService } from '../../utils/chart-config.service';
import { HttpRequestService } from '../../utils/http-request.service';
import { ActivatedRoute } from '@angular/router';
import { ServiceInfoParser } from '../../utils/service-info-parser';
import { FormField } from '../form-field';

@Component({
  selector: 'app-distro-selection',
  templateUrl: './distro-selection.component.html',
  styleUrls: ['./distro-selection.component.scss']
})
export class DistroSelectionComponent extends ServiceInfoParser implements FormField {

  availableDistros: string[] = [];
  selectedDistro: string;

  constructor(private chartConfigService: ChartConfigService,
              public activatedRoute: ActivatedRoute,
              private httpRequestService: HttpRequestService) {
    super(activatedRoute);
    const url = this.getServiceUri().addPath('/distros').get();
    httpRequestService.get<any>(url)
      .then(result => {
        this.availableDistros = result.map(entry => entry.value);
        if (this.availableDistros.nonEmpty()) {
          this.selectedDistro = this.availableDistros[0];
          this.set();
        }
      });
  }

  selectionChange($event) {
    this.selectedDistro = $event.value;
    this.set();
  }

  set() {
    this.chartConfigService.distro = this.selectedDistro;
  }

  setAdditionalData(additionalData: any) {
  }

  isOk(): boolean {
    return this.chartConfigService.distro !== undefined;
  }

}
