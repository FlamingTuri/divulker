export class AddableField<T> {

  private notInitialized = true;
  private minFieldsNumber: number = 0;
  currentFieldsNumber: number = 0;
  fieldsValues: T[] = [];

  /** Sets the minimum fields number, should be called once
   *
   * @param n the minimum field number
   */
  setMinFieldsNumber(n: number) {
    if (this.notInitialized) {
      this.minFieldsNumber = n;
      if (this.currentFieldsNumber < n) {
        this.currentFieldsNumber = n;
      }
      this.notInitialized = false;
    } else {
      throw new Error('already initialized');
    }
  }

  getMinFieldsNumber(): number {
    return this.minFieldsNumber;
  }

  getFieldNumberRange(): number[] {
    return Number.prototype.range(0, this.currentFieldsNumber - 1);
  }

  add() {
    this.currentFieldsNumber += 1;
  }

  remove(i: number) {
    if (this.canRemove()) {
      this.currentFieldsNumber -= 1;
      this.fieldsValues.splice(i, 1);
    }
  }

  canRemove(): boolean {
    return this.currentFieldsNumber > this.minFieldsNumber;
  }

}
