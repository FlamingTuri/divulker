export interface FormField {

  /** Method called once after the component has been created
   * to set additional external parameters
   *
   * @param additionalData the additional data necessary to the component
   */
  setAdditionalData(additionalData: any): void;

  isOk(): boolean;

}
