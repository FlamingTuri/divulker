import { Component } from '@angular/core';
import { ServiceInfoParser } from '../../utils/service-info-parser';
import { ChartConfigService } from '../../utils/chart-config.service';
import { ActivatedRoute } from '@angular/router';
import { HttpRequestService } from '../../utils/http-request.service';
import { AddableField } from '../addable-field';
import { FormField } from '../form-field';

@Component({
  selector: 'app-distros-selection',
  templateUrl: './distros-selection.component.html',
  styleUrls: ['./distros-selection.component.scss']
})
export class DistrosSelectionComponent extends AddableField<string> implements FormField {

  serviceInfoParser: ServiceInfoParser;
  availableDistros: string[] = [];

  constructor(private chartConfigService: ChartConfigService,
              public activatedRoute: ActivatedRoute,
              private httpRequestService: HttpRequestService) {
    super();
    this.setMinFieldsNumber(2);
    this.serviceInfoParser = new ServiceInfoParser(activatedRoute);
    const url = this.serviceInfoParser.getServiceUri().addPath('/distros').get();
    httpRequestService.get<any>(url)
      .then(result => {
        this.availableDistros = result.map(e => e.value);
        const config = this.chartConfigService;
        const distros = config.distros;
        if (distros === undefined) {
          if (this.availableDistros.length >= this.getMinFieldsNumber()) {
            this.availableDistros.forEach((e, i) => this.currentFieldsNumber[i] = e);
          }
        } else {
          this.currentFieldsNumber = distros.length;
          this.fieldsValues = distros;
        }
      });
  }

  getNotSelectedDistros(idx: number): string[] {
    // removes from the selected distros the current selected one to display its name in the picker
    const selected = this.fieldsValues.filter((value, index) => index !== idx);
    return this.availableDistros.filter(value => !selected.includes(value));
  }

  selectionChange($event, idx: number) {
    this.fieldsValues[idx] = $event.value;
    this.chartConfigService.distros = this.fieldsValues;
  }

  setAdditionalData(additionalData: any): void {
  }

  isOk(): boolean {
    return true; // TODO
  }

}
