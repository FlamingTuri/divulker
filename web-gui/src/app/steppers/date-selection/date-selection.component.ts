import { Component, Input, OnInit } from '@angular/core';
import { ChartConfigService } from '../../utils/chart-config.service';

@Component({
  selector: 'app-date-selection',
  templateUrl: './date-selection.component.html',
  styleUrls: ['./date-selection.component.scss']
})
export class DateSelectionComponent implements OnInit {

  @Input() label: string;
  @Input() key: string;
  @Input() options: string[] = [''];
  @Input() required = false;
  value: string;

  constructor(private chartConfigService: ChartConfigService) {
  }

  ngOnInit(): void {
    const oldValue = this.chartConfigService.config[this.key];
    if (oldValue) {
      this.value = oldValue;
    }
  }

  onSelectionChange(value: string) {
    if (value === '') {
      delete this.chartConfigService.config[this.key];
    } else {
      this.value = value;
      this.chartConfigService.config[this.key] = this.value;
    }
  }

}
