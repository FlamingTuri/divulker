import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialComponentsModule } from './material-components/material-components.module';
import { DataComparatorSelectionComponent } from './data-comparator-selection/data-comparator-selection.component';
import { DistroDataComparatorComponent } from './distro-comparator/distro-data-comparator.component';
import { HttpClientModule } from '@angular/common/http';
import { ChartsModule } from 'ng2-charts';
import { ChartComponent } from './chart/chart.component';
import { PackageComparatorComponent } from './package-comparator/package-comparator.component';
import { ComparatorModeSelectionComponent } from './comparator-mode-selection/comparator-mode-selection.component';
import { StepperDialogComponent } from './stepper-dialog/stepper-dialog.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StepperDirective } from './utils/stepper.directive';
import { YAxixMeasurementUnitComponent } from './steppers/y-axix-measurement-unit/y-axix-measurement-unit.component';
import { XAxisMeasurementUnitComponent } from './steppers/x-axis-measurement-unit/x-axis-measurement-unit.component';
import { DistroSelectionComponent } from './steppers/distro-selection/distro-selection.component';
import { PackageSelectionComponent } from './steppers/package-selection/package-selection.component';
import { DistrosSelectionComponent } from './steppers/distros-selection/distros-selection.component';
import { ChartConfigurationDialogComponent } from './chart-configuration-dialog/chart-configuration-dialog.component';
import { DateSelectionComponent } from './steppers/date-selection/date-selection.component';
import { ComparatorComponent } from './comparator/comparator.component';


@NgModule({
  declarations: [
    AppComponent,
    DataComparatorSelectionComponent,
    DistroDataComparatorComponent,
    ChartComponent,
    PackageComparatorComponent,
    ComparatorModeSelectionComponent,
    StepperDialogComponent,
    StepperDirective,
    YAxixMeasurementUnitComponent,
    XAxisMeasurementUnitComponent,
    DistroSelectionComponent,
    PackageSelectionComponent,
    DistrosSelectionComponent,
    ChartConfigurationDialogComponent,
    DateSelectionComponent,
    ComparatorComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialComponentsModule,
    ChartsModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
