export interface ServiceInfo {
  name: string;
  serviceType: string;
  host: string;
  port: number;
  wsPath: string;
}
