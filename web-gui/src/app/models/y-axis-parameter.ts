export enum YAxisParameter {
  AvgVulnerabilityWindow = 'Average Vulnerability Window',
  VulnerabilityNumber = 'Vulnerability Number',
  DisclosuresNumber = 'Disclosures Number',
  FixesNumber = 'Fixes Number'
}

export type YAxisParameterKey = keyof typeof YAxisParameter;

export class YAxisParameterUtils {

  static readonly entries = Object.entries(YAxisParameter);

  static readonly getAsKeyValue = YAxisParameterUtils.entries.map(e => {
    return {key: e[0], value: e[1]};
  });

  static readonly enumToStringMap = new Map<YAxisParameterKey, YAxisParameter>(
    YAxisParameterUtils.entries.map(([key, value]: [YAxisParameterKey, YAxisParameter]) => [key, value])
  );

  static readonly stringToEnumMap = new Map<YAxisParameter, YAxisParameterKey>(
    YAxisParameterUtils.entries.map(([key, value]: [YAxisParameterKey, YAxisParameter]) => [value, key])
  );

  static getEnumValue(option: YAxisParameter): string {
    return YAxisParameterUtils.enumToStringMap.get(option.toString() as YAxisParameterKey);
  }

  static getCompanionOptions(option: YAxisParameter): YAxisParameter[] {
    if (option === undefined) {
      return [];
    }
    switch (YAxisParameterUtils.getEnumValue(option)) {
      case YAxisParameter.VulnerabilityNumber:
      case YAxisParameter.AvgVulnerabilityWindow:
        return [YAxisParameter.VulnerabilityNumber, YAxisParameter.AvgVulnerabilityWindow];
      case YAxisParameter.FixesNumber:
      case YAxisParameter.DisclosuresNumber:
        return [YAxisParameter.FixesNumber, YAxisParameter.DisclosuresNumber];
      default:
        return [];
    }
  }

  static getFromIndex(idx: number): YAxisParameter {
    return Object.keys(YAxisParameter)[idx] as YAxisParameter;
  }

}
