import { HttpUriParam } from './http-uri-param';

export class HttpUriFirstParam extends HttpUriParam {

  protected readonly paramSeparator: string = '?';

}
