import { HttpUriFirstParam } from './http-uri-first-param';

export class HttpUri extends HttpUriFirstParam {

  constructor(uri: string) {
    super(uri);
  }

  static create(host: string, port: number, https: boolean = false): HttpUri {
    let protocol = 'http';
    if (https) {
      protocol += 's';
    }
    return new HttpUri(`${protocol}://${host}:${port}`);
  }

  addPath(path: string): HttpUri {
    return new HttpUri(`${this.uri}${path}`);
  }

}
