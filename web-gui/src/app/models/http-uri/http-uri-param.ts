export class HttpUriParam {

  protected readonly uri: string;
  protected readonly paramSeparator: string = '&';

  constructor(uri: string) {
    this.uri = uri;
  }

  addQueryParam(key: string, value: string | boolean | number | object): HttpUriParam {
    if (value === null || value === undefined) {
      return this;
    }
    const valueType = typeof value;
    if (valueType === 'number' || valueType === 'boolean') {
      value = `${value}`;
    } else if (valueType !== 'string') {
      value = JSON.stringify(value);
    }
    return new HttpUriParam(`${this.uri}${this.paramSeparator}${key}=${value}`);
  }

  get(): string {
    return this.uri;
  }

}
