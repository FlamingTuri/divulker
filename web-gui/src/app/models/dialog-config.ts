export interface DialogConfig {
  width: string;
  height: string;
  data?: any;
}

export class DefaultDialogConfigImpl implements DialogConfig {
  width = '100%';
  height = '90%';
  data;

  constructor(data = null) {
    this.data = data;
  }
}

export class DialogConfigImpl extends DefaultDialogConfigImpl {

  constructor(width: string, height: string, data) {
    super(data);
    this.width = width;
    this.height = height;
  }
}
