export class DataComparatorPaths {
  static readonly vulnerabilityWindow = '/vulnerability-window';
  static readonly packageVulnerabilityWindow = '/packages-vulnerability-window-by-year';
  static readonly disclosuresInYear = '/disclosures-in-year';
  static readonly disclosuresInMonth = '/disclosures-in-year';
  static readonly disclosuresInWeek = '/disclosures-in-week';
}
