import { Type } from '@angular/core';

export interface StepperEntry {
  label: string;
  viewer: Type<any>;
  additionalData?: any;
}
