import { YearDateInfo } from './year-date-info';

export interface MonthDateInfo extends YearDateInfo {
  month: string;
}
