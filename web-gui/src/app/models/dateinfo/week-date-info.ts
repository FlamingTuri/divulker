import { MonthDateInfo } from './month-date-info';

export interface WeekDateInfo extends MonthDateInfo {
  from: number;
  to: number;
}
