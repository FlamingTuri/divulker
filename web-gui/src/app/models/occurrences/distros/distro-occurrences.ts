export interface DistroOccurrences {
  distroName: string;
  occurrences: Map<any, number>;
}
