import { DistroOccurrences } from './distro-occurrences';
import { YearDateInfo } from '../../dateinfo/year-date-info';
import { MonthDateInfo } from '../../dateinfo/month-date-info';
import { WeekDateInfo } from '../../dateinfo/week-date-info';

export interface DistrosOccurrencesInfo {
  dateInfo: YearDateInfo | MonthDateInfo | WeekDateInfo;
  distrosOccurrences: DistroOccurrences[];
}
