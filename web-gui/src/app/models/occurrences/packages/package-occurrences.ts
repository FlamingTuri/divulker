export interface PackageOccurrences {
  packageName: string;
  occurrences: Map<any, number>;
}
