import { PackageOccurrences } from './package-occurrences';

export interface DistroPackagesOccurrences {
  distroName: string;
  dateInfo: any;
  packageOccurrences: PackageOccurrences[];
}
