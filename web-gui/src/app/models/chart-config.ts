import { YAxisParameter } from './y-axis-parameter';

// I am not happy about this solution
export interface ChartConfig {
  yAxis: YAxisParameter;
  useCommonCve: boolean;
  // x axis config
  fromCveIdYear?: number;
  toCveIdYear?: number;
  week?: any;
  month?: any;
  year?: any;
  // distros and packages selection
  distro?: string;
  distros?: string[];
  packages?: string[];
}

export interface DistroComparisonChartConfig extends ChartConfig {
  distroToCompare: string[];
  packages?: string[];
}

export interface PackageComparisonChartConfig extends ChartConfig {
  distro: string;
  packages: string[];
}
