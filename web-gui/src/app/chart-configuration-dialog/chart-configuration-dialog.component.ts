import {
  ChangeDetectorRef,
  Component,
  ComponentFactoryResolver,
  ComponentRef,
  Inject,
  OnInit,
  QueryList,
  ViewChildren
} from '@angular/core';
import { StepperEntry } from '../models/stepper-entry';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ChartConfigService } from '../utils/chart-config.service';
import { StepperDirective } from '../utils/stepper.directive';
import { FormField } from '../steppers/form-field';

@Component({
  selector: 'app-chart-configuration-dialog',
  templateUrl: './chart-configuration-dialog.component.html',
  styleUrls: ['./chart-configuration-dialog.component.scss']
})
export class ChartConfigurationDialogComponent implements OnInit {

  @ViewChildren(StepperDirective) formFields: QueryList<StepperDirective>;
  stepperEntries: StepperEntry[] = [];
  componentRefs: ComponentRef<FormField>[] = [];

  constructor(public dialogRef: MatDialogRef<ChartConfigurationDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: StepperEntry[],
              private changeDetectorRef: ChangeDetectorRef,
              private componentFactoryResolver: ComponentFactoryResolver,
              private chartConfigService: ChartConfigService) {
    this.stepperEntries = data;
  }

  ngOnInit(): void {
    this.changeDetectorRef.detectChanges();
    this.stepperEntries.forEach((e, idx) => {
      const viewContainerRef = this.formFields.toArray()[idx].viewContainerRef;
      viewContainerRef.clear();
      const componentFactory = this.componentFactoryResolver.resolveComponentFactory(e.viewer);
      const componentRef = viewContainerRef.createComponent(componentFactory);
      this.componentRefs.push(componentRef);
      if (e.additionalData !== undefined) {
        this.componentRefs[idx].instance.setAdditionalData(e.additionalData);
      }
    });
  }

  onOkClick() {
    console.log(this.chartConfigService.config);
    const notOk = this.componentRefs.filter(e => !e.instance.isOk());
    if (notOk.isEmpty()) {
      // the other dialog closures results will be undefined
      this.dialogRef.close(true);
    } else {
      notOk.forEach(console.log);
    }
  }

  onCancelClick() {
    this.dialogRef.close();
  }

}
