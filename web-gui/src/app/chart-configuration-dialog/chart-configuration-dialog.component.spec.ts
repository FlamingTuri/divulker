import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartConfigurationDialogComponent } from './chart-configuration-dialog.component';

describe('ChartConfigurationDialogComponent', () => {
  let component: ChartConfigurationDialogComponent;
  let fixture: ComponentFixture<ChartConfigurationDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartConfigurationDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartConfigurationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
