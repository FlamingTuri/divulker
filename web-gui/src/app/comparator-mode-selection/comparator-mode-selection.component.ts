import { Component } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { ServiceInfoParser } from '../utils/service-info-parser';

@Component({
  selector: 'app-comparator-mode-selection',
  templateUrl: './comparator-mode-selection.component.html',
  styleUrls: ['./comparator-mode-selection.component.scss']
})
export class ComparatorModeSelectionComponent extends ServiceInfoParser {

  constructor(public activatedRoute: ActivatedRoute,
              private router: Router) {
    super(activatedRoute);
  }

  click(route: string) {
    const navigationExtras: NavigationExtras = {
      queryParams: {
        serviceInfo: JSON.stringify(this.serviceInfo)
      }
    };
    this.router.navigate([route], navigationExtras)
      .catch(reason => console.error(reason));
  }

}
