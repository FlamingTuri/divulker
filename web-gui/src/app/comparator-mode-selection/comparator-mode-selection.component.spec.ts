import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComparatorModeSelectionComponent } from './comparator-mode-selection.component';

describe('ComparatorModeSelectionComponent', () => {
  let component: ComparatorModeSelectionComponent;
  let fixture: ComponentFixture<ComparatorModeSelectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComparatorModeSelectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComparatorModeSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
