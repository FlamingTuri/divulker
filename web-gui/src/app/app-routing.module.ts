import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DataComparatorSelectionComponent } from './data-comparator-selection/data-comparator-selection.component';
import { DistroDataComparatorComponent } from './distro-comparator/distro-data-comparator.component';
import { ComparatorModeSelectionComponent } from './comparator-mode-selection/comparator-mode-selection.component';
import { PackageComparatorComponent } from './package-comparator/package-comparator.component';


const routes: Routes = [
  {path: 'data-comparator-selection', component: DataComparatorSelectionComponent},
  {path: 'comparator-mode-selection', component: ComparatorModeSelectionComponent},
  {path: 'distro-data-comparator', component: DistroDataComparatorComponent},
  {path: 'package-comparator', component: PackageComparatorComponent},
  {path: '', redirectTo: 'data-comparator-selection', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
