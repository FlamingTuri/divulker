import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataComparatorSelectionComponent } from './data-comparator-selection.component';

describe('DataComparatorSelectionComponent', () => {
  let component: DataComparatorSelectionComponent;
  let fixture: ComponentFixture<DataComparatorSelectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataComparatorSelectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataComparatorSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
