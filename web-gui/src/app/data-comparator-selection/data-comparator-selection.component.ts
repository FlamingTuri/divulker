import { Component } from '@angular/core';
import { ServiceInfo } from '../models/service-info';
import { WebSocketComponent } from '../utils/web-socket.component';
import { NavigationExtras, Router } from '@angular/router';
import '../utils/array-extensions';

@Component({
  selector: 'app-data-comparator-selection',
  templateUrl: './data-comparator-selection.component.html',
  styleUrls: ['./data-comparator-selection.component.scss']
})
export class DataComparatorSelectionComponent extends WebSocketComponent {

  protected host = '127.0.0.1';
  protected port = 8080;
  protected uri = '/registry';

  displayedColumns: string[] = ['name', 'host', 'port', 'wsPath', 'selection'];
  distroComparatorServices: ServiceInfo[] = [];

  constructor(private router: Router) {
    super();
  }

  onMessage(self: DataComparatorSelectionComponent, data: string) {
    const services: ServiceInfo[] = JSON.parse(data);
    const distroComparatorServices = services.filter(v => v.serviceType === 'DistroComparator');
    self.distroComparatorServices = self.distroComparatorServices.concat(distroComparatorServices);
  }

  selectDistroComparator(distroComparatorInfo: ServiceInfo) {
    console.log(distroComparatorInfo);
    const navigationExtras: NavigationExtras = {
      queryParams: {
        serviceInfo: JSON.stringify(distroComparatorInfo)
      }
    };
    this.router.navigate(['comparator-mode-selection'], navigationExtras)
      .catch(reason => console.error(reason));
  }

}
