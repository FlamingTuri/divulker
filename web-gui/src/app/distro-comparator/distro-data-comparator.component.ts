import { Component } from '@angular/core';
import { DistroVulnerabilityWindowByYear } from '../models/distro-vulnerability-window-by-year';
import '../utils/array-extensions';
import { YAxisParameter, YAxisParameterUtils } from '../models/y-axis-parameter';
import { StepperEntry } from '../models/stepper-entry';
import { YAxixMeasurementUnitComponent } from '../steppers/y-axix-measurement-unit/y-axix-measurement-unit.component';
import { XAxisMeasurementUnitComponent } from '../steppers/x-axis-measurement-unit/x-axis-measurement-unit.component';
import { PackageSelectionComponent } from '../steppers/package-selection/package-selection.component';
import { DistrosSelectionComponent } from '../steppers/distros-selection/distros-selection.component';
import { VulnerabilityWindow } from '../models/vulnerability-window';
import { HttpUri } from '../models/http-uri/http-uri';
import { DistrosOccurrencesInfo } from '../models/occurrences/distros/distros-occurrences-info';
import { ChartConfigService } from '../utils/chart-config.service';
import { ServiceInfoParser } from '../utils/service-info-parser';
import { ActivatedRoute } from '@angular/router';
import { HttpRequestService } from '../utils/http-request.service';
import { ChartComponent } from '../chart/chart.component';

@Component({
  selector: 'app-distro-comparator',
  templateUrl: './distro-data-comparator.component.html',
  styleUrls: ['./distro-data-comparator.component.scss']
})
export class DistroDataComparatorComponent extends ServiceInfoParser {

  stepperEntries: StepperEntry[] = [
    {label: 'Distro comparison selection', viewer: DistrosSelectionComponent},
    {label: 'Packages comparison selection', viewer: PackageSelectionComponent},
    {label: 'Y axis unit of measurement', viewer: YAxixMeasurementUnitComponent},
    {label: 'X axis unit of measurement', viewer: XAxisMeasurementUnitComponent}
  ];

  constructor(public activatedRoute: ActivatedRoute,
              private chartConfigService: ChartConfigService,
              private httpRequestService: HttpRequestService) {
    super(activatedRoute);
  }

  onDataRequest(appChart: ChartComponent) {
    const chartConfig = this.chartConfigService;
    const yAxis = chartConfig.yAxis;
    const url = this.getServiceUri().addPath('/distros');
    const keys = YAxisParameterUtils.getAsKeyValue.map(e => e.key);
    switch (yAxis as YAxisParameter) {
      case keys[0]:
        this.showVulnerabilityWindow(appChart, url, 'vulnerability window', (e: VulnerabilityWindow) => e.averageDays);
        break;
      case keys[1]:
        this.showVulnerabilityWindow(appChart, url, 'vulnerability number', (e: VulnerabilityWindow) => e.cveNumber);
        break;
      case keys[2]:
      case keys[3]:
        const requestUrl = url
          .addPath('-occurrences')
          .addQueryParam('distros', chartConfig.distros)
          .addQueryParam('packages', chartConfig.packages)
          .addQueryParam('yAxisMode', yAxis)
          .addQueryParam('year', chartConfig.year)
          .addQueryParam('month', chartConfig.month)
          .addQueryParam('weekNumber', chartConfig.weekNumber)
          .get();
        this.httpRequestService.get<DistrosOccurrencesInfo>(requestUrl)
          .then(result => {
            console.log(result);
            appChart.clearYAxis();
            let title = yAxis;
            if (chartConfig.month && chartConfig.weekNumber) {
              appChart.xAxisLabel = 'week day';
              title += ` ${JSON.stringify(result.dateInfo)}`; // TODO: prettier format
            } else if (chartConfig.month) {
              appChart.xAxisLabel = 'day';
              title += ` ${JSON.stringify(result.dateInfo)}`; // TODO: prettier format
            } else {
              appChart.xAxisLabel = 'month';
              title += ` ${JSON.stringify(result.dateInfo)}`; // TODO: prettier format
            }
            // does not work
            appChart.chartTitle = title;
            const xLabels = Object.entries(result.distrosOccurrences[0].occurrences)
              .map(([key, _]) => `${key}`);
            appChart.setXAxisLabels(xLabels);
            appChart.clearYAxis();
            result.distrosOccurrences.forEach(e => {
              const values = Object.entries(e.occurrences)
                .map(([_, v]) => v);
              console.log(values);
              appChart.addData(e.distroName, values);
            });
          });
        break;
    }
  }

  private showVulnerabilityWindow(appChart: ChartComponent, url: HttpUri, yAxisLabel: string,
                                  getParam: (e: VulnerabilityWindow) => number) {
    const chartConfig = this.chartConfigService;
    const requestUrl = url
      .addPath('/cve/average-vulnerability-window')
      .addQueryParam('distros', chartConfig.distros)
      .addQueryParam('packages', chartConfig.packages)
      .addQueryParam('fromCveIdYear', chartConfig.fromCveIdYear)
      .addQueryParam('toCveIdYear', chartConfig.toCveIdYear)
      .addQueryParam('fixedCveOnly', chartConfig.fixedCveOnly)
      .get();
    this.httpRequestService.get<DistroVulnerabilityWindowByYear[]>(requestUrl)
      .then(result => {
        console.log(result);
        appChart.clearYAxis();
        appChart.chartTitle = chartConfig.yAxis;
        appChart.yAxisLabel = yAxisLabel;
        appChart.xAxisLabel = 'cve id year';
        const xLabels = Object.entries(result[0].vulnerabilityWindowByYear)
          .map(([key, _]) => `${key}`);
        console.log(xLabels);
        appChart.setXAxisLabels(xLabels);
        appChart.clearYAxis();
        result.forEach(e => {
          const values = Object.entries(e.vulnerabilityWindowByYear)
            .map(([_, v]) => getParam(v));
          console.log(values);
          appChart.addData(e.distro, values);
        });
      });
  }

}
