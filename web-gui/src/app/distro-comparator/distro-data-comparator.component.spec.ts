import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DistroDataComparatorComponent } from './distro-data-comparator.component';

describe('DistroComparatorComponent', () => {
  let component: DistroDataComparatorComponent;
  let fixture: ComponentFixture<DistroDataComparatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DistroDataComparatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DistroDataComparatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
