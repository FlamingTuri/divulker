import {
  ChangeDetectorRef,
  Component,
  ComponentFactoryResolver,
  ComponentRef,
  Inject,
  OnInit,
  QueryList,
  ViewChildren
} from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { StepperEntry } from '../models/stepper-entry';
import { StepperDirective } from '../utils/stepper.directive';
import { ChartConfigService } from '../utils/chart-config.service';
import { AbstractStepper } from '../steppers/abstract-stepper';

@Component({
  selector: 'app-stepper-dialog',
  templateUrl: './stepper-dialog.component.html',
  styleUrls: ['./stepper-dialog.component.scss']
})
export class StepperDialogComponent implements OnInit {

  @ViewChildren(StepperDirective) appSteppers: QueryList<StepperDirective>;
  stepperEntries: StepperEntry[] = [];
  componentRefs: ComponentRef<AbstractStepper>[] = [];

  constructor(public dialogRef: MatDialogRef<StepperDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: StepperEntry[],
              private changeDetectorRef: ChangeDetectorRef,
              private componentFactoryResolver: ComponentFactoryResolver,
              private chartConfigService: ChartConfigService) {
    this.stepperEntries = data;
  }

  ngOnInit() {
    this.changeDetectorRef.detectChanges();
    this.stepperEntries.forEach((e, idx) => {
      const viewContainerRef = this.appSteppers.toArray()[idx].viewContainerRef;
      viewContainerRef.clear();
      const componentFactory = this.componentFactoryResolver.resolveComponentFactory(e.viewer);
      const componentRef = viewContainerRef.createComponent(componentFactory);
      this.componentRefs.push(componentRef);
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onOkClick() {
    this.dialogRef.close();
  }

  onNextClick(idx: number) {
    this.componentRefs[idx].instance.onNextClick();
  }
}
