import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import { ChartComponent } from '../chart/chart.component';
import { StepperEntry } from '../models/stepper-entry';
import { DefaultDialogConfigImpl, DialogConfig } from '../models/dialog-config';
import { HttpRequestService } from '../utils/http-request.service';
import { DialogService } from '../utils/dialog.service';
import { ChartConfigService } from '../utils/chart-config.service';
import { ChartConfigurationDialogComponent } from '../chart-configuration-dialog/chart-configuration-dialog.component';
import { ServiceInfoParser } from '../utils/service-info-parser';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-comparator',
  templateUrl: './comparator.component.html',
  styleUrls: ['./comparator.component.scss']
})
export class ComparatorComponent extends ServiceInfoParser implements OnInit, AfterViewInit {

  @ViewChild(ChartComponent) appChart: ChartComponent;
  @Input() stepperEntries: StepperEntry[] = [];
  private dialogConfig: DialogConfig;
  @Output() dataRequest = new EventEmitter<ChartComponent>();

  constructor(public activatedRoute: ActivatedRoute,
              private httpRequestService: HttpRequestService,
              public dialogService: DialogService,
              private chartConfigService: ChartConfigService) {
    super(activatedRoute);
    this.chartConfigService.reset();
  }

  ngOnInit(): void {
    this.dialogConfig = new DefaultDialogConfigImpl(this.stepperEntries);
  }

  ngAfterViewInit() {
    this.openDialog();
  }

  openDialog() {
    const ref = this.dialogService.open(ChartConfigurationDialogComponent, this.dialogConfig);
    this.dialogService.getClosureEvent(ref).then(result => {
      if (result) {
        this.dataRequest.emit(this.appChart);
      }
    }).catch(this.openDialog);
  }

  saveData() {
    this.appChart.saveAsCsv(this.chartConfigService.yAxis);
  }

}
