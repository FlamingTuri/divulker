package it.unibo.handlers

import io.vertx.scala.core.http.WebSocket
import it.unibo.jackson.MarshallableImplicit._
import it.unibo.models.general.aggregator.AggregatedVulnerabilityData
import it.unibo.models.service.ServiceName
import it.unibo.models.ws.messages.aggregator.AggregatorResponses.AggregatorResponseMessage
import it.unibo.models.ws.messages.aggregator.{AggregatorOperation, AggregatorRequestMessage}
import it.unibo.models.ws.messages.operations.Subscribe
import it.unibo.updates.strategies.VulnerabilityDataStrategies
import it.unibo.verticle.DistroDataComparatorVerticle

import scala.collection.mutable.ListBuffer

case class GatheredDataHandler() extends WsHandler[DistroDataComparatorVerticle] {

  override protected val wsRequestMessage: AggregatorRequestMessage = {
    AggregatorRequestMessage(Subscribe)
  }

  override protected val sourceName: ServiceName.Value = ServiceName.DistroDataComparatorService

  override protected def textMessageHandler(webSocket: WebSocket, text: String): Unit = {
    /*val result = text.fromJson[AggregatorResponseMessage]()
    val aggregatorServiceName = result.aggregatorServiceName
    val verticleData = verticle.data
    val updatedEntries = ListBuffer[VulnerabilityData]()
    val newEntries = ListBuffer[VulnerabilityData]()
    result.data.foreach { update =>
      verticleData.find(current => VulnerabilityDataStrategies.compareStrategy(current, update)) match {
        case Some(value) =>
          if (VulnerabilityDataStrategies.updateStrategy(value, update, aggregatorServiceName) &&
            !newEntries.contains(value)) {
            updatedEntries += value
          }
        case None =>
          val newEntry = VulnerabilityDataStrategies.newEntryStrategy(update, aggregatorServiceName)
          verticleData += newEntry
          newEntries += newEntry
      }
    }*/
    //newEntries.take(10).foreach(println)
    //verticle.data.myUpdate(result.data, compareStrategy, updateStrategy, newEntryStrategy)
    /*verticle.data.get(aggregatorServiceName) match {
      case Some(gatheredDataList) =>
        val (updatedEntries, newEntries) = gatheredDataList
          .myUpdate(result.data, compareStrategy, updateStrategy)
        verticle.notifyDataComparatorSubscribers(aggregatorServiceName, updatedEntries ++ newEntries)
      case None =>
    }*/
  }

}
