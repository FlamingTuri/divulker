package it.unibo.verticle

import io.vertx.core.buffer.Buffer
import io.vertx.scala.core.Future
import io.vertx.scala.core.http.ServerWebSocket
import io.vertx.scala.ext.web.RoutingContext
import io.vertx.scala.ext.web.client.HttpResponse
import it.unibo.constants._
import it.unibo.handlers.{VulnerabilityDataHandler, WsHandler}
import it.unibo.jackson.MarshallableImplicit._
import it.unibo.models.CustomTypes.{ServicesData, YearMaskVulnerabilities}
import it.unibo.models.aggregator.GatheredData
import it.unibo.models.general.aggregator.AggregatedVulnerabilityData
import it.unibo.models.service.{ServiceInfo, ServiceKind, ServiceName}
import it.unibo.models.ws.messages.Entries
import it.unibo.models.ws.messages.comparator.{DataComparatorRequestMessage, DataComparatorResponseMessage}
import it.unibo.utils.DateUtilities
import it.unibo.verticle.aggregator.WsConnectionUtility
import it.unibo.vertx.Cors
import it.unibo.vertx.WebSocketHelpers.WebSocketExtensions

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

class DistroDataComparatorVerticle extends RegisterSourcesRetrieverVerticle[YearMaskVulnerabilities[AggregatedVulnerabilityData]] {

  override protected val verticleInfo: SourceVerticleInfo = DistroDataComparatorSourceInfo()
  override protected val registerRequestUri: String =
    s"${RegistryPaths.SourcesPath}?sourceKind=${ServiceKind.GeneralAggregator}"

  protected val subscribers: mutable.Map[ServiceName.Value, ListBuffer[ServerWebSocket]] = mutable.HashMap()
  val distros: mutable.Set[ServiceName.Value] = mutable.Set()
  var wsConnectionUtility: WsConnectionUtility[DistroDataComparatorVerticle] = _
  val generalAggregators: ListBuffer[ServicesData[WsHandler[DistroDataComparatorVerticle]]] = ListBuffer()
  override val data: YearMaskVulnerabilities[AggregatedVulnerabilityData] = mutable.SortedMap()

  override protected def setupRoutes(): Unit = {
    super.setupRoutes()
    // only the following routes will allow CORS requests
    router.route().handler(Cors.corsHandler)

    router.get(DistroDataComparatorPaths.DistrosPath)
      .produces(MimeType.ApplicationJson)
      .handler(getDistros)

    val vulnerabilityWindowHandler = WindowOfVulnerabilityRequestHandler(this)
    router.get(DistroDataComparatorPaths.DistroVulnerabilityWindowPath)
      .produces(MimeType.ApplicationJson)
      .handler(vulnerabilityWindowHandler.getOverallAverageWindowOfVulnerabilityHandler)

    router.get(DistroDataComparatorPaths.DistrosCveAverageVulnerabilityWindow)
      .produces(MimeType.ApplicationJson)
      .handler(vulnerabilityWindowHandler.getDistrosAverageWindowOfVulnerabilityByYearHandler)

    router.get(DistroDataComparatorPaths.PackagesCveAverageVulnerabilityWindow)
      .produces(MimeType.ApplicationJson)
      .handler(vulnerabilityWindowHandler.getPackagesAverageWindowOfVulnerabilityHandler)

    router.get(DistroDataComparatorPaths.DistroWoVInYearRange)
      .produces(MimeType.ApplicationJson)
      .handler(vulnerabilityWindowHandler.getWindowOfVulnerabilityInYearRangeHandler)

    router.get(DistroDataComparatorPaths.DistroNumberByYear)
      .produces(MimeType.ApplicationJson)
      .handler(vulnerabilityWindowHandler.getDisclosuresByYearHandler)

    router.get(DistroDataComparatorPaths.DistroNumberByYearSum)
      .produces(MimeType.ApplicationJson)
      .handler(vulnerabilityWindowHandler.getDisclosuresByYearSumHandler)

    val vulnerabilityFrequency = VulnerabilityFrequencyHandler(this)
    router.get(DistroDataComparatorPaths.PackagesOccurrences)
      .produces(MimeType.ApplicationJson)
      .handler { routingContext =>
        DateUtilities.toLocalDate(routingContext.request().params(),
          _ => vulnerabilityFrequency.packagesOccurrencesInYearHandler(routingContext),
          _ => vulnerabilityFrequency.packagesOccurrencesInMonthHandler(routingContext),
          _ => vulnerabilityFrequency.packagesOccurrencesInWeekHandler(routingContext))
      }

    router.get(DistroDataComparatorPaths.PackagesOccurrencesInYear)
      .produces(MimeType.ApplicationJson)
      .handler(vulnerabilityFrequency.packagesOccurrencesInYearHandler)

    router.get(DistroDataComparatorPaths.PackagesOccurrencesInMonth)
      .produces(MimeType.ApplicationJson)
      .handler(vulnerabilityFrequency.packagesOccurrencesInMonthHandler)

    router.get(DistroDataComparatorPaths.PackagesOccurrencesInWeek)
      .produces(MimeType.ApplicationJson)
      .handler(vulnerabilityFrequency.packagesOccurrencesInWeekHandler)

    router.get(DistroDataComparatorPaths.DistrosOccurrences)
      .produces(MimeType.ApplicationJson)
      .handler { routingContext =>
        DateUtilities.toLocalDate(routingContext.request().params(),
          _ => vulnerabilityFrequency.distrosOccurrencesInYearHandler(routingContext),
          _ => vulnerabilityFrequency.distrosOccurrencesInMonthHandler(routingContext),
          _ => vulnerabilityFrequency.distrosOccurrencesInWeekHandler(routingContext))
      }

    router.get(DistroDataComparatorPaths.DistrosOccurrencesInYear)
      .produces(MimeType.ApplicationJson)
      .handler(vulnerabilityFrequency.distrosOccurrencesInYearHandler)

    router.get(DistroDataComparatorPaths.DistrosOccurrencesInMonth)
      .produces(MimeType.ApplicationJson)
      .handler(vulnerabilityFrequency.distrosOccurrencesInMonthHandler)

    router.get(DistroDataComparatorPaths.DistrosOccurrencesInWeek)
      .produces(MimeType.ApplicationJson)
      .handler(vulnerabilityFrequency.distrosOccurrencesInWeekHandler)

    router.get(DistroDataComparatorPaths.DistroOccurrencesInYearByFourMonths)
      .produces(MimeType.ApplicationJson)
      .handler(vulnerabilityFrequency.distroOccurrencesInYearByFourMonthsHandler)

    val topPackages = TopPackagesHandler(this)
    router.get(DistroDataComparatorPaths.TopDistroPackages)
      .produces(MimeType.ApplicationJson)
      .handler(topPackages.distroTopPackagesHandler)
  }

  override protected def handleRegisterResponse(httpResponse: HttpResponse[Buffer]): Future[Unit] = {
    wsConnectionUtility = aggregator.WsConnectionUtility(this, httpClient, vertx)
    httpResponse.bodyAsString().get.fromJson[List[ServiceInfo]]()
      .foreach { serviceInfo =>
        val handler = VulnerabilityDataHandler()
        generalAggregators += ((serviceInfo, handler))
      }
    wsConnectionUtility.connectAll(generalAggregators)
  }

  def notifyDataComparatorSubscribers(aggregatorServiceName: ServiceName.Value,
                                      entries: Entries[GatheredData]): Unit = {
    if (subscribers.nonEmpty) {
      val subscribersToServiceName = subscribers(aggregatorServiceName)
      if (entries.nonEmpty && subscribersToServiceName.nonEmpty) {
        val response = DataComparatorResponseMessage(aggregatorServiceName, entries)
        subscribersToServiceName.foreach { serverWebSocket =>
          serverWebSocket.writeTextMessageFramed(response.toJson)
        }
      }
    }
  }

  override protected def wsTextMessageHandler(serverWebSocket: ServerWebSocket, textMessage: String): Unit = {
    val requestMessage = textMessage.fromJson[DataComparatorRequestMessage]()
    /*requestMessage.operation match {
      case DataComparatorOperation.Subscribe =>
        requestMessage.subscriptions.foreach { subscription =>
          // register subscription
          subscribers.get(subscription) match {
            case Some(value) => value += serverWebSocket
            case None => subscribers += (subscription -> ListBuffer(serverWebSocket))
          }
          // send current data to new subscriber
          //val response = DataComparatorResponseMessage(subscription, data(subscription))
          //serverWebSocket.writeTextMessage(response.toJson)
        }
      case DataComparatorOperation.Unsubscribe =>
        requestMessage.subscriptions.foreach { subscription =>
          subscribers.get(subscription) match {
            case Some(value) => value -= serverWebSocket
            case None =>
          }
        }
    }*/
  }

  override protected def wsCloseHandler(serverWebSocket: ServerWebSocket): Unit = {
    subscribers.values.foreach { subscribedServerWebSockets =>
      subscribedServerWebSockets -= serverWebSocket
    }
  }

  def getDistros(routingContext: RoutingContext): Unit = {
    val response = routingContext.response()
    response.end(distros.toJson)
  }

}
