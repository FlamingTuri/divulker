package it.unibo.verticle

import io.vertx.scala.core.MultiMap
import io.vertx.scala.ext.web.RoutingContext

class RequestHandler {

  /** Generic request handler that retrieves info from request
   *
   * @param routingContext the request's routing context
   * @param mainRequestParam
   * @param handler        the handler that will be called with the retrieved info,
   *                       the handler should return a json formatted string
   */
  protected def requestHandler(routingContext: RoutingContext, mainRequestParam: String,
                               handler: (String, MultiMap) => String): Unit = {
    val response = routingContext.response()
    val request = routingContext.request()
    val requestParams = request.params()
    println(requestParams.names())
    requestParams.get(mainRequestParam) match {
      case Some(value) => response.end(handler(value, requestParams))
      case None => response.setStatusCode(404).end(s"$mainRequestParam not found")
    }
  }

  protected def singleDistroHandler(routingContext: RoutingContext, handler: (String, MultiMap) => String): Unit = {
    requestHandler(routingContext, "distro", handler)
  }

  protected def multipleDistroHandler(routingContext: RoutingContext, handler: (String, MultiMap) => String): Unit = {
    requestHandler(routingContext, "distros", handler)
  }

}
