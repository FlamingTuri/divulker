package it.unibo.verticle

import io.vertx.scala.ext.web.RoutingContext
import it.unibo.jackson.MarshallableImplicit._
import it.unibo.models.service.ServiceName
import it.unibo.updates.collections.MapUpdateUtils._

case class TopPackagesHandler(verticle: DistroDataComparatorVerticle) {

  def distroTopPackagesHandler(routingContext: RoutingContext): Unit = {
    val response = routingContext.response()
    val request = routingContext.request()
    val requestParams = request.params()
    requestParams.get("distro") match {
      case Some(distro) =>
        val serviceName = ServiceName.withName(distro)
        val count = requestParams.get("count").getOrElse("5").toInt
        val map = verticle.data.getAsIterable
          // takes the package names only related to the specified distro
          .flatMap(_.fixes.get(serviceName))
          .flatMap(_.keySet).groupBy(identity)
          .mapValues(_.size).toList.sortBy(-_._2).take(count)
        // the sort is by the most affected to the least affected
        response.end(map.toJson)
      case None => response.setStatusCode(404).end(s"distro not found")
    }
  }

}
