package it.unibo.constants

import it.unibo.models.service.{ServiceKind, ServiceName}

case class DistroDataComparatorSourceInfo() extends SourceVerticleInfo {
  /** The service name */
  override val serviceName: ServiceName.Value = ServiceName.DistroDataComparatorService
  /** The service kind */
  override val serviceKind: ServiceKind.Value = ServiceKind.DistroComparator
  /** The path accepted for websocket messages */
  override val WebSocketPath: String = "/distro-data-comparator"
}
