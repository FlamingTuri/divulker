package it.unibo.utils.filters

trait YearRangePredicatesBuilder[B, T] extends PredicatesBuilder[B, T] with YearRangePredicates[B]
