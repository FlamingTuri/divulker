package it.unibo.utils.filters

import it.unibo.models.CustomTypes.{MaskVulnerabilities, YearMaskVulnerabilities}

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

object PredicatesUtils {

  implicit class FilterableList[T](list: ListBuffer[T]) {

    def filterByPredicates(predicates: ListBuffer[T => Boolean]): ListBuffer[T] = {
      if (predicates.nonEmpty) {
        list.filter(gatheredData => predicates.forall(p => p(gatheredData)))
      } else {
        list
      }
    }
  }

  implicit class FilterableMap[T](list: YearMaskVulnerabilities[T]) {

    def filterByPredicates(keyPredicates: ListBuffer[(Int, MaskVulnerabilities[T]) => Boolean],
                           entryPredicates: ListBuffer[T => Boolean]): YearMaskVulnerabilities[T] = {
      list.filterByKeyPredicates(keyPredicates).filterByPredicates(entryPredicates)
    }

    def filterByKeyPredicates(keyPredicates: ListBuffer[(Int, MaskVulnerabilities[T]) => Boolean]): YearMaskVulnerabilities[T] = {
      if (keyPredicates.nonEmpty) {
        list.filter(gatheredData => keyPredicates.forall(p => p(gatheredData._1, gatheredData._2)))
      } else {
        list
      }
    }

    def filterByPredicates(predicates: ListBuffer[T => Boolean]): YearMaskVulnerabilities[T] = {
      if (predicates.nonEmpty) {
        // TODO: surely there is a more functional way to do this with .collect()
        val result: YearMaskVulnerabilities[T] = mutable.SortedMap()
        list.foreach { entry =>
          val year = entry._1
          val maskVulnerabilities: MaskVulnerabilities[T] = mutable.ListMap()
          entry._2.foreach { e =>
            val vulnerabilityList = e._2.filterByPredicates(predicates)
            if (vulnerabilityList.nonEmpty) {
              maskVulnerabilities += (e._1 -> vulnerabilityList)
            }
          }
          if (maskVulnerabilities.nonEmpty) {
            result += (year -> maskVulnerabilities)
          }
        }
        result
      } else {
        list
      }
    }

  }


}
