package it.unibo.utils.filters

import scala.collection.mutable.ListBuffer

trait PredicatesBuilder[B, T] {

  /** List of predicates used to filter a collection */
  protected val predicates: ListBuffer[T] = ListBuffer()

  /** Gets the list of predicates
   *
   * @return the list of predicates
   */
  def build(): ListBuffer[T] = predicates

  /** Adds a new predicate
   *
   * @param predicate the predicate
   * @return this builder reference
   */
  def addPredicate(predicate: T): B = {
    predicates += predicate
    self
  }

  /** Returns this builder reference
   *
   * @return this builder reference
   */
  protected def self: B

}
