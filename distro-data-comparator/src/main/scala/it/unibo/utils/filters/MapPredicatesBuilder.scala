package it.unibo.utils.filters

import it.unibo.models.CustomTypes.MaskVulnerabilities
import it.unibo.models.general.aggregator.AggregatedVulnerabilityData

case class MapPredicatesBuilder() extends YearRangePredicatesBuilder[MapPredicatesBuilder,
  (Int, MaskVulnerabilities[AggregatedVulnerabilityData]) => Boolean] {

  override def setRange(fromCveIdYear: Int, toCveIdYear: Int): MapPredicatesBuilder = {
    addPredicate((year, _) => year >= fromCveIdYear && year <= toCveIdYear)
    this
  }

  override def setFromRange(fromCveIdYear: Int): MapPredicatesBuilder = {
    addPredicate((year, _) => year >= fromCveIdYear)
    this
  }

  override def setToRange(toCveIdYear: Int): MapPredicatesBuilder = {
    addPredicate((year, _) => year <= toCveIdYear)
    this
  }

  override def self: MapPredicatesBuilder = this
}
