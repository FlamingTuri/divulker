package it.unibo.utils.filters

import io.vertx.scala.core.MultiMap

trait YearRangePredicates[B] {

  private val fromCveIdYearValue = "fromCveIdYear"
  private val toCveIdYearValue = "toCveIdYear"

  /** Sets the cve id year range filter
   *
   * @param requestParams request params
   * @return this builder reference
   */
  def setCveIdYearRange(requestParams: MultiMap): B = {
    val fromPresent = requestParams.contains(fromCveIdYearValue)
    val toPresent = requestParams.contains(toCveIdYearValue)
    if (fromPresent && toPresent) {
      val fromCveIdYear = requestParams.get(fromCveIdYearValue).get.toInt
      val toCveIdYear = requestParams.get(toCveIdYearValue).get.toInt
      setRange(fromCveIdYear, toCveIdYear)
    } else if (fromPresent) {
      setFromRange(requestParams.get(fromCveIdYearValue).get.toInt)
    } else if (toPresent) {
      setToRange(requestParams.get(toCveIdYearValue).get.toInt)
    } else {
      self
    }
  }

  /** Adds a predicate that returns true if cveIdYear [[&ge]] fromCveIdYear && cveIdYear [[&le]] toCveIdYear
   *
   * @param fromCveIdYear the lower bound
   * @param toCveIdYear   the upper bound
   * @return this builder reference
   */
  def setRange(fromCveIdYear: Int, toCveIdYear: Int): B

  /** Adds a predicate that returns true if cveIdYear [[&ge]] fromCveIdYear
   *
   * @param fromCveIdYear the lower bound
   * @return this builder reference
   */
  def setFromRange(fromCveIdYear: Int): B

  /** Adds a predicate that returns true if cveIdYear [[&le]] toCveIdYear
   *
   * @param toCveIdYear the upper bound
   * @return this builder reference
   */
  def setToRange(toCveIdYear: Int): B

  /** Returns this builder reference
   *
   * @return this builder reference
   */
  protected def self: B

}
