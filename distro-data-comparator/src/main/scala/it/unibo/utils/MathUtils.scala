package it.unibo.utils

object MathUtils {

  /** Rounds a double to the specified decimal places
   *
   * @param value         the value to round
   * @param decimalPlaces the round decimal places
   * @return the rounded value
   */
  def round(value: Double, decimalPlaces: Int): Double = {
    val d = math.pow(10, decimalPlaces)
    math.round(value * d) / d
  }

}
