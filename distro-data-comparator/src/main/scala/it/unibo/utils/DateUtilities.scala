package it.unibo.utils

import java.time.format.DateTimeFormatter
import java.time.temporal.TemporalAdjusters
import java.time.{DayOfWeek, LocalDate}

import io.vertx.scala.core.MultiMap
import it.unibo.models.aggregator.SourceTemplate

object DateUtilities {

  private val dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
  private val regex = "(\\d{4})(?:-(\\d{2})(?:-([1-4]))?)?".r

  /** Gets the local date from the request params.
   *
   * @param requestParams the request params
   * @param first function called when only the year parameter is specified
   * @param second function called when year and month parameters are specified
   * @param third function called when year, month and weekNumber parameters are specified
   * @return the resulting local date
   */
  def toLocalDate(requestParams: MultiMap, first: LocalDate => Unit,
                  second: LocalDate => Unit, third: LocalDate => Unit): LocalDate = {
    var requested: LocalDate = null
    (requestParams.get("year"), requestParams.get("month"), requestParams.get("weekNumber")) match {
      case (Some(year), None, _) =>
        requested = LocalDate.of(year.toInt, 1, 1)
        first(requested)
      case (Some(year), Some(month), None) =>
        requested = LocalDate.of(year.toInt, month.toInt, 1)
        second(requested)
      case (Some(year), Some(month), Some(weekNumber)) =>
        // dayOfMonth is meaningless and it is used just for LocalDate initialization
        requested = LocalDate.of(year.toInt, month.toInt, 1)
          .`with`(TemporalAdjusters.firstInMonth(DayOfWeek.MONDAY))
          .plusWeeks(weekNumber.toInt - 1)
        third(requested)
      case _ => throw new IllegalArgumentException()
    }
    requested
  }

  def toLocalDate(requestParams: MultiMap): LocalDate = {
    toLocalDate(requestParams, _ => {}, _ => {}, _ => {})
  }

  /** Strategy that checks whether the two dates have the same year.
   *
   * @param requested the first date
   * @param current the second date
   * @return true if their year is the same
   */
  def yearComparison(requested: LocalDate, current: LocalDate): Boolean = {
    current.getYear == requested.getYear
  }

  /** Strategy that checks whether the two dates have the same year and month.
   *
   * @param requested the first date
   * @param current the second date
   * @return true if their year and month are the same
   */
  def monthComparison(requested: LocalDate, current: LocalDate): Boolean = {
    yearComparison(current, requested) && current.getMonth == requested.getMonth
  }

  /** Strategy that checks whether the two dates have the same year,
   * month and fall in the same week number.
   *
   * @param requested the first date
   * @param current the second date
   * @return true if their year and month are the same and they fall in the same week number
   */
  def weekNumberComparison(requested: LocalDate, current: LocalDate): Boolean = {
    monthComparison(current, requested) && current.compareTo(requested) >= 0 &&
      current.compareTo(requested.plusWeeks(1)) <= 0
  }

  /** Get the local date from the specified string date. If the parse fails today date is returned.
   *
   * @param stringDate the string date formatted as yyyy-MM-dd
   * @return the resulting local date
   */
  def getLocalDate(stringDate: String): LocalDate = {
    try {
      LocalDate.parse(stringDate, dateTimeFormatter)
    } catch {
      case _: Throwable => LocalDate.now()
    }
  }

  /** Returns the source's date as epoch date
   *
   * @param source the source
   * @return the date as epoch date
   */
  def getEpochDay(source: SourceTemplate): Long = {
    (if (source.date == null || source.date == "undefined") {
      LocalDate.now()
    } else {
      LocalDate.parse(source.date, dateTimeFormatter)
    }).toEpochDay
  }

}
