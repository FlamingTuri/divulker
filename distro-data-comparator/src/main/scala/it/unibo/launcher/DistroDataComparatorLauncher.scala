package it.unibo.launcher

import it.unibo.verticle.{DistroDataComparatorVerticle, VerticleLauncher}

object DistroDataComparatorLauncher extends App with VerticleLauncher {
  deployVerticle[DistroDataComparatorVerticle]()
}
