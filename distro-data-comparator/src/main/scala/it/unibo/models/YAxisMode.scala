package it.unibo.models

import com.fasterxml.jackson.core.`type`.TypeReference

object YAxisMode extends Enumeration {
  type YAxisMode = Value
  val DisclosuresNumber, FixesNumber = Value
}

class YAxisModeType extends TypeReference[YAxisMode.type]
