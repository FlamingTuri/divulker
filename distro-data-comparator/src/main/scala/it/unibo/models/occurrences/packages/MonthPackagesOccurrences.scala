package it.unibo.models.occurrences.packages

import java.time.Month

import it.unibo.models.occurrences.OccurrencesKeys

import scala.collection.mutable

case class MonthPackagesOccurrences(override val packageName: String,
                                    month: Month,
                                    leapYear: Boolean)
  extends PackagesOccurrences[Int](packageName, mutable.SortedMap[Int, Int]()) {

  override protected def keys: Iterable[Int] = OccurrencesKeys.monthKeys(month, leapYear)
}
