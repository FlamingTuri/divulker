package it.unibo.models.occurrences.distros

case class DistrosOccurrencesInfo[T](dateInfo: T,
                                     distrosOccurrences: Iterable[DistroOccurrences[_]])
