package it.unibo.models.occurrences.packages

import java.time.DayOfWeek

import it.unibo.models.occurrences.OccurrencesKeys

import scala.collection.mutable

case class WeekPackagesOccurrences(override val packageName: String)
  extends PackagesOccurrences[DayOfWeek](packageName, mutable.SortedMap[DayOfWeek, Int]()) {

  override protected def keys: Iterable[DayOfWeek] = OccurrencesKeys.weekKeys
}

