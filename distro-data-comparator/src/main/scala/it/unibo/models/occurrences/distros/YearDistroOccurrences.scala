package it.unibo.models.occurrences.distros

import java.time.Month

import com.fasterxml.jackson.annotation.{JsonGetter, JsonIgnore}
import com.fasterxml.jackson.module.scala.JsonScalaEnumeration
import it.unibo.models.occurrences.OccurrencesKeys
import it.unibo.models.service.{ServiceName, ServiceNameType}

import scala.collection.mutable

case class YearDistroOccurrences(@JsonScalaEnumeration(classOf[ServiceNameType])
                                 override val distroName: ServiceName.Value)
  extends DistroOccurrences[Month](distroName, mutable.SortedMap[Month, Int]()) {

  override protected def keys: Iterable[Month] = OccurrencesKeys.yearKeys

  @JsonIgnore
  override def occurrences: mutable.SortedMap[Month, Int] = super.occurrences

  @JsonGetter("occurrences")
  def jsonOccurrences: mutable.SortedMap[Int, Int] = occurrences.map(e => (e._1.getValue, e._2))

}
