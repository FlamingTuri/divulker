package it.unibo.models.occurrences.distros

import java.time.Month

import com.fasterxml.jackson.module.scala.JsonScalaEnumeration
import it.unibo.models.occurrences.OccurrencesKeys
import it.unibo.models.service.{ServiceName, ServiceNameType}

import scala.collection.mutable

case class MonthDistroOccurrences(@JsonScalaEnumeration(classOf[ServiceNameType])
                                  override val distroName: ServiceName.Value,
                                  month: Month,
                                  leapYear: Boolean)
  extends DistroOccurrences[Int](distroName, mutable.SortedMap[Int, Int]()) {

  override protected def keys: Iterable[Int] = OccurrencesKeys.monthKeys(month, leapYear)
}
