package it.unibo.models.occurrences.packages

import java.time.Month

import com.fasterxml.jackson.annotation.{JsonGetter, JsonIgnore}
import it.unibo.models.occurrences.OccurrencesKeys

import scala.collection.mutable

case class YearPackagesOccurrences(override val packageName: String)
  extends PackagesOccurrences[Month](packageName, mutable.SortedMap[Month, Int]()) {

  override protected def keys: Iterable[Month] = OccurrencesKeys.yearKeys

  @JsonIgnore
  override def occurrences: mutable.SortedMap[Month, Int] = super.occurrences

  @JsonGetter("occurrences")
  def jsonOccurrencesPerMonth: mutable.SortedMap[Int, Int] = {
    occurrences.map(e => (e._1.getValue, e._2))
  }

}
