package it.unibo.models.occurrences

import java.time.{DayOfWeek, Month}

object OccurrencesKeys {

  def weekKeys: Iterable[DayOfWeek] = DayOfWeek.values()

  def monthKeys(month: Month, leapYear: Boolean): Iterable[Int] = 1 to month.length(leapYear)

  def yearKeys: Iterable[Month] = Month.values()
}
