package it.unibo.models.occurrences.packages

import com.fasterxml.jackson.annotation.JsonGetter

import scala.collection.mutable

abstract class PackagesOccurrences[K](val packageName: String, private val _occurrences: mutable.SortedMap[K, Int]) {

  protected def keys: Iterable[K]

  protected def init(): Unit = keys foreach (_occurrences.put(_, 0))

  init()

  @JsonGetter
  def occurrences: mutable.SortedMap[K, Int] = _occurrences

}
