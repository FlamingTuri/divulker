package it.unibo.models.occurrences

import com.fasterxml.jackson.annotation.JsonGetter

import scala.collection.mutable

abstract class Occurrences[K](private val _occurrences: mutable.SortedMap[K, Int]) {

  protected def keys: Iterable[K]

  protected def init(): Unit = keys foreach (_occurrences.put(_, 0))

  init()

  @JsonGetter
  def occurrences: mutable.SortedMap[K, Int] = _occurrences

}
