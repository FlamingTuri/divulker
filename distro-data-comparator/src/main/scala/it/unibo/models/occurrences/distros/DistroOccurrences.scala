package it.unibo.models.occurrences.distros

import com.fasterxml.jackson.module.scala.JsonScalaEnumeration
import it.unibo.models.occurrences.Occurrences
import it.unibo.models.service.{ServiceName, ServiceNameType}

import scala.collection.mutable

abstract class DistroOccurrences[K](@JsonScalaEnumeration(classOf[ServiceNameType])
                                    val distroName: ServiceName.Value,
                                    private val _occurrences: mutable.SortedMap[K, Int])
  extends Occurrences[K](_occurrences)
