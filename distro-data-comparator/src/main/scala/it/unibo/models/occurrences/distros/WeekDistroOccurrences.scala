package it.unibo.models.occurrences.distros

import java.time.DayOfWeek

import com.fasterxml.jackson.module.scala.JsonScalaEnumeration
import it.unibo.models.occurrences.OccurrencesKeys
import it.unibo.models.service.{ServiceName, ServiceNameType}

import scala.collection.mutable

case class WeekDistroOccurrences(@JsonScalaEnumeration(classOf[ServiceNameType])
                                 override val distroName: ServiceName.Value)
  extends DistroOccurrences[DayOfWeek](distroName, mutable.SortedMap[DayOfWeek, Int]()) {

  override protected def keys: Iterable[DayOfWeek] = OccurrencesKeys.weekKeys
}

