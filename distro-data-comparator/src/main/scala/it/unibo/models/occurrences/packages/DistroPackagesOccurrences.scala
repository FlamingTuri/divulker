package it.unibo.models.occurrences.packages

import com.fasterxml.jackson.module.scala.JsonScalaEnumeration
import it.unibo.models.service.{ServiceName, ServiceNameType}

case class DistroPackagesOccurrences[T](@JsonScalaEnumeration(classOf[ServiceNameType])
                                        distroName: ServiceName.Value,
                                        dateInfo: T,
                                        packageOccurrences: Iterable[PackagesOccurrences[_]])
