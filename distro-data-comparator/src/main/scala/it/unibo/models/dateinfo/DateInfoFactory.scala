package it.unibo.models.dateinfo

import java.time.LocalDate

object DateInfoFactory {

  def YearDateInfo(request: LocalDate): YearDateInfo = new YearDateInfo(request.getYear)

  def MonthDateInfo(request: LocalDate): MonthDateInfo = {
    new MonthDateInfo(request.getYear, request.getMonth)
  }

  def WeekDateInfo(request: LocalDate): WeekDateInfo = {
    val from = request.getDayOfMonth
    val to = request.plusWeeks(1).getDayOfMonth
    new WeekDateInfo(request.getYear, request.getMonth, from, to)
  }

}
