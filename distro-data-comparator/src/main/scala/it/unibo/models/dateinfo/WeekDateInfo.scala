package it.unibo.models.dateinfo

import java.time.Month

case class WeekDateInfo(override val year: Int, override val month: Month, from: Int, to: Int)
  extends MonthDateInfo(year, month)
