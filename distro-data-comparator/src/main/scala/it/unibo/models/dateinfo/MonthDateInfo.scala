package it.unibo.models.dateinfo

import java.time.Month

class MonthDateInfo(override val year: Int, val month: Month) extends YearDateInfo(year)
